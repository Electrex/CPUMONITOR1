.class public final Lcom/cgollner/systemmonitor/b/a$h;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cgollner/systemmonitor/b/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "h"
.end annotation


# static fields
.field public static final No:I = 0x7f0c0000

.field public static final abc_action_bar_home_description:I = 0x7f0c0001

.field public static final abc_action_bar_home_description_format:I = 0x7f0c0002

.field public static final abc_action_bar_home_subtitle_description_format:I = 0x7f0c0003

.field public static final abc_action_bar_up_description:I = 0x7f0c0004

.field public static final abc_action_menu_overflow_description:I = 0x7f0c0005

.field public static final abc_action_mode_done:I = 0x7f0c0006

.field public static final abc_activity_chooser_view_see_all:I = 0x7f0c0007

.field public static final abc_activitychooserview_choose_application:I = 0x7f0c0008

.field public static final abc_searchview_description_clear:I = 0x7f0c0009

.field public static final abc_searchview_description_query:I = 0x7f0c000a

.field public static final abc_searchview_description_search:I = 0x7f0c000b

.field public static final abc_searchview_description_submit:I = 0x7f0c000c

.field public static final abc_searchview_description_voice:I = 0x7f0c000d

.field public static final abc_shareactionprovider_share_with:I = 0x7f0c000e

.field public static final abc_shareactionprovider_share_with_application:I = 0x7f0c000f

.field public static final abc_toolbar_collapse_description:I = 0x7f0c0010

.field public static final action_open_app:I = 0x7f0c0011

.field public static final action_open_settings:I = 0x7f0c0012

.field public static final all_apps:I = 0x7f0c0013

.field public static final allow:I = 0x7f0c0014

.field public static final and:I = 0x7f0c0015

.field public static final app:I = 0x7f0c0016

.field public static final app_description:I = 0x7f0c0017

.field public static final app_description_lite:I = 0x7f0c0018

.field public static final app_name:I = 0x7f0c0019

.field public static final app_name_lite:I = 0x7f0c001a

.field public static final application_error:I = 0x7f0c001b

.field public static final apps:I = 0x7f0c001c

.field public static final apps_monitor:I = 0x7f0c001d

.field public static final are_you_sure_delete:I = 0x7f0c001e

.field public static final are_you_sure_message:I = 0x7f0c001f

.field public static final are_you_sure_that_you_want_to_clear_the_cache_of_b_s_b_:I = 0x7f0c0020

.field public static final availableRam:I = 0x7f0c0021

.field public static final background_history_summay:I = 0x7f0c0022

.field public static final background_history_title:I = 0x7f0c0023

.field public static final background_history_title_key:I = 0x7f0c0024

.field public static final battery_calculation_strategy:I = 0x7f0c0025

.field public static final battery_charged_at:I = 0x7f0c0026

.field public static final battery_charged_in:I = 0x7f0c0027

.field public static final battery_clear_stats:I = 0x7f0c0028

.field public static final battery_clear_stats_key:I = 0x7f0c0029

.field public static final battery_clear_stats_message:I = 0x7f0c002a

.field public static final battery_clear_stats_summary:I = 0x7f0c002b

.field public static final battery_default_visible_range:I = 0x7f0c002c

.field public static final battery_empty_at:I = 0x7f0c002d

.field public static final battery_empty_in:I = 0x7f0c002e

.field public static final battery_enable_history:I = 0x7f0c002f

.field public static final battery_extension_description:I = 0x7f0c0030

.field public static final battery_health:I = 0x7f0c0031

.field public static final battery_history:I = 0x7f0c0032

.field public static final battery_history_key:I = 0x7f0c0033

.field public static final battery_history_size:I = 0x7f0c0034

.field public static final battery_history_size_key:I = 0x7f0c0035

.field public static final battery_percentage:I = 0x7f0c0036

.field public static final battery_service:I = 0x7f0c0037

.field public static final battery_settings_summary:I = 0x7f0c0038

.field public static final battery_stats:I = 0x7f0c0039

.field public static final battery_strategy_cycle:I = 0x7f0c003a

.field public static final battery_strategy_key:I = 0x7f0c003b

.field public static final battery_strategy_stats:I = 0x7f0c003c

.field public static final battery_strategy_stats_and_cycle:I = 0x7f0c003d

.field public static final battery_temperature:I = 0x7f0c003e

.field public static final battery_time_to_empty:I = 0x7f0c003f

.field public static final battery_time_to_full:I = 0x7f0c0040

.field public static final battery_title:I = 0x7f0c0041

.field public static final battery_visible_range_key:I = 0x7f0c0042

.field public static final battery_voltage:I = 0x7f0c0043

.field public static final batteryhistoryclearkey:I = 0x7f0c0044

.field public static final buy_button:I = 0x7f0c0045

.field public static final cache_stats:I = 0x7f0c0046

.field public static final charge_rate_per_hour:I = 0x7f0c0047

.field public static final charge_speed_1_:I = 0x7f0c0048

.field public static final charge_statistics:I = 0x7f0c0049

.field public static final check_license:I = 0x7f0c004a

.field public static final checking_license:I = 0x7f0c004b

.field public static final choose_current_folder:I = 0x7f0c004c

.field public static final clear_battery_history:I = 0x7f0c004d

.field public static final clear_cache:I = 0x7f0c004e

.field public static final clearing_cache_:I = 0x7f0c004f

.field public static final com_crashlytics_android_build_id:I = 0x7f0c0050

.field public static final cpuFrequency:I = 0x7f0c0051

.field public static final cpuUsage:I = 0x7f0c0052

.field public static final cpu_frequencies:I = 0x7f0c0053

.field public static final cpu_temperature:I = 0x7f0c0054

.field public static final cpu_title:I = 0x7f0c0055

.field public static final current:I = 0x7f0c0056

.field public static final current_cycle:I = 0x7f0c0057

.field public static final dashclock_desc:I = 0x7f0c0058

.field public static final day:I = 0x7f0c0059

.field public static final days:I = 0x7f0c005a

.field public static final delete:I = 0x7f0c005b

.field public static final dialog_cancel:I = 0x7f0c005c

.field public static final dialog_color_picker:I = 0x7f0c005d

.field public static final dialog_picker_title:I = 0x7f0c005e

.field public static final dialog_set_number:I = 0x7f0c005f

.field public static final discharge_rate_per_hour:I = 0x7f0c0060

.field public static final discharge_speed_1_:I = 0x7f0c0061

.field public static final discharge_statistics:I = 0x7f0c0062

.field public static final display_all_cores:I = 0x7f0c0063

.field public static final dont_allow:I = 0x7f0c0064

.field public static final drawer_close:I = 0x7f0c0065

.field public static final drawer_open:I = 0x7f0c0066

.field public static final dynamic:I = 0x7f0c0067

.field public static final email_key:I = 0x7f0c0068

.field public static final empty:I = 0x7f0c0069

.field public static final extension_cpu_description:I = 0x7f0c006a

.field public static final extension_io_description:I = 0x7f0c006b

.field public static final extension_net_description:I = 0x7f0c006c

.field public static final extension_ram_description:I = 0x7f0c006d

.field public static final features:I = 0x7f0c006e

.field public static final flying_monitor_is_minimized:I = 0x7f0c006f

.field public static final folder:I = 0x7f0c0070

.field public static final friday:I = 0x7f0c0071

.field public static final full_version_details:I = 0x7f0c0072

.field public static final full_version_feature_message:I = 0x7f0c0073

.field public static final full_version_feature_title:I = 0x7f0c0074

.field public static final get_full:I = 0x7f0c0075

.field public static final gpu:I = 0x7f0c0076

.field public static final graph_zoom:I = 0x7f0c0077

.field public static final history:I = 0x7f0c0078

.field public static final history_bg_click_to_schedule:I = 0x7f0c0079

.field public static final history_bg_components:I = 0x7f0c007a

.field public static final history_bg_configure:I = 0x7f0c007b

.field public static final history_bg_cpu_key:I = 0x7f0c007c

.field public static final history_bg_dialog_choose_action:I = 0x7f0c007d

.field public static final history_bg_frquency_title:I = 0x7f0c007e

.field public static final history_bg_history_click:I = 0x7f0c007f

.field public static final history_bg_history_enter_name:I = 0x7f0c0080

.field public static final history_bg_io_key:I = 0x7f0c0081

.field public static final history_bg_last:I = 0x7f0c0082

.field public static final history_bg_mobile_recv_title:I = 0x7f0c0083

.field public static final history_bg_mobile_send_title:I = 0x7f0c0084

.field public static final history_bg_net_key:I = 0x7f0c0085

.field public static final history_bg_not_running:I = 0x7f0c0086

.field public static final history_bg_not_scheduled:I = 0x7f0c0087

.field public static final history_bg_ram_key:I = 0x7f0c0088

.field public static final history_bg_readspeed_title:I = 0x7f0c0089

.field public static final history_bg_restart:I = 0x7f0c008a

.field public static final history_bg_running:I = 0x7f0c008b

.field public static final history_bg_save_history:I = 0x7f0c008c

.field public static final history_bg_save_history_desc:I = 0x7f0c008d

.field public static final history_bg_save_menu:I = 0x7f0c008e

.field public static final history_bg_saved_history:I = 0x7f0c008f

.field public static final history_bg_schedule:I = 0x7f0c0090

.field public static final history_bg_schedule_end:I = 0x7f0c0091

.field public static final history_bg_schedule_name:I = 0x7f0c0092

.field public static final history_bg_schedule_start:I = 0x7f0c0093

.field public static final history_bg_scheduled:I = 0x7f0c0094

.field public static final history_bg_scheduled_description:I = 0x7f0c0095

.field public static final history_bg_scheduled_key:I = 0x7f0c0096

.field public static final history_bg_scheduled_monitoring:I = 0x7f0c0097

.field public static final history_bg_see_report:I = 0x7f0c0098

.field public static final history_bg_service:I = 0x7f0c0099

.field public static final history_bg_service_is_finished:I = 0x7f0c009a

.field public static final history_bg_service_is_running:I = 0x7f0c009b

.field public static final history_bg_service_is_stopped:I = 0x7f0c009c

.field public static final history_bg_service_see_progress:I = 0x7f0c009d

.field public static final history_bg_service_see_report:I = 0x7f0c009e

.field public static final history_bg_status:I = 0x7f0c009f

.field public static final history_bg_status_key:I = 0x7f0c00a0

.field public static final history_bg_stop_start:I = 0x7f0c00a1

.field public static final history_bg_total_title:I = 0x7f0c00a2

.field public static final history_bg_update_interval_key:I = 0x7f0c00a3

.field public static final history_bg_usage_title:I = 0x7f0c00a4

.field public static final history_bg_wifi_receive_title:I = 0x7f0c00a5

.field public static final history_bg_wifi_send:I = 0x7f0c00a6

.field public static final history_bg_writespeed_title:I = 0x7f0c00a7

.field public static final holo_blue:I = 0x7f0c00a8

.field public static final hour:I = 0x7f0c00a9

.field public static final hours:I = 0x7f0c00aa

.field public static final hwCode:I = 0x7f0c00ab

.field public static final io_title:I = 0x7f0c00ac

.field public static final live_monitor:I = 0x7f0c00ad

.field public static final look_and_feel:I = 0x7f0c00ae

.field public static final max:I = 0x7f0c00af

.field public static final max_speed:I = 0x7f0c00b0

.field public static final menu_settings:I = 0x7f0c00b1

.field public static final menu_show_all_processes:I = 0x7f0c00b2

.field public static final min:I = 0x7f0c00b3

.field public static final min_speed:I = 0x7f0c00b4

.field public static final minute:I = 0x7f0c00b5

.field public static final minutes:I = 0x7f0c00b6

.field public static final monday:I = 0x7f0c00b7

.field public static final net:I = 0x7f0c00b8

.field public static final network_title:I = 0x7f0c00b9

.field public static final notif_graph_color:I = 0x7f0c00ba

.field public static final notif_icon_color:I = 0x7f0c00bb

.field public static final notif_priority_battery_key:I = 0x7f0c00bc

.field public static final notif_priority_cpu_key:I = 0x7f0c00bd

.field public static final notif_priority_def:I = 0x7f0c00be

.field public static final notif_priority_high:I = 0x7f0c00bf

.field public static final notif_priority_io_key:I = 0x7f0c00c0

.field public static final notif_priority_low:I = 0x7f0c00c1

.field public static final notif_priority_max:I = 0x7f0c00c2

.field public static final notif_priority_min:I = 0x7f0c00c3

.field public static final notif_priority_net_key:I = 0x7f0c00c4

.field public static final notif_priority_ram_key:I = 0x7f0c00c5

.field public static final notif_priority_title:I = 0x7f0c00c6

.field public static final pref_screen_app:I = 0x7f0c00c7

.field public static final prefscreen_title_floating:I = 0x7f0c00c8

.field public static final prefscreen_title_floating_full:I = 0x7f0c00c9

.field public static final press_color_to_apply:I = 0x7f0c00ca

.field public static final quit_button:I = 0x7f0c00cb

.field public static final ram_title:I = 0x7f0c00cc

.field public static final read:I = 0x7f0c00cd

.field public static final read_speed:I = 0x7f0c00ce

.field public static final receive:I = 0x7f0c00cf

.field public static final receive_speed:I = 0x7f0c00d0

.field public static final refresh:I = 0x7f0c00d1

.field public static final reset:I = 0x7f0c00d2

.field public static final reset_all_statistics:I = 0x7f0c00d3

.field public static final reset_battery_statistics:I = 0x7f0c00d4

.field public static final reset_charging_statistics:I = 0x7f0c00d5

.field public static final reset_discharging_statistics:I = 0x7f0c00d6

.field public static final reset_timers:I = 0x7f0c00d7

.field public static final retry_button:I = 0x7f0c00d8

.field public static final saturday:I = 0x7f0c00d9

.field public static final saved_history_key:I = 0x7f0c00da

.field public static final second:I = 0x7f0c00db

.field public static final seconds:I = 0x7f0c00dc

.field public static final send:I = 0x7f0c00dd

.field public static final send_speed:I = 0x7f0c00de

.field public static final settings:I = 0x7f0c00df

.field public static final settings_about:I = 0x7f0c00e0

.field public static final settings_actions:I = 0x7f0c00e1

.field public static final settings_app_cpu_updatefreq_key:I = 0x7f0c00e2

.field public static final settings_app_io_updatefreq_key:I = 0x7f0c00e3

.field public static final settings_app_net_updatefreq_key:I = 0x7f0c00e4

.field public static final settings_app_ram_updatefreq_key:I = 0x7f0c00e5

.field public static final settings_app_screen_summary:I = 0x7f0c00e6

.field public static final settings_app_topapps_updatefreq_key:I = 0x7f0c00e7

.field public static final settings_contact:I = 0x7f0c00e8

.field public static final settings_extension_all_cores:I = 0x7f0c00e9

.field public static final settings_extension_all_cores_key:I = 0x7f0c00ea

.field public static final settings_extension_cpu_updatefreq_key:I = 0x7f0c00eb

.field public static final settings_extension_io_updatefreq_key:I = 0x7f0c00ec

.field public static final settings_extension_net_updatefreq_key:I = 0x7f0c00ed

.field public static final settings_extension_ram_updatefreq_key:I = 0x7f0c00ee

.field public static final settings_floating_cpu_cat:I = 0x7f0c00ef

.field public static final settings_floating_cpu_height_key:I = 0x7f0c00f0

.field public static final settings_floating_cpu_key:I = 0x7f0c00f1

.field public static final settings_floating_cpu_showgraph_key:I = 0x7f0c00f2

.field public static final settings_floating_cpu_updatefreq_key:I = 0x7f0c00f3

.field public static final settings_floating_cpu_updatefreq_key_new:I = 0x7f0c00f4

.field public static final settings_floating_cpu_width_key:I = 0x7f0c00f5

.field public static final settings_floating_display_all_cores_key:I = 0x7f0c00f6

.field public static final settings_floating_height_key:I = 0x7f0c00f7

.field public static final settings_floating_io_cat:I = 0x7f0c00f8

.field public static final settings_floating_io_height_key:I = 0x7f0c00f9

.field public static final settings_floating_io_key:I = 0x7f0c00fa

.field public static final settings_floating_io_showgraph_key:I = 0x7f0c00fb

.field public static final settings_floating_io_updatefreq_key:I = 0x7f0c00fc

.field public static final settings_floating_io_width_key:I = 0x7f0c00fd

.field public static final settings_floating_monitor:I = 0x7f0c00fe

.field public static final settings_floating_net_cat:I = 0x7f0c00ff

.field public static final settings_floating_net_height_key:I = 0x7f0c0100

.field public static final settings_floating_net_key:I = 0x7f0c0101

.field public static final settings_floating_net_showgraph_key:I = 0x7f0c0102

.field public static final settings_floating_net_updatefreq_key:I = 0x7f0c0103

.field public static final settings_floating_net_width_key:I = 0x7f0c0104

.field public static final settings_floating_ram_cat:I = 0x7f0c0105

.field public static final settings_floating_ram_height_key:I = 0x7f0c0106

.field public static final settings_floating_ram_key:I = 0x7f0c0107

.field public static final settings_floating_ram_showgraph_key:I = 0x7f0c0108

.field public static final settings_floating_ram_updatefreq_key:I = 0x7f0c0109

.field public static final settings_floating_ram_width_key:I = 0x7f0c010a

.field public static final settings_floating_screen_summary:I = 0x7f0c010b

.field public static final settings_floating_showgraph:I = 0x7f0c010c

.field public static final settings_floating_top_apps_updatefreq_key:I = 0x7f0c010d

.field public static final settings_floating_topapps_key:I = 0x7f0c010e

.field public static final settings_floating_transparency:I = 0x7f0c010f

.field public static final settings_floating_ui_category:I = 0x7f0c0110

.field public static final settings_floating_width_key:I = 0x7f0c0111

.field public static final settings_follow_me:I = 0x7f0c0112

.field public static final settings_help_translating:I = 0x7f0c0113

.field public static final settings_notification_display_all_cores_key:I = 0x7f0c0114

.field public static final settings_notification_net_category:I = 0x7f0c0115

.field public static final settings_notification_screen_summary:I = 0x7f0c0116

.field public static final settings_notifications_battery_key:I = 0x7f0c0117

.field public static final settings_notifications_click_action:I = 0x7f0c0118

.field public static final settings_notifications_cpu_category:I = 0x7f0c0119

.field public static final settings_notifications_cpu_key:I = 0x7f0c011a

.field public static final settings_notifications_cpu_updatefreq:I = 0x7f0c011b

.field public static final settings_notifications_cpu_updatefreq_key:I = 0x7f0c011c

.field public static final settings_notifications_io_category:I = 0x7f0c011d

.field public static final settings_notifications_io_key:I = 0x7f0c011e

.field public static final settings_notifications_io_updatefreq_key:I = 0x7f0c011f

.field public static final settings_notifications_net_key:I = 0x7f0c0120

.field public static final settings_notifications_net_updatefreq_key:I = 0x7f0c0121

.field public static final settings_notifications_ram_category:I = 0x7f0c0122

.field public static final settings_notifications_ram_key:I = 0x7f0c0123

.field public static final settings_notifications_ram_updatefreq_key:I = 0x7f0c0124

.field public static final settings_notifications_show:I = 0x7f0c0125

.field public static final settings_notifications_show_expanded:I = 0x7f0c0126

.field public static final settings_notifications_show_expanded_battery_key:I = 0x7f0c0127

.field public static final settings_notifications_show_expanded_cpu_key:I = 0x7f0c0128

.field public static final settings_notifications_show_expanded_io_key:I = 0x7f0c0129

.field public static final settings_notifications_show_expanded_net_key:I = 0x7f0c012a

.field public static final settings_notifications_show_expanded_ram_key:I = 0x7f0c012b

.field public static final settings_notifications_title:I = 0x7f0c012c

.field public static final settings_notifications_title_full:I = 0x7f0c012d

.field public static final settings_on_click_action:I = 0x7f0c012e

.field public static final settings_others:I = 0x7f0c012f

.field public static final settings_theme_cat:I = 0x7f0c0130

.field public static final settings_theme_choose:I = 0x7f0c0131

.field public static final settings_theme_choose_key:I = 0x7f0c0132

.field public static final settings_translations:I = 0x7f0c0133

.field public static final statistics_average:I = 0x7f0c0134

.field public static final statistics_mode:I = 0x7f0c0135

.field public static final storage_stats:I = 0x7f0c0136

.field public static final sunday:I = 0x7f0c0137

.field public static final system_monitor:I = 0x7f0c0138

.field public static final system_monitor_battery:I = 0x7f0c0139

.field public static final system_stats:I = 0x7f0c013a

.field public static final tap_to_delete_battery_history:I = 0x7f0c013b

.field public static final tap_to_restore:I = 0x7f0c013c

.field public static final temperature_units_key:I = 0x7f0c013d

.field public static final through:I = 0x7f0c013e

.field public static final throughput:I = 0x7f0c013f

.field public static final thursday:I = 0x7f0c0140

.field public static final time_to_fully_charge:I = 0x7f0c0141

.field public static final time_to_fully_discharge:I = 0x7f0c0142

.field public static final today:I = 0x7f0c0143

.field public static final tomorrow:I = 0x7f0c0144

.field public static final top_apps_app_name:I = 0x7f0c0145

.field public static final top_apps_cpu_time:I = 0x7f0c0146

.field public static final top_apps_title:I = 0x7f0c0147

.field public static final total:I = 0x7f0c0148

.field public static final totalRam:I = 0x7f0c0149

.field public static final tuesday:I = 0x7f0c014a

.field public static final unlicensed_dialog_body:I = 0x7f0c014b

.field public static final unlicensed_dialog_retry_body:I = 0x7f0c014c

.field public static final unlicensed_dialog_title:I = 0x7f0c014d

.field public static final usedRam:I = 0x7f0c014e

.field public static final wednesday:I = 0x7f0c014f

.field public static final whats_new:I = 0x7f0c0150

.field public static final white:I = 0x7f0c0151

.field public static final widget_configuration:I = 0x7f0c0152

.field public static final widget_name_cpu:I = 0x7f0c0153

.field public static final widget_name_io:I = 0x7f0c0154

.field public static final widget_name_network:I = 0x7f0c0155

.field public static final widget_name_ram:I = 0x7f0c0156

.field public static final widgets:I = 0x7f0c0157

.field public static final widgets_desc:I = 0x7f0c0158

.field public static final write:I = 0x7f0c0159

.field public static final write_speed:I = 0x7f0c015a

.field public static final written:I = 0x7f0c015b

.field public static final x_app_apps_containing_cache:I = 0x7f0c015c

.field public static final x_mb_can_be_cleaned:I = 0x7f0c015d

.field public static final yes:I = 0x7f0c015e

.field public static final yesterday:I = 0x7f0c015f
