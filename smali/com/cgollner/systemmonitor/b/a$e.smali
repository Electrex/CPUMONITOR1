.class public final Lcom/cgollner/systemmonitor/b/a$e;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cgollner/systemmonitor/b/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "e"
.end annotation


# static fields
.field public static final action_bar:I = 0x7f0a003e

.field public static final action_bar_activity_content:I = 0x7f0a0000

.field public static final action_bar_container:I = 0x7f0a003d

.field public static final action_bar_root:I = 0x7f0a0039

.field public static final action_bar_spinner:I = 0x7f0a0001

.field public static final action_bar_subtitle:I = 0x7f0a002c

.field public static final action_bar_title:I = 0x7f0a002b

.field public static final action_context_bar:I = 0x7f0a003f

.field public static final action_menu_divider:I = 0x7f0a0002

.field public static final action_menu_presenter:I = 0x7f0a0003

.field public static final action_mode_bar:I = 0x7f0a003b

.field public static final action_mode_bar_stub:I = 0x7f0a003a

.field public static final action_mode_close_button:I = 0x7f0a002d

.field public static final activity_chooser_view_content:I = 0x7f0a002e

.field public static final always:I = 0x7f0a0023

.field public static final askRootPermissions:I = 0x7f0a007e

.field public static final askRootStart:I = 0x7f0a007f

.field public static final back:I = 0x7f0a006e

.field public static final batteryHistoryView:I = 0x7f0a006c

.field public static final batteryStats:I = 0x7f0a0071

.field public static final batteryView:I = 0x7f0a0057

.field public static final batteryWidgetImage:I = 0x7f0a007c

.field public static final beginning:I = 0x7f0a0020

.field public static final buttonCPU:I = 0x7f0a010a

.field public static final buttonCPUTIME:I = 0x7f0a010c

.field public static final buttonName:I = 0x7f0a0109

.field public static final buttonRAM:I = 0x7f0a010b

.field public static final cacheItemLayout:I = 0x7f0a0084

.field public static final cacheNumApps:I = 0x7f0a0080

.field public static final cacheTotalSize:I = 0x7f0a0081

.field public static final checkbox:I = 0x7f0a0036

.field public static final checked:I = 0x7f0a0085

.field public static final choose:I = 0x7f0a00e0

.field public static final circleWidthLayout:I = 0x7f0a005f

.field public static final circleWidthSeekBar:I = 0x7f0a0061

.field public static final circleWidthText:I = 0x7f0a0060

.field public static final collapseActionView:I = 0x7f0a0024

.field public static final colorInnerCircle:I = 0x7f0a0063

.field public static final colorInnerCircleBg:I = 0x7f0a0065

.field public static final colorSquare:I = 0x7f0a005e

.field public static final color_picker_view:I = 0x7f0a00a5

.field public static final contentFrame:I = 0x7f0a004c

.field public static final cpu0:I = 0x7f0a009c

.field public static final cpuStatTitle0:I = 0x7f0a00bb

.field public static final cpuStatTitle1:I = 0x7f0a00be

.field public static final cpuStatTitle2:I = 0x7f0a00c1

.field public static final cpuStatTitle3:I = 0x7f0a00c4

.field public static final cpuStatVal0:I = 0x7f0a00bc

.field public static final cpuStatVal1:I = 0x7f0a00bf

.field public static final cpuStatVal2:I = 0x7f0a00c2

.field public static final cpuStatVal3:I = 0x7f0a00c5

.field public static final cpuTempAvgTitle:I = 0x7f0a00a3

.field public static final cpuTempAvgValue:I = 0x7f0a00a4

.field public static final cpuTempCurrentTitle:I = 0x7f0a009d

.field public static final cpuTempCurrentValue:I = 0x7f0a009e

.field public static final cpuTempMaxTitle:I = 0x7f0a00a1

.field public static final cpuTempMaxValue:I = 0x7f0a00a2

.field public static final cpuTempMinTitle:I = 0x7f0a009f

.field public static final cpuTempMinValue:I = 0x7f0a00a0

.field public static final cpuTitle0:I = 0x7f0a00ba

.field public static final cpuTitle1:I = 0x7f0a00bd

.field public static final cpuTitle2:I = 0x7f0a00c0

.field public static final cpuTitle3:I = 0x7f0a00c3

.field public static final decor_content_parent:I = 0x7f0a003c

.field public static final decrement:I = 0x7f0a00fb

.field public static final default_activity_button:I = 0x7f0a0031

.field public static final delete:I = 0x7f0a0087

.field public static final deleteAll:I = 0x7f0a0083

.field public static final deleteAllSeparator:I = 0x7f0a0082

.field public static final dialog:I = 0x7f0a0028

.field public static final disableHome:I = 0x7f0a0019

.field public static final display_all_cores:I = 0x7f0a0115

.field public static final drawerContent:I = 0x7f0a00ae

.field public static final drawerFragment:I = 0x7f0a00e3

.field public static final drawer_layout:I = 0x7f0a00e1

.field public static final dropdown:I = 0x7f0a0029

.field public static final edit_query:I = 0x7f0a0040

.field public static final empty:I = 0x7f0a007d

.field public static final end:I = 0x7f0a0021

.field public static final expand_activities_button:I = 0x7f0a002f

.field public static final expanded_menu:I = 0x7f0a0035

.field public static final forward:I = 0x7f0a006d

.field public static final freeRamTitle:I = 0x7f0a00fd

.field public static final freeRamValue:I = 0x7f0a00fe

.field public static final freeStorage:I = 0x7f0a0107

.field public static final freqAvg:I = 0x7f0a008d

.field public static final freqLast:I = 0x7f0a0090

.field public static final freqMax:I = 0x7f0a008e

.field public static final freqMin:I = 0x7f0a008f

.field public static final freqsFileNotFound:I = 0x7f0a009a

.field public static final home:I = 0x7f0a0004

.field public static final homeAsUp:I = 0x7f0a001a

.field public static final icon:I = 0x7f0a0033

.field public static final iconLayout:I = 0x7f0a00aa

.field public static final ifRoom:I = 0x7f0a0025

.field public static final image:I = 0x7f0a0030

.field public static final increment:I = 0x7f0a00f9

.field public static final infoTextView:I = 0x7f0a0104

.field public static final innerCircleBgColorLayout:I = 0x7f0a0064

.field public static final innerCircleColorLayout:I = 0x7f0a0062

.field public static final itemLayout:I = 0x7f0a00a9

.field public static final left_drawer:I = 0x7f0a00ad

.field public static final listMode:I = 0x7f0a0016

.field public static final list_item:I = 0x7f0a0032

.field public static final mainLayout:I = 0x7f0a0056

.field public static final maxSpeedTitle:I = 0x7f0a00b8

.field public static final maxSpeedValue:I = 0x7f0a00b9

.field public static final menu_accept:I = 0x7f0a010e

.field public static final menu_settings:I = 0x7f0a010f

.field public static final menu_show_all_processes:I = 0x7f0a0118

.field public static final menu_storage_folder:I = 0x7f0a0117

.field public static final middle:I = 0x7f0a0022

.field public static final minSpeedTitle:I = 0x7f0a00b5

.field public static final minSpeedValue:I = 0x7f0a00b6

.field public static final minus:I = 0x7f0a0070

.field public static final mobileRecvAvg:I = 0x7f0a00eb

.field public static final mobileRecvLast:I = 0x7f0a00ed

.field public static final mobileRecvMax:I = 0x7f0a00ec

.field public static final mobileRecvTotal:I = 0x7f0a00ee

.field public static final mobileSendAvg:I = 0x7f0a00f3

.field public static final mobileSendLast:I = 0x7f0a00f5

.field public static final mobileSendMax:I = 0x7f0a00f4

.field public static final mobileSendTotal:I = 0x7f0a00f6

.field public static final monitorview:I = 0x7f0a0088

.field public static final monitorview2:I = 0x7f0a0093

.field public static final monitorview3:I = 0x7f0a0091

.field public static final monitorview4:I = 0x7f0a0094

.field public static final monitorviewMobile:I = 0x7f0a00e6

.field public static final monitorviewWifi:I = 0x7f0a00e5

.field public static final name:I = 0x7f0a00ce

.field public static final never:I = 0x7f0a0026

.field public static final newFolder:I = 0x7f0a0114

.field public static final new_color_panel:I = 0x7f0a00a7

.field public static final nextButton:I = 0x7f0a0058

.field public static final none:I = 0x7f0a001b

.field public static final normal:I = 0x7f0a0017

.field public static final num_picker:I = 0x7f0a00a8

.field public static final numpicker_input:I = 0x7f0a00fa

.field public static final old_color_panel:I = 0x7f0a00a6

.field public static final outerCircleWidthLayout:I = 0x7f0a0066

.field public static final outerCircleWidthSeekBar:I = 0x7f0a0068

.field public static final outerCircleWidthText:I = 0x7f0a0067

.field public static final pager_title_strip:I = 0x7f0a004f

.field public static final percentageCircleWidthLayout:I = 0x7f0a0069

.field public static final percentageCircleWidthSeekBar:I = 0x7f0a006b

.field public static final percentageCircleWidthText:I = 0x7f0a006a

.field public static final percentageLayout:I = 0x7f0a005a

.field public static final percentageSeekBar:I = 0x7f0a005c

.field public static final percentageText:I = 0x7f0a005b

.field public static final pie:I = 0x7f0a009b

.field public static final plus:I = 0x7f0a006f

.field public static final pref_num_picker:I = 0x7f0a00fc

.field public static final prevButton:I = 0x7f0a0059

.field public static final progressBar:I = 0x7f0a0105

.field public static final progress_circular:I = 0x7f0a0005

.field public static final progress_horizontal:I = 0x7f0a0006

.field public static final quadCoreLayout:I = 0x7f0a0092

.field public static final radio:I = 0x7f0a0038

.field public static final readAvg:I = 0x7f0a00d1

.field public static final readLast:I = 0x7f0a00d4

.field public static final readMax:I = 0x7f0a00d2

.field public static final readMin:I = 0x7f0a00d3

.field public static final readSpeedTitle:I = 0x7f0a00dc

.field public static final readSpeedValue:I = 0x7f0a00dd

.field public static final replay:I = 0x7f0a0110

.field public static final resetFrequencies:I = 0x7f0a0116

.field public static final resetStats:I = 0x7f0a0111

.field public static final save:I = 0x7f0a0113

.field public static final scroll:I = 0x7f0a0108

.field public static final search_badge:I = 0x7f0a0042

.field public static final search_bar:I = 0x7f0a0041

.field public static final search_button:I = 0x7f0a0043

.field public static final search_close_btn:I = 0x7f0a0048

.field public static final search_edit_frame:I = 0x7f0a0044

.field public static final search_go_btn:I = 0x7f0a004a

.field public static final search_mag_icon:I = 0x7f0a0045

.field public static final search_plate:I = 0x7f0a0046

.field public static final search_src_text:I = 0x7f0a0047

.field public static final search_voice_btn:I = 0x7f0a004b

.field public static final seekBar:I = 0x7f0a0103

.field public static final separator:I = 0x7f0a0086

.field public static final shadow:I = 0x7f0a00e4

.field public static final shadowToolbar:I = 0x7f0a00e2

.field public static final shortcut:I = 0x7f0a0037

.field public static final showCustom:I = 0x7f0a001c

.field public static final showHome:I = 0x7f0a001d

.field public static final showTitle:I = 0x7f0a001e

.field public static final speedTitle:I = 0x7f0a00b2

.field public static final speedValue:I = 0x7f0a00b3

.field public static final split_action_bar:I = 0x7f0a0007

.field public static final statLayout1:I = 0x7f0a00af

.field public static final statLayout2:I = 0x7f0a00b1

.field public static final statLayout3:I = 0x7f0a00b4

.field public static final statLayout4:I = 0x7f0a00b7

.field public static final statTitle0:I = 0x7f0a00c6

.field public static final statTitle1:I = 0x7f0a00c8

.field public static final statTitle2:I = 0x7f0a00ca

.field public static final statTitle3:I = 0x7f0a00cc

.field public static final statValue0:I = 0x7f0a00c7

.field public static final statValue1:I = 0x7f0a00c9

.field public static final statValue2:I = 0x7f0a00cb

.field public static final statValue3:I = 0x7f0a00cd

.field public static final stats:I = 0x7f0a0106

.field public static final stats_layout:I = 0x7f0a0095

.field public static final stats_layout_cpu:I = 0x7f0a0096

.field public static final stop_start:I = 0x7f0a0112

.field public static final submit_area:I = 0x7f0a0049

.field public static final tabMode:I = 0x7f0a0018

.field public static final tab_apps_cache:I = 0x7f0a0008

.field public static final tab_battery_history:I = 0x7f0a0009

.field public static final tab_battery_stats:I = 0x7f0a000a

.field public static final tab_battery_temperature:I = 0x7f0a000b

.field public static final tab_cpu:I = 0x7f0a000c

.field public static final tab_cpu_freqs:I = 0x7f0a000d

.field public static final tab_cpu_temperature:I = 0x7f0a000e

.field public static final tab_gpu:I = 0x7f0a000f

.field public static final tab_io:I = 0x7f0a0010

.field public static final tab_net:I = 0x7f0a0011

.field public static final tab_ram:I = 0x7f0a0012

.field public static final tab_storage_stats:I = 0x7f0a0013

.field public static final tab_top_apps:I = 0x7f0a0014

.field public static final text:I = 0x7f0a00ab

.field public static final text2:I = 0x7f0a00ac

.field public static final textColorLayout:I = 0x7f0a005d

.field public static final textViewCycleRateHour:I = 0x7f0a0075

.field public static final textViewCycleRateTitle:I = 0x7f0a0074

.field public static final textViewCycleSpeed:I = 0x7f0a0073

.field public static final textViewCycleSpeedTitle:I = 0x7f0a0072

.field public static final textViewStatsChargeRateHour:I = 0x7f0a0077

.field public static final textViewStatsChargeSpeed:I = 0x7f0a0076

.field public static final textViewStatsDisChargeRateHour:I = 0x7f0a007a

.field public static final textViewStatsDisChargeSpeed:I = 0x7f0a0079

.field public static final textViewStatsTimeToCharge:I = 0x7f0a0078

.field public static final textViewStatsTimeToDisCharge:I = 0x7f0a007b

.field public static final throughputTitle:I = 0x7f0a00f7

.field public static final throughputValue:I = 0x7f0a00f8

.field public static final timePickerEnd:I = 0x7f0a00d0

.field public static final timePickerStart:I = 0x7f0a00cf

.field public static final title:I = 0x7f0a0034

.field public static final toolbar:I = 0x7f0a004d

.field public static final topAppIcon:I = 0x7f0a0050

.field public static final topAppName:I = 0x7f0a0051

.field public static final topAppProcessName:I = 0x7f0a0052

.field public static final topAppUsageCpu:I = 0x7f0a0053

.field public static final topAppUsageNet:I = 0x7f0a0055

.field public static final topAppUsageRam:I = 0x7f0a0054

.field public static final totalRamTitle:I = 0x7f0a0101

.field public static final totalRamValue:I = 0x7f0a0102

.field public static final totalRead:I = 0x7f0a00d9

.field public static final totalTotal:I = 0x7f0a00db

.field public static final totalWrite:I = 0x7f0a00da

.field public static final tvFreq:I = 0x7f0a0097

.field public static final tvFreqPercentage:I = 0x7f0a0099

.field public static final tvFreqTime:I = 0x7f0a0098

.field public static final up:I = 0x7f0a0015

.field public static final usageAvg:I = 0x7f0a0089

.field public static final usageLast:I = 0x7f0a008c

.field public static final usageMax:I = 0x7f0a008a

.field public static final usageMin:I = 0x7f0a008b

.field public static final useLogo:I = 0x7f0a001f

.field public static final usedRamTitle:I = 0x7f0a00ff

.field public static final usedRamValue:I = 0x7f0a0100

.field public static final utilizationTitle:I = 0x7f0a00b0

.field public static final viewPager:I = 0x7f0a004e

.field public static final widgetConfigFragment:I = 0x7f0a010d

.field public static final wifiRecvAvg:I = 0x7f0a00e7

.field public static final wifiRecvLast:I = 0x7f0a00e9

.field public static final wifiRecvMax:I = 0x7f0a00e8

.field public static final wifiRecvTotal:I = 0x7f0a00ea

.field public static final wifiSendAvg:I = 0x7f0a00ef

.field public static final wifiSendLast:I = 0x7f0a00f1

.field public static final wifiSendMax:I = 0x7f0a00f0

.field public static final wifiSendTotal:I = 0x7f0a00f2

.field public static final withText:I = 0x7f0a0027

.field public static final wrap_content:I = 0x7f0a002a

.field public static final writeAvg:I = 0x7f0a00d5

.field public static final writeLast:I = 0x7f0a00d8

.field public static final writeMax:I = 0x7f0a00d6

.field public static final writeMin:I = 0x7f0a00d7

.field public static final writeSpeedTitle:I = 0x7f0a00de

.field public static final writeSpeedValue:I = 0x7f0a00df
