.class public final Lcom/cgollner/systemmonitor/b/a$c;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cgollner/systemmonitor/b/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "c"
.end annotation


# static fields
.field public static final abc_background_cache_hint_selector_material_dark:I = 0x7f08005f

.field public static final abc_background_cache_hint_selector_material_light:I = 0x7f080060

.field public static final abc_input_method_navigation_guard:I = 0x7f080000

.field public static final abc_primary_text_disable_only_material_dark:I = 0x7f080061

.field public static final abc_primary_text_disable_only_material_light:I = 0x7f080062

.field public static final abc_primary_text_material_dark:I = 0x7f080063

.field public static final abc_primary_text_material_light:I = 0x7f080064

.field public static final abc_search_url_text:I = 0x7f080065

.field public static final abc_search_url_text_normal:I = 0x7f080001

.field public static final abc_search_url_text_pressed:I = 0x7f080002

.field public static final abc_search_url_text_selected:I = 0x7f080003

.field public static final abc_secondary_text_material_dark:I = 0x7f080066

.field public static final abc_secondary_text_material_light:I = 0x7f080067

.field public static final accent_material_dark:I = 0x7f080004

.field public static final accent_material_light:I = 0x7f080005

.field public static final background_floating_material_dark:I = 0x7f080006

.field public static final background_floating_material_light:I = 0x7f080007

.field public static final background_material_dark:I = 0x7f080008

.field public static final background_material_light:I = 0x7f080009

.field public static final batteryQ1Fill:I = 0x7f08000a

.field public static final batteryQ1Line:I = 0x7f08000b

.field public static final batteryQ1Preview:I = 0x7f08000c

.field public static final batteryQ2Fill:I = 0x7f08000d

.field public static final batteryQ2Line:I = 0x7f08000e

.field public static final batteryQ2Preview:I = 0x7f08000f

.field public static final batteryQ3Fill:I = 0x7f080010

.field public static final batteryQ3Line:I = 0x7f080011

.field public static final batteryQ3Preview:I = 0x7f080012

.field public static final batteryQ4Fill:I = 0x7f080013

.field public static final batteryQ4Line:I = 0x7f080014

.field public static final batteryQ4Preview:I = 0x7f080015

.field public static final black_overlay:I = 0x7f080016

.field public static final bright_foreground_disabled_material_dark:I = 0x7f080017

.field public static final bright_foreground_disabled_material_light:I = 0x7f080018

.field public static final bright_foreground_inverse_material_dark:I = 0x7f080019

.field public static final bright_foreground_inverse_material_light:I = 0x7f08001a

.field public static final bright_foreground_material_dark:I = 0x7f08001b

.field public static final bright_foreground_material_light:I = 0x7f08001c

.field public static final button_material_dark:I = 0x7f08001d

.field public static final button_material_light:I = 0x7f08001e

.field public static final colorPrimaryDark:I = 0x7f08001f

.field public static final darkgray:I = 0x7f080020

.field public static final dim_foreground_disabled_material_dark:I = 0x7f080021

.field public static final dim_foreground_disabled_material_light:I = 0x7f080022

.field public static final dim_foreground_material_dark:I = 0x7f080023

.field public static final dim_foreground_material_light:I = 0x7f080024

.field public static final fillColor:I = 0x7f080025

.field public static final fillColorDark:I = 0x7f080026

.field public static final gridColor:I = 0x7f080027

.field public static final gridColorDark:I = 0x7f080028

.field public static final highlighted_text_material_dark:I = 0x7f080029

.field public static final highlighted_text_material_light:I = 0x7f08002a

.field public static final hint_foreground_material_dark:I = 0x7f08002b

.field public static final hint_foreground_material_light:I = 0x7f08002c

.field public static final holo_blue_dark:I = 0x7f08002d

.field public static final holo_blue_light:I = 0x7f08002e

.field public static final holo_green_dark:I = 0x7f08002f

.field public static final holo_green_light:I = 0x7f080030

.field public static final holo_orange_dark:I = 0x7f080031

.field public static final holo_orange_light:I = 0x7f080032

.field public static final holo_purple_dark:I = 0x7f080033

.field public static final holo_purple_light:I = 0x7f080034

.field public static final holo_red_dark:I = 0x7f080035

.field public static final holo_red_light:I = 0x7f080036

.field public static final ioFillColor:I = 0x7f080037

.field public static final ioGridColor:I = 0x7f080038

.field public static final ioLineColor:I = 0x7f080039

.field public static final lineColor:I = 0x7f08003a

.field public static final link_text_material_dark:I = 0x7f08003b

.field public static final link_text_material_light:I = 0x7f08003c

.field public static final material_blue_grey_800:I = 0x7f08003d

.field public static final material_blue_grey_900:I = 0x7f08003e

.field public static final material_blue_grey_950:I = 0x7f08003f

.field public static final material_deep_teal_200:I = 0x7f080040

.field public static final material_deep_teal_500:I = 0x7f080041

.field public static final networkFillColor:I = 0x7f080042

.field public static final networkGridColor:I = 0x7f080043

.field public static final networkLineColor:I = 0x7f080044

.field public static final notificationBG:I = 0x7f080045

.field public static final notificationIconBg:I = 0x7f080046

.field public static final notificationLine:I = 0x7f080047

.field public static final pressed_greenaction:I = 0x7f080048

.field public static final primary_dark_material_dark:I = 0x7f080049

.field public static final primary_dark_material_light:I = 0x7f08004a

.field public static final primary_material_dark:I = 0x7f08004b

.field public static final primary_material_light:I = 0x7f08004c

.field public static final primary_text_default_material_dark:I = 0x7f08004d

.field public static final primary_text_default_material_light:I = 0x7f08004e

.field public static final primary_text_disabled_material_dark:I = 0x7f08004f

.field public static final primary_text_disabled_material_light:I = 0x7f080050

.field public static final ramFillColor:I = 0x7f080051

.field public static final ramGridColor:I = 0x7f080052

.field public static final ramLineColor:I = 0x7f080053

.field public static final ripple_material_dark:I = 0x7f080054

.field public static final ripple_material_light:I = 0x7f080055

.field public static final secondary_text_default_material_dark:I = 0x7f080056

.field public static final secondary_text_default_material_light:I = 0x7f080057

.field public static final secondary_text_disabled_material_dark:I = 0x7f080058

.field public static final secondary_text_disabled_material_light:I = 0x7f080059

.field public static final switch_thumb_normal_material_dark:I = 0x7f08005a

.field public static final switch_thumb_normal_material_light:I = 0x7f08005b

.field public static final whiteFillColor:I = 0x7f08005c

.field public static final whiteGridColor:I = 0x7f08005d

.field public static final whiteLineColor:I = 0x7f08005e
