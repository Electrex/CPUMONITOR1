.class final Lcom/cgollner/systemmonitor/b$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnPreDrawListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/cgollner/systemmonitor/b;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/cgollner/systemmonitor/b;


# direct methods
.method constructor <init>(Lcom/cgollner/systemmonitor/b;)V
    .locals 0

    .prologue
    .line 119
    iput-object p1, p0, Lcom/cgollner/systemmonitor/b$1;->a:Lcom/cgollner/systemmonitor/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onPreDraw()Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 122
    iget-object v0, p0, Lcom/cgollner/systemmonitor/b$1;->a:Lcom/cgollner/systemmonitor/b;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/b;->a(Lcom/cgollner/systemmonitor/b;)Landroid/support/v4/view/ViewPager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 124
    iget-object v0, p0, Lcom/cgollner/systemmonitor/b$1;->a:Lcom/cgollner/systemmonitor/b;

    iget-object v2, p0, Lcom/cgollner/systemmonitor/b$1;->a:Lcom/cgollner/systemmonitor/b;

    invoke-virtual {v2}, Lcom/cgollner/systemmonitor/b;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "pos"

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    invoke-static {v0, v2}, Lcom/cgollner/systemmonitor/b;->a(Lcom/cgollner/systemmonitor/b;I)I

    move-result v0

    .line 125
    if-ltz v0, :cond_0

    iget-object v2, p0, Lcom/cgollner/systemmonitor/b$1;->a:Lcom/cgollner/systemmonitor/b;

    iget-object v2, v2, Lcom/cgollner/systemmonitor/b;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lt v0, v2, :cond_1

    :cond_0
    move v0, v1

    .line 128
    :cond_1
    if-eqz v0, :cond_3

    iget-object v2, p0, Lcom/cgollner/systemmonitor/b$1;->a:Lcom/cgollner/systemmonitor/b;

    invoke-static {v2}, Lcom/cgollner/systemmonitor/b;->a(Lcom/cgollner/systemmonitor/b;)Landroid/support/v4/view/ViewPager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v2

    if-eq v2, v0, :cond_3

    .line 129
    iget-object v1, p0, Lcom/cgollner/systemmonitor/b$1;->a:Lcom/cgollner/systemmonitor/b;

    invoke-static {v1}, Lcom/cgollner/systemmonitor/b;->a(Lcom/cgollner/systemmonitor/b;)Landroid/support/v4/view/ViewPager;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    move v1, v0

    .line 137
    :goto_0
    iget-object v0, p0, Lcom/cgollner/systemmonitor/b$1;->a:Lcom/cgollner/systemmonitor/b;

    iget-object v0, v0, Lcom/cgollner/systemmonitor/b;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/b$a;

    .line 138
    iget v2, v0, Lcom/cgollner/systemmonitor/b$a;->a:I

    .line 139
    iget-object v3, v0, Lcom/cgollner/systemmonitor/b$a;->d:Ljava/lang/Class;

    const-class v4, Lcom/cgollner/systemmonitor/d/c;

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 140
    iget-object v3, p0, Lcom/cgollner/systemmonitor/b$1;->a:Lcom/cgollner/systemmonitor/b;

    invoke-virtual {v3}, Lcom/cgollner/systemmonitor/b;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "android:switcher:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v5, Lcom/cgollner/systemmonitor/b/a$e;->viewPager:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ":"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/cgollner/systemmonitor/d/c;

    .line 143
    if-eqz v1, :cond_7

    .line 144
    invoke-virtual {v1}, Lcom/cgollner/systemmonitor/d/c;->a()I

    move-result v1

    :goto_1
    move v2, v1

    .line 159
    :cond_2
    :goto_2
    iget-object v1, p0, Lcom/cgollner/systemmonitor/b$1;->a:Lcom/cgollner/systemmonitor/b;

    iget v0, v0, Lcom/cgollner/systemmonitor/b$a;->c:I

    invoke-static {v1, v0, v2}, Lcom/cgollner/systemmonitor/b;->a(Lcom/cgollner/systemmonitor/b;II)V

    .line 161
    const/4 v0, 0x1

    return v0

    .line 131
    :cond_3
    sget-object v0, Lcom/cgollner/systemmonitor/App;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 132
    const-string v2, "pos"

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 133
    if-eqz v0, :cond_4

    iget-object v1, p0, Lcom/cgollner/systemmonitor/b$1;->a:Lcom/cgollner/systemmonitor/b;

    invoke-static {v1}, Lcom/cgollner/systemmonitor/b;->a(Lcom/cgollner/systemmonitor/b;)Landroid/support/v4/view/ViewPager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v1

    if-eq v1, v0, :cond_4

    .line 134
    iget-object v1, p0, Lcom/cgollner/systemmonitor/b$1;->a:Lcom/cgollner/systemmonitor/b;

    invoke-static {v1}, Lcom/cgollner/systemmonitor/b;->a(Lcom/cgollner/systemmonitor/b;)Landroid/support/v4/view/ViewPager;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    :cond_4
    move v1, v0

    goto :goto_0

    .line 147
    :cond_5
    iget-object v3, v0, Lcom/cgollner/systemmonitor/b$a;->d:Ljava/lang/Class;

    const-class v4, Lcom/cgollner/systemmonitor/d/b;

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 148
    iget-object v3, p0, Lcom/cgollner/systemmonitor/b$1;->a:Lcom/cgollner/systemmonitor/b;

    invoke-virtual {v3}, Lcom/cgollner/systemmonitor/b;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "android:switcher:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v5, Lcom/cgollner/systemmonitor/b/a$e;->viewPager:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ":"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/cgollner/systemmonitor/d/b;

    .line 151
    invoke-virtual {v1}, Lcom/cgollner/systemmonitor/d/b;->a()V

    goto :goto_2

    .line 153
    :cond_6
    iget-object v3, v0, Lcom/cgollner/systemmonitor/b$a;->d:Ljava/lang/Class;

    const-class v4, Lcom/cgollner/systemmonitor/d/p;

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 154
    iget-object v3, p0, Lcom/cgollner/systemmonitor/b$1;->a:Lcom/cgollner/systemmonitor/b;

    invoke-virtual {v3}, Lcom/cgollner/systemmonitor/b;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "android:switcher:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v5, Lcom/cgollner/systemmonitor/b/a$e;->viewPager:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ":"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/cgollner/systemmonitor/d/p;

    .line 157
    invoke-virtual {v1}, Lcom/cgollner/systemmonitor/d/p;->a()V

    goto/16 :goto_2

    :cond_7
    move v1, v2

    goto/16 :goto_1
.end method
