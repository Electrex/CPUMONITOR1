.class public final Lcom/cgollner/systemmonitor/FrequenciesPieChart$a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cgollner/systemmonitor/FrequenciesPieChart;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/cgollner/systemmonitor/FrequenciesPieChart$a;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:J

.field public d:F


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    return-void
.end method


# virtual methods
.method public final bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 4

    .prologue
    .line 43
    check-cast p1, Lcom/cgollner/systemmonitor/FrequenciesPieChart$a;

    iget-wide v0, p0, Lcom/cgollner/systemmonitor/FrequenciesPieChart$a;->c:J

    iget-wide v2, p1, Lcom/cgollner/systemmonitor/FrequenciesPieChart$a;->c:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    iget-wide v0, p0, Lcom/cgollner/systemmonitor/FrequenciesPieChart$a;->c:J

    iget-wide v2, p1, Lcom/cgollner/systemmonitor/FrequenciesPieChart$a;->c:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
