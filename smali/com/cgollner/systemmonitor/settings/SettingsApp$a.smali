.class public final Lcom/cgollner/systemmonitor/settings/SettingsApp$a;
.super Lcom/cgollner/systemmonitor/settings/b$a;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cgollner/systemmonitor/settings/SettingsApp;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# instance fields
.field private a:Landroid/preference/Preference$OnPreferenceClickListener;

.field private b:Landroid/preference/Preference$OnPreferenceClickListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/cgollner/systemmonitor/settings/b$a;-><init>()V

    .line 46
    new-instance v0, Lcom/cgollner/systemmonitor/settings/SettingsApp$a$1;

    invoke-direct {v0, p0}, Lcom/cgollner/systemmonitor/settings/SettingsApp$a$1;-><init>(Lcom/cgollner/systemmonitor/settings/SettingsApp$a;)V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/settings/SettingsApp$a;->a:Landroid/preference/Preference$OnPreferenceClickListener;

    .line 67
    new-instance v0, Lcom/cgollner/systemmonitor/settings/SettingsApp$a$2;

    invoke-direct {v0, p0}, Lcom/cgollner/systemmonitor/settings/SettingsApp$a$2;-><init>(Lcom/cgollner/systemmonitor/settings/SettingsApp$a;)V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/settings/SettingsApp$a;->b:Landroid/preference/Preference$OnPreferenceClickListener;

    .line 35
    return-void
.end method


# virtual methods
.method protected final a()I
    .locals 1

    .prologue
    .line 89
    sget v0, Lcom/cgollner/systemmonitor/b/a$k;->settings_app_prefs:I

    return v0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 39
    invoke-super {p0, p1}, Lcom/cgollner/systemmonitor/settings/b$a;->onCreate(Landroid/os/Bundle;)V

    .line 40
    sget-object v0, Lcom/cgollner/systemmonitor/App;->a:Landroid/content/Context;

    sget v1, Lcom/cgollner/systemmonitor/b/a$h;->battery_clear_stats_key:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/settings/SettingsApp$a;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 41
    iget-object v1, p0, Lcom/cgollner/systemmonitor/settings/SettingsApp$a;->b:Landroid/preference/Preference$OnPreferenceClickListener;

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 43
    sget-object v0, Lcom/cgollner/systemmonitor/App;->a:Landroid/content/Context;

    sget v1, Lcom/cgollner/systemmonitor/b/a$h;->batteryhistoryclearkey:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/settings/SettingsApp$a;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iget-object v1, p0, Lcom/cgollner/systemmonitor/settings/SettingsApp$a;->a:Landroid/preference/Preference$OnPreferenceClickListener;

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 44
    return-void
.end method

.method public final onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 96
    invoke-super {p0, p1, p2}, Lcom/cgollner/systemmonitor/settings/b$a;->onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V

    .line 98
    sget-object v0, Lcom/cgollner/systemmonitor/App;->a:Landroid/content/Context;

    sget v1, Lcom/cgollner/systemmonitor/b/a$h;->settings_theme_choose_key:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 99
    invoke-virtual {p0, p2}, Lcom/cgollner/systemmonitor/settings/SettingsApp$a;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/ListPreference;

    .line 100
    const-string v1, ""

    invoke-interface {p1, p2, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->findIndexOfValue(Ljava/lang/String;)I

    move-result v0

    .line 101
    invoke-interface {p1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 102
    const-string v2, "theme"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 103
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 105
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/settings/SettingsApp$a;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 106
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/settings/SettingsApp$a;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 107
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 108
    const-string v1, "settings"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 109
    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/settings/SettingsApp$a;->startActivity(Landroid/content/Intent;)V

    .line 111
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/settings/SettingsApp$a;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 154
    :cond_0
    :goto_0
    return-void

    .line 113
    :cond_1
    sget-object v0, Lcom/cgollner/systemmonitor/App;->a:Landroid/content/Context;

    sget v1, Lcom/cgollner/systemmonitor/b/a$h;->settings_app_cpu_updatefreq_key:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 114
    invoke-interface {p1, p2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 115
    mul-int/lit16 v0, v0, 0x3e8

    sput v0, Lcom/cgollner/systemmonitor/d/g;->a:I

    goto :goto_0

    .line 117
    :cond_2
    sget-object v0, Lcom/cgollner/systemmonitor/App;->a:Landroid/content/Context;

    sget v1, Lcom/cgollner/systemmonitor/b/a$h;->settings_app_ram_updatefreq_key:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 118
    invoke-interface {p1, p2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 119
    mul-int/lit16 v0, v0, 0x3e8

    sput v0, Lcom/cgollner/systemmonitor/d/o;->a:I

    goto :goto_0

    .line 121
    :cond_3
    sget-object v0, Lcom/cgollner/systemmonitor/App;->a:Landroid/content/Context;

    sget v1, Lcom/cgollner/systemmonitor/b/a$h;->settings_app_io_updatefreq_key:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 122
    invoke-interface {p1, p2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 123
    mul-int/lit16 v0, v0, 0x3e8

    sput v0, Lcom/cgollner/systemmonitor/d/k;->a:I

    goto :goto_0

    .line 125
    :cond_4
    sget-object v0, Lcom/cgollner/systemmonitor/App;->a:Landroid/content/Context;

    sget v1, Lcom/cgollner/systemmonitor/b/a$h;->settings_app_net_updatefreq_key:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 126
    invoke-interface {p1, p2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 127
    mul-int/lit16 v0, v0, 0x3e8

    sput v0, Lcom/cgollner/systemmonitor/d/l;->a:I

    goto :goto_0

    .line 129
    :cond_5
    sget-object v0, Lcom/cgollner/systemmonitor/App;->a:Landroid/content/Context;

    sget v1, Lcom/cgollner/systemmonitor/b/a$h;->settings_app_topapps_updatefreq_key:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 130
    invoke-interface {p1, p2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 131
    mul-int/lit16 v0, v0, 0x3e8

    sput v0, Lcom/cgollner/systemmonitor/d/n;->a:I

    goto :goto_0

    .line 135
    :cond_6
    sget-object v0, Lcom/cgollner/systemmonitor/App;->a:Landroid/content/Context;

    sget v1, Lcom/cgollner/systemmonitor/b/a$h;->battery_strategy_key:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 136
    invoke-virtual {p0, p2}, Lcom/cgollner/systemmonitor/settings/SettingsApp$a;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/ListPreference;

    .line 137
    invoke-virtual {v0}, Landroid/preference/ListPreference;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->findIndexOfValue(Ljava/lang/String;)I

    move-result v0

    .line 138
    sget-object v1, Lcom/cgollner/systemmonitor/App;->a:Landroid/content/Context;

    invoke-static {v0, v1}, Lcom/cgollner/systemmonitor/battery/BatteryService;->a(ILandroid/content/Context;)V

    goto/16 :goto_0

    .line 141
    :cond_7
    sget-object v0, Lcom/cgollner/systemmonitor/App;->a:Landroid/content/Context;

    sget v1, Lcom/cgollner/systemmonitor/b/a$h;->battery_history_key:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 142
    invoke-interface {p1, p2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 143
    if-eqz v0, :cond_8

    .line 144
    const-class v0, Lcom/cgollner/systemmonitor/battery/BatteryService;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/settings/SettingsApp$a;->a(Ljava/lang/Class;)V

    goto/16 :goto_0

    .line 147
    :cond_8
    const-class v0, Lcom/cgollner/systemmonitor/battery/BatteryService;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/settings/SettingsApp$a;->b(Ljava/lang/Class;)V

    goto/16 :goto_0

    .line 150
    :cond_9
    sget-object v0, Lcom/cgollner/systemmonitor/App;->a:Landroid/content/Context;

    sget v1, Lcom/cgollner/systemmonitor/b/a$h;->battery_history_size_key:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 151
    const/16 v0, 0x30

    invoke-interface {p1, p2, v0}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 152
    sget-object v1, Lcom/cgollner/systemmonitor/App;->a:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/cgollner/systemmonitor/battery/BatteryService;->a(Landroid/content/Context;I)V

    goto/16 :goto_0
.end method
