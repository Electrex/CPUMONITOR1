.class final Lcom/cgollner/systemmonitor/settings/SettingsBgHistory$a$1$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/cgollner/systemmonitor/settings/SettingsBgHistory$a$1;->onPreferenceClick(Landroid/preference/Preference;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/cgollner/systemmonitor/settings/SettingsBgHistory$a$1;


# direct methods
.method constructor <init>(Lcom/cgollner/systemmonitor/settings/SettingsBgHistory$a$1;)V
    .locals 0

    .prologue
    .line 75
    iput-object p1, p0, Lcom/cgollner/systemmonitor/settings/SettingsBgHistory$a$1$1;->a:Lcom/cgollner/systemmonitor/settings/SettingsBgHistory$a$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    .prologue
    .line 79
    if-nez p2, :cond_0

    .line 80
    new-instance v0, Landroid/content/Intent;

    sget-object v1, Lcom/cgollner/systemmonitor/App;->a:Landroid/content/Context;

    const-class v2, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 81
    const-string v1, "SEE_ONLY"

    iget-object v2, p0, Lcom/cgollner/systemmonitor/settings/SettingsBgHistory$a$1$1;->a:Lcom/cgollner/systemmonitor/settings/SettingsBgHistory$a$1;

    iget-object v2, v2, Lcom/cgollner/systemmonitor/settings/SettingsBgHistory$a$1;->a:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 82
    iget-object v1, p0, Lcom/cgollner/systemmonitor/settings/SettingsBgHistory$a$1$1;->a:Lcom/cgollner/systemmonitor/settings/SettingsBgHistory$a$1;

    iget-object v1, v1, Lcom/cgollner/systemmonitor/settings/SettingsBgHistory$a$1;->d:Lcom/cgollner/systemmonitor/settings/SettingsBgHistory$a;

    invoke-virtual {v1, v0}, Lcom/cgollner/systemmonitor/settings/SettingsBgHistory$a;->startActivity(Landroid/content/Intent;)V

    .line 88
    :goto_0
    return-void

    .line 85
    :cond_0
    iget-object v0, p0, Lcom/cgollner/systemmonitor/settings/SettingsBgHistory$a$1$1;->a:Lcom/cgollner/systemmonitor/settings/SettingsBgHistory$a$1;

    iget-object v0, v0, Lcom/cgollner/systemmonitor/settings/SettingsBgHistory$a$1;->a:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 86
    iget-object v0, p0, Lcom/cgollner/systemmonitor/settings/SettingsBgHistory$a$1$1;->a:Lcom/cgollner/systemmonitor/settings/SettingsBgHistory$a$1;

    iget-object v0, v0, Lcom/cgollner/systemmonitor/settings/SettingsBgHistory$a$1;->b:Landroid/preference/PreferenceCategory;

    iget-object v1, p0, Lcom/cgollner/systemmonitor/settings/SettingsBgHistory$a$1$1;->a:Lcom/cgollner/systemmonitor/settings/SettingsBgHistory$a$1;

    iget-object v1, v1, Lcom/cgollner/systemmonitor/settings/SettingsBgHistory$a$1;->c:Landroid/preference/Preference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_0
.end method
