.class public final Lcom/cgollner/systemmonitor/settings/SettingsAbout$a;
.super Lcom/cgollner/systemmonitor/settings/b$a;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cgollner/systemmonitor/settings/SettingsAbout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# instance fields
.field private a:Landroid/preference/Preference$OnPreferenceClickListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/cgollner/systemmonitor/settings/b$a;-><init>()V

    .line 55
    new-instance v0, Lcom/cgollner/systemmonitor/settings/SettingsAbout$a$1;

    invoke-direct {v0, p0}, Lcom/cgollner/systemmonitor/settings/SettingsAbout$a$1;-><init>(Lcom/cgollner/systemmonitor/settings/SettingsAbout$a;)V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/settings/SettingsAbout$a;->a:Landroid/preference/Preference$OnPreferenceClickListener;

    .line 23
    return-void
.end method


# virtual methods
.method protected final a()I
    .locals 1

    .prologue
    .line 70
    sget v0, Lcom/cgollner/systemmonitor/b/a$k;->settings_about:I

    return v0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 27
    invoke-super {p0, p1}, Lcom/cgollner/systemmonitor/settings/b$a;->onCreate(Landroid/os/Bundle;)V

    .line 28
    sget-object v0, Lcom/cgollner/systemmonitor/App;->a:Landroid/content/Context;

    sget v1, Lcom/cgollner/systemmonitor/b/a$h;->email_key:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/settings/SettingsAbout$a;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 29
    iget-object v1, p0, Lcom/cgollner/systemmonitor/settings/SettingsAbout$a;->a:Landroid/preference/Preference$OnPreferenceClickListener;

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 32
    :try_start_0
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/settings/SettingsAbout$a;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/settings/SettingsAbout$a;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    .line 33
    const-string v1, "versionKey"

    invoke-virtual {p0, v1}, Lcom/cgollner/systemmonitor/settings/SettingsAbout$a;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    .line 34
    iget-object v2, v0, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/settings/SettingsAbout$a;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 35
    iget-object v2, v0, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 36
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xb

    if-lt v2, v3, :cond_0

    .line 37
    iget-object v0, v0, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/settings/SettingsAbout$a;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/content/pm/ApplicationInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setIcon(Landroid/graphics/drawable/Drawable;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 43
    :cond_0
    :goto_0
    const-string v0, "facebookKey"

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/settings/SettingsAbout$a;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v6

    .line 44
    if-eqz v6, :cond_1

    .line 45
    invoke-static {}, Ljava/util/GregorianCalendar;->getInstance()Ljava/util/Calendar;

    move-result-object v7

    .line 46
    invoke-static {}, Ljava/util/GregorianCalendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 47
    const/16 v1, 0x7dd

    const/4 v2, 0x4

    const/4 v3, 0x5

    const/16 v4, 0x14

    invoke-virtual/range {v0 .. v5}, Ljava/util/Calendar;->set(IIIII)V

    .line 48
    invoke-virtual {v7, v0}, Ljava/util/Calendar;->before(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 49
    const-string v0, "Open contest for you to win 50$ with System Monitor! Visit the Facebook page for more details."

    invoke-virtual {v6, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 52
    :cond_1
    return-void

    .line 39
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0
.end method
