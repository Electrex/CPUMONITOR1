.class public abstract Lcom/cgollner/systemmonitor/settings/b;
.super Landroid/support/v7/app/ActionBarActivity;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/cgollner/systemmonitor/settings/b$a;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Landroid/support/v7/app/ActionBarActivity;-><init>()V

    .line 56
    return-void
.end method


# virtual methods
.method protected abstract a()Lcom/cgollner/systemmonitor/settings/b$a;
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 27
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 28
    const-string v3, "theme"

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0xb

    if-ge v0, v4, :cond_1

    move v0, v1

    :goto_0
    invoke-interface {v2, v3, v0}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 29
    packed-switch v0, :pswitch_data_0

    .line 40
    :goto_1
    invoke-super {p0, p1}, Landroid/support/v7/app/ActionBarActivity;->onCreate(Landroid/os/Bundle;)V

    .line 41
    sget v0, Lcom/cgollner/systemmonitor/b/a$f;->activity_frame:I

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/settings/b;->setContentView(I)V

    .line 43
    sget v0, Lcom/cgollner/systemmonitor/b/a$e;->toolbar:I

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/settings/b;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/settings/b;->setSupportActionBar(Landroid/support/v7/widget/Toolbar;)V

    .line 44
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/settings/b;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 46
    if-nez p1, :cond_0

    .line 47
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/settings/b;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    sget v1, Lcom/cgollner/systemmonitor/b/a$e;->contentFrame:I

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/settings/b;->a()Lcom/cgollner/systemmonitor/settings/b$a;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    .line 52
    :cond_0
    return-void

    .line 28
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 31
    :pswitch_0
    sget v0, Lcom/cgollner/systemmonitor/b/a$i;->AppThemeSettings:I

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/settings/b;->setTheme(I)V

    goto :goto_1

    .line 34
    :pswitch_1
    sget v0, Lcom/cgollner/systemmonitor/b/a$i;->AppThemeDarkSettings:I

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/settings/b;->setTheme(I)V

    goto :goto_1

    .line 37
    :pswitch_2
    sget v0, Lcom/cgollner/systemmonitor/b/a$i;->AppThemeSettings:I

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/settings/b;->setTheme(I)V

    goto :goto_1

    .line 29
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 135
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/settings/b;->finish()V

    .line 136
    const/4 v0, 0x1

    return v0
.end method
