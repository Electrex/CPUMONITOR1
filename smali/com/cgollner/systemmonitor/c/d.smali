.class public final Lcom/cgollner/systemmonitor/c/d;
.super Lcom/cgollner/systemmonitor/c/e;
.source "SourceFile"


# instance fields
.field a:J

.field b:J

.field public c:F

.field public d:J

.field public e:J

.field private j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private l:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private m:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private n:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private o:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(JLcom/cgollner/systemmonitor/c/e$a;)V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0, p1, p2, p3}, Lcom/cgollner/systemmonitor/c/e;-><init>(JLcom/cgollner/systemmonitor/c/e$a;)V

    .line 22
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/c/d;->start()V

    .line 25
    return-void
.end method


# virtual methods
.method protected final a()V
    .locals 2

    .prologue
    .line 30
    const/16 v0, 0xa

    invoke-static {v0}, Lcom/cgollner/systemmonitor/a/g;->a(I)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/c/d;->j:Ljava/util/List;

    .line 31
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/cgollner/systemmonitor/a/g;->a(I)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/c/d;->k:Ljava/util/List;

    .line 32
    const/4 v0, 0x7

    invoke-static {v0}, Lcom/cgollner/systemmonitor/a/g;->a(I)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/c/d;->l:Ljava/util/List;

    .line 33
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/cgollner/systemmonitor/c/d;->a:J

    .line 34
    return-void
.end method

.method protected final b()V
    .locals 14

    .prologue
    const-wide/16 v12, 0x200

    .line 38
    const/16 v0, 0xa

    invoke-static {v0}, Lcom/cgollner/systemmonitor/a/g;->a(I)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/c/d;->m:Ljava/util/List;

    .line 39
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/cgollner/systemmonitor/a/g;->a(I)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/c/d;->n:Ljava/util/List;

    .line 40
    const/4 v0, 0x7

    invoke-static {v0}, Lcom/cgollner/systemmonitor/a/g;->a(I)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/c/d;->o:Ljava/util/List;

    .line 41
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/cgollner/systemmonitor/c/d;->b:J

    .line 43
    iget-object v5, p0, Lcom/cgollner/systemmonitor/c/d;->j:Ljava/util/List;

    iget-wide v0, p0, Lcom/cgollner/systemmonitor/c/d;->a:J

    iget-object v6, p0, Lcom/cgollner/systemmonitor/c/d;->m:Ljava/util/List;

    iget-wide v2, p0, Lcom/cgollner/systemmonitor/c/d;->b:J

    sub-long v8, v2, v0

    const-wide/16 v0, 0x0

    const/4 v2, 0x0

    move v4, v2

    move-wide v2, v0

    :goto_0
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    if-ge v4, v0, :cond_0

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    if-ge v4, v0, :cond_1

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v0

    if-ge v4, v0, :cond_1

    invoke-interface {v5, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    invoke-interface {v6, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    sub-long/2addr v0, v10

    cmp-long v7, v0, v2

    if-lez v7, :cond_1

    :goto_1
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    move-wide v2, v0

    goto :goto_0

    :cond_0
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    long-to-double v2, v2

    long-to-double v4, v8

    div-double/2addr v2, v4

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(DD)D

    move-result-wide v0

    const-wide/high16 v2, 0x4059000000000000L    # 100.0

    mul-double/2addr v0, v2

    double-to-float v0, v0

    iput v0, p0, Lcom/cgollner/systemmonitor/c/d;->c:F

    .line 44
    iget-object v0, p0, Lcom/cgollner/systemmonitor/c/d;->k:Ljava/util/List;

    iget-object v1, p0, Lcom/cgollner/systemmonitor/c/d;->n:Ljava/util/List;

    invoke-static {v0, v1}, Lcom/cgollner/systemmonitor/a/g;->a(Ljava/util/List;Ljava/util/List;)J

    move-result-wide v0

    mul-long/2addr v0, v12

    iput-wide v0, p0, Lcom/cgollner/systemmonitor/c/d;->d:J

    .line 45
    iget-object v0, p0, Lcom/cgollner/systemmonitor/c/d;->l:Ljava/util/List;

    iget-object v1, p0, Lcom/cgollner/systemmonitor/c/d;->o:Ljava/util/List;

    invoke-static {v0, v1}, Lcom/cgollner/systemmonitor/a/g;->a(Ljava/util/List;Ljava/util/List;)J

    move-result-wide v0

    mul-long/2addr v0, v12

    iput-wide v0, p0, Lcom/cgollner/systemmonitor/c/d;->e:J

    .line 46
    return-void

    :cond_1
    move-wide v0, v2

    goto :goto_1
.end method
