.class public abstract Lcom/cgollner/systemmonitor/c/e;
.super Ljava/lang/Thread;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/cgollner/systemmonitor/c/e$a;
    }
.end annotation


# instance fields
.field private a:Lcom/cgollner/systemmonitor/c/e$a;

.field public f:J

.field public g:Z

.field public h:Ljava/lang/Boolean;

.field public i:Z


# direct methods
.method public constructor <init>(JLcom/cgollner/systemmonitor/c/e$a;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 13
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 14
    iput-wide p1, p0, Lcom/cgollner/systemmonitor/c/e;->f:J

    .line 15
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/c/e;->h:Ljava/lang/Boolean;

    .line 16
    iput-boolean v1, p0, Lcom/cgollner/systemmonitor/c/e;->g:Z

    .line 17
    iput-boolean v1, p0, Lcom/cgollner/systemmonitor/c/e;->i:Z

    .line 18
    iput-object p3, p0, Lcom/cgollner/systemmonitor/c/e;->a:Lcom/cgollner/systemmonitor/c/e$a;

    .line 19
    return-void
.end method


# virtual methods
.method protected abstract a()V
.end method

.method protected abstract b()V
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 47
    iget-object v1, p0, Lcom/cgollner/systemmonitor/c/e;->h:Ljava/lang/Boolean;

    monitor-enter v1

    .line 48
    const/4 v0, 0x0

    :try_start_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/c/e;->h:Ljava/lang/Boolean;

    .line 49
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public run()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x64

    .line 23
    :goto_0
    iget-object v0, p0, Lcom/cgollner/systemmonitor/c/e;->h:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 24
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/c/e;->a()V

    .line 26
    :try_start_0
    iget-boolean v0, p0, Lcom/cgollner/systemmonitor/c/e;->g:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/cgollner/systemmonitor/c/e;->i:Z

    if-eqz v0, :cond_1

    move-wide v0, v2

    :goto_1
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V

    .line 27
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/cgollner/systemmonitor/c/e;->g:Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_2

    .line 29
    :goto_2
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/c/e;->b()V

    .line 31
    iget-object v1, p0, Lcom/cgollner/systemmonitor/c/e;->h:Ljava/lang/Boolean;

    monitor-enter v1

    .line 32
    :try_start_1
    iget-object v0, p0, Lcom/cgollner/systemmonitor/c/e;->h:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 33
    iget-object v0, p0, Lcom/cgollner/systemmonitor/c/e;->a:Lcom/cgollner/systemmonitor/c/e$a;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v0, :cond_0

    :try_start_2
    iget-object v0, p0, Lcom/cgollner/systemmonitor/c/e;->a:Lcom/cgollner/systemmonitor/c/e$a;

    invoke-interface {v0}, Lcom/cgollner/systemmonitor/c/e$a;->a()V
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 35
    :cond_0
    :goto_3
    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 38
    const-wide/16 v0, 0x64

    :try_start_4
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_0

    goto :goto_0

    .line 39
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0

    .line 26
    :cond_1
    :try_start_5
    iget-wide v0, p0, Lcom/cgollner/systemmonitor/c/e;->f:J
    :try_end_5
    .catch Ljava/lang/InterruptedException; {:try_start_5 .. :try_end_5} :catch_2

    goto :goto_1

    .line 35
    :catchall_0
    move-exception v0

    :try_start_6
    monitor-exit v1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    throw v0

    .line 44
    :cond_2
    return-void

    :catch_1
    move-exception v0

    goto :goto_3

    :catch_2
    move-exception v0

    goto :goto_2
.end method
