.class public final Lcom/cgollner/systemmonitor/c/f;
.super Lcom/cgollner/systemmonitor/c/e;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/cgollner/systemmonitor/c/f$a;
    }
.end annotation


# instance fields
.field public a:J

.field public b:J

.field public c:J

.field public d:J

.field public e:J

.field public j:J

.field public k:J

.field public l:J

.field public m:J

.field private n:Lcom/cgollner/systemmonitor/c/f$a;


# direct methods
.method public constructor <init>(JLcom/cgollner/systemmonitor/c/e$a;)V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0, p1, p2, p3}, Lcom/cgollner/systemmonitor/c/e;-><init>(JLcom/cgollner/systemmonitor/c/e$a;)V

    .line 20
    new-instance v0, Lcom/cgollner/systemmonitor/c/f$a;

    invoke-direct {v0, p0}, Lcom/cgollner/systemmonitor/c/f$a;-><init>(Lcom/cgollner/systemmonitor/c/f;)V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/c/f;->n:Lcom/cgollner/systemmonitor/c/f$a;

    .line 21
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/c/f;->start()V

    .line 24
    return-void
.end method


# virtual methods
.method protected final a()V
    .locals 4

    .prologue
    .line 28
    iget-object v0, p0, Lcom/cgollner/systemmonitor/c/f;->n:Lcom/cgollner/systemmonitor/c/f$a;

    invoke-static {}, Landroid/net/TrafficStats;->getMobileRxBytes()J

    move-result-wide v2

    iput-wide v2, v0, Lcom/cgollner/systemmonitor/c/f$a;->b:J

    .line 29
    iget-object v0, p0, Lcom/cgollner/systemmonitor/c/f;->n:Lcom/cgollner/systemmonitor/c/f$a;

    invoke-static {}, Landroid/net/TrafficStats;->getTotalRxBytes()J

    move-result-wide v2

    iput-wide v2, v0, Lcom/cgollner/systemmonitor/c/f$a;->a:J

    .line 30
    iget-object v0, p0, Lcom/cgollner/systemmonitor/c/f;->n:Lcom/cgollner/systemmonitor/c/f$a;

    invoke-static {}, Landroid/net/TrafficStats;->getMobileTxBytes()J

    move-result-wide v2

    iput-wide v2, v0, Lcom/cgollner/systemmonitor/c/f$a;->d:J

    .line 31
    iget-object v0, p0, Lcom/cgollner/systemmonitor/c/f;->n:Lcom/cgollner/systemmonitor/c/f$a;

    invoke-static {}, Landroid/net/TrafficStats;->getTotalTxBytes()J

    move-result-wide v2

    iput-wide v2, v0, Lcom/cgollner/systemmonitor/c/f$a;->c:J

    .line 32
    return-void
.end method

.method protected final b()V
    .locals 4

    .prologue
    .line 36
    invoke-static {}, Landroid/net/TrafficStats;->getTotalRxBytes()J

    move-result-wide v0

    iget-object v2, p0, Lcom/cgollner/systemmonitor/c/f;->n:Lcom/cgollner/systemmonitor/c/f$a;

    iget-wide v2, v2, Lcom/cgollner/systemmonitor/c/f$a;->a:J

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lcom/cgollner/systemmonitor/c/f;->l:J

    .line 37
    invoke-static {}, Landroid/net/TrafficStats;->getTotalTxBytes()J

    move-result-wide v0

    iget-object v2, p0, Lcom/cgollner/systemmonitor/c/f;->n:Lcom/cgollner/systemmonitor/c/f$a;

    iget-wide v2, v2, Lcom/cgollner/systemmonitor/c/f$a;->c:J

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lcom/cgollner/systemmonitor/c/f;->m:J

    .line 38
    iget-wide v0, p0, Lcom/cgollner/systemmonitor/c/f;->l:J

    iget-wide v2, p0, Lcom/cgollner/systemmonitor/c/f;->m:J

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/cgollner/systemmonitor/c/f;->k:J

    .line 40
    invoke-static {}, Landroid/net/TrafficStats;->getMobileRxBytes()J

    move-result-wide v0

    iget-object v2, p0, Lcom/cgollner/systemmonitor/c/f;->n:Lcom/cgollner/systemmonitor/c/f$a;

    iget-wide v2, v2, Lcom/cgollner/systemmonitor/c/f$a;->b:J

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lcom/cgollner/systemmonitor/c/f;->d:J

    .line 41
    invoke-static {}, Landroid/net/TrafficStats;->getMobileTxBytes()J

    move-result-wide v0

    iget-object v2, p0, Lcom/cgollner/systemmonitor/c/f;->n:Lcom/cgollner/systemmonitor/c/f$a;

    iget-wide v2, v2, Lcom/cgollner/systemmonitor/c/f$a;->d:J

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lcom/cgollner/systemmonitor/c/f;->e:J

    .line 42
    iget-wide v0, p0, Lcom/cgollner/systemmonitor/c/f;->d:J

    iget-wide v2, p0, Lcom/cgollner/systemmonitor/c/f;->e:J

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/cgollner/systemmonitor/c/f;->j:J

    .line 44
    iget-wide v0, p0, Lcom/cgollner/systemmonitor/c/f;->l:J

    iget-wide v2, p0, Lcom/cgollner/systemmonitor/c/f;->d:J

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lcom/cgollner/systemmonitor/c/f;->a:J

    .line 45
    iget-wide v0, p0, Lcom/cgollner/systemmonitor/c/f;->m:J

    iget-wide v2, p0, Lcom/cgollner/systemmonitor/c/f;->e:J

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lcom/cgollner/systemmonitor/c/f;->b:J

    .line 46
    iget-wide v0, p0, Lcom/cgollner/systemmonitor/c/f;->a:J

    iget-wide v2, p0, Lcom/cgollner/systemmonitor/c/f;->b:J

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/cgollner/systemmonitor/c/f;->c:J

    .line 47
    return-void
.end method
