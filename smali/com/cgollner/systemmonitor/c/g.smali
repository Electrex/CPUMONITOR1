.class public final Lcom/cgollner/systemmonitor/c/g;
.super Lcom/cgollner/systemmonitor/c/e;
.source "SourceFile"


# instance fields
.field public a:F

.field public b:J

.field public c:J

.field public d:J

.field private e:Landroid/content/Context;

.field private j:Lcom/cgollner/systemmonitor/a/a/a;

.field private k:J

.field private l:Landroid/app/ActivityManager;


# direct methods
.method public constructor <init>(JLcom/cgollner/systemmonitor/c/e$a;Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 27
    invoke-direct {p0, p1, p2, p3}, Lcom/cgollner/systemmonitor/c/e;-><init>(JLcom/cgollner/systemmonitor/c/e$a;)V

    .line 28
    iput-object p4, p0, Lcom/cgollner/systemmonitor/c/g;->e:Landroid/content/Context;

    .line 29
    const-string v0, "activity"

    invoke-virtual {p4, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/c/g;->l:Landroid/app/ActivityManager;

    .line 31
    new-instance v0, Lcom/cgollner/systemmonitor/a/a/a;

    invoke-direct {v0}, Lcom/cgollner/systemmonitor/a/a/a;-><init>()V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/c/g;->j:Lcom/cgollner/systemmonitor/a/a/a;

    .line 32
    new-instance v0, Landroid/app/ActivityManager$MemoryInfo;

    invoke-direct {v0}, Landroid/app/ActivityManager$MemoryInfo;-><init>()V

    .line 33
    iget-object v1, p0, Lcom/cgollner/systemmonitor/c/g;->l:Landroid/app/ActivityManager;

    invoke-virtual {v1, v0}, Landroid/app/ActivityManager;->getMemoryInfo(Landroid/app/ActivityManager$MemoryInfo;)V

    .line 34
    iget-wide v0, v0, Landroid/app/ActivityManager$MemoryInfo;->threshold:J

    iput-wide v0, p0, Lcom/cgollner/systemmonitor/c/g;->k:J

    .line 36
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/c/g;->start()V

    .line 40
    return-void
.end method


# virtual methods
.method protected final a()V
    .locals 14

    .prologue
    const-wide/16 v12, 0x400

    const/4 v4, 0x0

    const-wide/16 v0, 0x0

    .line 53
    iget-object v5, p0, Lcom/cgollner/systemmonitor/c/g;->j:Lcom/cgollner/systemmonitor/a/a/a;

    invoke-static {}, Landroid/os/StrictMode;->allowThreadDiskReads()Landroid/os/StrictMode$ThreadPolicy;

    move-result-object v6

    const-wide/16 v2, 0x0

    :try_start_0
    iput-wide v2, v5, Lcom/cgollner/systemmonitor/a/a/a;->b:J

    const-wide/16 v2, 0x0

    iput-wide v2, v5, Lcom/cgollner/systemmonitor/a/a/a;->c:J

    const-wide/16 v2, 0x0

    iput-wide v2, v5, Lcom/cgollner/systemmonitor/a/a/a;->d:J

    new-instance v2, Ljava/io/FileInputStream;

    const-string v3, "/proc/meminfo"

    invoke-direct {v2, v3}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    iget-object v3, v5, Lcom/cgollner/systemmonitor/a/a/a;->a:[B

    invoke-virtual {v2, v3}, Ljava/io/FileInputStream;->read([B)I

    move-result v7

    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V

    iget-object v2, v5, Lcom/cgollner/systemmonitor/a/a/a;->a:[B

    array-length v8, v2

    move v2, v4

    move v3, v4

    :goto_0
    if-ge v2, v7, :cond_4

    const/4 v9, 0x3

    if-ge v3, v9, :cond_4

    iget-object v9, v5, Lcom/cgollner/systemmonitor/a/a/a;->a:[B

    const-string v10, "MemTotal"

    invoke-static {v9, v2, v10}, Lcom/cgollner/systemmonitor/a/a/a;->a([BILjava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_1

    add-int/lit8 v2, v2, 0x8

    iget-object v9, v5, Lcom/cgollner/systemmonitor/a/a/a;->a:[B

    invoke-static {v9, v2}, Lcom/cgollner/systemmonitor/a/a/a;->a([BI)J

    move-result-wide v10

    iput-wide v10, v5, Lcom/cgollner/systemmonitor/a/a/a;->b:J

    add-int/lit8 v3, v3, 0x1

    :cond_0
    :goto_1
    if-ge v2, v8, :cond_3

    iget-object v9, v5, Lcom/cgollner/systemmonitor/a/a/a;->a:[B

    aget-byte v9, v9, v2

    const/16 v10, 0xa

    if-eq v9, v10, :cond_3

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_1
    iget-object v9, v5, Lcom/cgollner/systemmonitor/a/a/a;->a:[B

    const-string v10, "MemFree"

    invoke-static {v9, v2, v10}, Lcom/cgollner/systemmonitor/a/a/a;->a([BILjava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_2

    add-int/lit8 v2, v2, 0x7

    iget-object v9, v5, Lcom/cgollner/systemmonitor/a/a/a;->a:[B

    invoke-static {v9, v2}, Lcom/cgollner/systemmonitor/a/a/a;->a([BI)J

    move-result-wide v10

    iput-wide v10, v5, Lcom/cgollner/systemmonitor/a/a/a;->c:J

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_2
    iget-object v9, v5, Lcom/cgollner/systemmonitor/a/a/a;->a:[B

    const-string v10, "Cached"

    invoke-static {v9, v2, v10}, Lcom/cgollner/systemmonitor/a/a/a;->a([BILjava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_0

    add-int/lit8 v2, v2, 0x6

    iget-object v9, v5, Lcom/cgollner/systemmonitor/a/a/a;->a:[B

    invoke-static {v9, v2}, Lcom/cgollner/systemmonitor/a/a/a;->a([BI)J

    move-result-wide v10

    iput-wide v10, v5, Lcom/cgollner/systemmonitor/a/a/a;->d:J
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_4
    invoke-static {v6}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    .line 54
    :goto_2
    iget-object v2, p0, Lcom/cgollner/systemmonitor/c/g;->j:Lcom/cgollner/systemmonitor/a/a/a;

    iget-wide v2, v2, Lcom/cgollner/systemmonitor/a/a/a;->c:J

    iget-object v5, p0, Lcom/cgollner/systemmonitor/c/g;->j:Lcom/cgollner/systemmonitor/a/a/a;

    iget-wide v6, v5, Lcom/cgollner/systemmonitor/a/a/a;->d:J

    add-long/2addr v2, v6

    iget-wide v6, p0, Lcom/cgollner/systemmonitor/c/g;->k:J

    sub-long/2addr v2, v6

    .line 56
    cmp-long v5, v2, v0

    if-gez v5, :cond_5

    move-wide v2, v0

    .line 59
    :cond_5
    iget-object v0, p0, Lcom/cgollner/systemmonitor/c/g;->l:Landroid/app/ActivityManager;

    invoke-virtual {v0}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;

    move-result-object v0

    .line 60
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 61
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_6
    :goto_3
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager$RunningAppProcessInfo;

    .line 63
    iget v6, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->importance:I

    const/16 v7, 0x190

    if-ne v6, v7, :cond_6

    .line 65
    iget v0, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->pid:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 53
    :catch_0
    move-exception v2

    invoke-static {v6}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    goto :goto_2

    :catch_1
    move-exception v2

    invoke-static {v6}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    goto :goto_2

    :catchall_0
    move-exception v0

    invoke-static {v6}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    throw v0

    .line 68
    :cond_7
    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v0

    new-array v6, v0, [I

    .line 70
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move v1, v4

    :goto_4
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 71
    add-int/lit8 v5, v1, 0x1

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    aput v0, v6, v1

    move v1, v5

    .line 72
    goto :goto_4

    .line 74
    :cond_8
    iget-object v0, p0, Lcom/cgollner/systemmonitor/c/g;->l:Landroid/app/ActivityManager;

    invoke-virtual {v0, v6}, Landroid/app/ActivityManager;->getProcessMemoryInfo([I)[Landroid/os/Debug$MemoryInfo;

    move-result-object v0

    .line 75
    array-length v1, v0

    :goto_5
    if-ge v4, v1, :cond_9

    aget-object v5, v0, v4

    .line 76
    iget v6, v5, Landroid/os/Debug$MemoryInfo;->dalvikPss:I

    iget v7, v5, Landroid/os/Debug$MemoryInfo;->nativePss:I

    add-int/2addr v6, v7

    iget v5, v5, Landroid/os/Debug$MemoryInfo;->otherPss:I

    add-int/2addr v5, v6

    .line 78
    mul-int/lit16 v5, v5, 0x400

    int-to-long v6, v5

    add-long/2addr v2, v6

    .line 75
    add-int/lit8 v4, v4, 0x1

    goto :goto_5

    .line 81
    :cond_9
    div-long v0, v2, v12

    div-long/2addr v0, v12

    iput-wide v0, p0, Lcom/cgollner/systemmonitor/c/g;->c:J

    .line 82
    iget-object v0, p0, Lcom/cgollner/systemmonitor/c/g;->j:Lcom/cgollner/systemmonitor/a/a/a;

    iget-wide v0, v0, Lcom/cgollner/systemmonitor/a/a/a;->b:J

    div-long/2addr v0, v12

    div-long/2addr v0, v12

    iput-wide v0, p0, Lcom/cgollner/systemmonitor/c/g;->d:J

    .line 83
    iget-wide v0, p0, Lcom/cgollner/systemmonitor/c/g;->d:J

    iget-wide v2, p0, Lcom/cgollner/systemmonitor/c/g;->c:J

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lcom/cgollner/systemmonitor/c/g;->b:J

    .line 84
    iget-wide v0, p0, Lcom/cgollner/systemmonitor/c/g;->b:J

    long-to-double v0, v0

    iget-wide v2, p0, Lcom/cgollner/systemmonitor/c/g;->d:J

    long-to-double v2, v2

    div-double/2addr v0, v2

    const-wide/high16 v2, 0x4059000000000000L    # 100.0

    mul-double/2addr v0, v2

    double-to-float v0, v0

    iput v0, p0, Lcom/cgollner/systemmonitor/c/g;->a:F

    .line 87
    return-void
.end method

.method protected final b()V
    .locals 0

    .prologue
    .line 91
    return-void
.end method
