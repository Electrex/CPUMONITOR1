.class public final Lcom/cgollner/systemmonitor/c/b;
.super Lcom/cgollner/systemmonitor/c/e;
.source "SourceFile"


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field public d:I

.field public e:Landroid/content/Context;

.field private j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(JLcom/cgollner/systemmonitor/c/e$a;Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0, p1, p2, p3}, Lcom/cgollner/systemmonitor/c/e;-><init>(JLcom/cgollner/systemmonitor/c/e$a;)V

    .line 23
    invoke-virtual {p4}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/c/b;->e:Landroid/content/Context;

    .line 24
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/c/b;->j:Ljava/util/List;

    .line 25
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/c/b;->start()V

    .line 28
    return-void
.end method


# virtual methods
.method protected final a()V
    .locals 4

    .prologue
    .line 32
    invoke-static {}, Lcom/cgollner/systemmonitor/a/b;->e()I

    move-result v0

    mul-int/lit8 v0, v0, 0xa

    iput v0, p0, Lcom/cgollner/systemmonitor/c/b;->a:I

    .line 33
    iget-object v0, p0, Lcom/cgollner/systemmonitor/c/b;->j:Ljava/util/List;

    iget v1, p0, Lcom/cgollner/systemmonitor/c/b;->a:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 34
    iget-object v0, p0, Lcom/cgollner/systemmonitor/c/b;->j:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->min(Ljava/util/Collection;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/cgollner/systemmonitor/c/b;->c:I

    .line 35
    iget-object v0, p0, Lcom/cgollner/systemmonitor/c/b;->j:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->max(Ljava/util/Collection;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/cgollner/systemmonitor/c/b;->b:I

    .line 36
    iget-object v2, p0, Lcom/cgollner/systemmonitor/c/b;->j:Ljava/util/List;

    const/4 v0, 0x0

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/2addr v0, v1

    move v1, v0

    goto :goto_0

    :cond_0
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    div-int v0, v1, v0

    iput v0, p0, Lcom/cgollner/systemmonitor/c/b;->d:I

    .line 37
    return-void
.end method

.method protected final b()V
    .locals 0

    .prologue
    .line 49
    return-void
.end method
