.class public final Lcom/cgollner/systemmonitor/a/h$b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cgollner/systemmonitor/a/h;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/cgollner/systemmonitor/a/h$b;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Ljava/io/File;

.field public b:J

.field public c:F


# direct methods
.method public constructor <init>(Ljava/io/File;J)V
    .locals 0

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    iput-object p1, p0, Lcom/cgollner/systemmonitor/a/h$b;->a:Ljava/io/File;

    iput-wide p2, p0, Lcom/cgollner/systemmonitor/a/h$b;->b:J

    .line 58
    return-void
.end method


# virtual methods
.method public final bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 4

    .prologue
    .line 51
    check-cast p1, Lcom/cgollner/systemmonitor/a/h$b;

    iget-wide v0, p0, Lcom/cgollner/systemmonitor/a/h$b;->b:J

    iget-wide v2, p1, Lcom/cgollner/systemmonitor/a/h$b;->b:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    iget-wide v0, p0, Lcom/cgollner/systemmonitor/a/h$b;->b:J

    iget-wide v2, p1, Lcom/cgollner/systemmonitor/a/h$b;->b:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    const/4 v0, -0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
