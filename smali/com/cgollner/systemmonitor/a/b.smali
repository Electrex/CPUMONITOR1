.class public final Lcom/cgollner/systemmonitor/a/b;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "DefaultLocale"
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/cgollner/systemmonitor/a/b$a;
    }
.end annotation


# static fields
.field static final a:Ljava/io/File;

.field public static b:Ljava/lang/String;

.field static c:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field static d:Ljava/io/File;

.field private static final e:Ljava/io/File;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 31
    new-instance v0, Ljava/io/File;

    const-string v1, "/proc/stat"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/cgollner/systemmonitor/a/b;->a:Ljava/io/File;

    .line 291
    new-instance v0, Ljava/io/File;

    const-string v1, "/proc"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/cgollner/systemmonitor/a/b;->e:Ljava/io/File;

    .line 409
    const-string v0, "/sys/devices/system/cpu/cpu0/cpufreq/stats/time_in_state"

    sput-object v0, Lcom/cgollner/systemmonitor/a/b;->b:Ljava/lang/String;

    .line 455
    sput-object v2, Lcom/cgollner/systemmonitor/a/b;->c:Ljava/util/HashMap;

    .line 457
    sput-object v2, Lcom/cgollner/systemmonitor/a/b;->d:Ljava/io/File;

    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)F
    .locals 14

    .prologue
    const-wide/16 v10, 0x0

    .line 94
    if-eqz p0, :cond_0

    if-nez p1, :cond_2

    .line 95
    :cond_0
    const/high16 v0, -0x40800000    # -1.0f

    .line 112
    :cond_1
    :goto_0
    return v0

    .line 96
    :cond_2
    const-string v0, " "

    invoke-virtual {p0, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 97
    invoke-static {v0}, Lcom/cgollner/systemmonitor/a/b;->c([Ljava/lang/String;)J

    move-result-wide v2

    .line 98
    invoke-static {v0}, Lcom/cgollner/systemmonitor/a/b;->b([Ljava/lang/String;)J

    move-result-wide v4

    .line 100
    const-string v0, " "

    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 101
    invoke-static {v0}, Lcom/cgollner/systemmonitor/a/b;->c([Ljava/lang/String;)J

    move-result-wide v6

    .line 102
    invoke-static {v0}, Lcom/cgollner/systemmonitor/a/b;->b([Ljava/lang/String;)J

    move-result-wide v8

    .line 104
    const/4 v0, 0x0

    .line 105
    cmp-long v1, v2, v10

    if-ltz v1, :cond_1

    cmp-long v1, v4, v10

    if-ltz v1, :cond_1

    cmp-long v1, v6, v10

    if-ltz v1, :cond_1

    cmp-long v1, v8, v10

    if-ltz v1, :cond_1

    .line 106
    add-long v10, v8, v6

    add-long v12, v4, v2

    cmp-long v1, v10, v12

    if-lez v1, :cond_1

    cmp-long v1, v8, v4

    if-ltz v1, :cond_1

    .line 107
    sub-long v0, v8, v4

    long-to-float v0, v0

    add-long/2addr v6, v8

    add-long/2addr v2, v4

    sub-long v2, v6, v2

    long-to-float v1, v2

    div-float/2addr v0, v1

    .line 108
    const/high16 v1, 0x42c80000    # 100.0f

    mul-float/2addr v0, v1

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;J)F
    .locals 10

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 186
    :try_start_0
    const-string v2, " "

    invoke-virtual {p0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 187
    invoke-static {v2}, Lcom/cgollner/systemmonitor/a/b;->d([Ljava/lang/String;)J

    move-result-wide v2

    .line 189
    const-string v4, " "

    invoke-virtual {p1, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 190
    invoke-static {v4}, Lcom/cgollner/systemmonitor/a/b;->d([Ljava/lang/String;)J

    move-result-wide v4

    .line 193
    const-wide/16 v6, 0x0

    cmp-long v6, v2, v6

    if-ltz v6, :cond_3

    cmp-long v6, v4, v2

    if-ltz v6, :cond_3

    long-to-double v6, p2

    const-wide/16 v8, 0x0

    cmpl-double v6, v6, v8

    if-lez v6, :cond_3

    .line 194
    const/high16 v6, 0x42c80000    # 100.0f

    sub-long v2, v4, v2

    long-to-float v2, v2

    mul-float/2addr v2, v6

    long-to-float v3, p2

    div-float/2addr v2, v3

    .line 197
    :goto_0
    const/4 v3, 0x0

    invoke-static {v3, v2}, Ljava/lang/Math;->max(FF)F

    move-result v4

    invoke-static {}, Lcom/cgollner/systemmonitor/a/b;->b()I

    move-result v3

    new-array v5, v3, [I

    new-array v6, v3, [I

    move v2, v1

    :goto_1
    if-ge v2, v3, :cond_0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-static {v8}, Lcom/cgollner/systemmonitor/a/b;->f(I)I

    move-result v8

    aput v8, v5, v7

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-static {v8}, Lcom/cgollner/systemmonitor/a/b;->e(I)I

    move-result v8

    aput v8, v6, v7

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_0
    array-length v7, v5

    move v2, v1

    move v3, v1

    :goto_2
    if-ge v2, v7, :cond_1

    aget v8, v5, v2

    add-int/2addr v3, v8

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_1
    array-length v5, v6

    move v2, v1

    :goto_3
    if-ge v1, v5, :cond_2

    aget v7, v6, v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    add-int/2addr v2, v7

    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :cond_2
    int-to-float v0, v3

    int-to-float v1, v2

    div-float/2addr v0, v1

    mul-float/2addr v0, v4

    .line 199
    :goto_4
    return v0

    :catch_0
    move-exception v1

    goto :goto_4

    :cond_3
    move v2, v0

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;JF)F
    .locals 2

    .prologue
    .line 325
    const/high16 v0, 0x42c80000    # 100.0f

    div-float v0, p4, v0

    invoke-static {p0, p1, p2, p3}, Lcom/cgollner/systemmonitor/a/b;->a(Ljava/lang/String;Ljava/lang/String;J)F

    move-result v1

    mul-float/2addr v0, v1

    return v0
.end method

.method public static a()Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 69
    :try_start_0
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v2, Ljava/io/FileReader;

    new-instance v3, Ljava/io/File;

    const-string v4, "/proc/stat"

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-direct {v2, v3}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V

    invoke-direct {v1, v2}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 70
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v2

    .line 71
    :goto_0
    if-eqz v2, :cond_2

    .line 72
    const-string v3, "cpu "

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    if-eqz v3, :cond_1

    .line 86
    :goto_1
    :try_start_1
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 90
    :goto_2
    if-eqz v2, :cond_0

    const/4 v0, 0x5

    invoke-virtual {v2, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    :cond_0
    return-object v0

    .line 79
    :cond_1
    :try_start_2
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-result-object v2

    goto :goto_0

    .line 87
    :catch_0
    move-exception v1

    move-object v2, v0

    :goto_3
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    :catch_1
    move-exception v1

    goto :goto_3

    :cond_2
    move-object v2, v0

    goto :goto_1
.end method

.method public static a(I)Ljava/lang/String;
    .locals 5

    .prologue
    .line 143
    const/4 v1, 0x0

    .line 147
    :try_start_0
    new-instance v2, Ljava/io/RandomAccessFile;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "/proc/"

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "/stat"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v3, "r"

    invoke-direct {v2, v0, v3}, Ljava/io/RandomAccessFile;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->readLine()Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 149
    :try_start_1
    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 154
    :goto_0
    return-object v0

    .line 150
    :catch_0
    move-exception v0

    move-object v4, v0

    move-object v0, v1

    move-object v1, v4

    :goto_1
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception v1

    goto :goto_1
.end method

.method public static a(Landroid/content/Context;)V
    .locals 14

    .prologue
    const-wide/16 v12, 0x0

    const/4 v10, 0x0

    .line 329
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    new-instance v1, Ljava/io/BufferedReader;

    new-instance v2, Ljava/io/FileReader;

    sget-object v3, Lcom/cgollner/systemmonitor/a/b;->b:Ljava/lang/String;

    invoke-direct {v2, v3}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v2}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    sub-long/2addr v2, v4

    invoke-static {v12, v13, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    new-instance v4, Lcom/cgollner/systemmonitor/a/b$a;

    invoke-direct {v4, v10, v2, v3}, Lcom/cgollner/systemmonitor/a/b$a;-><init>(IJ)V

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_0
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/cgollner/systemmonitor/a/b$a;

    aget-object v4, v2, v10

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    const/4 v5, 0x1

    aget-object v2, v2, v5

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    const-wide/16 v8, 0xa

    mul-long/2addr v6, v8

    invoke-static {v12, v13, v6, v7}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v6

    invoke-direct {v3, v4, v6, v7}, Lcom/cgollner/systemmonitor/a/b$a;-><init>(IJ)V

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V

    .line 330
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 331
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/a/b$a;

    .line 332
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget v4, v0, Lcom/cgollner/systemmonitor/a/b$a;->a:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iget-wide v4, v0, Lcom/cgollner/systemmonitor/a/b$a;->b:J

    invoke-interface {v1, v3, v4, v5}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    goto :goto_1

    .line 334
    :cond_1
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 335
    return-void
.end method

.method public static a([Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 35
    .line 36
    :try_start_0
    new-instance v4, Ljava/io/BufferedReader;

    new-instance v0, Ljava/io/FileReader;

    sget-object v1, Lcom/cgollner/systemmonitor/a/b;->a:Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V

    invoke-direct {v4, v0}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 40
    invoke-virtual {v4}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v0

    move-object v3, v0

    move v0, v2

    .line 41
    :goto_0
    if-eqz v3, :cond_2

    .line 42
    const-string v1, "cpu"

    invoke-virtual {v3, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 43
    const/4 v1, 0x3

    invoke-virtual {v3, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-static {v1}, Ljava/lang/Character;->isDigit(C)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 45
    const/4 v1, 0x3

    invoke-virtual {v3, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-static {v1}, Ljava/lang/Character;->getNumericValue(C)I

    move-result v1

    .line 46
    add-int/lit8 v1, v1, 0x1

    .line 50
    :goto_1
    const/4 v5, 0x5

    invoke-virtual {v3, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, p0, v1

    .line 51
    add-int/lit8 v0, v0, 0x1

    .line 53
    array-length v1, p0

    if-ge v0, v1, :cond_2

    .line 54
    :cond_0
    invoke-virtual {v4}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v1

    move-object v3, v1

    goto :goto_0

    :cond_1
    move v1, v2

    .line 48
    goto :goto_1

    .line 58
    :cond_2
    invoke-virtual {v4}, Ljava/io/BufferedReader;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 62
    :goto_2
    return-void

    .line 59
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2
.end method

.method public static b()I
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v1, 0x0

    .line 280
    new-instance v0, Ljava/io/File;

    const-string v2, "/sys/devices/system/cpu"

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v4

    .line 282
    array-length v5, v4

    move v3, v1

    move v2, v1

    :goto_0
    if-ge v3, v5, :cond_2

    aget-object v6, v4, v3

    .line 283
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v0

    if-lt v0, v8, :cond_0

    const/4 v0, 0x3

    invoke-virtual {v6, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 284
    :goto_1
    const/16 v7, 0x30

    if-lt v0, v7, :cond_1

    const/16 v7, 0x39

    if-gt v0, v7, :cond_1

    const/4 v0, 0x1

    .line 285
    :goto_2
    const-string v7, "cpu"

    invoke-virtual {v6, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_3

    if-eqz v0, :cond_3

    .line 286
    add-int/lit8 v0, v2, 0x1

    .line 282
    :goto_3
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v0

    goto :goto_0

    .line 283
    :cond_0
    const/16 v0, 0x6b

    goto :goto_1

    :cond_1
    move v0, v1

    .line 284
    goto :goto_2

    .line 288
    :cond_2
    invoke-static {v8, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    return v0

    :cond_3
    move v0, v2

    goto :goto_3
.end method

.method public static b([Ljava/lang/String;)J
    .locals 6

    .prologue
    .line 116
    const-wide/16 v2, 0x0

    .line 117
    const/4 v0, 0x0

    :goto_0
    array-length v1, p0

    if-ge v0, v1, :cond_1

    .line 118
    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    .line 120
    :try_start_0
    aget-object v1, p0, v0

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v4

    add-long/2addr v2, v4

    .line 117
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 123
    :catch_0
    move-exception v0

    const-wide/16 v2, -0x1

    .line 128
    :cond_1
    return-wide v2
.end method

.method public static b(I)Ljava/lang/CharSequence;
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v3, -0x1

    .line 212
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "/proc/"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/stat"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/cgollner/systemmonitor/a/c;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 213
    array-length v1, v0

    if-le v1, v2, :cond_2

    .line 214
    aget-object v2, v0, v2

    .line 215
    const/16 v0, 0x28

    invoke-virtual {v2, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    add-int/lit8 v1, v0, 0x1

    .line 216
    const/16 v0, 0x29

    invoke-virtual {v2, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    .line 217
    if-ne v1, v3, :cond_0

    const/4 v1, 0x0

    .line 218
    :cond_0
    if-ne v0, v3, :cond_1

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v0

    .line 219
    :cond_1
    invoke-virtual {v2, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 223
    :goto_0
    return-object v0

    :cond_2
    const-string v0, ""

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;)Ljava/util/List;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/cgollner/systemmonitor/a/b$a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 354
    invoke-static/range {p0 .. p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v4

    .line 356
    new-instance v5, Ljava/util/LinkedList;

    invoke-direct {v5}, Ljava/util/LinkedList;-><init>()V

    .line 358
    new-instance v6, Ljava/io/BufferedReader;

    new-instance v0, Ljava/io/FileReader;

    sget-object v1, Lcom/cgollner/systemmonitor/a/b;->b:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v6, v0}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 360
    const-string v0, "0"

    const-wide/16 v2, 0x0

    invoke-interface {v4, v0, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    .line 361
    const-wide/16 v2, 0x0

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v8

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v10

    sub-long/2addr v8, v10

    sub-long v0, v8, v0

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    .line 362
    new-instance v2, Lcom/cgollner/systemmonitor/a/b$a;

    const/4 v3, 0x0

    invoke-direct {v2, v3, v0, v1}, Lcom/cgollner/systemmonitor/a/b$a;-><init>(IJ)V

    invoke-interface {v5, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-wide v2, v0

    .line 366
    :goto_0
    invoke-virtual {v6}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 367
    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 368
    const/4 v1, 0x0

    aget-object v1, v0, v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 369
    const/4 v7, 0x1

    aget-object v0, v0, v7

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    int-to-long v8, v0

    .line 370
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    const-wide/16 v10, 0x0

    invoke-interface {v4, v0, v10, v11}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v10

    .line 371
    const-wide/16 v12, 0x0

    const-wide/16 v14, 0xa

    mul-long/2addr v8, v14

    sub-long/2addr v8, v10

    invoke-static {v12, v13, v8, v9}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v8

    .line 372
    new-instance v0, Lcom/cgollner/systemmonitor/a/b$a;

    invoke-direct {v0, v1, v8, v9}, Lcom/cgollner/systemmonitor/a/b$a;-><init>(IJ)V

    .line 373
    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 374
    iget-wide v0, v0, Lcom/cgollner/systemmonitor/a/b$a;->b:J

    add-long/2addr v0, v2

    move-wide v2, v0

    .line 375
    goto :goto_0

    .line 378
    :cond_0
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 379
    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 380
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/a/b$a;

    .line 381
    iget-wide v8, v0, Lcom/cgollner/systemmonitor/a/b$a;->b:J

    long-to-double v8, v8

    long-to-double v10, v2

    div-double/2addr v8, v10

    const-wide/high16 v10, 0x4059000000000000L    # 100.0

    mul-double/2addr v8, v10

    double-to-float v4, v8

    iput v4, v0, Lcom/cgollner/systemmonitor/a/b$a;->c:F

    .line 382
    iget v0, v0, Lcom/cgollner/systemmonitor/a/b$a;->c:F

    const/high16 v4, 0x3f800000    # 1.0f

    cmpg-float v0, v0, v4

    if-gez v0, :cond_1

    .line 383
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_1

    .line 386
    :cond_2
    const-wide/16 v0, 0x0

    .line 387
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move-wide v2, v0

    .line 388
    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 389
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/a/b$a;

    iget-wide v0, v0, Lcom/cgollner/systemmonitor/a/b$a;->b:J

    add-long/2addr v0, v2

    move-wide v2, v0

    goto :goto_2

    .line 393
    :cond_3
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 394
    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 395
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/a/b$a;

    .line 396
    iget-wide v8, v0, Lcom/cgollner/systemmonitor/a/b$a;->b:J

    long-to-double v8, v8

    long-to-double v10, v2

    div-double/2addr v8, v10

    const-wide/high16 v10, 0x4059000000000000L    # 100.0

    mul-double/2addr v8, v10

    double-to-float v4, v8

    iput v4, v0, Lcom/cgollner/systemmonitor/a/b$a;->c:F

    goto :goto_3

    .line 399
    :cond_4
    invoke-static {v5}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 400
    invoke-virtual {v6}, Ljava/io/BufferedReader;->close()V

    .line 401
    return-object v5
.end method

.method private static c([Ljava/lang/String;)J
    .locals 2

    .prologue
    .line 133
    const/4 v0, 0x3

    :try_start_0
    aget-object v0, p0, v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    .line 138
    :goto_0
    return-wide v0

    .line 134
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/NumberFormatException;->printStackTrace()V

    .line 138
    const-wide/16 v0, -0x1

    goto :goto_0
.end method

.method public static c()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 294
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 295
    sget-object v0, Lcom/cgollner/systemmonitor/a/b;->e:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 297
    :try_start_0
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    .line 298
    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 295
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 302
    :cond_0
    return-object v1

    :catch_0
    move-exception v4

    goto :goto_1
.end method

.method public static c(I)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 257
    const-string v2, "sys/devices/system/cpu/cpu%d/online"

    new-array v3, v0, [Ljava/lang/Object;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v1

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/cgollner/systemmonitor/a/c;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public static d(I)I
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 261
    const-string v0, "/sys/devices/system/cpu/cpu%d/cpufreq/scaling_min_freq"

    new-array v1, v4, [Ljava/lang/Object;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/cgollner/systemmonitor/a/c;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 262
    if-lez v0, :cond_0

    .line 264
    :goto_0
    return v0

    :cond_0
    const-string v0, "/sys/devices/system/cpu/cpu%d/cpufreq/cpuinfo_min_freq"

    new-array v1, v4, [Ljava/lang/Object;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/cgollner/systemmonitor/a/c;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method private static d([Ljava/lang/String;)J
    .locals 4

    .prologue
    .line 204
    const/16 v0, 0xd

    aget-object v0, p0, v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    const/16 v2, 0xe

    aget-object v2, p0, v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    add-long/2addr v0, v2

    return-wide v0
.end method

.method public static d()Ljava/util/Set;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 306
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 307
    sget-object v0, Lcom/cgollner/systemmonitor/a/b;->e:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 309
    :try_start_0
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    .line 310
    invoke-interface {v1, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 307
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 314
    :cond_0
    return-object v1

    :catch_0
    move-exception v4

    goto :goto_1
.end method

.method public static e()I
    .locals 4

    .prologue
    const/4 v0, -0x1

    .line 459
    invoke-static {}, Lcom/cgollner/systemmonitor/a/b;->f()Ljava/io/File;

    move-result-object v1

    .line 460
    sput-object v1, Lcom/cgollner/systemmonitor/a/b;->d:Ljava/io/File;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/cgollner/systemmonitor/a/b;->d:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_1

    .line 468
    :cond_0
    :goto_0
    return v0

    .line 463
    :cond_1
    :try_start_0
    sget-object v1, Lcom/cgollner/systemmonitor/a/b;->d:Ljava/io/File;

    invoke-static {v1}, Lorg/apache/commons/io/FileUtils;->readFileToString(Ljava/io/File;)Ljava/lang/String;

    move-result-object v1

    .line 464
    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x2

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_0

    .line 465
    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static e(I)I
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 268
    const-string v0, "/sys/devices/system/cpu/cpu%d/cpufreq/scaling_max_freq"

    new-array v1, v4, [Ljava/lang/Object;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/cgollner/systemmonitor/a/c;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 269
    if-lez v0, :cond_0

    .line 272
    :goto_0
    return v0

    :cond_0
    const-string v0, "/sys/devices/system/cpu/cpu%d/cpufreq/cpuinfo_max_freq"

    new-array v1, v4, [Ljava/lang/Object;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/cgollner/systemmonitor/a/c;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public static f(I)I
    .locals 4

    .prologue
    .line 276
    const-string v0, "/sys/devices/system/cpu/cpu%d/cpufreq/scaling_cur_freq"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/cgollner/systemmonitor/a/c;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public static f()Ljava/io/File;
    .locals 4

    .prologue
    .line 472
    sget-object v0, Lcom/cgollner/systemmonitor/a/b;->d:Ljava/io/File;

    if-eqz v0, :cond_0

    .line 473
    sget-object v0, Lcom/cgollner/systemmonitor/a/b;->d:Ljava/io/File;

    .line 478
    :goto_0
    return-object v0

    .line 474
    :cond_0
    sget-object v0, Lcom/cgollner/systemmonitor/a/b;->c:Ljava/util/HashMap;

    if-nez v0, :cond_1

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/cgollner/systemmonitor/a/b;->c:Ljava/util/HashMap;

    const-string v1, "tuna"

    const-string v2, "/sys/devices/platform/omap/omap_temp_sensor.0/temperature"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/cgollner/systemmonitor/a/b;->c:Ljava/util/HashMap;

    const-string v1, "maguro"

    const-string v2, "/sys/devices/platform/omap/omap_temp_sensor.0/temperature"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/cgollner/systemmonitor/a/b;->c:Ljava/util/HashMap;

    const-string v1, "toro"

    const-string v2, "/sys/devices/platform/omap/omap_temp_sensor.0/temperature"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/cgollner/systemmonitor/a/b;->c:Ljava/util/HashMap;

    const-string v1, "toroplus"

    const-string v2, "/sys/devices/platform/omap/omap_temp_sensor.0/temperature"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/cgollner/systemmonitor/a/b;->c:Ljava/util/HashMap;

    const-string v1, "mako"

    const-string v2, "/sys/devices/virtual/thermal/thermal_zone7/temp"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/cgollner/systemmonitor/a/b;->c:Ljava/util/HashMap;

    const-string v1, "grouper"

    const-string v2, "/sys/devices/platform/tegra-i2c.4/i2c-4/4-004c/temperature"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/cgollner/systemmonitor/a/b;->c:Ljava/util/HashMap;

    const-string v1, "flo"

    const-string v2, "/sys/devices/virtual/thermal/thermal_zone0/temp"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/cgollner/systemmonitor/a/b;->c:Ljava/util/HashMap;

    const-string v1, "hammerhead"

    const-string v2, "/sys/devices/virtual/thermal/thermal_zone7/temp"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/cgollner/systemmonitor/a/b;->c:Ljava/util/HashMap;

    const-string v1, "m7"

    const-string v2, "/sys/devices/virtual/thermal/thermal_zone1/temp"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 476
    :cond_1
    :try_start_0
    new-instance v1, Ljava/io/File;

    sget-object v0, Lcom/cgollner/systemmonitor/a/b;->c:Ljava/util/HashMap;

    sget-object v2, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v2, v3}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    goto :goto_0

    .line 478
    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method
