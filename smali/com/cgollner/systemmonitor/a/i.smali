.class public final Lcom/cgollner/systemmonitor/a/i;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "DefaultLocale"
    }
.end annotation


# static fields
.field static a:[I

.field private static final b:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 158
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "%.0f Bytes"

    aput-object v1, v0, v2

    const-string v1, "%.0f KB"

    aput-object v1, v0, v3

    const-string v1, "%.1f MB"

    aput-object v1, v0, v4

    const-string v1, "%.1f GB"

    aput-object v1, v0, v5

    const-string v1, "%.3f TB"

    aput-object v1, v0, v6

    sput-object v0, Lcom/cgollner/systemmonitor/a/i;->b:[Ljava/lang/String;

    .line 269
    const/4 v0, 0x7

    new-array v0, v0, [I

    sget v1, Lcom/cgollner/systemmonitor/b/a$h;->sunday:I

    aput v1, v0, v2

    sget v1, Lcom/cgollner/systemmonitor/b/a$h;->monday:I

    aput v1, v0, v3

    sget v1, Lcom/cgollner/systemmonitor/b/a$h;->tuesday:I

    aput v1, v0, v4

    sget v1, Lcom/cgollner/systemmonitor/b/a$h;->wednesday:I

    aput v1, v0, v5

    sget v1, Lcom/cgollner/systemmonitor/b/a$h;->thursday:I

    aput v1, v0, v6

    const/4 v1, 0x5

    sget v2, Lcom/cgollner/systemmonitor/b/a$h;->friday:I

    aput v2, v0, v1

    const/4 v1, 0x6

    sget v2, Lcom/cgollner/systemmonitor/b/a$h;->saturday:I

    aput v2, v0, v1

    sput-object v0, Lcom/cgollner/systemmonitor/a/i;->a:[I

    return-void
.end method

.method public static a(ILandroid/content/Context;)Ljava/lang/CharSequence;
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/high16 v4, 0x41200000    # 10.0f

    .line 195
    invoke-static {p1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 196
    sget-object v0, Lcom/cgollner/systemmonitor/App;->a:Landroid/content/Context;

    sget v2, Lcom/cgollner/systemmonitor/b/a$h;->temperature_units_key:I

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    invoke-interface {v1, v0, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 197
    if-nez v0, :cond_0

    .line 198
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 199
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    if-ne v0, v2, :cond_1

    const-string v0, "1"

    .line 200
    :goto_0
    sget-object v2, Lcom/cgollner/systemmonitor/App;->a:Landroid/content/Context;

    sget v3, Lcom/cgollner/systemmonitor/b/a$h;->temperature_units_key:I

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 201
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 203
    :cond_0
    const-string v1, "1"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 204
    int-to-float v0, p0

    div-float/2addr v0, v4

    const v1, 0x3fe66666    # 1.8f

    mul-float/2addr v0, v1

    const/high16 v1, 0x42000000    # 32.0f

    add-float/2addr v0, v1

    .line 205
    const-string v1, "%.0f\u00baF"

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    aput-object v0, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 208
    :goto_1
    return-object v0

    .line 199
    :cond_1
    const-string v0, "0"

    goto :goto_0

    .line 208
    :cond_2
    const-string v0, "%.0f\u00baC"

    new-array v1, v6, [Ljava/lang/Object;

    int-to-float v2, p0

    div-float/2addr v2, v4

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public static a(D)Ljava/lang/String;
    .locals 6

    .prologue
    const-wide v4, 0x408f400000000000L    # 1000.0

    .line 105
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    cmpg-double v0, p0, v0

    if-gez v0, :cond_0

    .line 106
    const-string v0, "-"

    .line 115
    :goto_0
    return-object v0

    .line 108
    :cond_0
    div-double v0, p0, v4

    .line 109
    const-string v2, "%.0fMHz"

    .line 110
    cmpl-double v3, v0, v4

    if-ltz v3, :cond_1

    .line 112
    div-double/2addr v0, v4

    .line 113
    const-string v2, "%.1fGHz"

    .line 115
    :cond_1
    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(DJ)Ljava/lang/String;
    .locals 8

    .prologue
    const-wide/high16 v6, 0x4090000000000000L    # 1024.0

    const-wide v4, 0x408f400000000000L    # 1000.0

    .line 35
    long-to-double v0, p2

    div-double v0, v4, v0

    .line 36
    mul-double v2, p0, v0

    .line 37
    const-string v0, "%.0fB/s"

    .line 39
    cmpl-double v1, v2, v4

    if-ltz v1, :cond_0

    .line 40
    div-double/2addr v2, v6

    .line 41
    const-string v0, "%.0fKB/s"

    .line 44
    :cond_0
    cmpl-double v1, v2, v4

    if-ltz v1, :cond_1

    .line 45
    div-double/2addr v2, v6

    .line 46
    const-string v0, "%.1fMB/s"

    .line 49
    :cond_1
    cmpl-double v1, v2, v4

    if-ltz v1, :cond_2

    .line 50
    div-double/2addr v2, v6

    .line 51
    const-string v0, "%.1GB/s"

    .line 53
    :cond_2
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(F)Ljava/lang/String;
    .locals 4

    .prologue
    .line 25
    const v0, -0x42333333    # -0.1f

    cmpg-float v0, p0, v0

    if-gez v0, :cond_0

    const-string v0, "0%"

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "%.0f%%"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(I)Ljava/lang/String;
    .locals 8

    .prologue
    const-wide v6, 0x408f400000000000L    # 1000.0

    .line 101
    const-string v0, "%.0fMHz"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    int-to-double v4, p0

    div-double/2addr v4, v6

    div-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(J)Ljava/lang/String;
    .locals 8

    .prologue
    const-wide/high16 v6, 0x4090000000000000L    # 1024.0

    const-wide v4, 0x408f400000000000L    # 1000.0

    .line 138
    long-to-double v0, p0

    .line 139
    const-string v2, "%.0fB"

    .line 141
    cmpl-double v3, v0, v4

    if-ltz v3, :cond_0

    .line 142
    div-double/2addr v0, v6

    .line 143
    const-string v2, "%.0fKB"

    .line 146
    :cond_0
    cmpl-double v3, v0, v4

    if-ltz v3, :cond_1

    .line 147
    div-double/2addr v0, v6

    .line 148
    const-string v2, "%.1fMB"

    .line 151
    :cond_1
    cmpl-double v3, v0, v4

    if-ltz v3, :cond_2

    .line 152
    div-double/2addr v0, v6

    .line 153
    const-string v2, "%.1fGB"

    .line 155
    :cond_2
    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(JLandroid/content/Context;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 241
    invoke-static {p2}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v0

    .line 242
    new-instance v1, Ljava/util/Date;

    invoke-direct {v1, p0, p1}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v0, v1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/util/Date;)Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v2, 0x6

    .line 284
    invoke-static {}, Ljava/util/GregorianCalendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 285
    invoke-static {}, Ljava/util/GregorianCalendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    .line 286
    invoke-virtual {v1, p0}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 288
    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v0

    invoke-virtual {v1, v2}, Ljava/util/Calendar;->get(I)I

    move-result v2

    sub-int/2addr v0, v2

    .line 289
    if-nez v0, :cond_0

    .line 291
    sget v0, Lcom/cgollner/systemmonitor/b/a$h;->today:I

    .line 298
    :goto_0
    sget-object v1, Lcom/cgollner/systemmonitor/App;->a:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 292
    :cond_0
    const/4 v2, 0x1

    if-ne v0, v2, :cond_1

    .line 293
    sget v0, Lcom/cgollner/systemmonitor/b/a$h;->yesterday:I

    goto :goto_0

    .line 294
    :cond_1
    const/4 v2, -0x1

    if-ne v0, v2, :cond_2

    .line 295
    sget v0, Lcom/cgollner/systemmonitor/b/a$h;->tomorrow:I

    goto :goto_0

    .line 297
    :cond_2
    sget-object v0, Lcom/cgollner/systemmonitor/a/i;->a:[I

    const/4 v2, 0x7

    invoke-virtual {v1, v2}, Ljava/util/Calendar;->get(I)I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    aget v0, v0, v1

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;)Z
    .locals 9

    .prologue
    const/4 v0, 0x0

    .line 58
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x40

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    iget-object v3, v1, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    .line 59
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/cgollner/systemmonitor/b/a$a;->chaves:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v4

    .line 60
    array-length v5, v3

    move v2, v0

    :goto_0
    if-ge v2, v5, :cond_2

    aget-object v1, v3, v2

    .line 62
    invoke-virtual {v1}, Landroid/content/pm/Signature;->toCharsString()Ljava/lang/String;

    move-result-object v6

    .line 63
    array-length v7, v4

    move v1, v0

    :goto_1
    if-ge v1, v7, :cond_1

    aget-object v8, v4, v1

    .line 65
    invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v8

    if-eqz v8, :cond_0

    .line 73
    :goto_2
    return v0

    .line 64
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 60
    :cond_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 70
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    .line 73
    :cond_2
    const/4 v0, 0x1

    goto :goto_2
.end method

.method public static b(I)Ljava/lang/CharSequence;
    .locals 8

    .prologue
    .line 119
    const-string v0, "%.0fMHz"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    int-to-double v4, p0

    const-wide v6, 0x408f400000000000L    # 1000.0

    div-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static b(D)Ljava/lang/String;
    .locals 4

    .prologue
    .line 122
    .line 123
    const-string v0, "%.0fMB"

    .line 125
    const-wide v2, 0x408f400000000000L    # 1000.0

    cmpl-double v1, p0, v2

    if-ltz v1, :cond_0

    .line 126
    const-wide/high16 v0, 0x4090000000000000L    # 1024.0

    div-double/2addr p0, v0

    .line 127
    const-string v0, "%.1fGB"

    .line 129
    :cond_0
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p0, p1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static b(F)Ljava/lang/String;
    .locals 4

    .prologue
    .line 29
    const v0, -0x42333333    # -0.1f

    cmpg-float v0, p0, v0

    if-gtz v0, :cond_0

    .line 30
    const-string v0, "Offline"

    .line 32
    :goto_0
    return-object v0

    :cond_0
    const-string v0, "%.0f%%"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static b(J)Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v3, 0x0

    const-wide/high16 v6, 0x4090000000000000L    # 1024.0

    .line 172
    long-to-double v0, p0

    move v2, v3

    :goto_0
    cmpg-double v4, v0, v6

    if-ltz v4, :cond_0

    sget-object v4, Lcom/cgollner/systemmonitor/a/i;->b:[Ljava/lang/String;

    array-length v4, v4

    add-int/lit8 v4, v4, -0x1

    if-ne v2, v4, :cond_1

    :cond_0
    sget-object v4, Lcom/cgollner/systemmonitor/a/i;->b:[Ljava/lang/String;

    aget-object v2, v4, v2

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    aput-object v0, v4, v3

    invoke-static {v2, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_1
    add-int/lit8 v2, v2, 0x1

    div-double/2addr v0, v6

    goto :goto_0
.end method

.method public static c(F)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 183
    const/high16 v0, 0x3f800000    # 1.0f

    cmpg-float v0, p0, v0

    if-gez v0, :cond_0

    .line 184
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    const-string v1, "%.2f%%"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 190
    :goto_0
    return-object v0

    .line 186
    :cond_0
    const/high16 v0, 0x41200000    # 10.0f

    cmpg-float v0, p0, v0

    if-gez v0, :cond_1

    .line 187
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    const-string v1, "%.1f%%"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 190
    :cond_1
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    const-string v1, "%.0f%%"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static c(J)Ljava/lang/String;
    .locals 2

    .prologue
    .line 176
    const-wide/32 v0, 0x100000

    div-long v0, p0, v0

    long-to-double v0, v0

    invoke-static {v0, v1}, Lcom/cgollner/systemmonitor/a/i;->b(D)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static d(J)Ljava/lang/String;
    .locals 16

    .prologue
    .line 213
    const-wide/16 v0, 0x3e8

    div-long v0, p0, v0

    .line 214
    const-wide/16 v2, 0x3c

    div-long v2, v0, v2

    const-wide/16 v4, 0x3c

    rem-long/2addr v2, v4

    .line 215
    const-wide/16 v4, 0xe10

    div-long v4, v0, v4

    const-wide/16 v6, 0x18

    rem-long/2addr v4, v6

    .line 216
    const-wide/16 v6, 0xe10

    div-long v6, v0, v6

    const-wide/16 v8, 0x18

    div-long/2addr v6, v8

    .line 217
    const-wide/16 v8, 0x3c

    rem-long v8, v0, v8

    .line 219
    sget-object v1, Lcom/cgollner/systemmonitor/App;->a:Landroid/content/Context;

    const-wide/16 v10, 0x1

    cmp-long v0, v6, v10

    if-nez v0, :cond_0

    sget v0, Lcom/cgollner/systemmonitor/b/a$h;->day:I

    :goto_0
    invoke-virtual {v1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 220
    sget-object v10, Lcom/cgollner/systemmonitor/App;->a:Landroid/content/Context;

    const-wide/16 v12, 0x1

    cmp-long v0, v4, v12

    if-nez v0, :cond_1

    sget v0, Lcom/cgollner/systemmonitor/b/a$h;->hour:I

    :goto_1
    invoke-virtual {v10, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 221
    sget-object v11, Lcom/cgollner/systemmonitor/App;->a:Landroid/content/Context;

    const-wide/16 v12, 0x1

    cmp-long v0, v2, v12

    if-nez v0, :cond_2

    sget v0, Lcom/cgollner/systemmonitor/b/a$h;->minute:I

    :goto_2
    invoke-virtual {v11, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 222
    sget-object v12, Lcom/cgollner/systemmonitor/App;->a:Landroid/content/Context;

    const-wide/16 v14, 0x1

    cmp-long v0, v8, v14

    if-nez v0, :cond_3

    sget v0, Lcom/cgollner/systemmonitor/b/a$h;->second:I

    :goto_3
    invoke-virtual {v12, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 224
    const-wide/16 v12, 0x1

    cmp-long v12, v6, v12

    if-ltz v12, :cond_4

    .line 225
    const-string v0, "%d %s %d %s"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v2, v3

    const/4 v3, 0x1

    aput-object v1, v2, v3

    const/4 v1, 0x2

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, v1

    const/4 v1, 0x3

    aput-object v10, v2, v1

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 237
    :goto_4
    return-object v0

    .line 219
    :cond_0
    sget v0, Lcom/cgollner/systemmonitor/b/a$h;->days:I

    goto :goto_0

    .line 220
    :cond_1
    sget v0, Lcom/cgollner/systemmonitor/b/a$h;->hours:I

    goto :goto_1

    .line 221
    :cond_2
    sget v0, Lcom/cgollner/systemmonitor/b/a$h;->minutes:I

    goto :goto_2

    .line 222
    :cond_3
    sget v0, Lcom/cgollner/systemmonitor/b/a$h;->seconds:I

    goto :goto_3

    .line 227
    :cond_4
    const-wide/16 v6, 0x1

    cmp-long v1, v4, v6

    if-ltz v1, :cond_5

    .line 228
    const-string v0, "%d %s %d %s"

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v1, v6

    const/4 v4, 0x1

    aput-object v10, v1, v4

    const/4 v4, 0x2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v4

    const/4 v2, 0x3

    aput-object v11, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_4

    .line 230
    :cond_5
    const-wide/16 v4, 0x1

    cmp-long v1, v2, v4

    if-lez v1, :cond_6

    .line 231
    const-string v0, "%d %s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v4

    const/4 v2, 0x1

    aput-object v11, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_4

    .line 233
    :cond_6
    const-wide/16 v4, 0x1

    cmp-long v1, v2, v4

    if-ltz v1, :cond_7

    .line 234
    const-string v1, "%d %s %d %s"

    const/4 v4, 0x4

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v4, v5

    const/4 v2, 0x1

    aput-object v11, v4, v2

    const/4 v2, 0x2

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v4, v2

    const/4 v2, 0x3

    aput-object v0, v4, v2

    invoke-static {v1, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_4

    .line 237
    :cond_7
    const-string v1, "%d %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_4
.end method

.method public static e(J)Ljava/lang/String;
    .locals 16

    .prologue
    .line 246
    const-wide/16 v0, 0x3e8

    div-long v0, p0, v0

    .line 247
    const-wide/16 v2, 0x3c

    div-long v2, v0, v2

    const-wide/16 v4, 0x3c

    rem-long/2addr v2, v4

    .line 248
    const-wide/16 v4, 0xe10

    div-long v4, v0, v4

    const-wide/16 v6, 0x18

    rem-long/2addr v4, v6

    .line 249
    const-wide/16 v6, 0xe10

    div-long v6, v0, v6

    const-wide/16 v8, 0x18

    div-long/2addr v6, v8

    .line 250
    const-wide/16 v8, 0x3c

    rem-long/2addr v0, v8

    .line 252
    const-string v8, "%02d%s%02d%s"

    .line 253
    const-string v9, "d"

    .line 254
    const-string v10, "h"

    .line 255
    const-string v11, "m"

    .line 256
    const-string v12, "s"

    .line 258
    const-wide/16 v14, 0x1

    cmp-long v13, v6, v14

    if-ltz v13, :cond_0

    .line 259
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    aput-object v9, v0, v1

    const/4 v1, 0x2

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    aput-object v10, v0, v1

    invoke-static {v8, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 265
    :goto_0
    return-object v0

    .line 261
    :cond_0
    const-wide/16 v6, 0x1

    cmp-long v6, v4, v6

    if-ltz v6, :cond_1

    .line 262
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v0, v1

    const/4 v1, 0x1

    aput-object v10, v0, v1

    const/4 v1, 0x2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    aput-object v11, v0, v1

    invoke-static {v8, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 265
    :cond_1
    const/4 v4, 0x4

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v4, v5

    const/4 v2, 0x1

    aput-object v11, v4, v2

    const/4 v2, 0x2

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v4, v2

    const/4 v0, 0x3

    aput-object v12, v4, v0

    invoke-static {v8, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static f(J)Ljava/lang/String;
    .locals 2

    .prologue
    .line 280
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0, p0, p1}, Ljava/util/Date;-><init>(J)V

    invoke-static {v0}, Lcom/cgollner/systemmonitor/a/i;->a(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
