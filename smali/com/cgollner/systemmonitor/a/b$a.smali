.class public final Lcom/cgollner/systemmonitor/a/b$a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cgollner/systemmonitor/a/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/cgollner/systemmonitor/a/b$a;",
        ">;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:J

.field public c:F

.field public d:Ljava/lang/String;


# direct methods
.method public constructor <init>(IJ)V
    .locals 2

    .prologue
    .line 419
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 420
    iput p1, p0, Lcom/cgollner/systemmonitor/a/b$a;->a:I

    .line 421
    iput-wide p2, p0, Lcom/cgollner/systemmonitor/a/b$a;->b:J

    .line 422
    if-nez p1, :cond_0

    const-string v0, "Deep sleep"

    :goto_0
    iput-object v0, p0, Lcom/cgollner/systemmonitor/a/b$a;->d:Ljava/lang/String;

    .line 423
    return-void

    .line 422
    :cond_0
    int-to-double v0, p1

    invoke-static {v0, v1}, Lcom/cgollner/systemmonitor/a/i;->a(D)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 4

    .prologue
    .line 412
    check-cast p1, Lcom/cgollner/systemmonitor/a/b$a;

    iget-wide v0, p0, Lcom/cgollner/systemmonitor/a/b$a;->b:J

    iget-wide v2, p1, Lcom/cgollner/systemmonitor/a/b$a;->b:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    iget-wide v0, p0, Lcom/cgollner/systemmonitor/a/b$a;->b:J

    iget-wide v2, p1, Lcom/cgollner/systemmonitor/a/b$a;->b:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    const/4 v0, -0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 427
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "[frequency="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/cgollner/systemmonitor/a/b$a;->a:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", time="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/cgollner/systemmonitor/a/b$a;->b:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", percentage="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/cgollner/systemmonitor/a/b$a;->c:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
