.class public final Lcom/cgollner/systemmonitor/a/a;
.super Landroid/support/v4/content/AsyncTaskLoader;
.source "SourceFile"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "SdCardPath"
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/cgollner/systemmonitor/a/a$b;,
        Lcom/cgollner/systemmonitor/a/a$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v4/content/AsyncTaskLoader",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/cgollner/systemmonitor/a/a$a;",
        ">;>;"
    }
.end annotation


# static fields
.field public static final a:Lcom/cgollner/systemmonitor/a/a$a;

.field private static final c:Ljava/lang/String;


# instance fields
.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/cgollner/systemmonitor/a/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/cgollner/systemmonitor/a/a$a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 24
    new-instance v0, Lcom/cgollner/systemmonitor/a/a$a;

    invoke-direct {v0}, Lcom/cgollner/systemmonitor/a/a$a;-><init>()V

    sput-object v0, Lcom/cgollner/systemmonitor/a/a;->a:Lcom/cgollner/systemmonitor/a/a$a;

    .line 96
    new-instance v0, Ljava/lang/String;

    const-string v1, "/data/data"

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/cgollner/systemmonitor/a/a;->c:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 101
    invoke-direct {p0, p1}, Landroid/support/v4/content/AsyncTaskLoader;-><init>(Landroid/content/Context;)V

    .line 102
    return-void
.end method

.method public static a(Lcom/cgollner/systemmonitor/a/a$a;Lcom/cgollner/systemmonitor/a/a$b;)V
    .locals 3

    .prologue
    .line 135
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    .line 136
    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/cgollner/systemmonitor/a/a$2;

    invoke-direct {v2, p0, p1, v0}, Lcom/cgollner/systemmonitor/a/a$2;-><init>(Lcom/cgollner/systemmonitor/a/a$a;Lcom/cgollner/systemmonitor/a/a$b;Landroid/os/Handler;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 153
    return-void
.end method

.method private a(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/cgollner/systemmonitor/a/a$a;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 30
    iput-object p1, p0, Lcom/cgollner/systemmonitor/a/a;->b:Ljava/util/List;

    .line 31
    invoke-super {p0, p1}, Landroid/support/v4/content/AsyncTaskLoader;->deliverResult(Ljava/lang/Object;)V

    .line 32
    return-void
.end method

.method public static a(Ljava/util/List;Lcom/cgollner/systemmonitor/a/a$b;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/cgollner/systemmonitor/a/a$a;",
            ">;",
            "Lcom/cgollner/systemmonitor/a/a$b;",
            ")V"
        }
    .end annotation

    .prologue
    .line 157
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    .line 158
    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/cgollner/systemmonitor/a/a$3;

    invoke-direct {v2, p0, p1, v0}, Lcom/cgollner/systemmonitor/a/a$3;-><init>(Ljava/util/List;Lcom/cgollner/systemmonitor/a/a$b;Landroid/os/Handler;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 177
    return-void
.end method


# virtual methods
.method public final synthetic deliverResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 22
    check-cast p1, Ljava/util/List;

    invoke-direct {p0, p1}, Lcom/cgollner/systemmonitor/a/a;->a(Ljava/util/List;)V

    return-void
.end method

.method public final synthetic loadInBackground()Ljava/lang/Object;
    .locals 8

    .prologue
    .line 22
    invoke-static {}, La/a/a/b$a;->a()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/cgollner/systemmonitor/a/a$a;

    const/4 v1, 0x0

    sget-object v2, Lcom/cgollner/systemmonitor/a/a;->a:Lcom/cgollner/systemmonitor/a/a$a;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/cgollner/systemmonitor/a/a;->c:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ls "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, La/a/a/b$a;->a(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    :cond_1
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    new-instance v2, Ljava/util/HashMap;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    mul-int/lit8 v3, v3, 0x2

    invoke-direct {v2, v3}, Ljava/util/HashMap;-><init>(I)V

    iput-object v2, p0, Lcom/cgollner/systemmonitor/a/a;->d:Ljava/util/HashMap;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "du -ks "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/Android/data/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/cache"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "du -ks /data/data/"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "/cache"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    invoke-static {v1}, La/a/a/b$a;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    new-instance v2, Lcom/cgollner/systemmonitor/a/a$a;

    invoke-direct {v2}, Lcom/cgollner/systemmonitor/a/a$a;-><init>()V

    const-string v3, "(.*/data/)(.*)(/cache)"

    const-string v4, "$2"

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/cgollner/systemmonitor/a/a$a;->c:Ljava/lang/String;

    const-wide/16 v4, 0x400

    const-string v3, "(\\d+)(.*)"

    const-string v6, "$1"

    invoke-virtual {v0, v3, v6}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    mul-long/2addr v4, v6

    iput-wide v4, v2, Lcom/cgollner/systemmonitor/a/a$a;->d:J

    iget-object v0, p0, Lcom/cgollner/systemmonitor/a/a;->d:Ljava/util/HashMap;

    iget-object v3, v2, Lcom/cgollner/systemmonitor/a/a$a;->c:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/a/a$a;

    if-eqz v0, :cond_3

    iget-wide v4, v0, Lcom/cgollner/systemmonitor/a/a$a;->d:J

    iget-wide v2, v2, Lcom/cgollner/systemmonitor/a/a$a;->d:J

    add-long/2addr v2, v4

    iput-wide v2, v0, Lcom/cgollner/systemmonitor/a/a$a;->d:J

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lcom/cgollner/systemmonitor/a/a;->d:Ljava/util/HashMap;

    iget-object v3, v2, Lcom/cgollner/systemmonitor/a/a$a;->c:Ljava/lang/String;

    invoke-virtual {v0, v3, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    :cond_4
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/cgollner/systemmonitor/a/a;->d:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    new-instance v1, Lcom/cgollner/systemmonitor/a/a$1;

    invoke-direct {v1, p0}, Lcom/cgollner/systemmonitor/a/a$1;-><init>(Lcom/cgollner/systemmonitor/a/a;)V

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    goto/16 :goto_0

    :cond_5
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    goto/16 :goto_0
.end method

.method protected final onStartLoading()V
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/cgollner/systemmonitor/a/a;->b:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 37
    iget-object v0, p0, Lcom/cgollner/systemmonitor/a/a;->b:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/cgollner/systemmonitor/a/a;->a(Ljava/util/List;)V

    .line 42
    :goto_0
    return-void

    .line 40
    :cond_0
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/a/a;->forceLoad()V

    goto :goto_0
.end method
