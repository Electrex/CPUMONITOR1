.class public final Lcom/cgollner/systemmonitor/a/h;
.super Landroid/support/v4/content/AsyncTaskLoader;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/cgollner/systemmonitor/a/h$a;,
        Lcom/cgollner/systemmonitor/a/h$b;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v4/content/AsyncTaskLoader",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/cgollner/systemmonitor/a/h$b;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:Ljava/io/File;

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/cgollner/systemmonitor/a/h$b;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/io/File;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0, p1}, Landroid/support/v4/content/AsyncTaskLoader;-><init>(Landroid/content/Context;)V

    .line 27
    iput-object p2, p0, Lcom/cgollner/systemmonitor/a/h;->a:Ljava/io/File;

    .line 28
    return-void
.end method

.method private static a(Ljava/io/File;Landroid/content/Context;)Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/cgollner/systemmonitor/a/h$b;",
            ">;"
        }
    .end annotation

    .prologue
    .line 65
    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    .line 66
    invoke-virtual {p0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    .line 67
    if-nez v1, :cond_0

    .line 68
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    .line 91
    :goto_0
    return-object v0

    .line 69
    :cond_0
    array-length v3, v1

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v3, :cond_1

    aget-object v4, v1, v0

    .line 70
    new-instance v5, Lcom/cgollner/systemmonitor/a/h$a;

    invoke-direct {v5, v4, p1}, Lcom/cgollner/systemmonitor/a/h$a;-><init>(Ljava/io/File;Landroid/content/Context;)V

    .line 71
    invoke-virtual {v5}, Lcom/cgollner/systemmonitor/a/h$a;->start()V

    .line 72
    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 69
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 74
    :cond_1
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 75
    const-wide/16 v0, 0x0

    .line 76
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move-wide v2, v0

    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/a/h$a;

    .line 78
    :try_start_0
    invoke-virtual {v0}, Lcom/cgollner/systemmonitor/a/h$a;->join()V

    .line 79
    invoke-static {v0}, Lcom/cgollner/systemmonitor/a/h$a;->a(Lcom/cgollner/systemmonitor/a/h$a;)Ljava/lang/Throwable;

    move-result-object v1

    if-nez v1, :cond_4

    iget-object v1, v0, Lcom/cgollner/systemmonitor/a/h$a;->a:Lcom/cgollner/systemmonitor/a/h$b;

    if-eqz v1, :cond_4

    .line 80
    iget-object v1, v0, Lcom/cgollner/systemmonitor/a/h$a;->a:Lcom/cgollner/systemmonitor/a/h$b;

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 81
    iget-object v0, v0, Lcom/cgollner/systemmonitor/a/h$a;->a:Lcom/cgollner/systemmonitor/a/h$b;

    iget-wide v0, v0, Lcom/cgollner/systemmonitor/a/h$b;->b:J
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    add-long/2addr v2, v0

    move-wide v0, v2

    :goto_3
    move-wide v2, v0

    .line 85
    goto :goto_2

    .line 83
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_2

    .line 87
    :cond_2
    invoke-static {v4}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 88
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/a/h$b;

    .line 89
    iget-wide v6, v0, Lcom/cgollner/systemmonitor/a/h$b;->b:J

    long-to-double v6, v6

    long-to-double v8, v2

    div-double/2addr v6, v8

    const-wide/high16 v8, 0x4059000000000000L    # 100.0

    mul-double/2addr v6, v8

    double-to-float v5, v6

    iput v5, v0, Lcom/cgollner/systemmonitor/a/h$b;->c:F

    goto :goto_4

    :cond_3
    move-object v0, v4

    .line 91
    goto :goto_0

    :cond_4
    move-wide v0, v2

    goto :goto_3
.end method

.method private a(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/cgollner/systemmonitor/a/h$b;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 42
    iput-object p1, p0, Lcom/cgollner/systemmonitor/a/h;->b:Ljava/util/List;

    .line 43
    invoke-super {p0, p1}, Landroid/support/v4/content/AsyncTaskLoader;->deliverResult(Ljava/lang/Object;)V

    .line 44
    return-void
.end method


# virtual methods
.method public final synthetic deliverResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 20
    check-cast p1, Ljava/util/List;

    invoke-direct {p0, p1}, Lcom/cgollner/systemmonitor/a/h;->a(Ljava/util/List;)V

    return-void
.end method

.method public final synthetic loadInBackground()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 20
    iget-object v0, p0, Lcom/cgollner/systemmonitor/a/h;->a:Ljava/io/File;

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/a/h;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/cgollner/systemmonitor/a/h;->a(Ljava/io/File;Landroid/content/Context;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected final onStartLoading()V
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/cgollner/systemmonitor/a/h;->b:Ljava/util/List;

    if-nez v0, :cond_0

    .line 33
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/a/h;->forceLoad()V

    .line 38
    :goto_0
    return-void

    .line 36
    :cond_0
    iget-object v0, p0, Lcom/cgollner/systemmonitor/a/h;->b:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/cgollner/systemmonitor/a/h;->a(Ljava/util/List;)V

    goto :goto_0
.end method
