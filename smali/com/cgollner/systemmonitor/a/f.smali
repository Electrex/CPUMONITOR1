.class public Lcom/cgollner/systemmonitor/a/f;
.super Lcom/cgollner/systemmonitor/a/d;
.source "SourceFile"


# static fields
.field private static final a:Ljava/io/File;

.field private static final b:Ljava/io/File;

.field private static final c:Ljava/io/File;

.field private static d:[Ljava/lang/Integer;

.field private static e:I

.field private static f:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/high16 v2, -0x80000000

    .line 14
    new-instance v0, Ljava/io/File;

    const-string v1, "/sys/devices/platform/mali.0/clock"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/cgollner/systemmonitor/a/f;->a:Ljava/io/File;

    .line 15
    new-instance v0, Ljava/io/File;

    const-string v1, "/sys/devices/platform/mali.0/clock"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/cgollner/systemmonitor/a/f;->b:Ljava/io/File;

    .line 16
    new-instance v0, Ljava/io/File;

    const-string v1, "/sys/devices/platform/mali.0/dvfs"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/cgollner/systemmonitor/a/f;->c:Ljava/io/File;

    .line 19
    sput v2, Lcom/cgollner/systemmonitor/a/f;->e:I

    .line 20
    sput v2, Lcom/cgollner/systemmonitor/a/f;->f:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/cgollner/systemmonitor/a/d;-><init>()V

    return-void
.end method

.method private static a(Ljava/io/File;)Ljava/io/File;
    .locals 2

    .prologue
    .line 23
    invoke-virtual {p0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Ljava/io/File;->canRead()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Ljava/io/File;->setReadable(ZZ)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    const/4 p0, 0x0

    goto :goto_0
.end method

.method private static g()[Ljava/lang/Integer;
    .locals 3

    .prologue
    .line 81
    sget-object v0, Lcom/cgollner/systemmonitor/a/f;->d:[Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 82
    sget-object v0, Lcom/cgollner/systemmonitor/a/f;->d:[Ljava/lang/Integer;

    .line 90
    :goto_0
    return-object v0

    .line 85
    :cond_0
    :try_start_0
    sget-object v0, Lcom/cgollner/systemmonitor/a/f;->b:Ljava/io/File;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/a/f;->a(Ljava/io/File;)Ljava/io/File;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/commons/io/FileUtils;->readFileToString(Ljava/io/File;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "\\n"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    aget-object v0, v0, v1

    .line 86
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    const-string v2, "\\d+"

    invoke-static {v2}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    :goto_1
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->group()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 90
    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0

    .line 86
    :cond_1
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/Integer;

    invoke-interface {v1, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Integer;

    .line 87
    sput-object v0, Lcom/cgollner/systemmonitor/a/f;->d:[Ljava/lang/Integer;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0
.end method

.method private static h()I
    .locals 2

    .prologue
    .line 103
    sget v0, Lcom/cgollner/systemmonitor/a/f;->f:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 104
    sget v0, Lcom/cgollner/systemmonitor/a/f;->f:I

    .line 108
    :goto_0
    return v0

    .line 105
    :cond_0
    invoke-static {}, Lcom/cgollner/systemmonitor/a/f;->g()[Ljava/lang/Integer;

    move-result-object v0

    .line 106
    invoke-static {v0}, Ljava/util/Arrays;->sort([Ljava/lang/Object;)V

    .line 107
    array-length v1, v0

    add-int/lit8 v1, v1, -0x1

    aget-object v0, v0, v1

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    mul-int/lit16 v0, v0, 0x3e8

    mul-int/lit16 v0, v0, 0x3e8

    .line 108
    sput v0, Lcom/cgollner/systemmonitor/a/f;->f:I

    goto :goto_0
.end method


# virtual methods
.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 117
    sget-object v1, Lcom/cgollner/systemmonitor/a/f;->b:Ljava/io/File;

    invoke-static {v1}, Lcom/cgollner/systemmonitor/a/f;->a(Ljava/io/File;)Ljava/io/File;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/cgollner/systemmonitor/a/f;->g()[Ljava/lang/Integer;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/cgollner/systemmonitor/a/f;->g()[Ljava/lang/Integer;

    move-result-object v1

    array-length v1, v1

    if-le v1, v0, :cond_0

    sget-object v1, Lcom/cgollner/systemmonitor/a/f;->c:Ljava/io/File;

    invoke-static {v1}, Lcom/cgollner/systemmonitor/a/f;->a(Ljava/io/File;)Ljava/io/File;

    move-result-object v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/cgollner/systemmonitor/a/f;->a:Ljava/io/File;

    invoke-static {v1}, Lcom/cgollner/systemmonitor/a/f;->a(Ljava/io/File;)Ljava/io/File;

    move-result-object v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/cgollner/systemmonitor/a/f;->b:Ljava/io/File;

    invoke-static {v1}, Lcom/cgollner/systemmonitor/a/f;->a(Ljava/io/File;)Ljava/io/File;

    move-result-object v1

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()F
    .locals 3

    .prologue
    .line 43
    :try_start_0
    sget-object v0, Lcom/cgollner/systemmonitor/a/f;->c:Ljava/io/File;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/a/f;->a(Ljava/io/File;)Ljava/io/File;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/commons/io/FileUtils;->readFileToString(Ljava/io/File;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "\\n"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "\\r"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "\\t"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 44
    const-string v1, "(.*utilisation:)(\\d{1,3})"

    const-string v2, "$2"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 45
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    int-to-float v0, v0

    .line 46
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/a/f;->d()I

    move-result v1

    int-to-float v1, v1

    invoke-static {}, Lcom/cgollner/systemmonitor/a/f;->h()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    .line 47
    mul-float/2addr v0, v1

    .line 50
    :goto_0
    return v0

    :catch_0
    move-exception v0

    const/high16 v0, -0x40800000    # -1.0f

    goto :goto_0
.end method

.method public final d()I
    .locals 3

    .prologue
    .line 55
    :try_start_0
    sget-object v0, Lcom/cgollner/systemmonitor/a/f;->a:Ljava/io/File;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/a/f;->a(Ljava/io/File;)Ljava/io/File;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/commons/io/FileUtils;->readFileToString(Ljava/io/File;)Ljava/lang/String;

    move-result-object v0

    .line 56
    const-string v1, "\\n"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 57
    const/4 v1, 0x0

    aget-object v0, v0, v1

    const-string v1, "(.*)(\\d{3,})(.*)"

    const-string v2, "$2"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 58
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    mul-int/lit16 v0, v0, 0x3e8

    mul-int/lit16 v0, v0, 0x3e8

    .line 62
    :goto_0
    return v0

    :catch_0
    move-exception v0

    const/4 v0, -0x1

    goto :goto_0
.end method

.method public final e()I
    .locals 2

    .prologue
    .line 94
    sget v0, Lcom/cgollner/systemmonitor/a/f;->e:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 95
    sget v0, Lcom/cgollner/systemmonitor/a/f;->e:I

    .line 99
    :goto_0
    return v0

    .line 96
    :cond_0
    invoke-static {}, Lcom/cgollner/systemmonitor/a/f;->g()[Ljava/lang/Integer;

    move-result-object v0

    .line 97
    invoke-static {v0}, Ljava/util/Arrays;->sort([Ljava/lang/Object;)V

    .line 98
    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    mul-int/lit16 v0, v0, 0x3e8

    mul-int/lit16 v0, v0, 0x3e8

    .line 99
    sput v0, Lcom/cgollner/systemmonitor/a/f;->e:I

    goto :goto_0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 112
    invoke-static {}, Lcom/cgollner/systemmonitor/a/f;->h()I

    move-result v0

    return v0
.end method
