.class public Lcom/cgollner/systemmonitor/historybg/c;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"

# interfaces
.implements Lcom/cgollner/systemmonitor/historybg/HistoryBgService$a;


# instance fields
.field private a:Lcom/cgollner/systemmonitor/TimeMonitorView;

.field private b:Landroid/view/View;

.field private c:F

.field private d:F

.field private e:F

.field private f:I

.field private g:I

.field private h:Landroid/widget/TextView;

.field private i:Landroid/widget/TextView;

.field private j:Landroid/widget/TextView;

.field private k:Landroid/widget/TextView;

.field private l:Landroid/widget/TextView;

.field private m:Landroid/widget/TextView;

.field private n:Lcom/cgollner/systemmonitor/historybg/b;

.field private o:I

.field private p:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private q:Landroid/widget/TextView;

.field private r:Landroid/widget/TextView;

.field private s:Z

.field private t:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/cgollner/systemmonitor/historybg/c;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/c;->k:Landroid/widget/TextView;

    return-object v0
.end method

.method private a()V
    .locals 2

    .prologue
    .line 153
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/c;->t:Landroid/os/Handler;

    new-instance v1, Lcom/cgollner/systemmonitor/historybg/c$1;

    invoke-direct {v1, p0}, Lcom/cgollner/systemmonitor/historybg/c$1;-><init>(Lcom/cgollner/systemmonitor/historybg/c;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 184
    return-void
.end method

.method private a(Lcom/cgollner/systemmonitor/historybg/b;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 135
    iget-object v0, p1, Lcom/cgollner/systemmonitor/historybg/b;->d:[F

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/cgollner/systemmonitor/historybg/b;->d:[F

    aget v2, v2, v5

    invoke-static {v1, v2}, Ljava/lang/Math;->max(FF)F

    move-result v1

    aput v1, v0, v5

    .line 136
    iget v0, p0, Lcom/cgollner/systemmonitor/historybg/c;->c:F

    iget-object v1, p1, Lcom/cgollner/systemmonitor/historybg/b;->d:[F

    aget v1, v1, v5

    add-float/2addr v0, v1

    iput v0, p0, Lcom/cgollner/systemmonitor/historybg/c;->c:F

    .line 137
    iget v0, p0, Lcom/cgollner/systemmonitor/historybg/c;->e:F

    iget-object v1, p1, Lcom/cgollner/systemmonitor/historybg/b;->d:[F

    aget v1, v1, v5

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    iput v0, p0, Lcom/cgollner/systemmonitor/historybg/c;->e:F

    .line 138
    iget v0, p0, Lcom/cgollner/systemmonitor/historybg/c;->d:F

    iget-object v1, p1, Lcom/cgollner/systemmonitor/historybg/b;->d:[F

    aget v1, v1, v5

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iput v0, p0, Lcom/cgollner/systemmonitor/historybg/c;->d:F

    .line 140
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/c;->p:Ljava/util/Map;

    iget-object v1, p1, Lcom/cgollner/systemmonitor/historybg/b;->a:[I

    aget v1, v1, v5

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 141
    if-nez v0, :cond_0

    .line 142
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 143
    :cond_0
    iget-object v1, p0, Lcom/cgollner/systemmonitor/historybg/c;->p:Ljava/util/Map;

    iget-object v2, p1, Lcom/cgollner/systemmonitor/historybg/b;->a:[I

    aget v2, v2, v5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 145
    iget v0, p0, Lcom/cgollner/systemmonitor/historybg/c;->g:I

    iget-object v1, p1, Lcom/cgollner/systemmonitor/historybg/b;->a:[I

    aget v1, v1, v5

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lcom/cgollner/systemmonitor/historybg/c;->g:I

    .line 146
    iget v0, p0, Lcom/cgollner/systemmonitor/historybg/c;->f:I

    iget-object v1, p1, Lcom/cgollner/systemmonitor/historybg/b;->a:[I

    aget v1, v1, v5

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/cgollner/systemmonitor/historybg/c;->f:I

    .line 148
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/c;->a:Lcom/cgollner/systemmonitor/TimeMonitorView;

    new-instance v1, Lcom/cgollner/systemmonitor/battery/c;

    iget-wide v2, p1, Lcom/cgollner/systemmonitor/historybg/b;->e:J

    iget-object v4, p1, Lcom/cgollner/systemmonitor/historybg/b;->d:[F

    aget v4, v4, v5

    invoke-direct {v1, v2, v3, v4}, Lcom/cgollner/systemmonitor/battery/c;-><init>(JF)V

    invoke-virtual {v0, v1}, Lcom/cgollner/systemmonitor/TimeMonitorView;->a(Lcom/cgollner/systemmonitor/battery/c;)V

    .line 150
    return-void
.end method

.method private a(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/cgollner/systemmonitor/historybg/b;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 103
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    iput v0, p0, Lcom/cgollner/systemmonitor/historybg/c;->o:I

    .line 104
    iget v0, p0, Lcom/cgollner/systemmonitor/historybg/c;->o:I

    if-nez v0, :cond_0

    .line 112
    :goto_0
    return-void

    .line 106
    :cond_0
    iget v0, p0, Lcom/cgollner/systemmonitor/historybg/c;->o:I

    add-int/lit8 v0, v0, -0x1

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/historybg/b;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/c;->n:Lcom/cgollner/systemmonitor/historybg/b;

    .line 108
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/historybg/b;

    .line 109
    invoke-direct {p0, v0}, Lcom/cgollner/systemmonitor/historybg/c;->a(Lcom/cgollner/systemmonitor/historybg/b;)V

    goto :goto_1

    .line 111
    :cond_1
    invoke-direct {p0}, Lcom/cgollner/systemmonitor/historybg/c;->a()V

    goto :goto_0
.end method

.method static synthetic b(Lcom/cgollner/systemmonitor/historybg/c;)I
    .locals 1

    .prologue
    .line 23
    iget v0, p0, Lcom/cgollner/systemmonitor/historybg/c;->g:I

    return v0
.end method

.method static synthetic c(Lcom/cgollner/systemmonitor/historybg/c;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/c;->l:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic d(Lcom/cgollner/systemmonitor/historybg/c;)I
    .locals 1

    .prologue
    .line 23
    iget v0, p0, Lcom/cgollner/systemmonitor/historybg/c;->f:I

    return v0
.end method

.method static synthetic e(Lcom/cgollner/systemmonitor/historybg/c;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/c;->m:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic f(Lcom/cgollner/systemmonitor/historybg/c;)Lcom/cgollner/systemmonitor/historybg/b;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/c;->n:Lcom/cgollner/systemmonitor/historybg/b;

    return-object v0
.end method

.method static synthetic g(Lcom/cgollner/systemmonitor/historybg/c;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/c;->r:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic h(Lcom/cgollner/systemmonitor/historybg/c;)F
    .locals 1

    .prologue
    .line 23
    iget v0, p0, Lcom/cgollner/systemmonitor/historybg/c;->c:F

    return v0
.end method

.method static synthetic i(Lcom/cgollner/systemmonitor/historybg/c;)I
    .locals 1

    .prologue
    .line 23
    iget v0, p0, Lcom/cgollner/systemmonitor/historybg/c;->o:I

    return v0
.end method

.method static synthetic j(Lcom/cgollner/systemmonitor/historybg/c;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/c;->h:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic k(Lcom/cgollner/systemmonitor/historybg/c;)F
    .locals 1

    .prologue
    .line 23
    iget v0, p0, Lcom/cgollner/systemmonitor/historybg/c;->d:F

    return v0
.end method

.method static synthetic l(Lcom/cgollner/systemmonitor/historybg/c;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/c;->j:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic m(Lcom/cgollner/systemmonitor/historybg/c;)F
    .locals 1

    .prologue
    .line 23
    iget v0, p0, Lcom/cgollner/systemmonitor/historybg/c;->e:F

    return v0
.end method

.method static synthetic n(Lcom/cgollner/systemmonitor/historybg/c;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/c;->i:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic o(Lcom/cgollner/systemmonitor/historybg/c;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/c;->q:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic p(Lcom/cgollner/systemmonitor/historybg/c;)Lcom/cgollner/systemmonitor/TimeMonitorView;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/c;->a:Lcom/cgollner/systemmonitor/TimeMonitorView;

    return-object v0
.end method

.method static synthetic q(Lcom/cgollner/systemmonitor/historybg/c;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/c;->p:Ljava/util/Map;

    return-object v0
.end method


# virtual methods
.method public final a(Z)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 116
    if-eqz p1, :cond_0

    .line 117
    iput v1, p0, Lcom/cgollner/systemmonitor/historybg/c;->c:F

    .line 118
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/c;->p:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 119
    const/high16 v0, 0x42c80000    # 100.0f

    iput v0, p0, Lcom/cgollner/systemmonitor/historybg/c;->e:F

    .line 120
    iput v1, p0, Lcom/cgollner/systemmonitor/historybg/c;->d:F

    .line 121
    const/4 v0, 0x0

    iput v0, p0, Lcom/cgollner/systemmonitor/historybg/c;->f:I

    .line 122
    const v0, 0x7fffffff

    iput v0, p0, Lcom/cgollner/systemmonitor/historybg/c;->g:I

    .line 123
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/c;->a:Lcom/cgollner/systemmonitor/TimeMonitorView;

    invoke-virtual {v0}, Lcom/cgollner/systemmonitor/TimeMonitorView;->a()V

    .line 124
    sget-object v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->a:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/cgollner/systemmonitor/historybg/c;->a(Ljava/util/List;)V

    .line 132
    :goto_0
    return-void

    .line 127
    :cond_0
    sget-object v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iput v0, p0, Lcom/cgollner/systemmonitor/historybg/c;->o:I

    .line 128
    sget-object v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->a:Ljava/util/List;

    iget v1, p0, Lcom/cgollner/systemmonitor/historybg/c;->o:I

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/historybg/b;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/c;->n:Lcom/cgollner/systemmonitor/historybg/b;

    .line 129
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/c;->n:Lcom/cgollner/systemmonitor/historybg/b;

    invoke-direct {p0, v0}, Lcom/cgollner/systemmonitor/historybg/c;->a(Lcom/cgollner/systemmonitor/historybg/b;)V

    .line 130
    invoke-direct {p0}, Lcom/cgollner/systemmonitor/historybg/c;->a()V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 45
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 46
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/historybg/c;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/historybg/c;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "SEE_ONLY"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    iput-boolean v0, p0, Lcom/cgollner/systemmonitor/historybg/c;->s:Z

    .line 47
    iget-boolean v0, p0, Lcom/cgollner/systemmonitor/historybg/c;->s:Z

    if-nez v0, :cond_1

    .line 50
    sget-object v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->f:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 52
    :cond_1
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "UseSparseArrays"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 66
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/c;->t:Landroid/os/Handler;

    .line 68
    sget v0, Lcom/cgollner/systemmonitor/b/a$f;->cpu_fragment_history_layout:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/c;->b:Landroid/view/View;

    .line 70
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/c;->b:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/b/a$e;->monitorview:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/TimeMonitorView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/c;->a:Lcom/cgollner/systemmonitor/TimeMonitorView;

    .line 71
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/c;->a:Lcom/cgollner/systemmonitor/TimeMonitorView;

    invoke-virtual {v0}, Lcom/cgollner/systemmonitor/TimeMonitorView;->a()V

    .line 73
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/c;->b:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/b/a$e;->usageAvg:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/c;->h:Landroid/widget/TextView;

    .line 74
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/c;->b:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/b/a$e;->usageMin:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/c;->i:Landroid/widget/TextView;

    .line 75
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/c;->b:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/b/a$e;->usageMax:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/c;->j:Landroid/widget/TextView;

    .line 76
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/c;->b:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/b/a$e;->usageLast:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/c;->q:Landroid/widget/TextView;

    .line 78
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/c;->b:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/b/a$e;->freqAvg:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/c;->k:Landroid/widget/TextView;

    .line 79
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/c;->b:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/b/a$e;->freqMin:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/c;->l:Landroid/widget/TextView;

    .line 80
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/c;->b:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/b/a$e;->freqMax:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/c;->m:Landroid/widget/TextView;

    .line 81
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/c;->b:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/b/a$e;->freqLast:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/c;->r:Landroid/widget/TextView;

    .line 83
    iput v2, p0, Lcom/cgollner/systemmonitor/historybg/c;->c:F

    .line 84
    const/high16 v0, 0x42c80000    # 100.0f

    iput v0, p0, Lcom/cgollner/systemmonitor/historybg/c;->e:F

    .line 85
    iput v2, p0, Lcom/cgollner/systemmonitor/historybg/c;->d:F

    .line 87
    const/4 v0, 0x0

    iput v0, p0, Lcom/cgollner/systemmonitor/historybg/c;->f:I

    .line 88
    const v0, 0x7fffffff

    iput v0, p0, Lcom/cgollner/systemmonitor/historybg/c;->g:I

    .line 90
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/c;->p:Ljava/util/Map;

    .line 92
    iget-boolean v0, p0, Lcom/cgollner/systemmonitor/historybg/c;->s:Z

    if-eqz v0, :cond_0

    .line 93
    sget-object v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->a:Lcom/cgollner/systemmonitor/historybg/a;

    iget-object v0, v0, Lcom/cgollner/systemmonitor/historybg/a;->a:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/cgollner/systemmonitor/historybg/c;->a(Ljava/util/List;)V

    .line 100
    :goto_0
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/c;->b:Landroid/view/View;

    return-object v0

    .line 96
    :cond_0
    sget-object v1, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->a:Ljava/util/List;

    monitor-enter v1

    .line 97
    :try_start_0
    sget-object v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->a:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/cgollner/systemmonitor/historybg/c;->a(Ljava/util/List;)V

    .line 98
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 56
    iget-boolean v0, p0, Lcom/cgollner/systemmonitor/historybg/c;->s:Z

    if-nez v0, :cond_0

    .line 57
    sget-object v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->f:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 59
    :cond_0
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    .line 60
    return-void
.end method
