.class final Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity$a;
.super Landroid/support/v4/app/FragmentPagerAdapter;
.source "SourceFile"

# interfaces
.implements Landroid/support/v4/view/ViewPager$OnPageChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;


# direct methods
.method public constructor <init>(Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;Landroid/support/v4/app/FragmentManager;)V
    .locals 0

    .prologue
    .line 224
    iput-object p1, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity$a;->a:Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;

    .line 225
    invoke-direct {p0, p2}, Landroid/support/v4/app/FragmentPagerAdapter;-><init>(Landroid/support/v4/app/FragmentManager;)V

    .line 226
    return-void
.end method


# virtual methods
.method public final getCount()I
    .locals 1

    .prologue
    .line 247
    invoke-static {}, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->b()[Ljava/lang/Class;

    move-result-object v0

    array-length v0, v0

    return v0
.end method

.method public final getItem(I)Landroid/support/v4/app/Fragment;
    .locals 4

    .prologue
    .line 231
    :try_start_0
    invoke-static {}, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->b()[Ljava/lang/Class;

    move-result-object v0

    aget-object v0, v0, p1

    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 232
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 233
    const-string v2, "SEE_ONLY"

    iget-object v3, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity$a;->a:Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;

    invoke-static {v3}, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->b(Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 234
    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    .line 241
    :goto_0
    return-object v0

    .line 236
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/InstantiationException;->printStackTrace()V

    .line 241
    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    .line 238
    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_1
.end method

.method public final getPageTitle(I)Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 252
    sget-object v0, Lcom/cgollner/systemmonitor/App;->a:Landroid/content/Context;

    invoke-static {}, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->c()[I

    move-result-object v1

    aget v1, v1, p1

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final onPageScrollStateChanged(I)V
    .locals 0

    .prologue
    .line 267
    return-void
.end method

.method public final onPageScrolled(IFI)V
    .locals 0

    .prologue
    .line 263
    return-void
.end method

.method public final onPageSelected(I)V
    .locals 2

    .prologue
    .line 258
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity$a;->a:Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;

    invoke-static {}, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->a()[I

    move-result-object v1

    aget v1, v1, p1

    invoke-static {v0, v1}, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->a(Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;I)V

    .line 259
    return-void
.end method
