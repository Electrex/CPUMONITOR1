.class public Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;
.super Landroid/support/v7/app/ActionBarActivity;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity$a;
    }
.end annotation


# static fields
.field public static a:Lcom/cgollner/systemmonitor/historybg/a;

.field private static final b:[I

.field private static final c:[I

.field private static final j:[Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Ljava/lang/Class",
            "<+",
            "Landroid/support/v4/app/Fragment;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private d:Landroid/support/v4/view/ViewPager;

.field private e:Landroid/support/v4/view/PagerTitleStrip;

.field private f:Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity$a;

.field private g:Z

.field private h:Ljava/lang/String;

.field private i:Z


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 34
    new-array v0, v6, [I

    sget v1, Lcom/cgollner/systemmonitor/b/a$c;->holo_blue_dark:I

    aput v1, v0, v2

    sget v1, Lcom/cgollner/systemmonitor/b/a$c;->holo_purple_dark:I

    aput v1, v0, v3

    sget v1, Lcom/cgollner/systemmonitor/b/a$c;->holo_green_dark:I

    aput v1, v0, v4

    sget v1, Lcom/cgollner/systemmonitor/b/a$c;->holo_orange_dark:I

    aput v1, v0, v5

    sput-object v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->b:[I

    .line 40
    new-array v0, v6, [I

    sget v1, Lcom/cgollner/systemmonitor/b/a$h;->cpu_title:I

    aput v1, v0, v2

    sget v1, Lcom/cgollner/systemmonitor/b/a$h;->ram_title:I

    aput v1, v0, v3

    sget v1, Lcom/cgollner/systemmonitor/b/a$h;->io_title:I

    aput v1, v0, v4

    sget v1, Lcom/cgollner/systemmonitor/b/a$h;->network_title:I

    aput v1, v0, v5

    sput-object v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->c:[I

    .line 215
    new-array v0, v6, [Ljava/lang/Class;

    const-class v1, Lcom/cgollner/systemmonitor/historybg/c;

    aput-object v1, v0, v2

    const-class v1, Lcom/cgollner/systemmonitor/historybg/i;

    aput-object v1, v0, v3

    const-class v1, Lcom/cgollner/systemmonitor/historybg/e;

    aput-object v1, v0, v4

    const-class v1, Lcom/cgollner/systemmonitor/historybg/g;

    aput-object v1, v0, v5

    sput-object v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->j:[Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Landroid/support/v7/app/ActionBarActivity;-><init>()V

    .line 222
    return-void
.end method

.method static synthetic a(Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;)Landroid/support/v4/view/ViewPager;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->d:Landroid/support/v4/view/ViewPager;

    return-object v0
.end method

.method static synthetic a(Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;I)V
    .locals 4

    .prologue
    const v3, 0x3f333333    # 0.7f

    .line 33
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v1

    new-instance v2, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v2, v0}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v1, v2}, Landroid/support/v7/app/ActionBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v1, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->e:Landroid/support/v4/view/PagerTitleStrip;

    invoke-virtual {v1, v0}, Landroid/support/v4/view/PagerTitleStrip;->setBackgroundColor(I)V

    invoke-static {v0}, Landroid/graphics/Color;->red(I)I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v1, v3

    float-to-int v1, v1

    invoke-static {v0}, Landroid/graphics/Color;->green(I)I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v2, v3

    float-to-int v2, v2

    invoke-static {v0}, Landroid/graphics/Color;->blue(I)I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v0, v3

    float-to-int v0, v0

    invoke-static {v1, v2, v0}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x15

    if-lt v1, v2, :cond_1

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Window;->setStatusBarColor(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x13

    if-lt v1, v2, :cond_0

    invoke-static {p0, v0}, Lcom/cgollner/systemmonitor/App;->a(Landroid/app/Activity;I)V

    goto :goto_0
.end method

.method static synthetic a()[I
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->b:[I

    return-object v0
.end method

.method static synthetic b(Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->h:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b()[Ljava/lang/Class;
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->j:[Ljava/lang/Class;

    return-object v0
.end method

.method static synthetic c()[I
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->c:[I

    return-object v0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 59
    sget-object v0, Lcom/cgollner/systemmonitor/App;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    const-string v3, "theme"

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0xb

    if-ge v0, v4, :cond_0

    move v0, v1

    :goto_0
    invoke-interface {v2, v3, v0}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 60
    packed-switch v0, :pswitch_data_0

    .line 71
    :goto_1
    invoke-super {p0, p1}, Landroid/support/v7/app/ActionBarActivity;->onCreate(Landroid/os/Bundle;)V

    .line 72
    sget v0, Lcom/cgollner/systemmonitor/b/a$f;->activity_main:I

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->setContentView(I)V

    .line 73
    sget v0, Lcom/cgollner/systemmonitor/b/a$e;->toolbar:I

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->setSupportActionBar(Landroid/support/v7/widget/Toolbar;)V

    .line 74
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    sget v2, Lcom/cgollner/systemmonitor/b/a$h;->background_history_title:I

    invoke-virtual {v0, v2}, Landroid/support/v7/app/ActionBar;->setTitle(I)V

    .line 76
    sget v0, Lcom/cgollner/systemmonitor/b/a$e;->viewPager:I

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->d:Landroid/support/v4/view/ViewPager;

    .line 77
    sget v0, Lcom/cgollner/systemmonitor/b/a$e;->pager_title_strip:I

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/PagerTitleStrip;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->e:Landroid/support/v4/view/PagerTitleStrip;

    .line 79
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "SEE_ONLY"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->h:Ljava/lang/String;

    .line 80
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->h:Ljava/lang/String;

    if-nez v0, :cond_1

    sget-object v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->e:Lcom/cgollner/systemmonitor/historybg/a;

    sput-object v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->a:Lcom/cgollner/systemmonitor/historybg/a;

    .line 82
    :goto_2
    new-instance v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity$a;

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    invoke-direct {v0, p0, v2}, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity$a;-><init>(Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;Landroid/support/v4/app/FragmentManager;)V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->f:Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity$a;

    .line 83
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->d:Landroid/support/v4/view/ViewPager;

    iget-object v2, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->f:Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity$a;

    invoke-virtual {v0, v2}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 84
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->d:Landroid/support/v4/view/ViewPager;

    iget-object v2, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->f:Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity$a;

    invoke-virtual {v0, v2}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 85
    iput-boolean v1, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->g:Z

    .line 87
    invoke-static {p0}, Lcom/cgollner/systemmonitor/a/i;->a(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->i:Z

    .line 89
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->d:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    new-instance v1, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity$1;

    invoke-direct {v1, p0}, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity$1;-><init>(Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 98
    return-void

    .line 59
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 62
    :pswitch_0
    sget v0, Lcom/cgollner/systemmonitor/b/a$i;->AppTheme:I

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->setTheme(I)V

    goto :goto_1

    .line 65
    :pswitch_1
    sget v0, Lcom/cgollner/systemmonitor/b/a$i;->AppThemeDark:I

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->setTheme(I)V

    goto/16 :goto_1

    .line 68
    :pswitch_2
    sget v0, Lcom/cgollner/systemmonitor/b/a$i;->AppTheme:I

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->setTheme(I)V

    goto/16 :goto_1

    .line 80
    :cond_1
    sget-object v2, Lcom/cgollner/systemmonitor/App;->a:Landroid/content/Context;

    const-string v3, "history"

    invoke-static {v2, v3, v0}, Lcom/cgollner/systemmonitor/battery/BatteryService;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/historybg/a;

    sput-object v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->a:Lcom/cgollner/systemmonitor/historybg/a;

    goto :goto_2

    .line 60
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    .prologue
    .line 111
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->h:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 112
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    sget v1, Lcom/cgollner/systemmonitor/b/a$g;->bg_activity_menu:I

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 113
    :cond_0
    invoke-super {p0, p1}, Landroid/support/v7/app/ActionBarActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 4
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    .line 119
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sget v1, Lcom/cgollner/systemmonitor/b/a$e;->save:I

    if-ne v0, v1, :cond_4

    .line 120
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->getPackageName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.cgollner.systemmonitor"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 121
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget-object v1, Lcom/cgollner/systemmonitor/App;->a:Landroid/content/Context;

    sget v2, Lcom/cgollner/systemmonitor/b/a$h;->full_version_feature_title:I

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, Lcom/cgollner/systemmonitor/b/a$h;->full_version_feature_message:I

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const-string v1, "Play Store"

    new-instance v2, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity$3;

    invoke-direct {v2, p0}, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity$3;-><init>(Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/high16 v1, 0x1040000

    new-instance v2, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity$2;

    invoke-direct {v2, p0}, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity$2;-><init>(Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 169
    :cond_0
    :goto_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_1

    .line 170
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->invalidateOptionsMenu()V

    .line 172
    :cond_1
    invoke-super {p0, p1}, Landroid/support/v7/app/ActionBarActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 141
    :cond_2
    iget-boolean v0, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->i:Z

    if-eqz v0, :cond_3

    .line 142
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget-object v1, Lcom/cgollner/systemmonitor/App;->a:Landroid/content/Context;

    sget v2, Lcom/cgollner/systemmonitor/b/a$h;->unlicensed_dialog_title:I

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, Lcom/cgollner/systemmonitor/b/a$h;->unlicensed_dialog_body:I

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x104000a

    new-instance v2, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity$4;

    invoke-direct {v2, p0}, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity$4;-><init>(Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    goto :goto_0

    .line 154
    :cond_3
    new-instance v0, Landroid/widget/EditText;

    invoke-direct {v0, p0}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    sget-object v1, Lcom/cgollner/systemmonitor/App;->a:Landroid/content/Context;

    sget v2, Lcom/cgollner/systemmonitor/b/a$h;->history_bg_history_enter_name:I

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget v2, Lcom/cgollner/systemmonitor/b/a$h;->history_bg_save_menu:I

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    sget v2, Lcom/cgollner/systemmonitor/b/a$h;->history_bg_save_menu:I

    new-instance v3, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity$6;

    invoke-direct {v3, p0, v0}, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity$6;-><init>(Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;Landroid/widget/EditText;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, Lcom/cgollner/systemmonitor/b/a$h;->dialog_cancel:I

    new-instance v2, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity$5;

    invoke-direct {v2, p0}, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity$5;-><init>(Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    goto :goto_0

    .line 157
    :cond_4
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sget v1, Lcom/cgollner/systemmonitor/b/a$e;->stop_start:I

    if-ne v0, v1, :cond_0

    .line 158
    iget-boolean v0, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->g:Z

    if-nez v0, :cond_5

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->g:Z

    .line 159
    new-instance v0, Landroid/content/Intent;

    sget-object v1, Lcom/cgollner/systemmonitor/App;->a:Landroid/content/Context;

    const-class v2, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 160
    iget-boolean v1, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->g:Z

    if-eqz v1, :cond_6

    .line 161
    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 162
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->finish()V

    .line 163
    new-instance v0, Landroid/content/Intent;

    sget-object v1, Lcom/cgollner/systemmonitor/App;->a:Landroid/content/Context;

    const-class v2, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 158
    :cond_5
    const/4 v0, 0x0

    goto :goto_1

    .line 166
    :cond_6
    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->stopService(Landroid/content/Intent;)Z

    goto/16 :goto_0
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 204
    iget-object v2, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->h:Ljava/lang/String;

    if-nez v2, :cond_0

    .line 205
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v2

    const/4 v3, 0x2

    new-array v3, v3, [I

    sget v4, Lcom/cgollner/systemmonitor/b/a$b;->playIcon:I

    aput v4, v3, v1

    sget v4, Lcom/cgollner/systemmonitor/b/a$b;->stopIcon:I

    aput v4, v3, v0

    invoke-virtual {v2, v3}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v2

    .line 208
    sget v3, Lcom/cgollner/systemmonitor/b/a$e;->stop_start:I

    invoke-interface {p1, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    iget-boolean v4, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->g:Z

    if-eqz v4, :cond_1

    :goto_0
    invoke-virtual {v2, v0}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-interface {v3, v0}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    .line 209
    invoke-virtual {v2}, Landroid/content/res/TypedArray;->recycle()V

    .line 211
    :cond_0
    invoke-super {p0, p1}, Landroid/support/v7/app/ActionBarActivity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0

    :cond_1
    move v0, v1

    .line 208
    goto :goto_0
.end method
