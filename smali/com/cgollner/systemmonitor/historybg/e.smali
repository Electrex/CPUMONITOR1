.class public Lcom/cgollner/systemmonitor/historybg/e;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"

# interfaces
.implements Lcom/cgollner/systemmonitor/historybg/HistoryBgService$a;


# instance fields
.field private A:Landroid/widget/TextView;

.field private B:Landroid/widget/TextView;

.field private C:Landroid/widget/TextView;

.field private D:Landroid/widget/TextView;

.field private E:Landroid/widget/TextView;

.field private F:Z

.field private G:Landroid/os/Handler;

.field private a:Lcom/cgollner/systemmonitor/TimeMonitorView;

.field private b:Landroid/view/View;

.field private c:F

.field private d:F

.field private e:F

.field private f:J

.field private g:J

.field private h:J

.field private i:J

.field private j:J

.field private k:J

.field private l:J

.field private m:J

.field private n:J

.field private o:Landroid/widget/TextView;

.field private p:Landroid/widget/TextView;

.field private q:Landroid/widget/TextView;

.field private r:Lcom/cgollner/systemmonitor/historybg/d;

.field private s:I

.field private t:Landroid/widget/TextView;

.field private u:Landroid/widget/TextView;

.field private v:Landroid/widget/TextView;

.field private w:Landroid/widget/TextView;

.field private x:Landroid/widget/TextView;

.field private y:Landroid/widget/TextView;

.field private z:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method

.method static synthetic A(Lcom/cgollner/systemmonitor/historybg/e;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/e;->D:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic B(Lcom/cgollner/systemmonitor/historybg/e;)J
    .locals 2

    .prologue
    .line 21
    iget-wide v0, p0, Lcom/cgollner/systemmonitor/historybg/e;->n:J

    return-wide v0
.end method

.method static synthetic C(Lcom/cgollner/systemmonitor/historybg/e;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/e;->E:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic D(Lcom/cgollner/systemmonitor/historybg/e;)Lcom/cgollner/systemmonitor/TimeMonitorView;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/e;->a:Lcom/cgollner/systemmonitor/TimeMonitorView;

    return-object v0
.end method

.method static synthetic a(Lcom/cgollner/systemmonitor/historybg/e;)F
    .locals 1

    .prologue
    .line 21
    iget v0, p0, Lcom/cgollner/systemmonitor/historybg/e;->c:F

    return v0
.end method

.method private a()V
    .locals 2

    .prologue
    .line 190
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/e;->G:Landroid/os/Handler;

    new-instance v1, Lcom/cgollner/systemmonitor/historybg/e$1;

    invoke-direct {v1, p0}, Lcom/cgollner/systemmonitor/historybg/e$1;-><init>(Lcom/cgollner/systemmonitor/historybg/e;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 217
    return-void
.end method

.method private a(Lcom/cgollner/systemmonitor/historybg/d;)V
    .locals 6

    .prologue
    .line 168
    iget v0, p0, Lcom/cgollner/systemmonitor/historybg/e;->c:F

    iget v1, p1, Lcom/cgollner/systemmonitor/historybg/d;->a:F

    add-float/2addr v0, v1

    iput v0, p0, Lcom/cgollner/systemmonitor/historybg/e;->c:F

    .line 169
    iget v0, p0, Lcom/cgollner/systemmonitor/historybg/e;->e:F

    iget v1, p1, Lcom/cgollner/systemmonitor/historybg/d;->a:F

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    iput v0, p0, Lcom/cgollner/systemmonitor/historybg/e;->e:F

    .line 170
    iget v0, p0, Lcom/cgollner/systemmonitor/historybg/e;->d:F

    iget v1, p1, Lcom/cgollner/systemmonitor/historybg/d;->a:F

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iput v0, p0, Lcom/cgollner/systemmonitor/historybg/e;->d:F

    .line 171
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/e;->a:Lcom/cgollner/systemmonitor/TimeMonitorView;

    new-instance v1, Lcom/cgollner/systemmonitor/battery/c;

    iget-wide v2, p1, Lcom/cgollner/systemmonitor/historybg/d;->d:J

    iget v4, p1, Lcom/cgollner/systemmonitor/historybg/d;->a:F

    invoke-direct {v1, v2, v3, v4}, Lcom/cgollner/systemmonitor/battery/c;-><init>(JF)V

    invoke-virtual {v0, v1}, Lcom/cgollner/systemmonitor/TimeMonitorView;->a(Lcom/cgollner/systemmonitor/battery/c;)V

    .line 173
    const-wide v0, 0x408f400000000000L    # 1000.0

    iget-wide v2, p1, Lcom/cgollner/systemmonitor/historybg/d;->e:J

    long-to-double v2, v2

    div-double/2addr v0, v2

    .line 174
    iget-wide v2, p1, Lcom/cgollner/systemmonitor/historybg/d;->b:J

    long-to-double v2, v2

    mul-double/2addr v2, v0

    double-to-long v2, v2

    .line 175
    iget-wide v4, p0, Lcom/cgollner/systemmonitor/historybg/e;->f:J

    add-long/2addr v4, v2

    iput-wide v4, p0, Lcom/cgollner/systemmonitor/historybg/e;->f:J

    .line 176
    iget-wide v4, p0, Lcom/cgollner/systemmonitor/historybg/e;->h:J

    invoke-static {v4, v5, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/cgollner/systemmonitor/historybg/e;->h:J

    .line 177
    iget-wide v4, p0, Lcom/cgollner/systemmonitor/historybg/e;->g:J

    invoke-static {v4, v5, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/cgollner/systemmonitor/historybg/e;->g:J

    .line 179
    iget-wide v2, p1, Lcom/cgollner/systemmonitor/historybg/d;->c:J

    long-to-double v2, v2

    mul-double/2addr v0, v2

    double-to-long v0, v0

    .line 180
    iget-wide v2, p0, Lcom/cgollner/systemmonitor/historybg/e;->i:J

    add-long/2addr v2, v0

    iput-wide v2, p0, Lcom/cgollner/systemmonitor/historybg/e;->i:J

    .line 181
    iget-wide v2, p0, Lcom/cgollner/systemmonitor/historybg/e;->k:J

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/cgollner/systemmonitor/historybg/e;->k:J

    .line 182
    iget-wide v2, p0, Lcom/cgollner/systemmonitor/historybg/e;->j:J

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/cgollner/systemmonitor/historybg/e;->j:J

    .line 184
    iget-wide v0, p0, Lcom/cgollner/systemmonitor/historybg/e;->l:J

    iget-wide v2, p1, Lcom/cgollner/systemmonitor/historybg/d;->b:J

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/cgollner/systemmonitor/historybg/e;->l:J

    .line 185
    iget-wide v0, p0, Lcom/cgollner/systemmonitor/historybg/e;->m:J

    iget-wide v2, p1, Lcom/cgollner/systemmonitor/historybg/d;->c:J

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/cgollner/systemmonitor/historybg/e;->m:J

    .line 186
    iget-wide v0, p0, Lcom/cgollner/systemmonitor/historybg/e;->n:J

    iget-wide v2, p1, Lcom/cgollner/systemmonitor/historybg/d;->b:J

    iget-wide v4, p1, Lcom/cgollner/systemmonitor/historybg/d;->c:J

    add-long/2addr v2, v4

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/cgollner/systemmonitor/historybg/e;->n:J

    .line 188
    return-void
.end method

.method private a(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/cgollner/systemmonitor/historybg/d;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 127
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    iput v0, p0, Lcom/cgollner/systemmonitor/historybg/e;->s:I

    .line 128
    iget v0, p0, Lcom/cgollner/systemmonitor/historybg/e;->s:I

    if-nez v0, :cond_0

    .line 136
    :goto_0
    return-void

    .line 130
    :cond_0
    iget v0, p0, Lcom/cgollner/systemmonitor/historybg/e;->s:I

    add-int/lit8 v0, v0, -0x1

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/historybg/d;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/e;->r:Lcom/cgollner/systemmonitor/historybg/d;

    .line 132
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/historybg/d;

    .line 133
    invoke-direct {p0, v0}, Lcom/cgollner/systemmonitor/historybg/e;->a(Lcom/cgollner/systemmonitor/historybg/d;)V

    goto :goto_1

    .line 135
    :cond_1
    invoke-direct {p0}, Lcom/cgollner/systemmonitor/historybg/e;->a()V

    goto :goto_0
.end method

.method static synthetic b(Lcom/cgollner/systemmonitor/historybg/e;)I
    .locals 1

    .prologue
    .line 21
    iget v0, p0, Lcom/cgollner/systemmonitor/historybg/e;->s:I

    return v0
.end method

.method static synthetic c(Lcom/cgollner/systemmonitor/historybg/e;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/e;->o:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic d(Lcom/cgollner/systemmonitor/historybg/e;)F
    .locals 1

    .prologue
    .line 21
    iget v0, p0, Lcom/cgollner/systemmonitor/historybg/e;->d:F

    return v0
.end method

.method static synthetic e(Lcom/cgollner/systemmonitor/historybg/e;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/e;->q:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic f(Lcom/cgollner/systemmonitor/historybg/e;)F
    .locals 1

    .prologue
    .line 21
    iget v0, p0, Lcom/cgollner/systemmonitor/historybg/e;->e:F

    return v0
.end method

.method static synthetic g(Lcom/cgollner/systemmonitor/historybg/e;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/e;->p:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic h(Lcom/cgollner/systemmonitor/historybg/e;)Lcom/cgollner/systemmonitor/historybg/d;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/e;->r:Lcom/cgollner/systemmonitor/historybg/d;

    return-object v0
.end method

.method static synthetic i(Lcom/cgollner/systemmonitor/historybg/e;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/e;->t:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic j(Lcom/cgollner/systemmonitor/historybg/e;)J
    .locals 2

    .prologue
    .line 21
    iget-wide v0, p0, Lcom/cgollner/systemmonitor/historybg/e;->f:J

    return-wide v0
.end method

.method static synthetic k(Lcom/cgollner/systemmonitor/historybg/e;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/e;->u:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic l(Lcom/cgollner/systemmonitor/historybg/e;)J
    .locals 2

    .prologue
    .line 21
    iget-wide v0, p0, Lcom/cgollner/systemmonitor/historybg/e;->h:J

    return-wide v0
.end method

.method static synthetic m(Lcom/cgollner/systemmonitor/historybg/e;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/e;->w:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic n(Lcom/cgollner/systemmonitor/historybg/e;)J
    .locals 2

    .prologue
    .line 21
    iget-wide v0, p0, Lcom/cgollner/systemmonitor/historybg/e;->g:J

    return-wide v0
.end method

.method static synthetic o(Lcom/cgollner/systemmonitor/historybg/e;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/e;->v:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic p(Lcom/cgollner/systemmonitor/historybg/e;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/e;->x:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic q(Lcom/cgollner/systemmonitor/historybg/e;)J
    .locals 2

    .prologue
    .line 21
    iget-wide v0, p0, Lcom/cgollner/systemmonitor/historybg/e;->i:J

    return-wide v0
.end method

.method static synthetic r(Lcom/cgollner/systemmonitor/historybg/e;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/e;->y:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic s(Lcom/cgollner/systemmonitor/historybg/e;)J
    .locals 2

    .prologue
    .line 21
    iget-wide v0, p0, Lcom/cgollner/systemmonitor/historybg/e;->k:J

    return-wide v0
.end method

.method static synthetic t(Lcom/cgollner/systemmonitor/historybg/e;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/e;->A:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic u(Lcom/cgollner/systemmonitor/historybg/e;)J
    .locals 2

    .prologue
    .line 21
    iget-wide v0, p0, Lcom/cgollner/systemmonitor/historybg/e;->j:J

    return-wide v0
.end method

.method static synthetic v(Lcom/cgollner/systemmonitor/historybg/e;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/e;->z:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic w(Lcom/cgollner/systemmonitor/historybg/e;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/e;->B:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic x(Lcom/cgollner/systemmonitor/historybg/e;)J
    .locals 2

    .prologue
    .line 21
    iget-wide v0, p0, Lcom/cgollner/systemmonitor/historybg/e;->l:J

    return-wide v0
.end method

.method static synthetic y(Lcom/cgollner/systemmonitor/historybg/e;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/e;->C:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic z(Lcom/cgollner/systemmonitor/historybg/e;)J
    .locals 2

    .prologue
    .line 21
    iget-wide v0, p0, Lcom/cgollner/systemmonitor/historybg/e;->m:J

    return-wide v0
.end method


# virtual methods
.method public final a(Z)V
    .locals 6

    .prologue
    const-wide v4, 0x7fffffffffffffffL

    const/4 v1, 0x0

    const-wide/16 v2, 0x0

    .line 140
    if-eqz p1, :cond_0

    .line 141
    iput v1, p0, Lcom/cgollner/systemmonitor/historybg/e;->c:F

    .line 142
    const/high16 v0, 0x42c80000    # 100.0f

    iput v0, p0, Lcom/cgollner/systemmonitor/historybg/e;->e:F

    .line 143
    iput v1, p0, Lcom/cgollner/systemmonitor/historybg/e;->d:F

    .line 145
    iput-wide v2, p0, Lcom/cgollner/systemmonitor/historybg/e;->f:J

    .line 146
    iput-wide v4, p0, Lcom/cgollner/systemmonitor/historybg/e;->h:J

    .line 147
    iput-wide v2, p0, Lcom/cgollner/systemmonitor/historybg/e;->g:J

    .line 149
    iput-wide v2, p0, Lcom/cgollner/systemmonitor/historybg/e;->i:J

    .line 150
    iput-wide v4, p0, Lcom/cgollner/systemmonitor/historybg/e;->k:J

    .line 151
    iput-wide v2, p0, Lcom/cgollner/systemmonitor/historybg/e;->j:J

    .line 153
    iput-wide v2, p0, Lcom/cgollner/systemmonitor/historybg/e;->l:J

    .line 154
    iput-wide v2, p0, Lcom/cgollner/systemmonitor/historybg/e;->l:J

    .line 155
    iput-wide v2, p0, Lcom/cgollner/systemmonitor/historybg/e;->n:J

    .line 156
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/e;->a:Lcom/cgollner/systemmonitor/TimeMonitorView;

    invoke-virtual {v0}, Lcom/cgollner/systemmonitor/TimeMonitorView;->a()V

    .line 157
    sget-object v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->c:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/cgollner/systemmonitor/historybg/e;->a(Ljava/util/List;)V

    .line 165
    :goto_0
    return-void

    .line 160
    :cond_0
    sget-object v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iput v0, p0, Lcom/cgollner/systemmonitor/historybg/e;->s:I

    .line 161
    sget-object v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->c:Ljava/util/List;

    iget v1, p0, Lcom/cgollner/systemmonitor/historybg/e;->s:I

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/historybg/d;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/e;->r:Lcom/cgollner/systemmonitor/historybg/d;

    .line 162
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/e;->r:Lcom/cgollner/systemmonitor/historybg/d;

    invoke-direct {p0, v0}, Lcom/cgollner/systemmonitor/historybg/e;->a(Lcom/cgollner/systemmonitor/historybg/d;)V

    .line 163
    invoke-direct {p0}, Lcom/cgollner/systemmonitor/historybg/e;->a()V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 54
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 55
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/historybg/e;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "SEE_ONLY"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/cgollner/systemmonitor/historybg/e;->F:Z

    .line 56
    iget-boolean v0, p0, Lcom/cgollner/systemmonitor/historybg/e;->F:Z

    if-nez v0, :cond_0

    .line 59
    sget-object v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->h:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 61
    :cond_0
    return-void

    .line 55
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "UseSparseArrays"
        }
    .end annotation

    .prologue
    const-wide v6, 0x7fffffffffffffffL

    const/4 v4, 0x0

    const-wide/16 v2, 0x0

    .line 75
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/e;->G:Landroid/os/Handler;

    .line 76
    sget v0, Lcom/cgollner/systemmonitor/b/a$f;->io_fragment_history_layout:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/e;->b:Landroid/view/View;

    .line 78
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/e;->b:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/b/a$e;->monitorview:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/TimeMonitorView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/e;->a:Lcom/cgollner/systemmonitor/TimeMonitorView;

    .line 79
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/e;->a:Lcom/cgollner/systemmonitor/TimeMonitorView;

    invoke-virtual {v0}, Lcom/cgollner/systemmonitor/TimeMonitorView;->a()V

    .line 81
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/e;->b:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/b/a$e;->usageAvg:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/e;->o:Landroid/widget/TextView;

    .line 82
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/e;->b:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/b/a$e;->usageMin:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/e;->p:Landroid/widget/TextView;

    .line 83
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/e;->b:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/b/a$e;->usageMax:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/e;->q:Landroid/widget/TextView;

    .line 84
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/e;->b:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/b/a$e;->usageLast:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/e;->t:Landroid/widget/TextView;

    .line 86
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/e;->b:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/b/a$e;->readAvg:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/e;->u:Landroid/widget/TextView;

    .line 87
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/e;->b:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/b/a$e;->readMax:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/e;->v:Landroid/widget/TextView;

    .line 88
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/e;->b:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/b/a$e;->readMin:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/e;->w:Landroid/widget/TextView;

    .line 89
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/e;->b:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/b/a$e;->readLast:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/e;->x:Landroid/widget/TextView;

    .line 91
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/e;->b:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/b/a$e;->writeAvg:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/e;->y:Landroid/widget/TextView;

    .line 92
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/e;->b:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/b/a$e;->writeMax:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/e;->z:Landroid/widget/TextView;

    .line 93
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/e;->b:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/b/a$e;->writeMin:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/e;->A:Landroid/widget/TextView;

    .line 94
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/e;->b:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/b/a$e;->writeLast:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/e;->B:Landroid/widget/TextView;

    .line 96
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/e;->b:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/b/a$e;->totalRead:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/e;->C:Landroid/widget/TextView;

    .line 97
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/e;->b:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/b/a$e;->totalWrite:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/e;->D:Landroid/widget/TextView;

    .line 98
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/e;->b:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/b/a$e;->totalTotal:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/e;->E:Landroid/widget/TextView;

    .line 100
    iput v4, p0, Lcom/cgollner/systemmonitor/historybg/e;->c:F

    .line 101
    const/high16 v0, 0x42c80000    # 100.0f

    iput v0, p0, Lcom/cgollner/systemmonitor/historybg/e;->e:F

    .line 102
    iput v4, p0, Lcom/cgollner/systemmonitor/historybg/e;->d:F

    .line 104
    iput-wide v2, p0, Lcom/cgollner/systemmonitor/historybg/e;->f:J

    .line 105
    iput-wide v6, p0, Lcom/cgollner/systemmonitor/historybg/e;->h:J

    .line 106
    iput-wide v2, p0, Lcom/cgollner/systemmonitor/historybg/e;->g:J

    .line 108
    iput-wide v2, p0, Lcom/cgollner/systemmonitor/historybg/e;->i:J

    .line 109
    iput-wide v6, p0, Lcom/cgollner/systemmonitor/historybg/e;->k:J

    .line 110
    iput-wide v2, p0, Lcom/cgollner/systemmonitor/historybg/e;->j:J

    .line 112
    iput-wide v2, p0, Lcom/cgollner/systemmonitor/historybg/e;->l:J

    .line 113
    iput-wide v2, p0, Lcom/cgollner/systemmonitor/historybg/e;->l:J

    .line 114
    iput-wide v2, p0, Lcom/cgollner/systemmonitor/historybg/e;->n:J

    .line 116
    iget-boolean v0, p0, Lcom/cgollner/systemmonitor/historybg/e;->F:Z

    if-eqz v0, :cond_0

    .line 117
    sget-object v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->a:Lcom/cgollner/systemmonitor/historybg/a;

    iget-object v0, v0, Lcom/cgollner/systemmonitor/historybg/a;->c:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/cgollner/systemmonitor/historybg/e;->a(Ljava/util/List;)V

    .line 124
    :goto_0
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/e;->b:Landroid/view/View;

    return-object v0

    .line 120
    :cond_0
    sget-object v1, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->c:Ljava/util/List;

    monitor-enter v1

    .line 121
    :try_start_0
    sget-object v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->c:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/cgollner/systemmonitor/historybg/e;->a(Ljava/util/List;)V

    .line 122
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 65
    iget-boolean v0, p0, Lcom/cgollner/systemmonitor/historybg/e;->F:Z

    if-nez v0, :cond_0

    .line 66
    sget-object v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->h:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 68
    :cond_0
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    .line 69
    return-void
.end method
