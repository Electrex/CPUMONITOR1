.class final Lcom/cgollner/systemmonitor/historybg/HistoryBgService$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/cgollner/systemmonitor/c/e$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cgollner/systemmonitor/historybg/HistoryBgService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/cgollner/systemmonitor/historybg/HistoryBgService;


# direct methods
.method constructor <init>(Lcom/cgollner/systemmonitor/historybg/HistoryBgService;)V
    .locals 0

    .prologue
    .line 163
    iput-object p1, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService$1;->a:Lcom/cgollner/systemmonitor/historybg/HistoryBgService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 12

    .prologue
    const/4 v5, 0x1

    .line 165
    new-instance v6, Lcom/cgollner/systemmonitor/historybg/b;

    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService$1;->a:Lcom/cgollner/systemmonitor/historybg/HistoryBgService;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->a(Lcom/cgollner/systemmonitor/historybg/HistoryBgService;)Lcom/cgollner/systemmonitor/c/a;

    move-result-object v0

    iget-object v0, v0, Lcom/cgollner/systemmonitor/c/a;->b:[I

    invoke-virtual {v0}, [I->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I

    iget-object v1, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService$1;->a:Lcom/cgollner/systemmonitor/historybg/HistoryBgService;

    invoke-static {v1}, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->a(Lcom/cgollner/systemmonitor/historybg/HistoryBgService;)Lcom/cgollner/systemmonitor/c/a;

    move-result-object v1

    iget-object v1, v1, Lcom/cgollner/systemmonitor/c/a;->c:[I

    invoke-virtual {v1}, [I->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [I

    iget-object v2, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService$1;->a:Lcom/cgollner/systemmonitor/historybg/HistoryBgService;

    invoke-static {v2}, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->a(Lcom/cgollner/systemmonitor/historybg/HistoryBgService;)Lcom/cgollner/systemmonitor/c/a;

    move-result-object v2

    iget-object v2, v2, Lcom/cgollner/systemmonitor/c/a;->d:[I

    invoke-virtual {v2}, [I->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [I

    iget-object v3, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService$1;->a:Lcom/cgollner/systemmonitor/historybg/HistoryBgService;

    invoke-static {v3}, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->a(Lcom/cgollner/systemmonitor/historybg/HistoryBgService;)Lcom/cgollner/systemmonitor/c/a;

    move-result-object v3

    iget-object v3, v3, Lcom/cgollner/systemmonitor/c/a;->a:[F

    invoke-virtual {v3}, [F->clone()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [F

    invoke-direct {v6, v0, v1, v2, v3}, Lcom/cgollner/systemmonitor/historybg/b;-><init>([I[I[I[F)V

    .line 171
    const/4 v1, 0x0

    .line 172
    sget-object v7, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->a:Ljava/util/List;

    monitor-enter v7

    .line 173
    :try_start_0
    sget-object v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 174
    sget-object v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->a:Ljava/util/List;

    sget-object v2, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/historybg/b;

    .line 176
    iget-wide v2, v0, Lcom/cgollner/systemmonitor/historybg/b;->e:J

    .line 177
    iget-wide v8, v6, Lcom/cgollner/systemmonitor/historybg/b;->e:J

    iget-wide v10, v0, Lcom/cgollner/systemmonitor/historybg/b;->e:J

    sub-long/2addr v8, v10

    .line 178
    sget-wide v10, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->j:J

    div-long/2addr v8, v10

    move v4, v5

    move v0, v1

    .line 179
    :goto_0
    int-to-long v10, v4

    cmp-long v1, v10, v8

    if-gez v1, :cond_0

    .line 180
    sget-wide v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->j:J

    add-long/2addr v0, v2

    .line 181
    sget-object v2, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->a:Ljava/util/List;

    new-instance v3, Lcom/cgollner/systemmonitor/historybg/b;

    invoke-direct {v3, v0, v1}, Lcom/cgollner/systemmonitor/historybg/b;-><init>(J)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 179
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    move-wide v2, v0

    move v0, v5

    goto :goto_0

    :cond_0
    move v1, v0

    .line 185
    :cond_1
    sget-object v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->a:Ljava/util/List;

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 186
    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 188
    sget-object v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService$a;

    .line 189
    invoke-interface {v0, v1}, Lcom/cgollner/systemmonitor/historybg/HistoryBgService$a;->a(Z)V

    goto :goto_1

    .line 186
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 191
    :cond_2
    return-void
.end method
