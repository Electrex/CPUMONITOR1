.class public Lcom/cgollner/systemmonitor/historybg/HistoryBgService;
.super Landroid/app/Service;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/cgollner/systemmonitor/historybg/HistoryBgService$a;
    }
.end annotation


# static fields
.field public static a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/cgollner/systemmonitor/historybg/b;",
            ">;"
        }
    .end annotation
.end field

.field public static b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/cgollner/systemmonitor/historybg/h;",
            ">;"
        }
    .end annotation
.end field

.field public static c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/cgollner/systemmonitor/historybg/d;",
            ">;"
        }
    .end annotation
.end field

.field public static d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/cgollner/systemmonitor/historybg/f;",
            ">;"
        }
    .end annotation
.end field

.field public static e:Lcom/cgollner/systemmonitor/historybg/a;

.field public static f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/cgollner/systemmonitor/historybg/HistoryBgService$a;",
            ">;"
        }
    .end annotation
.end field

.field public static g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/cgollner/systemmonitor/historybg/HistoryBgService$a;",
            ">;"
        }
    .end annotation
.end field

.field public static h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/cgollner/systemmonitor/historybg/HistoryBgService$a;",
            ">;"
        }
    .end annotation
.end field

.field public static i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/cgollner/systemmonitor/historybg/HistoryBgService$a;",
            ">;"
        }
    .end annotation
.end field

.field public static j:J


# instance fields
.field private k:Lcom/cgollner/systemmonitor/c/a;

.field private l:Lcom/cgollner/systemmonitor/c/g;

.field private m:Lcom/cgollner/systemmonitor/c/d;

.field private n:Lcom/cgollner/systemmonitor/c/f;

.field private o:Landroid/app/Notification;

.field private p:Landroid/support/v4/app/NotificationCompat$Builder;

.field private q:Lcom/cgollner/systemmonitor/c/e$a;

.field private r:Lcom/cgollner/systemmonitor/c/e$a;

.field private s:Lcom/cgollner/systemmonitor/c/e$a;

.field private t:Lcom/cgollner/systemmonitor/c/e$a;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 55
    const-wide/16 v0, 0x1388

    sput-wide v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->j:J

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 163
    new-instance v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService$1;

    invoke-direct {v0, p0}, Lcom/cgollner/systemmonitor/historybg/HistoryBgService$1;-><init>(Lcom/cgollner/systemmonitor/historybg/HistoryBgService;)V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->q:Lcom/cgollner/systemmonitor/c/e$a;

    .line 193
    new-instance v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService$2;

    invoke-direct {v0, p0}, Lcom/cgollner/systemmonitor/historybg/HistoryBgService$2;-><init>(Lcom/cgollner/systemmonitor/historybg/HistoryBgService;)V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->r:Lcom/cgollner/systemmonitor/c/e$a;

    .line 211
    new-instance v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService$3;

    invoke-direct {v0, p0}, Lcom/cgollner/systemmonitor/historybg/HistoryBgService$3;-><init>(Lcom/cgollner/systemmonitor/historybg/HistoryBgService;)V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->s:Lcom/cgollner/systemmonitor/c/e$a;

    .line 241
    new-instance v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService$4;

    invoke-direct {v0, p0}, Lcom/cgollner/systemmonitor/historybg/HistoryBgService$4;-><init>(Lcom/cgollner/systemmonitor/historybg/HistoryBgService;)V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->t:Lcom/cgollner/systemmonitor/c/e$a;

    return-void
.end method

.method static synthetic a(Lcom/cgollner/systemmonitor/historybg/HistoryBgService;)Lcom/cgollner/systemmonitor/c/a;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->k:Lcom/cgollner/systemmonitor/c/a;

    return-object v0
.end method

.method public static a(JJLandroid/content/Context;)V
    .locals 6

    .prologue
    const/high16 v5, 0x8000000

    .line 326
    const-string v0, "alarm"

    invoke-virtual {p4, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 327
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;

    invoke-direct {v1, p4, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 328
    long-to-int v2, p0

    invoke-static {p4, v2, v1, v5}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 330
    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;

    invoke-direct {v2, p4, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 331
    const-string v3, "EXTRA_FINISH"

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 332
    long-to-int v3, p2

    invoke-static {p4, v3, v2, v5}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    .line 334
    invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 335
    invoke-virtual {v0, v2}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 336
    return-void
.end method

.method public static a(JJLjava/lang/String;Landroid/content/Context;)V
    .locals 8

    .prologue
    const/high16 v6, 0x8000000

    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 308
    const-string v0, "schedules"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "\""

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {p5, v0, v1, v2}, Lcom/cgollner/systemmonitor/battery/BatteryService;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    .line 311
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;

    invoke-direct {v0, p5, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 312
    long-to-int v1, p0

    invoke-static {p5, v1, v0, v6}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 314
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;

    invoke-direct {v0, p5, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 315
    const-string v2, "EXTRA_FINISH"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 316
    const-string v2, "SCHEDULE_NAME"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "\""

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "-"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 317
    const-string v2, "EXTRA_FINISH_TIME"

    invoke-virtual {v0, v2, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 318
    long-to-int v2, p2

    invoke-static {p5, v2, v0, v6}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    .line 320
    const-string v0, "alarm"

    invoke-virtual {p5, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 321
    invoke-virtual {v0, v5, p0, p1, v1}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    .line 322
    invoke-virtual {v0, v5, p2, p3, v2}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    .line 323
    return-void
.end method

.method static synthetic b(Lcom/cgollner/systemmonitor/historybg/HistoryBgService;)Lcom/cgollner/systemmonitor/c/g;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->l:Lcom/cgollner/systemmonitor/c/g;

    return-object v0
.end method

.method static synthetic c(Lcom/cgollner/systemmonitor/historybg/HistoryBgService;)Lcom/cgollner/systemmonitor/c/d;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->m:Lcom/cgollner/systemmonitor/c/d;

    return-object v0
.end method

.method static synthetic d(Lcom/cgollner/systemmonitor/historybg/HistoryBgService;)Lcom/cgollner/systemmonitor/c/f;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->n:Lcom/cgollner/systemmonitor/c/f;

    return-object v0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 279
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 79
    sget-object v0, Lcom/cgollner/systemmonitor/App;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 80
    sget-object v1, Lcom/cgollner/systemmonitor/App;->a:Landroid/content/Context;

    sget v2, Lcom/cgollner/systemmonitor/b/a$h;->history_bg_status_key:I

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, v6}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 81
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 82
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 84
    sget-object v0, Lcom/cgollner/systemmonitor/App;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    sget-object v1, Lcom/cgollner/systemmonitor/App;->a:Landroid/content/Context;

    sget v2, Lcom/cgollner/systemmonitor/b/a$h;->history_bg_update_interval_key:I

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x5

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    mul-int/lit16 v0, v0, 0x3e8

    int-to-long v0, v0

    sput-wide v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->j:J

    .line 86
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    sput-object v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->f:Ljava/util/List;

    .line 87
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    sput-object v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->g:Ljava/util/List;

    .line 88
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    sput-object v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->h:Ljava/util/List;

    .line 89
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    sput-object v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->i:Ljava/util/List;

    .line 91
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    sput-object v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->a:Ljava/util/List;

    .line 92
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    sput-object v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->b:Ljava/util/List;

    .line 93
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    sput-object v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->c:Ljava/util/List;

    .line 94
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    sput-object v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->d:Ljava/util/List;

    .line 96
    new-instance v0, Lcom/cgollner/systemmonitor/historybg/a;

    sget-object v1, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->a:Ljava/util/List;

    sget-object v2, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->b:Ljava/util/List;

    sget-object v3, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->c:Ljava/util/List;

    sget-object v4, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->d:Ljava/util/List;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/cgollner/systemmonitor/historybg/a;-><init>(Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V

    sput-object v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->e:Lcom/cgollner/systemmonitor/historybg/a;

    .line 97
    new-instance v0, Lcom/cgollner/systemmonitor/c/a;

    sget-wide v2, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->j:J

    iget-object v1, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->q:Lcom/cgollner/systemmonitor/c/e$a;

    invoke-direct {v0, v2, v3, v1}, Lcom/cgollner/systemmonitor/c/a;-><init>(JLcom/cgollner/systemmonitor/c/e$a;)V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->k:Lcom/cgollner/systemmonitor/c/a;

    new-instance v0, Lcom/cgollner/systemmonitor/c/g;

    sget-wide v2, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->j:J

    iget-object v1, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->r:Lcom/cgollner/systemmonitor/c/e$a;

    sget-object v4, Lcom/cgollner/systemmonitor/App;->a:Landroid/content/Context;

    invoke-direct {v0, v2, v3, v1, v4}, Lcom/cgollner/systemmonitor/c/g;-><init>(JLcom/cgollner/systemmonitor/c/e$a;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->l:Lcom/cgollner/systemmonitor/c/g;

    new-instance v0, Lcom/cgollner/systemmonitor/c/d;

    sget-wide v2, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->j:J

    iget-object v1, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->s:Lcom/cgollner/systemmonitor/c/e$a;

    invoke-direct {v0, v2, v3, v1}, Lcom/cgollner/systemmonitor/c/d;-><init>(JLcom/cgollner/systemmonitor/c/e$a;)V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->m:Lcom/cgollner/systemmonitor/c/d;

    new-instance v0, Lcom/cgollner/systemmonitor/c/f;

    sget-wide v2, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->j:J

    iget-object v1, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->t:Lcom/cgollner/systemmonitor/c/e$a;

    invoke-direct {v0, v2, v3, v1}, Lcom/cgollner/systemmonitor/c/f;-><init>(JLcom/cgollner/systemmonitor/c/e$a;)V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->n:Lcom/cgollner/systemmonitor/c/f;

    .line 99
    new-instance v0, Landroid/content/Intent;

    sget-object v1, Lcom/cgollner/systemmonitor/App;->a:Landroid/content/Context;

    const-class v2, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 100
    sget-object v1, Lcom/cgollner/systemmonitor/App;->a:Landroid/content/Context;

    const/high16 v2, 0x8000000

    invoke-static {v1, v5, v0, v2}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 102
    new-instance v1, Landroid/support/v4/app/NotificationCompat$Builder;

    sget-object v2, Lcom/cgollner/systemmonitor/App;->a:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/support/v4/app/NotificationCompat$Builder;-><init>(Landroid/content/Context;)V

    sget-object v2, Lcom/cgollner/systemmonitor/App;->a:Landroid/content/Context;

    sget v3, Lcom/cgollner/systemmonitor/b/a$h;->history_bg_service_is_running:I

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v1

    sget-object v2, Lcom/cgollner/systemmonitor/App;->a:Landroid/content/Context;

    sget v3, Lcom/cgollner/systemmonitor/b/a$h;->history_bg_service_see_progress:I

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v1

    sget-object v2, Lcom/cgollner/systemmonitor/App;->a:Landroid/content/Context;

    sget v3, Lcom/cgollner/systemmonitor/b/a$h;->history_bg_service_is_running:I

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v4/app/NotificationCompat$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v1

    sget v2, Lcom/cgollner/systemmonitor/b/a$d;->ic_stat_iconstatus:I

    invoke-virtual {v1, v2}, Landroid/support/v4/app/NotificationCompat$Builder;->setSmallIcon(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/support/v4/app/NotificationCompat$Builder;->setAutoCancel(Z)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v1

    const-wide/16 v2, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/support/v4/app/NotificationCompat$Builder;->setWhen(J)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/support/v4/app/NotificationCompat$Builder;->setOngoing(Z)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->p:Landroid/support/v4/app/NotificationCompat$Builder;

    .line 112
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->p:Landroid/support/v4/app/NotificationCompat$Builder;

    invoke-virtual {v0}, Landroid/support/v4/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object v0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->o:Landroid/app/Notification;

    .line 113
    const/16 v0, 0x26c0

    iget-object v1, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->o:Landroid/app/Notification;

    invoke-virtual {p0, v0, v1}, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->startForeground(ILandroid/app/Notification;)V

    .line 114
    return-void
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    .line 64
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->stopForeground(Z)V

    .line 65
    sget-object v0, Lcom/cgollner/systemmonitor/App;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 66
    sget-object v1, Lcom/cgollner/systemmonitor/App;->a:Landroid/content/Context;

    sget v2, Lcom/cgollner/systemmonitor/b/a$h;->history_bg_status_key:I

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 67
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 68
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->k:Lcom/cgollner/systemmonitor/c/a;

    invoke-virtual {v0}, Lcom/cgollner/systemmonitor/c/a;->c()V

    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->l:Lcom/cgollner/systemmonitor/c/g;

    invoke-virtual {v0}, Lcom/cgollner/systemmonitor/c/g;->c()V

    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->m:Lcom/cgollner/systemmonitor/c/d;

    invoke-virtual {v0}, Lcom/cgollner/systemmonitor/c/d;->c()V

    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->n:Lcom/cgollner/systemmonitor/c/f;

    invoke-virtual {v0}, Lcom/cgollner/systemmonitor/c/f;->c()V

    .line 70
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 71
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 14

    .prologue
    .line 131
    if-eqz p1, :cond_4

    const-string v0, "EXTRA_FINISH"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 132
    const-string v0, "SCHEDULE_NAME"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 133
    sget-object v11, Lcom/cgollner/systemmonitor/App;->a:Landroid/content/Context;

    sget-object v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_2

    sget-object v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->a:Ljava/util/List;

    sget-object v1, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/historybg/b;

    iget-wide v2, v0, Lcom/cgollner/systemmonitor/historybg/b;->e:J

    invoke-static {}, Ljava/util/GregorianCalendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v8

    sub-long v0, v8, v2

    sget-wide v4, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->j:J

    div-long v4, v0, v4

    const-wide/16 v0, 0x1

    cmp-long v0, v4, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const/4 v1, 0x1

    :goto_1
    int-to-long v6, v1

    cmp-long v6, v6, v4

    if-gez v6, :cond_1

    sget-wide v6, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->j:J

    add-long/2addr v2, v6

    sget-object v6, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->a:Ljava/util/List;

    new-instance v7, Lcom/cgollner/systemmonitor/historybg/b;

    invoke-direct {v7, v2, v3}, Lcom/cgollner/systemmonitor/historybg/b;-><init>(J)V

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v6, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->c:Ljava/util/List;

    new-instance v7, Lcom/cgollner/systemmonitor/historybg/d;

    sget-wide v12, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->j:J

    invoke-direct {v7, v2, v3, v12, v13}, Lcom/cgollner/systemmonitor/historybg/d;-><init>(JJ)V

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v6, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->d:Ljava/util/List;

    new-instance v7, Lcom/cgollner/systemmonitor/historybg/f;

    sget-wide v12, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->j:J

    invoke-direct {v7, v2, v3, v12, v13}, Lcom/cgollner/systemmonitor/historybg/f;-><init>(JJ)V

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    if-eqz v0, :cond_2

    sget-object v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->b:Ljava/util/List;

    sget-object v1, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/cgollner/systemmonitor/historybg/h;

    sget-object v12, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->b:Ljava/util/List;

    new-instance v0, Lcom/cgollner/systemmonitor/historybg/h;

    iget v1, v6, Lcom/cgollner/systemmonitor/historybg/h;->a:F

    iget-wide v2, v6, Lcom/cgollner/systemmonitor/historybg/h;->c:J

    iget-wide v4, v6, Lcom/cgollner/systemmonitor/historybg/h;->b:J

    iget-wide v6, v6, Lcom/cgollner/systemmonitor/historybg/h;->d:J

    invoke-direct/range {v0 .. v9}, Lcom/cgollner/systemmonitor/historybg/h;-><init>(FJJJJ)V

    invoke-interface {v12, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    const-string v0, "history"

    sget-object v1, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->e:Lcom/cgollner/systemmonitor/historybg/a;

    invoke-static {v11, v0, v10, v1}, Lcom/cgollner/systemmonitor/battery/BatteryService;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    .line 134
    sget-object v0, Lcom/cgollner/systemmonitor/App;->a:Landroid/content/Context;

    const-string v1, "schedules"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v0

    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    :goto_2
    if-ge v0, v3, :cond_6

    aget-object v4, v2, v0

    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    const/16 v6, 0x22

    invoke-virtual {v4, v6}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    const/16 v6, 0x2d

    invoke-virtual {v5, v6}, Ljava/lang/String;->indexOf(I)I

    move-result v6

    invoke-virtual {v5, v4, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    new-instance v6, Ljava/util/Date;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-direct {v6, v8, v9}, Ljava/util/Date;-><init>(J)V

    const/16 v4, 0x2d

    invoke-virtual {v5, v4}, Ljava/lang/String;->indexOf(I)I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    invoke-virtual {v5, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    new-instance v5, Ljava/util/Date;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-direct {v5, v8, v9}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v6, v1}, Ljava/util/Date;->before(Ljava/util/Date;)Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-virtual {v5, v1}, Ljava/util/Date;->after(Ljava/util/Date;)Z

    move-result v4

    if-eqz v4, :cond_5

    const/4 v0, 0x0

    :goto_3
    if-eqz v0, :cond_3

    .line 135
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->stopSelf()V

    .line 137
    :cond_3
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 138
    const-string v1, "SEE_ONLY"

    invoke-virtual {v0, v1, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 140
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "EXTRA_FINISH_TIME"

    const-wide/16 v4, 0x1

    invoke-virtual {p1, v2, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    long-to-int v2, v2

    const/high16 v3, 0x8000000

    invoke-static {v1, v2, v0, v3}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 146
    const/4 v1, 0x1

    const/16 v2, 0x22

    invoke-virtual {v10, v2}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v2

    invoke-virtual {v10, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 147
    iget-object v2, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->p:Landroid/support/v4/app/NotificationCompat$Builder;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lcom/cgollner/systemmonitor/App;->a:Landroid/content/Context;

    sget v5, Lcom/cgollner/systemmonitor/b/a$h;->history_bg_service_is_finished:I

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/support/v4/app/NotificationCompat$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v3, Lcom/cgollner/systemmonitor/App;->a:Landroid/content/Context;

    sget v4, Lcom/cgollner/systemmonitor/b/a$h;->history_bg_service_is_finished:I

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->setOngoing(Z)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->setAutoCancel(Z)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 155
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->p:Landroid/support/v4/app/NotificationCompat$Builder;

    invoke-virtual {v0}, Landroid/support/v4/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object v1

    .line 156
    const-string v0, "notification"

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 157
    const/16 v2, 0xc

    invoke-virtual {v0, v10, v2, v1}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    .line 160
    :cond_4
    const/4 v0, 0x1

    return v0

    .line 134
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_2

    :cond_6
    const/4 v0, 0x1

    goto/16 :goto_3
.end method
