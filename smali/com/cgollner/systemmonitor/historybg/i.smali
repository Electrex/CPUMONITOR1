.class public Lcom/cgollner/systemmonitor/historybg/i;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"

# interfaces
.implements Lcom/cgollner/systemmonitor/historybg/HistoryBgService$a;


# instance fields
.field private a:Lcom/cgollner/systemmonitor/TimeMonitorView;

.field private b:Landroid/view/View;

.field private c:F

.field private d:F

.field private e:F

.field private f:Landroid/widget/TextView;

.field private g:Landroid/widget/TextView;

.field private h:Landroid/widget/TextView;

.field private i:Lcom/cgollner/systemmonitor/historybg/h;

.field private j:I

.field private k:Landroid/widget/TextView;

.field private l:Z

.field private m:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/cgollner/systemmonitor/historybg/i;)F
    .locals 1

    .prologue
    .line 21
    iget v0, p0, Lcom/cgollner/systemmonitor/historybg/i;->c:F

    return v0
.end method

.method private a()V
    .locals 2

    .prologue
    .line 112
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/i;->m:Landroid/os/Handler;

    new-instance v1, Lcom/cgollner/systemmonitor/historybg/i$1;

    invoke-direct {v1, p0}, Lcom/cgollner/systemmonitor/historybg/i$1;-><init>(Lcom/cgollner/systemmonitor/historybg/i;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 125
    return-void
.end method

.method private a(Lcom/cgollner/systemmonitor/historybg/h;)V
    .locals 5

    .prologue
    .line 105
    iget v0, p0, Lcom/cgollner/systemmonitor/historybg/i;->c:F

    iget v1, p1, Lcom/cgollner/systemmonitor/historybg/h;->a:F

    add-float/2addr v0, v1

    iput v0, p0, Lcom/cgollner/systemmonitor/historybg/i;->c:F

    .line 106
    iget v0, p0, Lcom/cgollner/systemmonitor/historybg/i;->e:F

    iget v1, p1, Lcom/cgollner/systemmonitor/historybg/h;->a:F

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    iput v0, p0, Lcom/cgollner/systemmonitor/historybg/i;->e:F

    .line 107
    iget v0, p0, Lcom/cgollner/systemmonitor/historybg/i;->d:F

    iget v1, p1, Lcom/cgollner/systemmonitor/historybg/h;->a:F

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iput v0, p0, Lcom/cgollner/systemmonitor/historybg/i;->d:F

    .line 108
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/i;->a:Lcom/cgollner/systemmonitor/TimeMonitorView;

    new-instance v1, Lcom/cgollner/systemmonitor/battery/c;

    iget-wide v2, p1, Lcom/cgollner/systemmonitor/historybg/h;->e:J

    iget v4, p1, Lcom/cgollner/systemmonitor/historybg/h;->a:F

    invoke-direct {v1, v2, v3, v4}, Lcom/cgollner/systemmonitor/battery/c;-><init>(JF)V

    invoke-virtual {v0, v1}, Lcom/cgollner/systemmonitor/TimeMonitorView;->a(Lcom/cgollner/systemmonitor/battery/c;)V

    .line 110
    return-void
.end method

.method private a(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/cgollner/systemmonitor/historybg/h;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 85
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    iput v0, p0, Lcom/cgollner/systemmonitor/historybg/i;->j:I

    .line 86
    iget v0, p0, Lcom/cgollner/systemmonitor/historybg/i;->j:I

    if-nez v0, :cond_0

    .line 94
    :goto_0
    return-void

    .line 88
    :cond_0
    iget v0, p0, Lcom/cgollner/systemmonitor/historybg/i;->j:I

    add-int/lit8 v0, v0, -0x1

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/historybg/h;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/i;->i:Lcom/cgollner/systemmonitor/historybg/h;

    .line 90
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/historybg/h;

    .line 91
    invoke-direct {p0, v0}, Lcom/cgollner/systemmonitor/historybg/i;->a(Lcom/cgollner/systemmonitor/historybg/h;)V

    goto :goto_1

    .line 93
    :cond_1
    invoke-direct {p0}, Lcom/cgollner/systemmonitor/historybg/i;->a()V

    goto :goto_0
.end method

.method static synthetic b(Lcom/cgollner/systemmonitor/historybg/i;)I
    .locals 1

    .prologue
    .line 21
    iget v0, p0, Lcom/cgollner/systemmonitor/historybg/i;->j:I

    return v0
.end method

.method static synthetic c(Lcom/cgollner/systemmonitor/historybg/i;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/i;->f:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic d(Lcom/cgollner/systemmonitor/historybg/i;)F
    .locals 1

    .prologue
    .line 21
    iget v0, p0, Lcom/cgollner/systemmonitor/historybg/i;->d:F

    return v0
.end method

.method static synthetic e(Lcom/cgollner/systemmonitor/historybg/i;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/i;->h:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic f(Lcom/cgollner/systemmonitor/historybg/i;)F
    .locals 1

    .prologue
    .line 21
    iget v0, p0, Lcom/cgollner/systemmonitor/historybg/i;->e:F

    return v0
.end method

.method static synthetic g(Lcom/cgollner/systemmonitor/historybg/i;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/i;->g:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic h(Lcom/cgollner/systemmonitor/historybg/i;)Lcom/cgollner/systemmonitor/historybg/h;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/i;->i:Lcom/cgollner/systemmonitor/historybg/h;

    return-object v0
.end method

.method static synthetic i(Lcom/cgollner/systemmonitor/historybg/i;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/i;->k:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic j(Lcom/cgollner/systemmonitor/historybg/i;)Lcom/cgollner/systemmonitor/TimeMonitorView;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/i;->a:Lcom/cgollner/systemmonitor/TimeMonitorView;

    return-object v0
.end method


# virtual methods
.method public final a(Z)V
    .locals 2

    .prologue
    .line 98
    sget-object v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iput v0, p0, Lcom/cgollner/systemmonitor/historybg/i;->j:I

    .line 99
    sget-object v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->b:Ljava/util/List;

    iget v1, p0, Lcom/cgollner/systemmonitor/historybg/i;->j:I

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/historybg/h;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/i;->i:Lcom/cgollner/systemmonitor/historybg/h;

    .line 100
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/i;->i:Lcom/cgollner/systemmonitor/historybg/h;

    invoke-direct {p0, v0}, Lcom/cgollner/systemmonitor/historybg/i;->a(Lcom/cgollner/systemmonitor/historybg/h;)V

    .line 101
    invoke-direct {p0}, Lcom/cgollner/systemmonitor/historybg/i;->a()V

    .line 102
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 38
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 39
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/historybg/i;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "SEE_ONLY"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/cgollner/systemmonitor/historybg/i;->l:Z

    .line 40
    iget-boolean v0, p0, Lcom/cgollner/systemmonitor/historybg/i;->l:Z

    if-nez v0, :cond_0

    .line 43
    sget-object v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->g:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 45
    :cond_0
    return-void

    .line 39
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "UseSparseArrays"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 59
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/i;->m:Landroid/os/Handler;

    .line 60
    sget v0, Lcom/cgollner/systemmonitor/b/a$f;->ram_fragment_history_layout:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/i;->b:Landroid/view/View;

    .line 62
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/i;->b:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/b/a$e;->monitorview:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/TimeMonitorView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/i;->a:Lcom/cgollner/systemmonitor/TimeMonitorView;

    .line 63
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/i;->a:Lcom/cgollner/systemmonitor/TimeMonitorView;

    invoke-virtual {v0}, Lcom/cgollner/systemmonitor/TimeMonitorView;->a()V

    .line 65
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/i;->b:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/b/a$e;->usageAvg:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/i;->f:Landroid/widget/TextView;

    .line 66
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/i;->b:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/b/a$e;->usageMin:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/i;->g:Landroid/widget/TextView;

    .line 67
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/i;->b:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/b/a$e;->usageMax:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/i;->h:Landroid/widget/TextView;

    .line 68
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/i;->b:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/b/a$e;->usageLast:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/i;->k:Landroid/widget/TextView;

    .line 70
    iput v2, p0, Lcom/cgollner/systemmonitor/historybg/i;->c:F

    .line 71
    const/high16 v0, 0x42c80000    # 100.0f

    iput v0, p0, Lcom/cgollner/systemmonitor/historybg/i;->e:F

    .line 72
    iput v2, p0, Lcom/cgollner/systemmonitor/historybg/i;->d:F

    .line 74
    iget-boolean v0, p0, Lcom/cgollner/systemmonitor/historybg/i;->l:Z

    if-eqz v0, :cond_0

    .line 75
    sget-object v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->a:Lcom/cgollner/systemmonitor/historybg/a;

    iget-object v0, v0, Lcom/cgollner/systemmonitor/historybg/a;->b:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/cgollner/systemmonitor/historybg/i;->a(Ljava/util/List;)V

    .line 82
    :goto_0
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/i;->b:Landroid/view/View;

    return-object v0

    .line 78
    :cond_0
    sget-object v1, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->b:Ljava/util/List;

    monitor-enter v1

    .line 79
    :try_start_0
    sget-object v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->b:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/cgollner/systemmonitor/historybg/i;->a(Ljava/util/List;)V

    .line 80
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 49
    iget-boolean v0, p0, Lcom/cgollner/systemmonitor/historybg/i;->l:Z

    if-nez v0, :cond_0

    .line 50
    sget-object v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->g:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 52
    :cond_0
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    .line 53
    return-void
.end method
