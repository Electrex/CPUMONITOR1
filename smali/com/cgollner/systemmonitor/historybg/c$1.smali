.class final Lcom/cgollner/systemmonitor/historybg/c$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/cgollner/systemmonitor/historybg/c;->a()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/cgollner/systemmonitor/historybg/c;


# direct methods
.method constructor <init>(Lcom/cgollner/systemmonitor/historybg/c;)V
    .locals 0

    .prologue
    .line 153
    iput-object p1, p0, Lcom/cgollner/systemmonitor/historybg/c$1;->a:Lcom/cgollner/systemmonitor/historybg/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 172
    .line 174
    iget-object v1, p0, Lcom/cgollner/systemmonitor/historybg/c$1;->a:Lcom/cgollner/systemmonitor/historybg/c;

    invoke-static {v1}, Lcom/cgollner/systemmonitor/historybg/c;->q(Lcom/cgollner/systemmonitor/historybg/c;)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v2, v0

    move v3, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 175
    iget-object v1, p0, Lcom/cgollner/systemmonitor/historybg/c$1;->a:Lcom/cgollner/systemmonitor/historybg/c;

    invoke-static {v1}, Lcom/cgollner/systemmonitor/historybg/c;->q(Lcom/cgollner/systemmonitor/historybg/c;)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 176
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v5

    if-le v5, v3, :cond_1

    .line 177
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 178
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :goto_1
    move v2, v0

    move v3, v1

    .line 180
    goto :goto_0

    .line 181
    :cond_0
    return v2

    :cond_1
    move v0, v2

    move v1, v3

    goto :goto_1
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 155
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/c$1;->a:Lcom/cgollner/systemmonitor/historybg/c;

    invoke-virtual {v0}, Lcom/cgollner/systemmonitor/historybg/c;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/c$1;->a:Lcom/cgollner/systemmonitor/historybg/c;

    invoke-virtual {v0}, Lcom/cgollner/systemmonitor/historybg/c;->isDetached()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 169
    :cond_0
    :goto_0
    return-void

    .line 157
    :cond_1
    invoke-direct {p0}, Lcom/cgollner/systemmonitor/historybg/c$1;->a()I

    move-result v0

    .line 158
    iget-object v1, p0, Lcom/cgollner/systemmonitor/historybg/c$1;->a:Lcom/cgollner/systemmonitor/historybg/c;

    invoke-static {v1}, Lcom/cgollner/systemmonitor/historybg/c;->a(Lcom/cgollner/systemmonitor/historybg/c;)Landroid/widget/TextView;

    move-result-object v1

    if-gtz v0, :cond_2

    const-string v0, "Offline"

    :goto_1
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 159
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/c$1;->a:Lcom/cgollner/systemmonitor/historybg/c;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/historybg/c;->c(Lcom/cgollner/systemmonitor/historybg/c;)Landroid/widget/TextView;

    move-result-object v1

    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/c$1;->a:Lcom/cgollner/systemmonitor/historybg/c;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/historybg/c;->b(Lcom/cgollner/systemmonitor/historybg/c;)I

    move-result v0

    if-gtz v0, :cond_3

    const-string v0, "Offline"

    :goto_2
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 160
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/c$1;->a:Lcom/cgollner/systemmonitor/historybg/c;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/historybg/c;->e(Lcom/cgollner/systemmonitor/historybg/c;)Landroid/widget/TextView;

    move-result-object v1

    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/c$1;->a:Lcom/cgollner/systemmonitor/historybg/c;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/historybg/c;->d(Lcom/cgollner/systemmonitor/historybg/c;)I

    move-result v0

    if-gtz v0, :cond_4

    const-string v0, "Offline"

    :goto_3
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 161
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/c$1;->a:Lcom/cgollner/systemmonitor/historybg/c;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/historybg/c;->g(Lcom/cgollner/systemmonitor/historybg/c;)Landroid/widget/TextView;

    move-result-object v1

    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/c$1;->a:Lcom/cgollner/systemmonitor/historybg/c;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/historybg/c;->f(Lcom/cgollner/systemmonitor/historybg/c;)Lcom/cgollner/systemmonitor/historybg/b;

    move-result-object v0

    iget-object v0, v0, Lcom/cgollner/systemmonitor/historybg/b;->a:[I

    aget v0, v0, v4

    if-gtz v0, :cond_5

    const-string v0, "Offline"

    :goto_4
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 163
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/c$1;->a:Lcom/cgollner/systemmonitor/historybg/c;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/historybg/c;->j(Lcom/cgollner/systemmonitor/historybg/c;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/cgollner/systemmonitor/historybg/c$1;->a:Lcom/cgollner/systemmonitor/historybg/c;

    invoke-static {v1}, Lcom/cgollner/systemmonitor/historybg/c;->h(Lcom/cgollner/systemmonitor/historybg/c;)F

    move-result v1

    iget-object v2, p0, Lcom/cgollner/systemmonitor/historybg/c$1;->a:Lcom/cgollner/systemmonitor/historybg/c;

    invoke-static {v2}, Lcom/cgollner/systemmonitor/historybg/c;->i(Lcom/cgollner/systemmonitor/historybg/c;)I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    invoke-static {v1}, Lcom/cgollner/systemmonitor/a/i;->c(F)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 164
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/c$1;->a:Lcom/cgollner/systemmonitor/historybg/c;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/historybg/c;->l(Lcom/cgollner/systemmonitor/historybg/c;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/cgollner/systemmonitor/historybg/c$1;->a:Lcom/cgollner/systemmonitor/historybg/c;

    invoke-static {v1}, Lcom/cgollner/systemmonitor/historybg/c;->k(Lcom/cgollner/systemmonitor/historybg/c;)F

    move-result v1

    invoke-static {v1}, Lcom/cgollner/systemmonitor/a/i;->c(F)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 165
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/c$1;->a:Lcom/cgollner/systemmonitor/historybg/c;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/historybg/c;->n(Lcom/cgollner/systemmonitor/historybg/c;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/cgollner/systemmonitor/historybg/c$1;->a:Lcom/cgollner/systemmonitor/historybg/c;

    invoke-static {v1}, Lcom/cgollner/systemmonitor/historybg/c;->m(Lcom/cgollner/systemmonitor/historybg/c;)F

    move-result v1

    invoke-static {v1}, Lcom/cgollner/systemmonitor/a/i;->c(F)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 166
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/c$1;->a:Lcom/cgollner/systemmonitor/historybg/c;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/historybg/c;->o(Lcom/cgollner/systemmonitor/historybg/c;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/cgollner/systemmonitor/historybg/c$1;->a:Lcom/cgollner/systemmonitor/historybg/c;

    invoke-static {v1}, Lcom/cgollner/systemmonitor/historybg/c;->f(Lcom/cgollner/systemmonitor/historybg/c;)Lcom/cgollner/systemmonitor/historybg/b;

    move-result-object v1

    iget-object v1, v1, Lcom/cgollner/systemmonitor/historybg/b;->d:[F

    aget v1, v1, v4

    invoke-static {v1}, Lcom/cgollner/systemmonitor/a/i;->c(F)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 168
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/c$1;->a:Lcom/cgollner/systemmonitor/historybg/c;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/historybg/c;->p(Lcom/cgollner/systemmonitor/historybg/c;)Lcom/cgollner/systemmonitor/TimeMonitorView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/cgollner/systemmonitor/TimeMonitorView;->invalidate()V

    goto/16 :goto_0

    .line 158
    :cond_2
    invoke-direct {p0}, Lcom/cgollner/systemmonitor/historybg/c$1;->a()I

    move-result v0

    int-to-double v2, v0

    invoke-static {v2, v3}, Lcom/cgollner/systemmonitor/a/i;->a(D)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 159
    :cond_3
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/c$1;->a:Lcom/cgollner/systemmonitor/historybg/c;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/historybg/c;->b(Lcom/cgollner/systemmonitor/historybg/c;)I

    move-result v0

    int-to-double v2, v0

    invoke-static {v2, v3}, Lcom/cgollner/systemmonitor/a/i;->a(D)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 160
    :cond_4
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/c$1;->a:Lcom/cgollner/systemmonitor/historybg/c;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/historybg/c;->d(Lcom/cgollner/systemmonitor/historybg/c;)I

    move-result v0

    int-to-double v2, v0

    invoke-static {v2, v3}, Lcom/cgollner/systemmonitor/a/i;->a(D)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_3

    .line 161
    :cond_5
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/c$1;->a:Lcom/cgollner/systemmonitor/historybg/c;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/historybg/c;->f(Lcom/cgollner/systemmonitor/historybg/c;)Lcom/cgollner/systemmonitor/historybg/b;

    move-result-object v0

    iget-object v0, v0, Lcom/cgollner/systemmonitor/historybg/b;->a:[I

    aget v0, v0, v4

    int-to-double v2, v0

    invoke-static {v2, v3}, Lcom/cgollner/systemmonitor/a/i;->a(D)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_4
.end method
