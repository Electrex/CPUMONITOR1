.class final Lcom/cgollner/systemmonitor/historybg/HistoryBgService$3;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/cgollner/systemmonitor/c/e$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cgollner/systemmonitor/historybg/HistoryBgService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/cgollner/systemmonitor/historybg/HistoryBgService;


# direct methods
.method constructor <init>(Lcom/cgollner/systemmonitor/historybg/HistoryBgService;)V
    .locals 0

    .prologue
    .line 211
    iput-object p1, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService$3;->a:Lcom/cgollner/systemmonitor/historybg/HistoryBgService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 14

    .prologue
    const/4 v8, 0x1

    .line 213
    new-instance v0, Lcom/cgollner/systemmonitor/historybg/d;

    iget-object v1, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService$3;->a:Lcom/cgollner/systemmonitor/historybg/HistoryBgService;

    invoke-static {v1}, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->c(Lcom/cgollner/systemmonitor/historybg/HistoryBgService;)Lcom/cgollner/systemmonitor/c/d;

    move-result-object v1

    iget v1, v1, Lcom/cgollner/systemmonitor/c/d;->c:F

    iget-object v2, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService$3;->a:Lcom/cgollner/systemmonitor/historybg/HistoryBgService;

    invoke-static {v2}, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->c(Lcom/cgollner/systemmonitor/historybg/HistoryBgService;)Lcom/cgollner/systemmonitor/c/d;

    move-result-object v2

    iget-wide v2, v2, Lcom/cgollner/systemmonitor/c/d;->d:J

    iget-object v4, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService$3;->a:Lcom/cgollner/systemmonitor/historybg/HistoryBgService;

    invoke-static {v4}, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->c(Lcom/cgollner/systemmonitor/historybg/HistoryBgService;)Lcom/cgollner/systemmonitor/c/d;

    move-result-object v4

    iget-wide v4, v4, Lcom/cgollner/systemmonitor/c/d;->e:J

    sget-wide v6, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->j:J

    invoke-direct/range {v0 .. v7}, Lcom/cgollner/systemmonitor/historybg/d;-><init>(FJJJ)V

    .line 219
    const/4 v2, 0x0

    .line 220
    sget-object v6, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->c:Ljava/util/List;

    monitor-enter v6

    .line 221
    :try_start_0
    sget-object v1, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 222
    sget-object v1, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->c:Ljava/util/List;

    sget-object v3, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->c:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/cgollner/systemmonitor/historybg/d;

    .line 224
    iget-wide v4, v1, Lcom/cgollner/systemmonitor/historybg/d;->d:J

    .line 225
    iget-wide v10, v0, Lcom/cgollner/systemmonitor/historybg/d;->d:J

    iget-wide v12, v1, Lcom/cgollner/systemmonitor/historybg/d;->d:J

    sub-long/2addr v10, v12

    .line 226
    sget-wide v12, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->j:J

    div-long/2addr v10, v12

    move v1, v2

    move-wide v2, v4

    move v4, v8

    .line 227
    :goto_0
    int-to-long v12, v4

    cmp-long v5, v12, v10

    if-gez v5, :cond_1

    .line 228
    sget-wide v12, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->j:J

    add-long/2addr v2, v12

    .line 229
    sget-object v1, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->c:Ljava/util/List;

    new-instance v5, Lcom/cgollner/systemmonitor/historybg/d;

    sget-wide v12, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->j:J

    invoke-direct {v5, v2, v3, v12, v13}, Lcom/cgollner/systemmonitor/historybg/d;-><init>(JJ)V

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 227
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    move v1, v8

    goto :goto_0

    :cond_0
    move v1, v2

    .line 233
    :cond_1
    sget-object v2, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->c:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 234
    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 236
    sget-object v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService$a;

    .line 237
    invoke-interface {v0, v1}, Lcom/cgollner/systemmonitor/historybg/HistoryBgService$a;->a(Z)V

    goto :goto_1

    .line 234
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 239
    :cond_2
    return-void
.end method
