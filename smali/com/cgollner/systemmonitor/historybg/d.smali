.class public final Lcom/cgollner/systemmonitor/historybg/d;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x5c9a38e378e6016dL


# instance fields
.field public a:F

.field public b:J

.field public c:J

.field public d:J

.field public e:J


# direct methods
.method public constructor <init>(FJJJ)V
    .locals 2

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    iput p1, p0, Lcom/cgollner/systemmonitor/historybg/d;->a:F

    .line 14
    iput-wide p2, p0, Lcom/cgollner/systemmonitor/historybg/d;->b:J

    .line 15
    iput-wide p4, p0, Lcom/cgollner/systemmonitor/historybg/d;->c:J

    .line 16
    invoke-static {}, Ljava/util/GregorianCalendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/cgollner/systemmonitor/historybg/d;->d:J

    .line 17
    iput-wide p6, p0, Lcom/cgollner/systemmonitor/historybg/d;->e:J

    .line 18
    return-void
.end method

.method public constructor <init>(JJ)V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-wide p1, p0, Lcom/cgollner/systemmonitor/historybg/d;->d:J

    .line 21
    iput-wide p3, p0, Lcom/cgollner/systemmonitor/historybg/d;->e:J

    .line 22
    return-void
.end method
