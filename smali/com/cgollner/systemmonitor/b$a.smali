.class public final Lcom/cgollner/systemmonitor/b$a;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cgollner/systemmonitor/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field public d:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "Landroid/support/v4/app/Fragment;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(I)V
    .locals 0

    .prologue
    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    iput p1, p0, Lcom/cgollner/systemmonitor/b$a;->c:I

    .line 72
    return-void
.end method

.method public constructor <init>(IIILjava/lang/Class;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(III",
            "Ljava/lang/Class",
            "<+",
            "Landroid/support/v4/app/Fragment;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    iput p1, p0, Lcom/cgollner/systemmonitor/b$a;->c:I

    .line 65
    iput p2, p0, Lcom/cgollner/systemmonitor/b$a;->a:I

    .line 66
    iput p3, p0, Lcom/cgollner/systemmonitor/b$a;->b:I

    .line 67
    iput-object p4, p0, Lcom/cgollner/systemmonitor/b$a;->d:Ljava/lang/Class;

    .line 68
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 84
    if-ne p0, p1, :cond_1

    .line 93
    :cond_0
    :goto_0
    return v0

    .line 86
    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    .line 87
    goto :goto_0

    .line 88
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 89
    goto :goto_0

    .line 90
    :cond_3
    check-cast p1, Lcom/cgollner/systemmonitor/b$a;

    .line 91
    iget v2, p0, Lcom/cgollner/systemmonitor/b$a;->c:I

    iget v3, p1, Lcom/cgollner/systemmonitor/b$a;->c:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 92
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 76
    iget v0, p0, Lcom/cgollner/systemmonitor/b$a;->c:I

    add-int/lit8 v0, v0, 0x1f

    .line 79
    return v0
.end method
