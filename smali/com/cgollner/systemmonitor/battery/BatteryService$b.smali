.class final Lcom/cgollner/systemmonitor/battery/BatteryService$b;
.super Ljava/lang/Thread;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cgollner/systemmonitor/battery/BatteryService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "b"
.end annotation


# instance fields
.field final synthetic a:Lcom/cgollner/systemmonitor/battery/BatteryService;

.field private b:Landroid/content/Context;

.field private c:Landroid/content/Intent;

.field private d:J

.field private e:I


# direct methods
.method public constructor <init>(Lcom/cgollner/systemmonitor/battery/BatteryService;Landroid/content/Context;Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 142
    iput-object p1, p0, Lcom/cgollner/systemmonitor/battery/BatteryService$b;->a:Lcom/cgollner/systemmonitor/battery/BatteryService;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 143
    iput-object p2, p0, Lcom/cgollner/systemmonitor/battery/BatteryService$b;->b:Landroid/content/Context;

    .line 144
    iput-object p3, p0, Lcom/cgollner/systemmonitor/battery/BatteryService$b;->c:Landroid/content/Intent;

    .line 145
    return-void
.end method


# virtual methods
.method public final run()V
    .locals 12

    .prologue
    .line 148
    iget-object v0, p0, Lcom/cgollner/systemmonitor/battery/BatteryService$b;->c:Landroid/content/Intent;

    const-string v1, "clear"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 149
    if-nez v0, :cond_0

    iget-object v1, p0, Lcom/cgollner/systemmonitor/battery/BatteryService$b;->c:Landroid/content/Intent;

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-string v2, "android.intent.action.BATTERY_CHANGED"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_13

    .line 150
    :cond_0
    if-eqz v0, :cond_1

    .line 151
    const/4 v0, 0x0

    sput-object v0, Lcom/cgollner/systemmonitor/battery/BatteryService;->b:Lcom/cgollner/systemmonitor/battery/a;

    .line 152
    const/4 v0, 0x0

    sput-object v0, Lcom/cgollner/systemmonitor/battery/BatteryService;->a:Lcom/cgollner/systemmonitor/battery/a;

    .line 153
    sget-object v0, Lcom/cgollner/systemmonitor/battery/BatteryService;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 154
    sget-object v0, Lcom/cgollner/systemmonitor/battery/BatteryService;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 155
    sget-object v0, Lcom/cgollner/systemmonitor/battery/BatteryService;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 156
    iget-object v0, p0, Lcom/cgollner/systemmonitor/battery/BatteryService$b;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/battery/BatteryService;->d(Landroid/content/Context;)V

    .line 157
    iget-object v0, p0, Lcom/cgollner/systemmonitor/battery/BatteryService$b;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/battery/BatteryService;->g(Landroid/content/Context;)V

    .line 158
    iget-object v0, p0, Lcom/cgollner/systemmonitor/battery/BatteryService$b;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/battery/BatteryService;->h(Landroid/content/Context;)V

    .line 159
    iget-object v0, p0, Lcom/cgollner/systemmonitor/battery/BatteryService$b;->a:Lcom/cgollner/systemmonitor/battery/BatteryService;

    const/4 v1, 0x0

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "android.intent.action.BATTERY_CHANGED"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Lcom/cgollner/systemmonitor/battery/BatteryService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    move-result-object v0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/battery/BatteryService$b;->c:Landroid/content/Intent;

    .line 161
    :cond_1
    new-instance v2, Lcom/cgollner/systemmonitor/battery/a;

    invoke-direct {v2}, Lcom/cgollner/systemmonitor/battery/a;-><init>()V

    .line 162
    iget-object v0, p0, Lcom/cgollner/systemmonitor/battery/BatteryService$b;->c:Landroid/content/Intent;

    const-string v1, "status"

    const/4 v3, -0x1

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 163
    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x5

    if-ne v0, v1, :cond_4

    :cond_2
    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, v2, Lcom/cgollner/systemmonitor/battery/a;->d:Z

    .line 166
    iget-object v0, p0, Lcom/cgollner/systemmonitor/battery/BatteryService$b;->c:Landroid/content/Intent;

    const-string v1, "level"

    const/4 v3, -0x1

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 167
    iget-object v1, p0, Lcom/cgollner/systemmonitor/battery/BatteryService$b;->c:Landroid/content/Intent;

    const-string v3, "scale"

    const/4 v4, -0x1

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 169
    int-to-float v0, v0

    int-to-float v1, v1

    div-float/2addr v0, v1

    const/high16 v1, 0x42c80000    # 100.0f

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, v2, Lcom/cgollner/systemmonitor/battery/a;->a:I

    .line 170
    iget-object v0, p0, Lcom/cgollner/systemmonitor/battery/BatteryService$b;->c:Landroid/content/Intent;

    const-string v1, "temperature"

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, v2, Lcom/cgollner/systemmonitor/battery/a;->b:I

    .line 171
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, v2, Lcom/cgollner/systemmonitor/battery/a;->c:J

    .line 173
    const/4 v0, 0x0

    .line 174
    sget-object v1, Lcom/cgollner/systemmonitor/battery/BatteryService;->a:Lcom/cgollner/systemmonitor/battery/a;

    if-eqz v1, :cond_7

    .line 175
    iget v0, v2, Lcom/cgollner/systemmonitor/battery/a;->a:I

    sget-object v1, Lcom/cgollner/systemmonitor/battery/BatteryService;->a:Lcom/cgollner/systemmonitor/battery/a;

    iget v1, v1, Lcom/cgollner/systemmonitor/battery/a;->a:I

    if-ge v0, v1, :cond_5

    iget-boolean v0, v2, Lcom/cgollner/systemmonitor/battery/a;->d:Z

    if-eqz v0, :cond_5

    const/4 v0, 0x1

    .line 176
    :goto_1
    sget-object v1, Lcom/cgollner/systemmonitor/battery/BatteryService;->a:Lcom/cgollner/systemmonitor/battery/a;

    iget v1, v1, Lcom/cgollner/systemmonitor/battery/a;->a:I

    iget v3, v2, Lcom/cgollner/systemmonitor/battery/a;->a:I

    sub-int/2addr v1, v3

    if-nez v1, :cond_6

    sget-object v1, Lcom/cgollner/systemmonitor/battery/BatteryService;->a:Lcom/cgollner/systemmonitor/battery/a;

    iget-boolean v1, v1, Lcom/cgollner/systemmonitor/battery/a;->d:Z

    iget-boolean v3, v2, Lcom/cgollner/systemmonitor/battery/a;->d:Z

    if-ne v1, v3, :cond_6

    .line 266
    :cond_3
    :goto_2
    return-void

    .line 163
    :cond_4
    const/4 v0, 0x0

    goto :goto_0

    .line 175
    :cond_5
    const/4 v0, 0x0

    goto :goto_1

    .line 180
    :cond_6
    sget-object v1, Lcom/cgollner/systemmonitor/battery/BatteryService;->a:Lcom/cgollner/systemmonitor/battery/a;

    sput-object v1, Lcom/cgollner/systemmonitor/battery/BatteryService;->b:Lcom/cgollner/systemmonitor/battery/a;

    :cond_7
    move v1, v0

    .line 182
    sput-object v2, Lcom/cgollner/systemmonitor/battery/BatteryService;->a:Lcom/cgollner/systemmonitor/battery/a;

    .line 184
    sget-object v0, Lcom/cgollner/systemmonitor/battery/BatteryService;->c:Ljava/util/List;

    if-nez v0, :cond_8

    .line 185
    iget-object v0, p0, Lcom/cgollner/systemmonitor/battery/BatteryService$b;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/battery/BatteryService;->e(Landroid/content/Context;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/cgollner/systemmonitor/battery/BatteryService;->c:Ljava/util/List;

    .line 187
    :cond_8
    sget-object v0, Lcom/cgollner/systemmonitor/battery/BatteryService;->d:Ljava/util/List;

    if-nez v0, :cond_9

    .line 188
    iget-object v0, p0, Lcom/cgollner/systemmonitor/battery/BatteryService$b;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/battery/BatteryService;->f(Landroid/content/Context;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/cgollner/systemmonitor/battery/BatteryService;->d:Ljava/util/List;

    .line 191
    :cond_9
    iget-object v0, p0, Lcom/cgollner/systemmonitor/battery/BatteryService$b;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/battery/BatteryService;->m(Landroid/content/Context;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 192
    sget-object v3, Lcom/cgollner/systemmonitor/battery/BatteryService;->c:Ljava/util/List;

    monitor-enter v3

    .line 193
    :try_start_0
    sget-object v0, Lcom/cgollner/systemmonitor/battery/BatteryService;->b:Lcom/cgollner/systemmonitor/battery/a;

    if-eqz v0, :cond_a

    sget-object v0, Lcom/cgollner/systemmonitor/battery/BatteryService;->b:Lcom/cgollner/systemmonitor/battery/a;

    if-eqz v0, :cond_b

    sget-object v0, Lcom/cgollner/systemmonitor/battery/BatteryService;->a:Lcom/cgollner/systemmonitor/battery/a;

    iget v0, v0, Lcom/cgollner/systemmonitor/battery/a;->a:I

    sget-object v4, Lcom/cgollner/systemmonitor/battery/BatteryService;->b:Lcom/cgollner/systemmonitor/battery/a;

    iget v4, v4, Lcom/cgollner/systemmonitor/battery/a;->a:I

    sub-int/2addr v0, v4

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    if-eqz v0, :cond_b

    .line 194
    :cond_a
    sget-object v0, Lcom/cgollner/systemmonitor/battery/BatteryService;->c:Ljava/util/List;

    new-instance v4, Lcom/cgollner/systemmonitor/battery/c;

    sget-object v5, Lcom/cgollner/systemmonitor/battery/BatteryService;->a:Lcom/cgollner/systemmonitor/battery/a;

    iget-wide v6, v5, Lcom/cgollner/systemmonitor/battery/a;->c:J

    sget-object v5, Lcom/cgollner/systemmonitor/battery/BatteryService;->a:Lcom/cgollner/systemmonitor/battery/a;

    iget v5, v5, Lcom/cgollner/systemmonitor/battery/a;->a:I

    int-to-float v5, v5

    invoke-direct {v4, v6, v7, v5}, Lcom/cgollner/systemmonitor/battery/c;-><init>(JF)V

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 196
    :try_start_1
    sget-object v0, Lcom/cgollner/systemmonitor/battery/BatteryService;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .line 197
    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 198
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/battery/c;

    .line 199
    sget-object v5, Lcom/cgollner/systemmonitor/battery/BatteryService;->a:Lcom/cgollner/systemmonitor/battery/a;

    iget-wide v6, v5, Lcom/cgollner/systemmonitor/battery/a;->c:J

    iget-wide v8, v0, Lcom/cgollner/systemmonitor/battery/c;->a:J

    sub-long/2addr v6, v8

    .line 200
    int-to-long v8, v2

    const-wide/16 v10, 0xe10

    mul-long/2addr v8, v10

    const-wide/16 v10, 0x3e8

    mul-long/2addr v8, v10

    cmp-long v0, v6, v8

    if-lez v0, :cond_b

    .line 201
    invoke-interface {v4}, Ljava/util/Iterator;->remove()V
    :try_end_1
    .catch Ljava/lang/ClassCastException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_3

    .line 207
    :catch_0
    move-exception v0

    :try_start_2
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    .line 208
    sput-object v0, Lcom/cgollner/systemmonitor/battery/BatteryService;->c:Ljava/util/List;

    new-instance v4, Lcom/cgollner/systemmonitor/battery/c;

    sget-object v5, Lcom/cgollner/systemmonitor/battery/BatteryService;->a:Lcom/cgollner/systemmonitor/battery/a;

    iget-wide v6, v5, Lcom/cgollner/systemmonitor/battery/a;->c:J

    sget-object v5, Lcom/cgollner/systemmonitor/battery/BatteryService;->a:Lcom/cgollner/systemmonitor/battery/a;

    iget v5, v5, Lcom/cgollner/systemmonitor/battery/a;->a:I

    int-to-float v5, v5

    invoke-direct {v4, v6, v7, v5}, Lcom/cgollner/systemmonitor/battery/c;-><init>(JF)V

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 211
    :cond_b
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 213
    sget-object v3, Lcom/cgollner/systemmonitor/battery/BatteryService;->d:Ljava/util/List;

    monitor-enter v3

    .line 214
    :try_start_3
    sget-object v0, Lcom/cgollner/systemmonitor/battery/BatteryService;->d:Ljava/util/List;

    new-instance v4, Lcom/cgollner/systemmonitor/battery/c;

    sget-object v5, Lcom/cgollner/systemmonitor/battery/BatteryService;->a:Lcom/cgollner/systemmonitor/battery/a;

    iget-wide v6, v5, Lcom/cgollner/systemmonitor/battery/a;->c:J

    sget-object v5, Lcom/cgollner/systemmonitor/battery/BatteryService;->a:Lcom/cgollner/systemmonitor/battery/a;

    iget v5, v5, Lcom/cgollner/systemmonitor/battery/a;->b:I

    int-to-float v5, v5

    invoke-direct {v4, v6, v7, v5}, Lcom/cgollner/systemmonitor/battery/c;-><init>(JF)V

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 215
    sget-object v0, Lcom/cgollner/systemmonitor/battery/BatteryService;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .line 216
    :goto_4
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 217
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/battery/c;

    .line 218
    sget-object v5, Lcom/cgollner/systemmonitor/battery/BatteryService;->a:Lcom/cgollner/systemmonitor/battery/a;

    iget-wide v6, v5, Lcom/cgollner/systemmonitor/battery/a;->c:J

    iget-wide v8, v0, Lcom/cgollner/systemmonitor/battery/c;->a:J

    sub-long/2addr v6, v8

    .line 219
    int-to-long v8, v2

    const-wide/16 v10, 0xe10

    mul-long/2addr v8, v10

    const-wide/16 v10, 0x3e8

    mul-long/2addr v8, v10

    cmp-long v0, v6, v8

    if-lez v0, :cond_c

    .line 220
    invoke-interface {v4}, Ljava/util/Iterator;->remove()V

    goto :goto_4

    .line 225
    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0

    .line 211
    :catchall_1
    move-exception v0

    :try_start_4
    monitor-exit v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0

    .line 225
    :cond_c
    :try_start_5
    monitor-exit v3
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 227
    iget-object v0, p0, Lcom/cgollner/systemmonitor/battery/BatteryService$b;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/battery/BatteryService;->b(Landroid/content/Context;)V

    .line 228
    iget-object v0, p0, Lcom/cgollner/systemmonitor/battery/BatteryService$b;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/battery/BatteryService;->g(Landroid/content/Context;)V

    .line 229
    iget-object v0, p0, Lcom/cgollner/systemmonitor/battery/BatteryService$b;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/battery/BatteryService;->d(Landroid/content/Context;)V

    .line 231
    sget-object v0, Lcom/cgollner/systemmonitor/battery/BatteryService;->b:Lcom/cgollner/systemmonitor/battery/a;

    if-eqz v0, :cond_12

    sget-object v0, Lcom/cgollner/systemmonitor/battery/BatteryService;->a:Lcom/cgollner/systemmonitor/battery/a;

    iget-boolean v0, v0, Lcom/cgollner/systemmonitor/battery/a;->d:Z

    sget-object v2, Lcom/cgollner/systemmonitor/battery/BatteryService;->b:Lcom/cgollner/systemmonitor/battery/a;

    iget-boolean v2, v2, Lcom/cgollner/systemmonitor/battery/a;->d:Z

    if-ne v0, v2, :cond_12

    if-nez v1, :cond_12

    const/4 v0, 0x1

    .line 234
    :goto_5
    if-eqz v0, :cond_f

    .line 235
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sget-object v2, Lcom/cgollner/systemmonitor/battery/BatteryService;->b:Lcom/cgollner/systemmonitor/battery/a;

    iget-wide v2, v2, Lcom/cgollner/systemmonitor/battery/a;->c:J

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lcom/cgollner/systemmonitor/battery/BatteryService$b;->d:J

    .line 236
    sget-object v0, Lcom/cgollner/systemmonitor/battery/BatteryService;->a:Lcom/cgollner/systemmonitor/battery/a;

    iget v0, v0, Lcom/cgollner/systemmonitor/battery/a;->a:I

    sget-object v1, Lcom/cgollner/systemmonitor/battery/BatteryService;->b:Lcom/cgollner/systemmonitor/battery/a;

    iget v1, v1, Lcom/cgollner/systemmonitor/battery/a;->a:I

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/cgollner/systemmonitor/battery/BatteryService$b;->e:I

    .line 238
    sget-object v0, Lcom/cgollner/systemmonitor/battery/BatteryService;->a:Lcom/cgollner/systemmonitor/battery/a;

    iget-boolean v0, v0, Lcom/cgollner/systemmonitor/battery/a;->d:Z

    if-eqz v0, :cond_d

    iget v0, p0, Lcom/cgollner/systemmonitor/battery/BatteryService$b;->e:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_e

    :cond_d
    sget-object v0, Lcom/cgollner/systemmonitor/battery/BatteryService;->a:Lcom/cgollner/systemmonitor/battery/a;

    iget-boolean v0, v0, Lcom/cgollner/systemmonitor/battery/a;->d:Z

    if-nez v0, :cond_f

    iget v0, p0, Lcom/cgollner/systemmonitor/battery/BatteryService$b;->e:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_f

    .line 241
    :cond_e
    iget-object v0, p0, Lcom/cgollner/systemmonitor/battery/BatteryService$b;->b:Landroid/content/Context;

    sget-object v1, Lcom/cgollner/systemmonitor/battery/BatteryService;->a:Lcom/cgollner/systemmonitor/battery/a;

    iget-boolean v1, v1, Lcom/cgollner/systemmonitor/battery/a;->d:Z

    sget-object v2, Lcom/cgollner/systemmonitor/battery/BatteryService;->a:Lcom/cgollner/systemmonitor/battery/a;

    iget v2, v2, Lcom/cgollner/systemmonitor/battery/a;->a:I

    iget-wide v4, p0, Lcom/cgollner/systemmonitor/battery/BatteryService$b;->d:J

    iget v3, p0, Lcom/cgollner/systemmonitor/battery/BatteryService$b;->e:I

    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    invoke-static {v0, v1, v2, v4, v5}, Lcom/cgollner/systemmonitor/battery/b;->a(Landroid/content/Context;ZIJ)V

    .line 244
    :cond_f
    sget-object v0, Lcom/cgollner/systemmonitor/battery/BatteryService;->b:Lcom/cgollner/systemmonitor/battery/a;

    if-eqz v0, :cond_10

    sget-object v0, Lcom/cgollner/systemmonitor/battery/BatteryService;->b:Lcom/cgollner/systemmonitor/battery/a;

    iget-boolean v0, v0, Lcom/cgollner/systemmonitor/battery/a;->d:Z

    sget-object v1, Lcom/cgollner/systemmonitor/battery/BatteryService;->a:Lcom/cgollner/systemmonitor/battery/a;

    iget-boolean v1, v1, Lcom/cgollner/systemmonitor/battery/a;->d:Z

    if-eq v0, v1, :cond_11

    .line 245
    :cond_10
    const/4 v0, 0x0

    sput-object v0, Lcom/cgollner/systemmonitor/battery/b;->c:Ljava/util/List;

    .line 246
    sget-object v0, Lcom/cgollner/systemmonitor/App;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/battery/b;->a(Landroid/content/Context;)V

    .line 249
    :cond_11
    sget-object v0, Lcom/cgollner/systemmonitor/battery/BatteryService;->a:Lcom/cgollner/systemmonitor/battery/a;

    if-eqz v0, :cond_3

    .line 250
    iget-object v0, p0, Lcom/cgollner/systemmonitor/battery/BatteryService$b;->b:Landroid/content/Context;

    sget-object v1, Lcom/cgollner/systemmonitor/battery/BatteryService;->a:Lcom/cgollner/systemmonitor/battery/a;

    iget-boolean v1, v1, Lcom/cgollner/systemmonitor/battery/a;->d:Z

    sget-object v2, Lcom/cgollner/systemmonitor/battery/BatteryService;->a:Lcom/cgollner/systemmonitor/battery/a;

    iget v2, v2, Lcom/cgollner/systemmonitor/battery/a;->a:I

    invoke-static {v0, v1, v2}, Lcom/cgollner/systemmonitor/battery/b;->a(Landroid/content/Context;ZI)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/cgollner/systemmonitor/battery/BatteryService;->e:Ljava/util/List;

    .line 251
    iget-object v0, p0, Lcom/cgollner/systemmonitor/battery/BatteryService$b;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/battery/BatteryService;->h(Landroid/content/Context;)V

    .line 252
    iget-object v0, p0, Lcom/cgollner/systemmonitor/battery/BatteryService$b;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/battery/BatteryService;->n(Landroid/content/Context;)V

    goto/16 :goto_2

    .line 231
    :cond_12
    const/4 v0, 0x0

    goto :goto_5

    .line 255
    :cond_13
    iget-object v0, p0, Lcom/cgollner/systemmonitor/battery/BatteryService$b;->c:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.cgollner.systemmonitor.battery.ACTION_UPDATE_REQUEST"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 256
    iget-object v0, p0, Lcom/cgollner/systemmonitor/battery/BatteryService$b;->b:Landroid/content/Context;

    sget-object v1, Lcom/cgollner/systemmonitor/battery/BatteryService;->a:Lcom/cgollner/systemmonitor/battery/a;

    iget-boolean v1, v1, Lcom/cgollner/systemmonitor/battery/a;->d:Z

    sget-object v2, Lcom/cgollner/systemmonitor/battery/BatteryService;->a:Lcom/cgollner/systemmonitor/battery/a;

    iget v2, v2, Lcom/cgollner/systemmonitor/battery/a;->a:I

    invoke-static {v0, v1, v2}, Lcom/cgollner/systemmonitor/battery/b;->a(Landroid/content/Context;ZI)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/cgollner/systemmonitor/battery/BatteryService;->e:Ljava/util/List;

    .line 257
    iget-object v0, p0, Lcom/cgollner/systemmonitor/battery/BatteryService$b;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/battery/BatteryService;->h(Landroid/content/Context;)V

    .line 258
    iget-object v0, p0, Lcom/cgollner/systemmonitor/battery/BatteryService$b;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/battery/BatteryService;->n(Landroid/content/Context;)V

    goto/16 :goto_2

    .line 260
    :cond_14
    sget-object v0, Lcom/cgollner/systemmonitor/battery/BatteryService;->a:Lcom/cgollner/systemmonitor/battery/a;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/cgollner/systemmonitor/battery/BatteryService$b;->c:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.cgollner.systemmonitor.battery.ACTION_INFO_REQUEST"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 261
    iget-object v0, p0, Lcom/cgollner/systemmonitor/battery/BatteryService$b;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/battery/BatteryService;->n(Landroid/content/Context;)V

    goto/16 :goto_2
.end method
