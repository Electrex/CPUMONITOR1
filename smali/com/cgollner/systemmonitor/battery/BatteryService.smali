.class public Lcom/cgollner/systemmonitor/battery/BatteryService;
.super Landroid/app/Service;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/cgollner/systemmonitor/battery/BatteryService$b;,
        Lcom/cgollner/systemmonitor/battery/BatteryService$a;
    }
.end annotation


# static fields
.field public static a:Lcom/cgollner/systemmonitor/battery/a;

.field public static b:Lcom/cgollner/systemmonitor/battery/a;

.field public static c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/cgollner/systemmonitor/battery/c;",
            ">;"
        }
    .end annotation
.end field

.field public static d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/cgollner/systemmonitor/battery/c;",
            ">;"
        }
    .end annotation
.end field

.field public static e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/cgollner/systemmonitor/battery/c;",
            ">;"
        }
    .end annotation
.end field

.field private static f:Lcom/cgollner/systemmonitor/battery/BatteryService$a;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 136
    return-void
.end method

.method public static a(Landroid/content/Context;)Lcom/cgollner/systemmonitor/battery/a;
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v5, -0x1

    .line 124
    const/4 v0, 0x0

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "android.intent.action.BATTERY_CHANGED"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    move-result-object v2

    .line 125
    new-instance v3, Lcom/cgollner/systemmonitor/battery/a;

    invoke-direct {v3}, Lcom/cgollner/systemmonitor/battery/a;-><init>()V

    .line 126
    const-string v0, "status"

    invoke-virtual {v2, v0, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 127
    const/4 v4, 0x2

    if-eq v0, v4, :cond_0

    const/4 v4, 0x5

    if-ne v0, v4, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, v3, Lcom/cgollner/systemmonitor/battery/a;->d:Z

    .line 129
    const-string v0, "level"

    invoke-virtual {v2, v0, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 130
    const-string v4, "scale"

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    .line 131
    int-to-float v0, v0

    int-to-float v4, v4

    div-float/2addr v0, v4

    const/high16 v4, 0x42c80000    # 100.0f

    mul-float/2addr v0, v4

    float-to-int v0, v0

    iput v0, v3, Lcom/cgollner/systemmonitor/battery/a;->a:I

    .line 132
    const-string v0, "temperature"

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, v3, Lcom/cgollner/systemmonitor/battery/a;->b:I

    .line 133
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, v3, Lcom/cgollner/systemmonitor/battery/a;->c:J

    .line 134
    return-object v3

    :cond_1
    move v0, v1

    .line 127
    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 426
    invoke-virtual {p0, p1}, Landroid/content/Context;->getFileStreamPath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    invoke-static {v0}, Lcom/cgollner/systemmonitor/battery/BatteryService;->a(Ljava/io/File;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/io/File;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 431
    :try_start_0
    new-instance v0, Ljava/io/RandomAccessFile;

    const-string v1, "rws"

    invoke-direct {v0, p0, v1}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 432
    invoke-virtual {v0}, Ljava/io/RandomAccessFile;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v1

    .line 433
    invoke-virtual {v1}, Ljava/nio/channels/FileChannel;->lock()Ljava/nio/channels/FileLock;

    move-result-object v1

    .line 434
    invoke-virtual {v0}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v2

    long-to-int v2, v2

    new-array v2, v2, [B

    .line 435
    invoke-virtual {v0, v2}, Ljava/io/RandomAccessFile;->read([B)I

    .line 436
    invoke-virtual {v1}, Ljava/nio/channels/FileLock;->release()V

    .line 437
    invoke-virtual {v0}, Ljava/io/RandomAccessFile;->close()V

    .line 438
    new-instance v1, Ljava/io/ByteArrayInputStream;

    invoke-direct {v1, v2}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 439
    new-instance v2, Ljava/io/ObjectInputStream;

    invoke-direct {v2, v1}, Ljava/io/ObjectInputStream;-><init>(Ljava/io/InputStream;)V

    .line 440
    invoke-interface {v2}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    .line 441
    invoke-virtual {v1}, Ljava/io/ByteArrayInputStream;->close()V

    .line 442
    invoke-interface {v2}, Ljava/io/ObjectInput;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 447
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    .line 446
    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method public static a(ILandroid/content/Context;)V
    .locals 2

    .prologue
    .line 335
    const-string v0, "STRATEGY_FILE"

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcom/cgollner/systemmonitor/battery/BatteryService;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Object;)V

    .line 336
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.cgollner.systemmonitor.battery.ACTION_UPDATE_REQUEST"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 337
    return-void
.end method

.method public static a(Landroid/content/Context;I)V
    .locals 2

    .prologue
    .line 281
    const-string v0, "BATT_HIST_SIZE_FILE"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/cgollner/systemmonitor/battery/BatteryService;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Object;)V

    .line 282
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 363
    invoke-virtual {p0, p1}, Landroid/content/Context;->getFileStreamPath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    invoke-static {v0, p2}, Lcom/cgollner/systemmonitor/battery/BatteryService;->a(Ljava/io/File;Ljava/lang/Object;)V

    .line 364
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 450
    new-instance v0, Ljava/io/File;

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v1}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v1

    invoke-direct {v0, v1, p2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 451
    invoke-static {v0, p3}, Lcom/cgollner/systemmonitor/battery/BatteryService;->a(Ljava/io/File;Ljava/lang/Object;)V

    .line 452
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;[B)V
    .locals 3

    .prologue
    .line 405
    new-instance v0, Ljava/io/File;

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v1}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v1

    invoke-direct {v0, v1, p2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 407
    :try_start_0
    monitor-enter p3
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 408
    :try_start_1
    new-instance v1, Ljava/io/RandomAccessFile;

    const-string v2, "rws"

    invoke-direct {v1, v0, v2}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 409
    invoke-virtual {v1}, Ljava/io/RandomAccessFile;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v0

    .line 410
    invoke-virtual {v0}, Ljava/nio/channels/FileChannel;->lock()Ljava/nio/channels/FileLock;

    move-result-object v0

    .line 411
    invoke-virtual {v1, p3}, Ljava/io/RandomAccessFile;->write([B)V

    .line 412
    invoke-virtual {v0}, Ljava/nio/channels/FileLock;->release()V

    .line 413
    invoke-virtual {v1}, Ljava/io/RandomAccessFile;->close()V

    .line 414
    monitor-exit p3

    .line 418
    :goto_0
    return-void

    .line 414
    :catchall_0
    move-exception v0

    monitor-exit p3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v0
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 417
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Z)V
    .locals 2

    .prologue
    .line 352
    invoke-static {p0, p1}, Lcom/cgollner/systemmonitor/battery/b;->b(Landroid/content/Context;Z)V

    .line 353
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.cgollner.systemmonitor.battery.ACTION_UPDATE_REQUEST"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 354
    return-void
.end method

.method private static a(Ljava/io/File;Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 368
    :try_start_0
    monitor-enter p1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 369
    :try_start_1
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 370
    new-instance v1, Ljava/io/ObjectOutputStream;

    invoke-direct {v1, v0}, Ljava/io/ObjectOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 371
    invoke-interface {v1, p1}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    .line 372
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v2

    .line 373
    invoke-interface {v1}, Ljava/io/ObjectOutput;->close()V

    .line 374
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V

    .line 375
    new-instance v0, Ljava/io/RandomAccessFile;

    const-string v1, "rws"

    invoke-direct {v0, p0, v1}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 376
    invoke-virtual {v0}, Ljava/io/RandomAccessFile;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v1

    .line 377
    invoke-virtual {v1}, Ljava/nio/channels/FileChannel;->lock()Ljava/nio/channels/FileLock;

    move-result-object v1

    .line 378
    invoke-virtual {v0, v2}, Ljava/io/RandomAccessFile;->write([B)V

    .line 379
    invoke-virtual {v1}, Ljava/nio/channels/FileLock;->release()V

    .line 380
    invoke-virtual {v0}, Ljava/io/RandomAccessFile;->close()V

    .line 381
    monitor-exit p1

    .line 385
    :goto_0
    return-void

    .line 381
    :catchall_0
    move-exception v0

    monitor-exit p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v0
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 384
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)[B
    .locals 6

    .prologue
    .line 388
    new-instance v0, Ljava/io/File;

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v1}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v1

    invoke-direct {v0, v1, p2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 390
    :try_start_0
    new-instance v1, Ljava/io/RandomAccessFile;

    const-string v2, "rws"

    invoke-direct {v1, v0, v2}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 391
    invoke-virtual {v1}, Ljava/io/RandomAccessFile;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v0

    .line 392
    invoke-virtual {v0}, Ljava/nio/channels/FileChannel;->lock()Ljava/nio/channels/FileLock;

    move-result-object v2

    .line 393
    invoke-virtual {v1}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v4

    long-to-int v0, v4

    new-array v0, v0, [B

    .line 394
    invoke-virtual {v1, v0}, Ljava/io/RandomAccessFile;->read([B)I

    .line 395
    invoke-virtual {v2}, Ljava/nio/channels/FileLock;->release()V

    .line 396
    invoke-virtual {v1}, Ljava/io/RandomAccessFile;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 401
    :goto_0
    return-object v0

    .line 398
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 401
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 421
    new-instance v0, Ljava/io/File;

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v1}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v1

    invoke-direct {v0, v1, p2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 422
    invoke-static {v0}, Lcom/cgollner/systemmonitor/battery/BatteryService;->a(Ljava/io/File;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public static b(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 285
    const-string v0, "CURRENT_BATTERY_INFO"

    sget-object v1, Lcom/cgollner/systemmonitor/battery/BatteryService;->a:Lcom/cgollner/systemmonitor/battery/a;

    invoke-static {p0, v0, v1}, Lcom/cgollner/systemmonitor/battery/BatteryService;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Object;)V

    .line 286
    return-void
.end method

.method public static c(Landroid/content/Context;)Lcom/cgollner/systemmonitor/battery/a;
    .locals 1

    .prologue
    .line 289
    const-string v0, "CURRENT_BATTERY_INFO"

    invoke-static {p0, v0}, Lcom/cgollner/systemmonitor/battery/BatteryService;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/battery/a;

    return-object v0
.end method

.method public static d(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 292
    const-string v0, "BATTERY_HISTORY_FILE"

    sget-object v1, Lcom/cgollner/systemmonitor/battery/BatteryService;->c:Ljava/util/List;

    invoke-static {p0, v0, v1}, Lcom/cgollner/systemmonitor/battery/BatteryService;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Object;)V

    .line 293
    return-void
.end method

.method public static e(Landroid/content/Context;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/cgollner/systemmonitor/battery/c;",
            ">;"
        }
    .end annotation

    .prologue
    .line 297
    const-string v0, "BATTERY_HISTORY_FILE"

    invoke-static {p0, v0}, Lcom/cgollner/systemmonitor/battery/BatteryService;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 298
    if-nez v0, :cond_0

    .line 299
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    .line 302
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/util/List;

    goto :goto_0
.end method

.method public static f(Landroid/content/Context;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/cgollner/systemmonitor/battery/c;",
            ">;"
        }
    .end annotation

    .prologue
    .line 308
    const-string v0, "TEMPERATURE_HISTORY_FILE"

    invoke-static {p0, v0}, Lcom/cgollner/systemmonitor/battery/BatteryService;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 309
    if-nez v0, :cond_0

    .line 310
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    .line 313
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/util/List;

    goto :goto_0
.end method

.method public static g(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 319
    :try_start_0
    const-string v0, "TEMPERATURE_HISTORY_FILE"

    sget-object v1, Lcom/cgollner/systemmonitor/battery/BatteryService;->d:Ljava/util/List;

    invoke-static {p0, v0, v1}, Lcom/cgollner/systemmonitor/battery/BatteryService;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/util/ConcurrentModificationException; {:try_start_0 .. :try_end_0} :catch_0

    .line 323
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public static h(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 326
    const-string v0, "PREDICTION_ARRAY"

    sget-object v1, Lcom/cgollner/systemmonitor/battery/BatteryService;->e:Ljava/util/List;

    invoke-static {p0, v0, v1}, Lcom/cgollner/systemmonitor/battery/BatteryService;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Object;)V

    .line 327
    return-void
.end method

.method public static i(Landroid/content/Context;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/cgollner/systemmonitor/battery/c;",
            ">;"
        }
    .end annotation

    .prologue
    .line 331
    const-string v0, "PREDICTION_ARRAY"

    invoke-static {p0, v0}, Lcom/cgollner/systemmonitor/battery/BatteryService;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method public static j(Landroid/content/Context;)Ljava/lang/Integer;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 339
    const-string v0, "STRATEGY_FILE"

    invoke-static {p0, v0}, Lcom/cgollner/systemmonitor/battery/BatteryService;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 340
    if-nez v0, :cond_0

    .line 341
    invoke-static {v1, p0}, Lcom/cgollner/systemmonitor/battery/BatteryService;->a(ILandroid/content/Context;)V

    .line 343
    :cond_0
    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0

    :cond_1
    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0
.end method

.method public static k(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 347
    invoke-static {p0}, Lcom/cgollner/systemmonitor/battery/b;->c(Landroid/content/Context;)V

    .line 348
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.cgollner.systemmonitor.battery.ACTION_UPDATE_REQUEST"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 349
    return-void
.end method

.method public static l(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 357
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.cgollner.systemmonitor.battery.ACTION_UPDATE_REQUEST"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 358
    const-string v1, "clear"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 359
    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 360
    return-void
.end method

.method static synthetic m(Landroid/content/Context;)Ljava/lang/Integer;
    .locals 3

    .prologue
    const/16 v2, 0x30

    .line 35
    const-string v0, "BATT_HIST_SIZE_FILE"

    invoke-static {p0, v0}, Lcom/cgollner/systemmonitor/battery/BatteryService;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, "BATT_HIST_SIZE_FILE"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/cgollner/systemmonitor/battery/BatteryService;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Object;)V

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/Integer;

    goto :goto_0
.end method

.method static synthetic n(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 35
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.cgollner.systemmonitor.battery.ACTION_BATTERY_INFO_UPDATE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/cgollner/systemmonitor/f/b;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    new-instance v1, Landroid/content/ComponentName;

    const-class v2, Lcom/cgollner/systemmonitor/f/b;

    invoke-direct {v1, p0, v2}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "android.appwidget.action.APPWIDGET_UPDATE"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "appWidgetIds"

    invoke-static {p0}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/appwidget/AppWidgetManager;->getAppWidgetIds(Landroid/content/ComponentName;)[I

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[I)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 91
    const/4 v0, 0x0

    return-object v0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 84
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 85
    sget-object v0, Lcom/cgollner/systemmonitor/battery/BatteryService;->f:Lcom/cgollner/systemmonitor/battery/BatteryService$a;

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/battery/BatteryService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 86
    const/4 v0, 0x0

    sput-object v0, Lcom/cgollner/systemmonitor/battery/BatteryService;->f:Lcom/cgollner/systemmonitor/battery/BatteryService$a;

    .line 87
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 3

    .prologue
    .line 73
    sget-object v0, Lcom/cgollner/systemmonitor/battery/BatteryService;->f:Lcom/cgollner/systemmonitor/battery/BatteryService$a;

    if-nez v0, :cond_0

    .line 74
    new-instance v0, Lcom/cgollner/systemmonitor/battery/BatteryService$a;

    invoke-direct {v0, p0}, Lcom/cgollner/systemmonitor/battery/BatteryService$a;-><init>(Lcom/cgollner/systemmonitor/battery/BatteryService;)V

    sput-object v0, Lcom/cgollner/systemmonitor/battery/BatteryService;->f:Lcom/cgollner/systemmonitor/battery/BatteryService$a;

    .line 75
    sget-object v0, Lcom/cgollner/systemmonitor/battery/BatteryService;->f:Lcom/cgollner/systemmonitor/battery/BatteryService$a;

    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "android.intent.action.BATTERY_CHANGED"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0, v1}, Lcom/cgollner/systemmonitor/battery/BatteryService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 76
    sget-object v0, Lcom/cgollner/systemmonitor/battery/BatteryService;->f:Lcom/cgollner/systemmonitor/battery/BatteryService$a;

    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "com.cgollner.systemmonitor.battery.ACTION_INFO_REQUEST"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0, v1}, Lcom/cgollner/systemmonitor/battery/BatteryService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 77
    sget-object v0, Lcom/cgollner/systemmonitor/battery/BatteryService;->f:Lcom/cgollner/systemmonitor/battery/BatteryService$a;

    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "com.cgollner.systemmonitor.battery.ACTION_UPDATE_REQUEST"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0, v1}, Lcom/cgollner/systemmonitor/battery/BatteryService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 79
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public onTaskRemoved(Landroid/content/Intent;)V
    .locals 8

    .prologue
    const/4 v4, 0x0

    .line 64
    const-string v0, "alarm"

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/battery/BatteryService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 65
    new-instance v1, Landroid/content/Intent;

    sget-object v2, Lcom/cgollner/systemmonitor/App;->a:Landroid/content/Context;

    const-class v3, Lcom/cgollner/systemmonitor/battery/BatteryService;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 66
    sget-object v2, Lcom/cgollner/systemmonitor/App;->a:Landroid/content/Context;

    invoke-static {v2, v4, v1, v4}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 67
    const/4 v2, 0x1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    const-wide/16 v6, 0x1388

    add-long/2addr v4, v6

    invoke-virtual {v0, v2, v4, v5, v1}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    .line 68
    return-void
.end method
