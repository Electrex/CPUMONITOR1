.class public Lcom/cgollner/systemmonitor/App;
.super Landroid/app/Application;
.source "SourceFile"

# interfaces
.implements Landroid/app/Application$ActivityLifecycleCallbacks;


# static fields
.field public static a:Landroid/content/Context;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Landroid/app/Application;-><init>()V

    return-void
.end method

.method public static a(Landroid/app/Activity;I)V
    .locals 7

    .prologue
    const/high16 v6, 0x4000000

    const/4 v5, 0x0

    const/high16 v4, 0x3f800000    # 1.0f

    .line 33
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_3

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-ge v0, v1, :cond_3

    .line 34
    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 37
    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    .line 42
    iget v2, v1, Landroid/view/WindowManager$LayoutParams;->flags:I

    const v3, -0x8000001

    and-int/2addr v2, v3

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 43
    iget v2, v1, Landroid/view/WindowManager$LayoutParams;->flags:I

    const v3, -0x4000001

    and-int/2addr v2, v3

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 44
    invoke-virtual {v0, v1}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 45
    const/16 v1, 0x200

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    .line 48
    invoke-virtual {v0, v6, v6}, Landroid/view/Window;->setFlags(II)V

    .line 51
    invoke-static {p1}, Landroid/graphics/Color;->red(I)I

    move-result v0

    invoke-static {p1}, Landroid/graphics/Color;->green(I)I

    move-result v1

    invoke-static {p1}, Landroid/graphics/Color;->blue(I)I

    move-result v2

    .line 55
    int-to-float v0, v0

    mul-float/2addr v0, v4

    float-to-int v0, v0

    .line 56
    int-to-float v1, v1

    mul-float/2addr v1, v4

    float-to-int v1, v1

    .line 57
    int-to-float v2, v2

    mul-float/2addr v2, v4

    float-to-int v2, v2

    .line 58
    invoke-static {v0, v1, v2}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    .line 60
    new-instance v1, Lcom/b/a/a;

    invoke-direct {v1, p0}, Lcom/b/a/a;-><init>(Landroid/app/Activity;)V

    .line 62
    iget-boolean v2, v1, Lcom/b/a/a;->a:Z

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/b/a/a;->e:Landroid/view/View;

    invoke-virtual {v2, v0}, Landroid/view/View;->setBackgroundColor(I)V

    .line 63
    :cond_0
    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/b/a/a;->c:Z

    iget-boolean v2, v1, Lcom/b/a/a;->a:Z

    if-eqz v2, :cond_1

    iget-object v2, v1, Lcom/b/a/a;->e:Landroid/view/View;

    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    .line 65
    :cond_1
    iput-boolean v5, v1, Lcom/b/a/a;->d:Z

    iget-boolean v2, v1, Lcom/b/a/a;->b:Z

    if-eqz v2, :cond_2

    iget-object v2, v1, Lcom/b/a/a;->f:Landroid/view/View;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 66
    :cond_2
    iget-boolean v2, v1, Lcom/b/a/a;->b:Z

    if-eqz v2, :cond_3

    iget-object v1, v1, Lcom/b/a/a;->f:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setBackgroundColor(I)V

    .line 68
    :cond_3
    return-void
.end method


# virtual methods
.method public onActivityCreated(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 77
    sget-object v0, Lcom/cgollner/systemmonitor/App;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/cgollner/systemmonitor/b/a$c;->colorPrimaryDark:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-static {p1, v0}, Lcom/cgollner/systemmonitor/App;->a(Landroid/app/Activity;I)V

    .line 78
    return-void
.end method

.method public onActivityDestroyed(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 108
    return-void
.end method

.method public onActivityPaused(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 93
    return-void
.end method

.method public onActivityResumed(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 88
    return-void
.end method

.method public onActivitySaveInstanceState(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 103
    return-void
.end method

.method public onActivityStarted(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 83
    return-void
.end method

.method public onActivityStopped(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 98
    return-void
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 26
    invoke-super {p0}, Landroid/app/Application;->onCreate()V

    .line 27
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/App;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    sput-object v0, Lcom/cgollner/systemmonitor/App;->a:Landroid/content/Context;

    .line 29
    invoke-virtual {p0, p0}, Lcom/cgollner/systemmonitor/App;->registerActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V

    .line 30
    return-void
.end method
