.class public abstract Lcom/cgollner/systemmonitor/b;
.super Landroid/support/v7/app/ActionBarActivity;
.source "SourceFile"

# interfaces
.implements Lcom/cgollner/systemmonitor/NavigationDrawerFragment$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/cgollner/systemmonitor/b$b;,
        Lcom/cgollner/systemmonitor/b$a;
    }
.end annotation


# static fields
.field public static a:Z


# instance fields
.field protected b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/cgollner/systemmonitor/b$a;",
            ">;"
        }
    .end annotation
.end field

.field private c:Landroid/support/v4/view/ViewPager;

.field private d:Landroid/support/v4/view/PagerTitleStrip;

.field private e:I

.field private f:Landroid/support/v4/widget/DrawerLayout;

.field private g:Landroid/support/v7/app/ActionBarDrawerToggle;

.field private h:Lcom/cgollner/systemmonitor/NavigationDrawerFragment;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 100
    const/4 v0, 0x1

    sput-boolean v0, Lcom/cgollner/systemmonitor/b;->a:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Landroid/support/v7/app/ActionBarActivity;-><init>()V

    .line 353
    return-void
.end method

.method static synthetic a(Lcom/cgollner/systemmonitor/b;I)I
    .locals 1

    .prologue
    .line 50
    invoke-direct {p0, p1}, Lcom/cgollner/systemmonitor/b;->b(I)I

    move-result v0

    return v0
.end method

.method static synthetic a(Lcom/cgollner/systemmonitor/b;)Landroid/support/v4/view/ViewPager;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/cgollner/systemmonitor/b;->c:Landroid/support/v4/view/ViewPager;

    return-object v0
.end method

.method static synthetic a(Lcom/cgollner/systemmonitor/b;II)V
    .locals 4

    .prologue
    const v3, 0x3f333333    # 0.7f

    .line 50
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/b;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iget-object v1, p0, Lcom/cgollner/systemmonitor/b;->h:Lcom/cgollner/systemmonitor/NavigationDrawerFragment;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, p2}, Lcom/cgollner/systemmonitor/NavigationDrawerFragment;->a(Ljava/lang/Integer;I)V

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/b;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v1

    new-instance v2, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v2, v0}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v1, v2}, Landroid/support/v7/app/ActionBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v1, p0, Lcom/cgollner/systemmonitor/b;->d:Landroid/support/v4/view/PagerTitleStrip;

    invoke-virtual {v1, p2}, Landroid/support/v4/view/PagerTitleStrip;->setBackgroundResource(I)V

    invoke-static {v0}, Landroid/graphics/Color;->red(I)I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v1, v3

    float-to-int v1, v1

    invoke-static {v0}, Landroid/graphics/Color;->green(I)I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v2, v3

    float-to-int v2, v2

    invoke-static {v0}, Landroid/graphics/Color;->blue(I)I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v0, v3

    float-to-int v0, v0

    invoke-static {v1, v2, v0}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x15

    if-lt v1, v2, :cond_1

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/b;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Window;->setStatusBarColor(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x13

    if-lt v1, v2, :cond_0

    invoke-static {p0, v0}, Lcom/cgollner/systemmonitor/App;->a(Landroid/app/Activity;I)V

    goto :goto_0
.end method

.method private b(I)I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 468
    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/cgollner/systemmonitor/b;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 469
    iget-object v0, p0, Lcom/cgollner/systemmonitor/b;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/b$a;

    iget v0, v0, Lcom/cgollner/systemmonitor/b$a;->c:I

    if-ne v0, p1, :cond_0

    .line 472
    :goto_1
    return v1

    .line 468
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    move v1, v2

    .line 472
    goto :goto_1
.end method

.method static synthetic b(Lcom/cgollner/systemmonitor/b;)Landroid/support/v4/view/PagerTitleStrip;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/cgollner/systemmonitor/b;->d:Landroid/support/v4/view/PagerTitleStrip;

    return-object v0
.end method

.method static synthetic c(Lcom/cgollner/systemmonitor/b;)Lcom/cgollner/systemmonitor/NavigationDrawerFragment;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/cgollner/systemmonitor/b;->h:Lcom/cgollner/systemmonitor/NavigationDrawerFragment;

    return-object v0
.end method


# virtual methods
.method protected abstract a()V
.end method

.method public final a(I)V
    .locals 4

    .prologue
    .line 449
    invoke-direct {p0, p1}, Lcom/cgollner/systemmonitor/b;->b(I)I

    move-result v2

    .line 450
    iget-object v0, p0, Lcom/cgollner/systemmonitor/b;->b:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/b$a;

    .line 451
    iget-object v1, p0, Lcom/cgollner/systemmonitor/b;->c:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v1

    if-eq v2, v1, :cond_0

    .line 452
    iget-object v1, p0, Lcom/cgollner/systemmonitor/b;->c:Landroid/support/v4/view/ViewPager;

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/support/v4/view/ViewPager;->setCurrentItem(IZ)V

    .line 454
    :cond_0
    iget-object v1, p0, Lcom/cgollner/systemmonitor/b;->f:Landroid/support/v4/widget/DrawerLayout;

    if-eqz v1, :cond_1

    .line 455
    iget-object v1, p0, Lcom/cgollner/systemmonitor/b;->f:Landroid/support/v4/widget/DrawerLayout;

    const/4 v3, 0x3

    invoke-virtual {v1, v3}, Landroid/support/v4/widget/DrawerLayout;->closeDrawer(I)V

    .line 457
    :cond_1
    iget v1, v0, Lcom/cgollner/systemmonitor/b$a;->a:I

    .line 458
    iget-object v0, v0, Lcom/cgollner/systemmonitor/b$a;->d:Ljava/lang/Class;

    const-class v3, Lcom/cgollner/systemmonitor/d/c;

    invoke-virtual {v0, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 459
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/b;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "android:switcher:"

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v3, Lcom/cgollner/systemmonitor/b/a$e;->viewPager:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ":"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/d/c;

    .line 462
    invoke-virtual {v0}, Lcom/cgollner/systemmonitor/d/c;->a()I

    move-result v0

    .line 464
    :goto_0
    iget-object v1, p0, Lcom/cgollner/systemmonitor/b;->h:Lcom/cgollner/systemmonitor/NavigationDrawerFragment;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/cgollner/systemmonitor/NavigationDrawerFragment;->a(Ljava/lang/Integer;I)V

    .line 465
    return-void

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method protected abstract b()Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Landroid/support/v4/app/Fragment;",
            ">;"
        }
    .end annotation
.end method

.method protected abstract c()Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Landroid/support/v4/app/Fragment;",
            ">;"
        }
    .end annotation
.end method

.method protected abstract d()Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Landroid/support/v4/app/Fragment;",
            ">;"
        }
    .end annotation
.end method

.method protected abstract e()Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Landroid/support/v4/app/Fragment;",
            ">;"
        }
    .end annotation
.end method

.method protected abstract f()Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Landroid/support/v4/app/Fragment;",
            ">;"
        }
    .end annotation
.end method

.method protected abstract g()Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Landroid/support/v4/app/Fragment;",
            ">;"
        }
    .end annotation
.end method

.method protected abstract h()Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Landroid/support/v4/app/Fragment;",
            ">;"
        }
    .end annotation
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 293
    invoke-super {p0, p1}, Landroid/support/v7/app/ActionBarActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 294
    iget-object v0, p0, Lcom/cgollner/systemmonitor/b;->g:Landroid/support/v7/app/ActionBarDrawerToggle;

    if-eqz v0, :cond_0

    .line 295
    iget-object v0, p0, Lcom/cgollner/systemmonitor/b;->g:Landroid/support/v7/app/ActionBarDrawerToggle;

    invoke-virtual {v0, p1}, Landroid/support/v7/app/ActionBarDrawerToggle;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 297
    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v6, 0x1

    .line 105
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    const-string v0, "theme"

    const/4 v3, -0x1

    invoke-interface {v2, v0, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/cgollner/systemmonitor/b;->e:I

    iget v0, p0, Lcom/cgollner/systemmonitor/b;->e:I

    packed-switch v0, :pswitch_data_0

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/b;->getPackageName()Ljava/lang/String;

    move-result-object v0

    const-string v3, "com.franco.kernel"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0xb

    if-lt v3, v4, :cond_0

    if-eqz v0, :cond_6

    :cond_0
    move v0, v6

    :goto_0
    iput v0, p0, Lcom/cgollner/systemmonitor/b;->e:I

    iget v0, p0, Lcom/cgollner/systemmonitor/b;->e:I

    if-ne v0, v6, :cond_7

    sget v0, Lcom/cgollner/systemmonitor/b/a$i;->AppThemeDark:I

    :goto_1
    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/b;->setTheme(I)V

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "theme"

    iget v3, p0, Lcom/cgollner/systemmonitor/b;->e:I

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 106
    :goto_2
    invoke-super {p0, p1}, Landroid/support/v7/app/ActionBarActivity;->onCreate(Landroid/os/Bundle;)V

    .line 107
    sget v0, Lcom/cgollner/systemmonitor/b/a$f;->nav_drawer:I

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/b;->setContentView(I)V

    .line 109
    sget v0, Lcom/cgollner/systemmonitor/b/a$e;->toolbar:I

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/b;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/b;->setSupportActionBar(Landroid/support/v7/widget/Toolbar;)V

    .line 111
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/b;->b:Ljava/util/List;

    iget-object v0, p0, Lcom/cgollner/systemmonitor/b;->b:Ljava/util/List;

    new-instance v2, Lcom/cgollner/systemmonitor/b$a;

    sget v3, Lcom/cgollner/systemmonitor/b/a$e;->tab_cpu:I

    sget v4, Lcom/cgollner/systemmonitor/b/a$c;->holo_blue_dark:I

    sget v5, Lcom/cgollner/systemmonitor/b/a$h;->cpu_title:I

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/b;->e()Ljava/lang/Class;

    move-result-object v7

    invoke-direct {v2, v3, v4, v5, v7}, Lcom/cgollner/systemmonitor/b$a;-><init>(IIILjava/lang/Class;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-static {}, Lcom/cgollner/systemmonitor/a/b;->f()Ljava/io/File;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/cgollner/systemmonitor/a/b;->f()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/cgollner/systemmonitor/b;->b:Ljava/util/List;

    new-instance v2, Lcom/cgollner/systemmonitor/b$a;

    sget v3, Lcom/cgollner/systemmonitor/b/a$e;->tab_cpu_temperature:I

    sget v4, Lcom/cgollner/systemmonitor/b/a$c;->holo_blue_dark:I

    sget v5, Lcom/cgollner/systemmonitor/b/a$h;->cpu_temperature:I

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/b;->g()Ljava/lang/Class;

    move-result-object v7

    invoke-direct {v2, v3, v4, v5, v7}, Lcom/cgollner/systemmonitor/b$a;-><init>(IIILjava/lang/Class;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    invoke-static {}, Lcom/cgollner/systemmonitor/a/d;->a()Lcom/cgollner/systemmonitor/a/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/cgollner/systemmonitor/a/d;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/cgollner/systemmonitor/b;->b:Ljava/util/List;

    new-instance v2, Lcom/cgollner/systemmonitor/b$a;

    sget v3, Lcom/cgollner/systemmonitor/b/a$e;->tab_gpu:I

    sget v4, Lcom/cgollner/systemmonitor/b/a$c;->holo_blue_dark:I

    sget v5, Lcom/cgollner/systemmonitor/b/a$h;->gpu:I

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/b;->h()Ljava/lang/Class;

    move-result-object v7

    invoke-direct {v2, v3, v4, v5, v7}, Lcom/cgollner/systemmonitor/b$a;-><init>(IIILjava/lang/Class;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    iget-object v0, p0, Lcom/cgollner/systemmonitor/b;->b:Ljava/util/List;

    new-instance v2, Lcom/cgollner/systemmonitor/b$a;

    sget v3, Lcom/cgollner/systemmonitor/b/a$e;->tab_ram:I

    sget v4, Lcom/cgollner/systemmonitor/b/a$c;->holo_purple_dark:I

    sget v5, Lcom/cgollner/systemmonitor/b/a$h;->ram_title:I

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/b;->d()Ljava/lang/Class;

    move-result-object v7

    invoke-direct {v2, v3, v4, v5, v7}, Lcom/cgollner/systemmonitor/b$a;-><init>(IIILjava/lang/Class;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/cgollner/systemmonitor/b;->b:Ljava/util/List;

    new-instance v2, Lcom/cgollner/systemmonitor/b$a;

    sget v3, Lcom/cgollner/systemmonitor/b/a$e;->tab_io:I

    sget v4, Lcom/cgollner/systemmonitor/b/a$c;->holo_green_dark:I

    sget v5, Lcom/cgollner/systemmonitor/b/a$h;->io_title:I

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/b;->c()Ljava/lang/Class;

    move-result-object v7

    invoke-direct {v2, v3, v4, v5, v7}, Lcom/cgollner/systemmonitor/b$a;-><init>(IIILjava/lang/Class;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/cgollner/systemmonitor/b;->b:Ljava/util/List;

    new-instance v2, Lcom/cgollner/systemmonitor/b$a;

    sget v3, Lcom/cgollner/systemmonitor/b/a$e;->tab_net:I

    sget v4, Lcom/cgollner/systemmonitor/b/a$c;->holo_orange_dark:I

    sget v5, Lcom/cgollner/systemmonitor/b/a$h;->network_title:I

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/b;->b()Ljava/lang/Class;

    move-result-object v7

    invoke-direct {v2, v3, v4, v5, v7}, Lcom/cgollner/systemmonitor/b$a;-><init>(IIILjava/lang/Class;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/cgollner/systemmonitor/b;->b:Ljava/util/List;

    new-instance v2, Lcom/cgollner/systemmonitor/b$a;

    sget v3, Lcom/cgollner/systemmonitor/b/a$e;->tab_top_apps:I

    sget v4, Lcom/cgollner/systemmonitor/b/a$c;->holo_red_dark:I

    sget v5, Lcom/cgollner/systemmonitor/b/a$h;->top_apps_title:I

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/b;->f()Ljava/lang/Class;

    move-result-object v7

    invoke-direct {v2, v3, v4, v5, v7}, Lcom/cgollner/systemmonitor/b$a;-><init>(IIILjava/lang/Class;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/cgollner/systemmonitor/b;->b:Ljava/util/List;

    new-instance v2, Lcom/cgollner/systemmonitor/b$a;

    sget v3, Lcom/cgollner/systemmonitor/b/a$e;->tab_cpu_freqs:I

    sget v4, Lcom/cgollner/systemmonitor/b/a$c;->holo_blue_light:I

    sget v5, Lcom/cgollner/systemmonitor/b/a$h;->cpu_frequencies:I

    const-class v7, Lcom/cgollner/systemmonitor/d/h;

    invoke-direct {v2, v3, v4, v5, v7}, Lcom/cgollner/systemmonitor/b$a;-><init>(IIILjava/lang/Class;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/cgollner/systemmonitor/b;->b:Ljava/util/List;

    new-instance v2, Lcom/cgollner/systemmonitor/b$a;

    sget v3, Lcom/cgollner/systemmonitor/b/a$e;->tab_storage_stats:I

    sget v4, Lcom/cgollner/systemmonitor/b/a$c;->holo_blue_dark:I

    sget v5, Lcom/cgollner/systemmonitor/b/a$h;->storage_stats:I

    const-class v7, Lcom/cgollner/systemmonitor/d/p;

    invoke-direct {v2, v3, v4, v5, v7}, Lcom/cgollner/systemmonitor/b$a;-><init>(IIILjava/lang/Class;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/cgollner/systemmonitor/b;->b:Ljava/util/List;

    new-instance v2, Lcom/cgollner/systemmonitor/b$a;

    sget v3, Lcom/cgollner/systemmonitor/b/a$e;->tab_apps_cache:I

    sget v4, Lcom/cgollner/systemmonitor/b/a$c;->holo_blue_dark:I

    sget v5, Lcom/cgollner/systemmonitor/b/a$h;->cache_stats:I

    const-class v7, Lcom/cgollner/systemmonitor/d/b;

    invoke-direct {v2, v3, v4, v5, v7}, Lcom/cgollner/systemmonitor/b$a;-><init>(IIILjava/lang/Class;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/cgollner/systemmonitor/b;->b:Ljava/util/List;

    new-instance v2, Lcom/cgollner/systemmonitor/b$a;

    sget v3, Lcom/cgollner/systemmonitor/b/a$e;->tab_battery_history:I

    sget v4, Lcom/cgollner/systemmonitor/b/a$c;->holo_red_light:I

    sget v5, Lcom/cgollner/systemmonitor/b/a$h;->battery_title:I

    const-class v7, Lcom/cgollner/systemmonitor/d/c;

    invoke-direct {v2, v3, v4, v5, v7}, Lcom/cgollner/systemmonitor/b$a;-><init>(IIILjava/lang/Class;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/cgollner/systemmonitor/b;->b:Ljava/util/List;

    new-instance v2, Lcom/cgollner/systemmonitor/b$a;

    sget v3, Lcom/cgollner/systemmonitor/b/a$e;->tab_battery_stats:I

    sget v4, Lcom/cgollner/systemmonitor/b/a$c;->holo_blue_dark:I

    sget v5, Lcom/cgollner/systemmonitor/b/a$h;->battery_stats:I

    const-class v7, Lcom/cgollner/systemmonitor/d/d;

    invoke-direct {v2, v3, v4, v5, v7}, Lcom/cgollner/systemmonitor/b$a;-><init>(IIILjava/lang/Class;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/cgollner/systemmonitor/b;->b:Ljava/util/List;

    new-instance v2, Lcom/cgollner/systemmonitor/b$a;

    sget v3, Lcom/cgollner/systemmonitor/b/a$e;->tab_battery_temperature:I

    sget v4, Lcom/cgollner/systemmonitor/b/a$c;->holo_blue_light:I

    sget v5, Lcom/cgollner/systemmonitor/b/a$h;->battery_temperature:I

    const-class v7, Lcom/cgollner/systemmonitor/d/e;

    invoke-direct {v2, v3, v4, v5, v7}, Lcom/cgollner/systemmonitor/b$a;-><init>(IIILjava/lang/Class;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 113
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/b;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    sget v2, Lcom/cgollner/systemmonitor/b/a$e;->drawerFragment:I

    invoke-virtual {v0, v2}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/NavigationDrawerFragment;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/b;->h:Lcom/cgollner/systemmonitor/NavigationDrawerFragment;

    .line 114
    iget-object v0, p0, Lcom/cgollner/systemmonitor/b;->h:Lcom/cgollner/systemmonitor/NavigationDrawerFragment;

    iget-object v2, p0, Lcom/cgollner/systemmonitor/b;->b:Ljava/util/List;

    invoke-virtual {v0, v2}, Lcom/cgollner/systemmonitor/NavigationDrawerFragment;->a(Ljava/util/List;)V

    .line 115
    sget v0, Lcom/cgollner/systemmonitor/b/a$e;->viewPager:I

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/b;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/b;->c:Landroid/support/v4/view/ViewPager;

    .line 116
    iget-object v0, p0, Lcom/cgollner/systemmonitor/b;->c:Landroid/support/v4/view/ViewPager;

    new-instance v2, Lcom/cgollner/systemmonitor/e/a;

    invoke-direct {v2}, Lcom/cgollner/systemmonitor/e/a;-><init>()V

    invoke-virtual {v0, v6, v2}, Landroid/support/v4/view/ViewPager;->setPageTransformer(ZLandroid/support/v4/view/ViewPager$PageTransformer;)V

    .line 117
    sget v0, Lcom/cgollner/systemmonitor/b/a$e;->pager_title_strip:I

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/b;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/PagerTitleStrip;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/b;->d:Landroid/support/v4/view/PagerTitleStrip;

    .line 119
    iget-object v0, p0, Lcom/cgollner/systemmonitor/b;->c:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    new-instance v2, Lcom/cgollner/systemmonitor/b$1;

    invoke-direct {v2, p0}, Lcom/cgollner/systemmonitor/b$1;-><init>(Lcom/cgollner/systemmonitor/b;)V

    invoke-virtual {v0, v2}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 166
    new-instance v0, Lcom/cgollner/systemmonitor/b$b;

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/b;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    invoke-direct {v0, p0, v2}, Lcom/cgollner/systemmonitor/b$b;-><init>(Lcom/cgollner/systemmonitor/b;Landroid/support/v4/app/FragmentManager;)V

    .line 167
    iget-object v2, p0, Lcom/cgollner/systemmonitor/b;->c:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v2, v0}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 168
    iget-object v2, p0, Lcom/cgollner/systemmonitor/b;->c:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v2, v0}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 169
    iget-object v0, p0, Lcom/cgollner/systemmonitor/b;->c:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v6}, Landroid/support/v4/view/ViewPager;->setOffscreenPageLimit(I)V

    .line 170
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/b;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "settings"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 171
    if-eqz v0, :cond_3

    .line 172
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/b;->a()V

    .line 175
    :cond_3
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    sget-object v1, Lcom/cgollner/systemmonitor/App;->a:Landroid/content/Context;

    sget v2, Lcom/cgollner/systemmonitor/b/a$h;->battery_history_key:I

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 180
    new-instance v0, Landroid/content/Intent;

    sget-object v1, Lcom/cgollner/systemmonitor/App;->a:Landroid/content/Context;

    const-class v2, Lcom/cgollner/systemmonitor/battery/BatteryService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/b;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 190
    :cond_4
    sget v0, Lcom/cgollner/systemmonitor/b/a$e;->drawer_layout:I

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/b;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/DrawerLayout;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/b;->f:Landroid/support/v4/widget/DrawerLayout;

    .line 191
    iget-object v0, p0, Lcom/cgollner/systemmonitor/b;->f:Landroid/support/v4/widget/DrawerLayout;

    if-eqz v0, :cond_5

    .line 192
    new-instance v0, Lcom/cgollner/systemmonitor/b$2;

    iget-object v3, p0, Lcom/cgollner/systemmonitor/b;->f:Landroid/support/v4/widget/DrawerLayout;

    sget v4, Lcom/cgollner/systemmonitor/b/a$h;->drawer_open:I

    sget v5, Lcom/cgollner/systemmonitor/b/a$h;->drawer_close:I

    move-object v1, p0

    move-object v2, p0

    invoke-direct/range {v0 .. v5}, Lcom/cgollner/systemmonitor/b$2;-><init>(Lcom/cgollner/systemmonitor/b;Landroid/app/Activity;Landroid/support/v4/widget/DrawerLayout;II)V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/b;->g:Landroid/support/v7/app/ActionBarDrawerToggle;

    .line 207
    iget-object v0, p0, Lcom/cgollner/systemmonitor/b;->f:Landroid/support/v4/widget/DrawerLayout;

    iget-object v1, p0, Lcom/cgollner/systemmonitor/b;->g:Landroid/support/v7/app/ActionBarDrawerToggle;

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/DrawerLayout;->setDrawerListener(Landroid/support/v4/widget/DrawerLayout$DrawerListener;)V

    .line 209
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/b;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/support/v7/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 210
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/b;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/support/v7/app/ActionBar;->setHomeButtonEnabled(Z)V

    .line 218
    :cond_5
    return-void

    .line 105
    :pswitch_0
    sget v0, Lcom/cgollner/systemmonitor/b/a$i;->AppTheme:I

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/b;->setTheme(I)V

    goto/16 :goto_2

    :pswitch_1
    sget v0, Lcom/cgollner/systemmonitor/b/a$i;->AppThemeDark:I

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/b;->setTheme(I)V

    goto/16 :goto_2

    :cond_6
    move v0, v1

    goto/16 :goto_0

    :cond_7
    sget v0, Lcom/cgollner/systemmonitor/b/a$i;->AppTheme:I

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    .prologue
    .line 321
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/b;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 322
    sget v1, Lcom/cgollner/systemmonitor/b/a$g;->activity_main:I

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 323
    invoke-super {p0, p1}, Landroid/support/v7/app/ActionBarActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v3, 0x3

    .line 330
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x102002c

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lcom/cgollner/systemmonitor/b;->f:Landroid/support/v4/widget/DrawerLayout;

    if-eqz v1, :cond_1

    .line 331
    iget-object v1, p0, Lcom/cgollner/systemmonitor/b;->f:Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {v1, v3}, Landroid/support/v4/widget/DrawerLayout;->isDrawerOpen(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 332
    iget-object v1, p0, Lcom/cgollner/systemmonitor/b;->f:Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {v1, v3}, Landroid/support/v4/widget/DrawerLayout;->closeDrawer(I)V

    .line 343
    :goto_0
    return v0

    .line 335
    :cond_0
    iget-object v1, p0, Lcom/cgollner/systemmonitor/b;->f:Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {v1, v3}, Landroid/support/v4/widget/DrawerLayout;->openDrawer(I)V

    goto :goto_0

    .line 339
    :cond_1
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sget v2, Lcom/cgollner/systemmonitor/b/a$e;->menu_settings:I

    if-ne v1, v2, :cond_2

    .line 340
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/b;->a()V

    goto :goto_0

    .line 343
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onPause()V
    .locals 3

    .prologue
    .line 434
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 435
    const-string v1, "pos"

    iget-object v2, p0, Lcom/cgollner/systemmonitor/b;->c:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v2}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 436
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 437
    invoke-super {p0}, Landroid/support/v7/app/ActionBarActivity;->onPause()V

    .line 438
    return-void
.end method

.method protected onPostCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 284
    invoke-super {p0, p1}, Landroid/support/v7/app/ActionBarActivity;->onPostCreate(Landroid/os/Bundle;)V

    .line 286
    iget-object v0, p0, Lcom/cgollner/systemmonitor/b;->f:Landroid/support/v4/widget/DrawerLayout;

    if-eqz v0, :cond_0

    .line 287
    iget-object v0, p0, Lcom/cgollner/systemmonitor/b;->g:Landroid/support/v7/app/ActionBarDrawerToggle;

    invoke-virtual {v0}, Landroid/support/v7/app/ActionBarDrawerToggle;->syncState()V

    .line 289
    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 270
    invoke-super {p0}, Landroid/support/v7/app/ActionBarActivity;->onResume()V

    .line 271
    iget-object v0, p0, Lcom/cgollner/systemmonitor/b;->f:Landroid/support/v4/widget/DrawerLayout;

    if-nez v0, :cond_0

    .line 272
    iget-object v0, p0, Lcom/cgollner/systemmonitor/b;->h:Lcom/cgollner/systemmonitor/NavigationDrawerFragment;

    invoke-virtual {v0}, Lcom/cgollner/systemmonitor/NavigationDrawerFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    .line 273
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/b;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    .line 275
    invoke-virtual {v1}, Landroid/view/Display;->getWidth()I

    move-result v1

    .line 276
    sub-int v0, v1, v0

    .line 277
    iget-object v1, p0, Lcom/cgollner/systemmonitor/b;->c:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 278
    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 279
    iget-object v0, p0, Lcom/cgollner/systemmonitor/b;->c:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 281
    :cond_0
    return-void
.end method
