.class final Lcom/cgollner/systemmonitor/d/o$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/cgollner/systemmonitor/d/o;->a()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/cgollner/systemmonitor/d/o;


# direct methods
.method constructor <init>(Lcom/cgollner/systemmonitor/d/o;)V
    .locals 0

    .prologue
    .line 75
    iput-object p1, p0, Lcom/cgollner/systemmonitor/d/o$1;->a:Lcom/cgollner/systemmonitor/d/o;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    .line 77
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/o$1;->a:Lcom/cgollner/systemmonitor/d/o;

    invoke-virtual {v0}, Lcom/cgollner/systemmonitor/d/o;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/o$1;->a:Lcom/cgollner/systemmonitor/d/o;

    invoke-virtual {v0}, Lcom/cgollner/systemmonitor/d/o;->isDetached()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 85
    :cond_0
    :goto_0
    return-void

    .line 80
    :cond_1
    :try_start_0
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/o$1;->a:Lcom/cgollner/systemmonitor/d/o;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/d/o;->b(Lcom/cgollner/systemmonitor/d/o;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/cgollner/systemmonitor/d/o$1;->a:Lcom/cgollner/systemmonitor/d/o;

    invoke-static {v1}, Lcom/cgollner/systemmonitor/d/o;->a(Lcom/cgollner/systemmonitor/d/o;)Lcom/cgollner/systemmonitor/c/g;

    move-result-object v1

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    const-string v3, "%.0f%%"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget v1, v1, Lcom/cgollner/systemmonitor/c/g;->a:F

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    aput-object v1, v4, v5

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 81
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/o$1;->a:Lcom/cgollner/systemmonitor/d/o;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/d/o;->c(Lcom/cgollner/systemmonitor/d/o;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/cgollner/systemmonitor/d/o$1;->a:Lcom/cgollner/systemmonitor/d/o;

    invoke-static {v1}, Lcom/cgollner/systemmonitor/d/o;->a(Lcom/cgollner/systemmonitor/d/o;)Lcom/cgollner/systemmonitor/c/g;

    move-result-object v1

    iget-wide v2, v1, Lcom/cgollner/systemmonitor/c/g;->c:J

    long-to-double v2, v2

    invoke-static {v2, v3}, Lcom/cgollner/systemmonitor/a/i;->b(D)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 82
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/o$1;->a:Lcom/cgollner/systemmonitor/d/o;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/d/o;->d(Lcom/cgollner/systemmonitor/d/o;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/cgollner/systemmonitor/d/o$1;->a:Lcom/cgollner/systemmonitor/d/o;

    invoke-static {v1}, Lcom/cgollner/systemmonitor/d/o;->a(Lcom/cgollner/systemmonitor/d/o;)Lcom/cgollner/systemmonitor/c/g;

    move-result-object v1

    iget-wide v2, v1, Lcom/cgollner/systemmonitor/c/g;->d:J

    long-to-double v2, v2

    invoke-static {v2, v3}, Lcom/cgollner/systemmonitor/a/i;->b(D)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 83
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/o$1;->a:Lcom/cgollner/systemmonitor/d/o;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/d/o;->e(Lcom/cgollner/systemmonitor/d/o;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/cgollner/systemmonitor/d/o$1;->a:Lcom/cgollner/systemmonitor/d/o;

    invoke-static {v1}, Lcom/cgollner/systemmonitor/d/o;->a(Lcom/cgollner/systemmonitor/d/o;)Lcom/cgollner/systemmonitor/c/g;

    move-result-object v1

    iget-wide v2, v1, Lcom/cgollner/systemmonitor/c/g;->b:J

    long-to-double v2, v2

    invoke-static {v2, v3}, Lcom/cgollner/systemmonitor/a/i;->b(D)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 85
    :catch_0
    move-exception v0

    goto :goto_0
.end method
