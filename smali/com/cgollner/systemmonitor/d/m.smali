.class public Lcom/cgollner/systemmonitor/d/m;
.super Landroid/support/v4/app/ListFragment;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "UseSparseArrays"
    }
.end annotation


# instance fields
.field private a:Landroid/os/Handler;

.field private b:Landroid/content/Context;

.field private c:Lcom/cgollner/systemmonitor/d/n;

.field private d:Landroid/view/View;

.field private e:Landroid/view/View;

.field private f:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/util/Comparator",
            "<",
            "Lcom/cgollner/systemmonitor/d/a;",
            ">;>;"
        }
    .end annotation
.end field

.field private g:Lcom/cgollner/systemmonitor/d/q;

.field private h:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Landroid/support/v4/app/ListFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 119
    instance-of v0, p1, Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 120
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/m;->h:Landroid/widget/TextView;

    sget-object v1, Landroid/graphics/Typeface;->SANS_SERIF:Landroid/graphics/Typeface;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 121
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/m;->h:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "<u>"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "</u>"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 122
    iget-object v1, p0, Lcom/cgollner/systemmonitor/d/m;->h:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object v0, p1

    .line 124
    check-cast v0, Landroid/widget/TextView;

    .line 125
    sget-object v1, Landroid/graphics/Typeface;->SANS_SERIF:Landroid/graphics/Typeface;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 127
    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/m;->h:Landroid/widget/TextView;

    .line 128
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/m;->h:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "<u>"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/cgollner/systemmonitor/d/m;->h:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "</u>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 130
    :cond_0
    iget-object v1, p0, Lcom/cgollner/systemmonitor/d/m;->c:Lcom/cgollner/systemmonitor/d/n;

    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/m;->f:Ljava/util/HashMap;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Comparator;

    iput-object v0, v1, Lcom/cgollner/systemmonitor/d/n;->l:Ljava/util/Comparator;

    .line 131
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/m;->g:Lcom/cgollner/systemmonitor/d/q;

    iget-object v1, v0, Lcom/cgollner/systemmonitor/d/q;->a:Ljava/util/List;

    monitor-enter v1

    .line 133
    :try_start_0
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/m;->g:Lcom/cgollner/systemmonitor/d/q;

    iget-object v0, v0, Lcom/cgollner/systemmonitor/d/q;->a:Ljava/util/List;

    iget-object v2, p0, Lcom/cgollner/systemmonitor/d/m;->c:Lcom/cgollner/systemmonitor/d/n;

    iget-object v2, v2, Lcom/cgollner/systemmonitor/d/n;->l:Ljava/util/Comparator;

    invoke-static {v0, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 137
    :goto_0
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 138
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/m;->g:Lcom/cgollner/systemmonitor/d/q;

    invoke-virtual {v0}, Lcom/cgollner/systemmonitor/d/q;->notifyDataSetChanged()V

    .line 139
    return-void

    .line 137
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 44
    invoke-super {p0, p1}, Landroid/support/v4/app/ListFragment;->onCreate(Landroid/os/Bundle;)V

    .line 45
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/m;->a:Landroid/os/Handler;

    .line 47
    new-instance v0, Lcom/cgollner/systemmonitor/d/q;

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/d/m;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    sget v2, Lcom/cgollner/systemmonitor/b/a$f;->apps_top_entry:I

    invoke-direct {v0, v1, v2}, Lcom/cgollner/systemmonitor/d/q;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/m;->g:Lcom/cgollner/systemmonitor/d/q;

    .line 48
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/m;->g:Lcom/cgollner/systemmonitor/d/q;

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/d/m;->setListAdapter(Landroid/widget/ListAdapter;)V

    .line 50
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/d/m;->setHasOptionsMenu(Z)V

    .line 51
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 1

    .prologue
    .line 61
    sget v0, Lcom/cgollner/systemmonitor/b/a$g;->topapps_fragment_menu:I

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 62
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .prologue
    .line 78
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/d/m;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/m;->b:Landroid/content/Context;

    .line 79
    sget v0, Lcom/cgollner/systemmonitor/b/a$f;->top_apps_layout:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/m;->d:Landroid/view/View;

    .line 80
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/m;->d:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/b/a$e;->progressBar:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/m;->e:Landroid/view/View;

    .line 82
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/m;->d:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/b/a$e;->buttonCPU:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 83
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/m;->d:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/b/a$e;->buttonRAM:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 84
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/m;->d:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/b/a$e;->buttonCPUTIME:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 85
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/m;->d:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/b/a$e;->buttonName:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 87
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/m;->f:Ljava/util/HashMap;

    .line 88
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/m;->f:Ljava/util/HashMap;

    sget v1, Lcom/cgollner/systemmonitor/b/a$e;->buttonName:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v2, Lcom/cgollner/systemmonitor/d/a$c;

    invoke-direct {v2}, Lcom/cgollner/systemmonitor/d/a$c;-><init>()V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 89
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/m;->f:Ljava/util/HashMap;

    sget v1, Lcom/cgollner/systemmonitor/b/a$e;->buttonCPU:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v2, Lcom/cgollner/systemmonitor/d/a$a;

    invoke-direct {v2}, Lcom/cgollner/systemmonitor/d/a$a;-><init>()V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 90
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/m;->f:Ljava/util/HashMap;

    sget v1, Lcom/cgollner/systemmonitor/b/a$e;->buttonRAM:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v2, Lcom/cgollner/systemmonitor/d/a$d;

    invoke-direct {v2}, Lcom/cgollner/systemmonitor/d/a$d;-><init>()V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 91
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/m;->f:Ljava/util/HashMap;

    sget v1, Lcom/cgollner/systemmonitor/b/a$e;->buttonCPUTIME:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v2, Lcom/cgollner/systemmonitor/d/a$b;

    invoke-direct {v2}, Lcom/cgollner/systemmonitor/d/a$b;-><init>()V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 93
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/m;->d:Landroid/view/View;

    return-object v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 66
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sget v2, Lcom/cgollner/systemmonitor/b/a$e;->menu_show_all_processes:I

    if-ne v0, v2, :cond_1

    .line 67
    iget-object v2, p0, Lcom/cgollner/systemmonitor/d/m;->c:Lcom/cgollner/systemmonitor/d/n;

    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/m;->c:Lcom/cgollner/systemmonitor/d/n;

    iget-boolean v0, v0, Lcom/cgollner/systemmonitor/d/n;->k:Z

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, v2, Lcom/cgollner/systemmonitor/d/n;->k:Z

    .line 68
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/m;->c:Lcom/cgollner/systemmonitor/d/n;

    iget-boolean v0, v0, Lcom/cgollner/systemmonitor/d/n;->k:Z

    invoke-interface {p1, v0}, Landroid/view/MenuItem;->setChecked(Z)Landroid/view/MenuItem;

    .line 69
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/m;->b:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "showAllProcesses"

    iget-object v3, p0, Lcom/cgollner/systemmonitor/d/m;->c:Lcom/cgollner/systemmonitor/d/n;

    iget-boolean v3, v3, Lcom/cgollner/systemmonitor/d/n;->k:Z

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 73
    :goto_1
    return v1

    .line 67
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 73
    :cond_1
    invoke-super {p0, p1}, Landroid/support/v4/app/ListFragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v1

    goto :goto_1
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 98
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/m;->c:Lcom/cgollner/systemmonitor/d/n;

    if-eqz v0, :cond_0

    .line 99
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/m;->c:Lcom/cgollner/systemmonitor/d/n;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/cgollner/systemmonitor/d/n;->g:Z

    .line 100
    :cond_0
    invoke-super {p0}, Landroid/support/v4/app/ListFragment;->onPause()V

    .line 101
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 4

    .prologue
    .line 55
    sget v0, Lcom/cgollner/systemmonitor/b/a$e;->menu_show_all_processes:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/d/m;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v2, "showAllProcesses"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setChecked(Z)Landroid/view/MenuItem;

    .line 57
    invoke-super {p0, p1}, Landroid/support/v4/app/ListFragment;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 58
    return-void
.end method

.method public onResume()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 105
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/m;->c:Lcom/cgollner/systemmonitor/d/n;

    if-eqz v0, :cond_0

    .line 106
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/m;->c:Lcom/cgollner/systemmonitor/d/n;

    iput-boolean v5, v0, Lcom/cgollner/systemmonitor/d/n;->g:Z

    .line 107
    :cond_0
    new-instance v0, Lcom/cgollner/systemmonitor/d/n;

    iget-object v1, p0, Lcom/cgollner/systemmonitor/d/m;->g:Lcom/cgollner/systemmonitor/d/q;

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/d/m;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/cgollner/systemmonitor/d/m;->a:Landroid/os/Handler;

    iget-object v4, p0, Lcom/cgollner/systemmonitor/d/m;->e:Landroid/view/View;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/cgollner/systemmonitor/d/n;-><init>(Lcom/cgollner/systemmonitor/d/q;Landroid/content/Context;Landroid/os/Handler;Landroid/view/View;)V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/m;->c:Lcom/cgollner/systemmonitor/d/n;

    .line 108
    iget-object v1, p0, Lcom/cgollner/systemmonitor/d/m;->c:Lcom/cgollner/systemmonitor/d/n;

    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/m;->f:Ljava/util/HashMap;

    sget v2, Lcom/cgollner/systemmonitor/b/a$e;->buttonCPU:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Comparator;

    iput-object v0, v1, Lcom/cgollner/systemmonitor/d/n;->l:Ljava/util/Comparator;

    .line 109
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/m;->d:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/b/a$e;->buttonCPU:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/m;->h:Landroid/widget/TextView;

    .line 110
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/m;->h:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "<u>"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/cgollner/systemmonitor/d/m;->h:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "</u>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 111
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/m;->c:Lcom/cgollner/systemmonitor/d/n;

    iget-object v1, p0, Lcom/cgollner/systemmonitor/d/m;->b:Landroid/content/Context;

    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v2, "showAllProcesses"

    invoke-interface {v1, v2, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, v0, Lcom/cgollner/systemmonitor/d/n;->k:Z

    .line 112
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/m;->c:Lcom/cgollner/systemmonitor/d/n;

    invoke-virtual {v0}, Lcom/cgollner/systemmonitor/d/n;->start()V

    .line 113
    invoke-super {p0}, Landroid/support/v4/app/ListFragment;->onResume()V

    .line 114
    return-void
.end method
