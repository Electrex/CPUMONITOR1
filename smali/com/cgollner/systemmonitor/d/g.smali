.class public Lcom/cgollner/systemmonitor/d/g;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"

# interfaces
.implements Lcom/cgollner/systemmonitor/c/e$a;


# static fields
.field public static a:I

.field private static j:I

.field private static n:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private static o:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private static p:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private static q:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private static v:Z


# instance fields
.field private A:Landroid/content/Context;

.field private b:Lcom/cgollner/systemmonitor/MonitorView;

.field private c:Lcom/cgollner/systemmonitor/MonitorView;

.field private d:Lcom/cgollner/systemmonitor/MonitorView;

.field private e:Lcom/cgollner/systemmonitor/MonitorView;

.field private f:Landroid/widget/TextView;

.field private g:Landroid/widget/TextView;

.field private h:Landroid/widget/TextView;

.field private i:Landroid/widget/TextView;

.field private k:Landroid/view/View;

.field private l:Landroid/view/View;

.field private m:Landroid/view/View;

.field private r:[Landroid/widget/TextView;

.field private s:[Landroid/widget/TextView;

.field private t:[Landroid/widget/TextView;

.field private u:Landroid/view/View;

.field private w:Landroid/os/Handler;

.field private x:Lcom/cgollner/systemmonitor/c/a;

.field private y:[Lcom/cgollner/systemmonitor/MonitorView;

.field private z:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/cgollner/systemmonitor/d/g;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/g;->f:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic b(Lcom/cgollner/systemmonitor/d/g;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/g;->g:Landroid/widget/TextView;

    return-object v0
.end method

.method private b()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/16 v3, 0x8

    const/4 v1, 0x0

    .line 114
    invoke-static {}, Lcom/cgollner/systemmonitor/a/b;->b()I

    move-result v0

    iput v0, p0, Lcom/cgollner/systemmonitor/d/g;->z:I

    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/g;->A:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v2, "displayAllCores"

    invoke-interface {v0, v2, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/cgollner/systemmonitor/d/g;->v:Z

    if-nez v0, :cond_0

    sput v4, Lcom/cgollner/systemmonitor/d/g;->j:I

    .line 115
    :goto_0
    const/4 v0, 0x3

    :goto_1
    sget v2, Lcom/cgollner/systemmonitor/d/g;->j:I

    if-lt v0, v2, :cond_1

    .line 116
    iget-object v2, p0, Lcom/cgollner/systemmonitor/d/g;->y:[Lcom/cgollner/systemmonitor/MonitorView;

    aget-object v2, v2, v0

    invoke-virtual {v2, v3}, Lcom/cgollner/systemmonitor/MonitorView;->setVisibility(I)V

    .line 117
    iget-object v2, p0, Lcom/cgollner/systemmonitor/d/g;->s:[Landroid/widget/TextView;

    aget-object v2, v2, v0

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 118
    iget-object v2, p0, Lcom/cgollner/systemmonitor/d/g;->t:[Landroid/widget/TextView;

    aget-object v2, v2, v0

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 119
    iget-object v2, p0, Lcom/cgollner/systemmonitor/d/g;->r:[Landroid/widget/TextView;

    aget-object v2, v2, v0

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 115
    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    .line 114
    :cond_0
    iget v0, p0, Lcom/cgollner/systemmonitor/d/g;->z:I

    sput v0, Lcom/cgollner/systemmonitor/d/g;->j:I

    goto :goto_0

    :cond_1
    move v0, v1

    .line 121
    :goto_2
    sget v2, Lcom/cgollner/systemmonitor/d/g;->j:I

    if-ge v0, v2, :cond_2

    .line 122
    iget-object v2, p0, Lcom/cgollner/systemmonitor/d/g;->y:[Lcom/cgollner/systemmonitor/MonitorView;

    aget-object v2, v2, v0

    invoke-virtual {v2, v1}, Lcom/cgollner/systemmonitor/MonitorView;->setVisibility(I)V

    .line 123
    iget-object v2, p0, Lcom/cgollner/systemmonitor/d/g;->s:[Landroid/widget/TextView;

    aget-object v2, v2, v0

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 124
    iget-object v2, p0, Lcom/cgollner/systemmonitor/d/g;->t:[Landroid/widget/TextView;

    aget-object v2, v2, v0

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 125
    iget-object v2, p0, Lcom/cgollner/systemmonitor/d/g;->r:[Landroid/widget/TextView;

    aget-object v2, v2, v0

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 121
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 127
    :cond_2
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/d/g;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v5, :cond_4

    .line 128
    sget v0, Lcom/cgollner/systemmonitor/d/g;->j:I

    const/4 v2, 0x4

    if-ge v0, v2, :cond_3

    .line 129
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/g;->k:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 142
    :goto_3
    sget v0, Lcom/cgollner/systemmonitor/d/g;->j:I

    if-le v0, v4, :cond_6

    .line 143
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/g;->l:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 144
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/g;->m:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 150
    :goto_4
    return-void

    .line 131
    :cond_3
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/g;->k:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_3

    .line 135
    :cond_4
    sget v0, Lcom/cgollner/systemmonitor/d/g;->j:I

    if-ge v0, v5, :cond_5

    .line 136
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/g;->k:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_3

    .line 138
    :cond_5
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/g;->k:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_3

    .line 147
    :cond_6
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/g;->l:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 148
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/g;->m:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_4
.end method

.method static synthetic c(Lcom/cgollner/systemmonitor/d/g;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/g;->h:Landroid/widget/TextView;

    return-object v0
.end method

.method private c()V
    .locals 1

    .prologue
    .line 251
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/g;->x:Lcom/cgollner/systemmonitor/c/a;

    if-eqz v0, :cond_0

    .line 252
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/g;->x:Lcom/cgollner/systemmonitor/c/a;

    invoke-virtual {v0}, Lcom/cgollner/systemmonitor/c/a;->c()V

    .line 253
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/g;->x:Lcom/cgollner/systemmonitor/c/a;

    .line 255
    :cond_0
    return-void
.end method

.method static synthetic d(Lcom/cgollner/systemmonitor/d/g;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/g;->i:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic e(Lcom/cgollner/systemmonitor/d/g;)[Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/g;->s:[Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic f(Lcom/cgollner/systemmonitor/d/g;)[Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/g;->t:[Landroid/widget/TextView;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 13

    .prologue
    const/4 v12, 0x3

    const/4 v11, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 185
    sget v0, Lcom/cgollner/systemmonitor/d/g;->j:I

    if-ne v0, v8, :cond_2

    move v6, v7

    :goto_0
    move v9, v7

    .line 187
    :goto_1
    sget v0, Lcom/cgollner/systemmonitor/d/g;->j:I

    if-ge v9, v0, :cond_8

    .line 189
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/g;->y:[Lcom/cgollner/systemmonitor/MonitorView;

    aget-object v0, v0, v9

    invoke-static {v9}, Lcom/cgollner/systemmonitor/a/b;->c(I)Z

    move-result v1

    iput-boolean v1, v0, Lcom/cgollner/systemmonitor/MonitorView;->c:Z

    .line 190
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/g;->y:[Lcom/cgollner/systemmonitor/MonitorView;

    aget-object v0, v0, v9

    iget-object v1, p0, Lcom/cgollner/systemmonitor/d/g;->x:Lcom/cgollner/systemmonitor/c/a;

    iget-object v1, v1, Lcom/cgollner/systemmonitor/c/a;->a:[F

    add-int v2, v9, v6

    aget v1, v1, v2

    sget-boolean v2, Lcom/cgollner/systemmonitor/b;->a:Z

    invoke-virtual {v0, v1, v2}, Lcom/cgollner/systemmonitor/MonitorView;->a(FZ)V

    .line 192
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/d/g;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v11, :cond_4

    .line 193
    if-eq v9, v8, :cond_0

    if-ne v9, v12, :cond_3

    .line 194
    :cond_0
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/g;->y:[Lcom/cgollner/systemmonitor/MonitorView;

    aget-object v0, v0, v9

    invoke-virtual {v0, v8}, Lcom/cgollner/systemmonitor/MonitorView;->setDrawBorders$1d54120b(Z)V

    .line 205
    :goto_2
    sget-boolean v0, Lcom/cgollner/systemmonitor/b;->a:Z

    if-eqz v0, :cond_1

    .line 206
    sget v0, Lcom/cgollner/systemmonitor/d/g;->j:I

    if-le v0, v8, :cond_7

    .line 209
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/g;->x:Lcom/cgollner/systemmonitor/c/a;

    add-int v1, v9, v6

    invoke-virtual {v0, v1}, Lcom/cgollner/systemmonitor/c/a;->a(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/cgollner/systemmonitor/d/g;->x:Lcom/cgollner/systemmonitor/c/a;

    invoke-virtual {v1, v9}, Lcom/cgollner/systemmonitor/c/a;->b(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/cgollner/systemmonitor/d/g;->w:Landroid/os/Handler;

    new-instance v3, Lcom/cgollner/systemmonitor/d/g$2;

    invoke-direct {v3, p0, v9, v0, v1}, Lcom/cgollner/systemmonitor/d/g$2;-><init>(Lcom/cgollner/systemmonitor/d/g;ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 187
    :cond_1
    :goto_3
    add-int/lit8 v0, v9, 0x1

    move v9, v0

    goto :goto_1

    :cond_2
    move v6, v8

    .line 185
    goto :goto_0

    .line 196
    :cond_3
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/g;->y:[Lcom/cgollner/systemmonitor/MonitorView;

    aget-object v0, v0, v9

    invoke-virtual {v0, v7}, Lcom/cgollner/systemmonitor/MonitorView;->setDrawBorders$1d54120b(Z)V

    goto :goto_2

    .line 199
    :cond_4
    if-eq v9, v11, :cond_5

    if-ne v9, v12, :cond_6

    .line 200
    :cond_5
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/g;->y:[Lcom/cgollner/systemmonitor/MonitorView;

    aget-object v0, v0, v9

    invoke-virtual {v0, v8}, Lcom/cgollner/systemmonitor/MonitorView;->setDrawBorders$1d54120b(Z)V

    goto :goto_2

    .line 202
    :cond_6
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/g;->y:[Lcom/cgollner/systemmonitor/MonitorView;

    aget-object v0, v0, v9

    invoke-virtual {v0, v7}, Lcom/cgollner/systemmonitor/MonitorView;->setDrawBorders$1d54120b(Z)V

    goto :goto_2

    .line 212
    :cond_7
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/g;->x:Lcom/cgollner/systemmonitor/c/a;

    add-int v1, v9, v6

    invoke-virtual {v0, v1}, Lcom/cgollner/systemmonitor/c/a;->a(I)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/g;->x:Lcom/cgollner/systemmonitor/c/a;

    invoke-virtual {v0, v7}, Lcom/cgollner/systemmonitor/c/a;->b(I)Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/g;->x:Lcom/cgollner/systemmonitor/c/a;

    iget-object v0, v0, Lcom/cgollner/systemmonitor/c/a;->c:[I

    aget v0, v0, v7

    int-to-double v0, v0

    invoke-static {v0, v1}, Lcom/cgollner/systemmonitor/a/i;->a(D)Ljava/lang/String;

    move-result-object v4

    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/g;->x:Lcom/cgollner/systemmonitor/c/a;

    iget-object v0, v0, Lcom/cgollner/systemmonitor/c/a;->d:[I

    aget v0, v0, v7

    int-to-double v0, v0

    invoke-static {v0, v1}, Lcom/cgollner/systemmonitor/a/i;->a(D)Ljava/lang/String;

    move-result-object v5

    iget-object v10, p0, Lcom/cgollner/systemmonitor/d/g;->w:Landroid/os/Handler;

    new-instance v0, Lcom/cgollner/systemmonitor/d/g$1;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/cgollner/systemmonitor/d/g$1;-><init>(Lcom/cgollner/systemmonitor/d/g;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v10, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_3

    .line 220
    :cond_8
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 2

    .prologue
    .line 155
    invoke-static {}, Lcom/cgollner/systemmonitor/a/b;->b()I

    move-result v0

    .line 156
    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    .line 157
    sget v0, Lcom/cgollner/systemmonitor/b/a$g;->cpu_fragment_menu:I

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 158
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 9

    .prologue
    const/4 v1, 0x0

    const/4 v8, 0x1

    const/4 v7, 0x4

    .line 52
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/d/g;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/g;->A:Landroid/content/Context;

    .line 53
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/g;->A:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    sget-object v2, Lcom/cgollner/systemmonitor/App;->a:Landroid/content/Context;

    sget v3, Lcom/cgollner/systemmonitor/b/a$h;->settings_app_cpu_updatefreq_key:I

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2, v8}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    mul-int/lit16 v0, v0, 0x3e8

    sput v0, Lcom/cgollner/systemmonitor/d/g;->a:I

    .line 54
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/g;->w:Landroid/os/Handler;

    .line 55
    sget v0, Lcom/cgollner/systemmonitor/b/a$f;->cpu_fragment_layout:I

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/g;->u:Landroid/view/View;

    .line 56
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/g;->u:Landroid/view/View;

    sget v2, Lcom/cgollner/systemmonitor/b/a$e;->monitorview:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/MonitorView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/g;->b:Lcom/cgollner/systemmonitor/MonitorView;

    .line 57
    sget-object v0, Lcom/cgollner/systemmonitor/d/g;->n:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 58
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/g;->b:Lcom/cgollner/systemmonitor/MonitorView;

    iget-object v0, v0, Lcom/cgollner/systemmonitor/MonitorView;->a:Ljava/util/List;

    sget-object v2, Lcom/cgollner/systemmonitor/d/g;->n:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 60
    :cond_0
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/g;->u:Landroid/view/View;

    sget v2, Lcom/cgollner/systemmonitor/b/a$e;->monitorview2:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/MonitorView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/g;->c:Lcom/cgollner/systemmonitor/MonitorView;

    .line 61
    sget-object v0, Lcom/cgollner/systemmonitor/d/g;->o:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 62
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/g;->c:Lcom/cgollner/systemmonitor/MonitorView;

    iget-object v0, v0, Lcom/cgollner/systemmonitor/MonitorView;->a:Ljava/util/List;

    sget-object v2, Lcom/cgollner/systemmonitor/d/g;->o:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 64
    :cond_1
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/g;->u:Landroid/view/View;

    sget v2, Lcom/cgollner/systemmonitor/b/a$e;->monitorview3:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/MonitorView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/g;->d:Lcom/cgollner/systemmonitor/MonitorView;

    .line 65
    sget-object v0, Lcom/cgollner/systemmonitor/d/g;->p:Ljava/util/List;

    if-eqz v0, :cond_2

    .line 66
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/g;->d:Lcom/cgollner/systemmonitor/MonitorView;

    iget-object v0, v0, Lcom/cgollner/systemmonitor/MonitorView;->a:Ljava/util/List;

    sget-object v2, Lcom/cgollner/systemmonitor/d/g;->p:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 68
    :cond_2
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/g;->u:Landroid/view/View;

    sget v2, Lcom/cgollner/systemmonitor/b/a$e;->monitorview4:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/MonitorView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/g;->e:Lcom/cgollner/systemmonitor/MonitorView;

    .line 69
    sget-object v0, Lcom/cgollner/systemmonitor/d/g;->q:Ljava/util/List;

    if-eqz v0, :cond_3

    .line 70
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/g;->c:Lcom/cgollner/systemmonitor/MonitorView;

    iget-object v0, v0, Lcom/cgollner/systemmonitor/MonitorView;->a:Ljava/util/List;

    sget-object v2, Lcom/cgollner/systemmonitor/d/g;->q:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 72
    :cond_3
    new-array v0, v7, [Lcom/cgollner/systemmonitor/MonitorView;

    iget-object v2, p0, Lcom/cgollner/systemmonitor/d/g;->b:Lcom/cgollner/systemmonitor/MonitorView;

    aput-object v2, v0, v1

    iget-object v2, p0, Lcom/cgollner/systemmonitor/d/g;->c:Lcom/cgollner/systemmonitor/MonitorView;

    aput-object v2, v0, v8

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/cgollner/systemmonitor/d/g;->d:Lcom/cgollner/systemmonitor/MonitorView;

    aput-object v3, v0, v2

    const/4 v2, 0x3

    iget-object v3, p0, Lcom/cgollner/systemmonitor/d/g;->e:Lcom/cgollner/systemmonitor/MonitorView;

    aput-object v3, v0, v2

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/g;->y:[Lcom/cgollner/systemmonitor/MonitorView;

    .line 74
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/g;->u:Landroid/view/View;

    sget v2, Lcom/cgollner/systemmonitor/b/a$e;->usageAvg:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/g;->f:Landroid/widget/TextView;

    .line 75
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/g;->u:Landroid/view/View;

    sget v2, Lcom/cgollner/systemmonitor/b/a$e;->speedValue:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/g;->g:Landroid/widget/TextView;

    .line 76
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/g;->u:Landroid/view/View;

    sget v2, Lcom/cgollner/systemmonitor/b/a$e;->minSpeedValue:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/g;->h:Landroid/widget/TextView;

    .line 77
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/g;->u:Landroid/view/View;

    sget v2, Lcom/cgollner/systemmonitor/b/a$e;->maxSpeedValue:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/g;->i:Landroid/widget/TextView;

    .line 78
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/g;->u:Landroid/view/View;

    sget v2, Lcom/cgollner/systemmonitor/b/a$e;->quadCoreLayout:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/g;->k:Landroid/view/View;

    .line 80
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/g;->u:Landroid/view/View;

    sget v2, Lcom/cgollner/systemmonitor/b/a$e;->stats_layout:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/g;->l:Landroid/view/View;

    .line 81
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/g;->u:Landroid/view/View;

    sget v2, Lcom/cgollner/systemmonitor/b/a$e;->stats_layout_cpu:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/g;->m:Landroid/view/View;

    .line 83
    new-array v0, v7, [Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/g;->s:[Landroid/widget/TextView;

    .line 84
    new-array v0, v7, [Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/g;->t:[Landroid/widget/TextView;

    .line 85
    new-array v0, v7, [Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/g;->r:[Landroid/widget/TextView;

    .line 87
    :goto_0
    if-ge v1, v7, :cond_4

    .line 88
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/d/g;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "cpuStatTitle"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "id"

    iget-object v4, p0, Lcom/cgollner/systemmonitor/d/g;->A:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v2, v3, v4}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 89
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/d/g;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "cpuStatVal"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "id"

    iget-object v5, p0, Lcom/cgollner/systemmonitor/d/g;->A:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    .line 90
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/d/g;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "cpuTitle"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "id"

    iget-object v6, p0, Lcom/cgollner/systemmonitor/d/g;->A:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v4, v5, v6}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    .line 92
    iget-object v4, p0, Lcom/cgollner/systemmonitor/d/g;->s:[Landroid/widget/TextView;

    iget-object v5, p0, Lcom/cgollner/systemmonitor/d/g;->u:Landroid/view/View;

    invoke-virtual {v5, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    aput-object v0, v4, v1

    .line 93
    iget-object v4, p0, Lcom/cgollner/systemmonitor/d/g;->t:[Landroid/widget/TextView;

    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/g;->u:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    aput-object v0, v4, v1

    .line 94
    iget-object v2, p0, Lcom/cgollner/systemmonitor/d/g;->r:[Landroid/widget/TextView;

    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/g;->u:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    aput-object v0, v2, v1

    .line 87
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_0

    .line 97
    :cond_4
    invoke-direct {p0}, Lcom/cgollner/systemmonitor/d/g;->b()V

    .line 99
    invoke-virtual {p0, v8}, Lcom/cgollner/systemmonitor/d/g;->setHasOptionsMenu(Z)V

    .line 100
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/g;->u:Landroid/view/View;

    return-object v0
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/g;->b:Lcom/cgollner/systemmonitor/MonitorView;

    iget-object v0, v0, Lcom/cgollner/systemmonitor/MonitorView;->a:Ljava/util/List;

    sput-object v0, Lcom/cgollner/systemmonitor/d/g;->n:Ljava/util/List;

    .line 175
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/g;->c:Lcom/cgollner/systemmonitor/MonitorView;

    iget-object v0, v0, Lcom/cgollner/systemmonitor/MonitorView;->a:Ljava/util/List;

    sput-object v0, Lcom/cgollner/systemmonitor/d/g;->o:Ljava/util/List;

    .line 176
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/g;->d:Lcom/cgollner/systemmonitor/MonitorView;

    iget-object v0, v0, Lcom/cgollner/systemmonitor/MonitorView;->a:Ljava/util/List;

    sput-object v0, Lcom/cgollner/systemmonitor/d/g;->p:Ljava/util/List;

    .line 177
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/g;->e:Lcom/cgollner/systemmonitor/MonitorView;

    iget-object v0, v0, Lcom/cgollner/systemmonitor/MonitorView;->a:Ljava/util/List;

    sput-object v0, Lcom/cgollner/systemmonitor/d/g;->q:Ljava/util/List;

    .line 179
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroyView()V

    .line 180
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 162
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    sget v3, Lcom/cgollner/systemmonitor/b/a$e;->display_all_cores:I

    if-ne v2, v3, :cond_1

    .line 163
    iget-object v2, p0, Lcom/cgollner/systemmonitor/d/g;->A:Landroid/content/Context;

    invoke-static {v2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 164
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    .line 165
    const-string v4, "displayAllCores"

    const-string v5, "displayAllCores"

    invoke-interface {v2, v5, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    :cond_0
    invoke-interface {v3, v4, v0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 166
    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 167
    invoke-direct {p0}, Lcom/cgollner/systemmonitor/d/g;->b()V

    .line 170
    :goto_0
    return v1

    :cond_1
    move v1, v0

    goto :goto_0
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 265
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onPause()V

    .line 266
    invoke-direct {p0}, Lcom/cgollner/systemmonitor/d/g;->c()V

    .line 267
    return-void
.end method

.method public onResume()V
    .locals 4

    .prologue
    .line 271
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 273
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/g;->x:Lcom/cgollner/systemmonitor/c/a;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/cgollner/systemmonitor/d/g;->c()V

    :cond_0
    new-instance v0, Lcom/cgollner/systemmonitor/c/a;

    sget v1, Lcom/cgollner/systemmonitor/d/g;->a:I

    int-to-long v2, v1

    invoke-direct {v0, v2, v3, p0}, Lcom/cgollner/systemmonitor/c/a;-><init>(JLcom/cgollner/systemmonitor/c/e$a;)V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/g;->x:Lcom/cgollner/systemmonitor/c/a;

    .line 275
    return-void
.end method
