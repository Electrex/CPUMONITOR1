.class final Lcom/cgollner/systemmonitor/d/b$b;
.super Landroid/widget/BaseAdapter;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cgollner/systemmonitor/d/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "b"
.end annotation


# instance fields
.field private final a:Landroid/widget/ListView;

.field private final b:Landroid/content/Context;

.field private final c:Lcom/cgollner/systemmonitor/a/a$b;

.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/cgollner/systemmonitor/a/a$a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Landroid/content/Context;Landroid/widget/ListView;Lcom/cgollner/systemmonitor/a/a$b;)V
    .locals 0

    .prologue
    .line 396
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 397
    iput-object p1, p0, Lcom/cgollner/systemmonitor/d/b$b;->b:Landroid/content/Context;

    .line 398
    iput-object p2, p0, Lcom/cgollner/systemmonitor/d/b$b;->a:Landroid/widget/ListView;

    .line 399
    iput-object p3, p0, Lcom/cgollner/systemmonitor/d/b$b;->c:Lcom/cgollner/systemmonitor/a/a$b;

    .line 400
    return-void
.end method

.method static synthetic a(Lcom/cgollner/systemmonitor/d/b$b;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 390
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/b$b;->b:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic a(Lcom/cgollner/systemmonitor/d/b$b;Ljava/util/List;)Ljava/util/List;
    .locals 0

    .prologue
    .line 390
    iput-object p1, p0, Lcom/cgollner/systemmonitor/d/b$b;->d:Ljava/util/List;

    return-object p1
.end method

.method static synthetic b(Lcom/cgollner/systemmonitor/d/b$b;)Lcom/cgollner/systemmonitor/a/a$b;
    .locals 1

    .prologue
    .line 390
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/b$b;->c:Lcom/cgollner/systemmonitor/a/a$b;

    return-object v0
.end method

.method static synthetic c(Lcom/cgollner/systemmonitor/d/b$b;)Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 390
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/b$b;->a:Landroid/widget/ListView;

    return-object v0
.end method


# virtual methods
.method public final getCount()I
    .locals 1

    .prologue
    .line 404
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/b$b;->d:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/b$b;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 409
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/b$b;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 414
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v2, 0x0

    .line 419
    .line 420
    if-nez p2, :cond_0

    .line 421
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/b$b;->b:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/cgollner/systemmonitor/b/a$f;->cache_item:I

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 423
    new-instance v1, Lcom/cgollner/systemmonitor/d/b$a;

    invoke-direct {v1}, Lcom/cgollner/systemmonitor/d/b$a;-><init>()V

    .line 424
    const v0, 0x1020014

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/cgollner/systemmonitor/d/b$a;->b:Landroid/widget/TextView;

    .line 425
    const v0, 0x1020015

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/cgollner/systemmonitor/d/b$a;->c:Landroid/widget/TextView;

    .line 426
    const v0, 0x1020006

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lcom/cgollner/systemmonitor/d/b$a;->a:Landroid/widget/ImageView;

    .line 427
    sget v0, Lcom/cgollner/systemmonitor/b/a$e;->delete:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lcom/cgollner/systemmonitor/d/b$a;->d:Landroid/widget/ImageView;

    .line 428
    sget v0, Lcom/cgollner/systemmonitor/b/a$e;->checked:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lcom/cgollner/systemmonitor/d/b$a;->e:Landroid/widget/ImageView;

    .line 429
    sget v0, Lcom/cgollner/systemmonitor/b/a$e;->separator:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v1, Lcom/cgollner/systemmonitor/d/b$a;->f:Landroid/view/View;

    .line 430
    sget v0, Lcom/cgollner/systemmonitor/b/a$e;->cacheItemLayout:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v1, Lcom/cgollner/systemmonitor/d/b$a;->g:Landroid/view/View;

    .line 432
    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 434
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/d/b$a;

    .line 436
    invoke-virtual {p0, p1}, Lcom/cgollner/systemmonitor/d/b$b;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/cgollner/systemmonitor/a/a$a;

    .line 437
    sget-object v3, Lcom/cgollner/systemmonitor/App;->a:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 438
    iget-object v4, v1, Lcom/cgollner/systemmonitor/a/a$a;->a:Ljava/lang/String;

    if-nez v4, :cond_1

    .line 440
    :try_start_0
    iget-object v4, v1, Lcom/cgollner/systemmonitor/a/a$a;->c:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v4

    .line 441
    invoke-virtual {v4, v3}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-interface {v5}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v1, Lcom/cgollner/systemmonitor/a/a$a;->a:Ljava/lang/String;

    .line 442
    invoke-virtual {v4, v3}, Landroid/content/pm/ApplicationInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    iput-object v3, v1, Lcom/cgollner/systemmonitor/a/a$a;->b:Landroid/graphics/drawable/Drawable;

    .line 443
    iget-object v3, v0, Lcom/cgollner/systemmonitor/d/b$a;->b:Landroid/widget/TextView;

    iget-object v4, v1, Lcom/cgollner/systemmonitor/a/a$a;->a:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 452
    :goto_0
    iget-object v3, v1, Lcom/cgollner/systemmonitor/a/a$a;->b:Landroid/graphics/drawable/Drawable;

    if-eqz v3, :cond_2

    .line 453
    iget-object v3, v0, Lcom/cgollner/systemmonitor/d/b$a;->a:Landroid/widget/ImageView;

    iget-object v4, v1, Lcom/cgollner/systemmonitor/a/a$a;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 456
    :goto_1
    iget-object v3, v0, Lcom/cgollner/systemmonitor/d/b$a;->c:Landroid/widget/TextView;

    iget-wide v4, v1, Lcom/cgollner/systemmonitor/a/a$a;->d:J

    invoke-static {v4, v5}, Lcom/cgollner/systemmonitor/a/i;->a(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 458
    iget-object v3, v0, Lcom/cgollner/systemmonitor/d/b$a;->d:Landroid/widget/ImageView;

    new-instance v4, Lcom/cgollner/systemmonitor/d/b$b$1;

    invoke-direct {v4, p0, v1}, Lcom/cgollner/systemmonitor/d/b$b$1;-><init>(Lcom/cgollner/systemmonitor/d/b$b;Lcom/cgollner/systemmonitor/a/a$a;)V

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 481
    iget-object v1, p0, Lcom/cgollner/systemmonitor/d/b$b;->a:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->getCheckedItemCount()I

    move-result v1

    if-lez v1, :cond_3

    const/4 v1, 0x1

    .line 482
    :goto_2
    iget-object v3, p0, Lcom/cgollner/systemmonitor/d/b$b;->a:Landroid/widget/ListView;

    invoke-virtual {v3, p1}, Landroid/widget/ListView;->isItemChecked(I)Z

    move-result v3

    .line 484
    if-eqz v1, :cond_5

    .line 485
    iget-object v1, v0, Lcom/cgollner/systemmonitor/d/b$a;->d:Landroid/widget/ImageView;

    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 486
    iget-object v1, v0, Lcom/cgollner/systemmonitor/d/b$a;->f:Landroid/view/View;

    invoke-virtual {v1, v6}, Landroid/view/View;->setVisibility(I)V

    .line 488
    if-eqz v3, :cond_4

    .line 489
    iget-object v1, v0, Lcom/cgollner/systemmonitor/d/b$a;->e:Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 490
    iget-object v1, v0, Lcom/cgollner/systemmonitor/d/b$a;->a:Landroid/widget/ImageView;

    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 501
    :goto_3
    iget-object v1, v0, Lcom/cgollner/systemmonitor/d/b$a;->g:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setSelected(Z)V

    .line 503
    iget-object v0, v0, Lcom/cgollner/systemmonitor/d/b$a;->g:Landroid/view/View;

    new-instance v1, Lcom/cgollner/systemmonitor/d/b$b$2;

    invoke-direct {v1, p0, p1}, Lcom/cgollner/systemmonitor/d/b$b$2;-><init>(Lcom/cgollner/systemmonitor/d/b$b;I)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 521
    return-object p2

    .line 445
    :catch_0
    move-exception v3

    invoke-virtual {v3}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    .line 447
    iget-object v3, v0, Lcom/cgollner/systemmonitor/d/b$a;->b:Landroid/widget/TextView;

    iget-object v4, v1, Lcom/cgollner/systemmonitor/a/a$a;->c:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 450
    :cond_1
    iget-object v3, v0, Lcom/cgollner/systemmonitor/d/b$a;->b:Landroid/widget/TextView;

    iget-object v4, v1, Lcom/cgollner/systemmonitor/a/a$a;->a:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 455
    :cond_2
    iget-object v3, v0, Lcom/cgollner/systemmonitor/d/b$a;->a:Landroid/widget/ImageView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_1

    :cond_3
    move v1, v2

    .line 481
    goto :goto_2

    .line 492
    :cond_4
    iget-object v1, v0, Lcom/cgollner/systemmonitor/d/b$a;->a:Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 493
    iget-object v1, v0, Lcom/cgollner/systemmonitor/d/b$a;->e:Landroid/widget/ImageView;

    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_3

    .line 496
    :cond_5
    iget-object v1, v0, Lcom/cgollner/systemmonitor/d/b$a;->a:Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 497
    iget-object v1, v0, Lcom/cgollner/systemmonitor/d/b$a;->d:Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 498
    iget-object v1, v0, Lcom/cgollner/systemmonitor/d/b$a;->f:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 499
    iget-object v1, v0, Lcom/cgollner/systemmonitor/d/b$a;->e:Landroid/widget/ImageView;

    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_3
.end method
