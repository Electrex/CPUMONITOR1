.class final Lcom/cgollner/systemmonitor/d/b$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cgollner/systemmonitor/d/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/cgollner/systemmonitor/d/b;


# direct methods
.method constructor <init>(Lcom/cgollner/systemmonitor/d/b;)V
    .locals 0

    .prologue
    .line 60
    iput-object p1, p0, Lcom/cgollner/systemmonitor/d/b$1;->a:Lcom/cgollner/systemmonitor/d/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    .line 63
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/b$1;->a:Lcom/cgollner/systemmonitor/d/b;

    invoke-virtual {v0}, Lcom/cgollner/systemmonitor/d/b;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/cgollner/systemmonitor/d/b;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 84
    :goto_0
    return-void

    .line 66
    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/cgollner/systemmonitor/d/b$1;->a:Lcom/cgollner/systemmonitor/d/b;

    invoke-virtual {v1}, Lcom/cgollner/systemmonitor/d/b;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget v1, Lcom/cgollner/systemmonitor/b/a$h;->clear_cache:I

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    sget-object v2, Lcom/cgollner/systemmonitor/App;->a:Landroid/content/Context;

    sget v3, Lcom/cgollner/systemmonitor/b/a$h;->are_you_sure_that_you_want_to_clear_the_cache_of_b_s_b_:I

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    sget-object v5, Lcom/cgollner/systemmonitor/App;->a:Landroid/content/Context;

    sget v6, Lcom/cgollner/systemmonitor/b/a$h;->all_apps:I

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x1040013

    new-instance v2, Lcom/cgollner/systemmonitor/d/b$1$2;

    invoke-direct {v2, p0}, Lcom/cgollner/systemmonitor/d/b$1$2;-><init>(Lcom/cgollner/systemmonitor/d/b$1;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x1040009

    new-instance v2, Lcom/cgollner/systemmonitor/d/b$1$1;

    invoke-direct {v2, p0}, Lcom/cgollner/systemmonitor/d/b$1$1;-><init>(Lcom/cgollner/systemmonitor/d/b$1;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto :goto_0
.end method
