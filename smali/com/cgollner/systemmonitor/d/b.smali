.class public Lcom/cgollner/systemmonitor/d/b;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Landroid/support/v4/widget/SwipeRefreshLayout$OnRefreshListener;
.implements Landroid/widget/AbsListView$MultiChoiceModeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/cgollner/systemmonitor/d/b$b;,
        Lcom/cgollner/systemmonitor/d/b$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v4/app/Fragment;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/cgollner/systemmonitor/a/a$a;",
        ">;>;",
        "Landroid/support/v4/widget/SwipeRefreshLayout$OnRefreshListener;",
        "Landroid/widget/AbsListView$MultiChoiceModeListener;"
    }
.end annotation


# static fields
.field protected static a:Landroid/app/ProgressDialog;


# instance fields
.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/cgollner/systemmonitor/a/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private c:Landroid/widget/TextView;

.field private d:Landroid/widget/TextView;

.field private e:Landroid/view/View;

.field private f:Landroid/view/View;

.field private g:Landroid/view/View;

.field private h:Landroid/widget/TextView;

.field private i:Landroid/view/View;

.field private j:Landroid/view/View;

.field private k:Landroid/widget/ListView;

.field private l:Z

.field private m:Landroid/view/View$OnClickListener;

.field private n:Lcom/cgollner/systemmonitor/a/a$b;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 60
    new-instance v0, Lcom/cgollner/systemmonitor/d/b$1;

    invoke-direct {v0, p0}, Lcom/cgollner/systemmonitor/d/b$1;-><init>(Lcom/cgollner/systemmonitor/d/b;)V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/b;->m:Landroid/view/View$OnClickListener;

    .line 86
    new-instance v0, Lcom/cgollner/systemmonitor/d/b$2;

    invoke-direct {v0, p0}, Lcom/cgollner/systemmonitor/d/b$2;-><init>(Lcom/cgollner/systemmonitor/d/b;)V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/b;->n:Lcom/cgollner/systemmonitor/a/a$b;

    .line 390
    return-void
.end method

.method private static a(Ljava/util/List;)J
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/cgollner/systemmonitor/a/a$a;",
            ">;)J"
        }
    .end annotation

    .prologue
    .line 227
    const-wide/16 v0, 0x0

    .line 228
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move-wide v2, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/a/a$a;

    .line 229
    iget-wide v0, v0, Lcom/cgollner/systemmonitor/a/a$a;->d:J

    add-long/2addr v0, v2

    move-wide v2, v0

    goto :goto_0

    .line 230
    :cond_0
    return-wide v2
.end method

.method static synthetic a(Lcom/cgollner/systemmonitor/d/b;)Ljava/util/List;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/b;->b:Ljava/util/List;

    return-object v0
.end method

.method protected static a(Landroid/content/Context;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 99
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "com.cgollner.systemmonitor"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v2, v1

    .line 100
    :goto_0
    if-nez v2, :cond_1

    .line 123
    :goto_1
    return v0

    :cond_0
    move v2, v0

    .line 99
    goto :goto_0

    .line 103
    :cond_1
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget-object v2, Lcom/cgollner/systemmonitor/App;->a:Landroid/content/Context;

    sget v3, Lcom/cgollner/systemmonitor/b/a$h;->full_version_feature_title:I

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v2, Lcom/cgollner/systemmonitor/b/a$h;->full_version_feature_message:I

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const-string v2, "Play Store"

    new-instance v3, Lcom/cgollner/systemmonitor/d/b$4;

    invoke-direct {v3, p0}, Lcom/cgollner/systemmonitor/d/b$4;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/high16 v2, 0x1040000

    new-instance v3, Lcom/cgollner/systemmonitor/d/b$3;

    invoke-direct {v3}, Lcom/cgollner/systemmonitor/d/b$3;-><init>()V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    move v0, v1

    .line 123
    goto :goto_1
.end method

.method static synthetic b(Lcom/cgollner/systemmonitor/d/b;)Lcom/cgollner/systemmonitor/a/a$b;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/b;->n:Lcom/cgollner/systemmonitor/a/a$b;

    return-object v0
.end method

.method private b()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/16 v3, 0x8

    .line 194
    sget-object v0, Lcom/cgollner/systemmonitor/App;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 195
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "save_start_root"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 199
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/b;->j:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 201
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/b;->h:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 202
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/b;->g:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 203
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/b;->f:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 205
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/d/b;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v4, v1, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 206
    return-void
.end method

.method static synthetic c(Lcom/cgollner/systemmonitor/d/b;)Landroid/view/View;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/b;->g:Landroid/view/View;

    return-object v0
.end method

.method static synthetic d(Lcom/cgollner/systemmonitor/d/b;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/b;->h:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic e(Lcom/cgollner/systemmonitor/d/b;)Landroid/view/View;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/b;->f:Landroid/view/View;

    return-object v0
.end method

.method static synthetic f(Lcom/cgollner/systemmonitor/d/b;)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/cgollner/systemmonitor/d/b;->b()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 182
    iget-boolean v0, p0, Lcom/cgollner/systemmonitor/d/b;->l:Z

    if-nez v0, :cond_0

    .line 183
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/cgollner/systemmonitor/d/b;->l:Z

    .line 184
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/d/b;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 185
    const-string v1, "save_start_root"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 186
    invoke-direct {p0}, Lcom/cgollner/systemmonitor/d/b;->b()V

    .line 191
    :cond_0
    :goto_0
    return-void

    .line 188
    :cond_1
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/b;->j:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public onActionItemClicked(Landroid/view/ActionMode;Landroid/view/MenuItem;)Z
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 326
    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sget v1, Lcom/cgollner/systemmonitor/b/a$e;->delete:I

    if-ne v0, v1, :cond_5

    .line 327
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/d/b;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/cgollner/systemmonitor/d/b;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v2

    .line 366
    :goto_0
    return v0

    .line 329
    :cond_0
    const-string v0, ""

    .line 330
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 331
    iget-object v1, p0, Lcom/cgollner/systemmonitor/d/b;->k:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->getCheckedItemPositions()Landroid/util/SparseBooleanArray;

    move-result-object v6

    .line 332
    invoke-virtual {v6}, Landroid/util/SparseBooleanArray;->size()I

    move-result v7

    move v4, v3

    .line 334
    :goto_1
    if-ge v4, v7, :cond_4

    .line 335
    invoke-virtual {v6, v4}, Landroid/util/SparseBooleanArray;->keyAt(I)I

    move-result v8

    .line 336
    invoke-virtual {v6, v8}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 337
    if-lez v4, :cond_2

    add-int/lit8 v1, v7, -0x1

    if-ge v4, v1, :cond_2

    .line 338
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 342
    :goto_2
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/b;->k:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    invoke-interface {v0, v8}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/a/a$a;

    .line 343
    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 344
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, v0, Lcom/cgollner/systemmonitor/a/a$a;->a:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 334
    :cond_1
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_1

    .line 339
    :cond_2
    if-lez v4, :cond_6

    add-int/lit8 v1, v7, -0x1

    if-ne v4, v1, :cond_6

    .line 340
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v0, 0x2

    if-le v7, v0, :cond_3

    const-string v0, ","

    :goto_3
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " and "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    goto :goto_2

    :cond_3
    const-string v0, ""

    goto :goto_3

    .line 347
    :cond_4
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/d/b;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-direct {v1, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget v4, Lcom/cgollner/systemmonitor/b/a$h;->clear_cache:I

    invoke-virtual {v1, v4}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    sget-object v6, Lcom/cgollner/systemmonitor/App;->a:Landroid/content/Context;

    sget v7, Lcom/cgollner/systemmonitor/b/a$h;->are_you_sure_that_you_want_to_clear_the_cache_of_b_s_b_:I

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    new-array v7, v2, [Ljava/lang/Object;

    aput-object v0, v7, v3

    invoke-static {v4, v6, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x1040013

    new-instance v3, Lcom/cgollner/systemmonitor/d/b$7;

    invoke-direct {v3, p0, v5}, Lcom/cgollner/systemmonitor/d/b$7;-><init>(Lcom/cgollner/systemmonitor/d/b;Ljava/util/List;)V

    invoke-virtual {v0, v1, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x1040009

    new-instance v3, Lcom/cgollner/systemmonitor/d/b$6;

    invoke-direct {v3, p0}, Lcom/cgollner/systemmonitor/d/b$6;-><init>(Lcom/cgollner/systemmonitor/d/b;)V

    invoke-virtual {v0, v1, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    move v0, v2

    .line 364
    goto/16 :goto_0

    :cond_5
    move v0, v3

    .line 366
    goto/16 :goto_0

    :cond_6
    move-object v1, v0

    goto/16 :goto_2
.end method

.method public onCreateActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 296
    invoke-virtual {p1}, Landroid/view/ActionMode;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    sget v1, Lcom/cgollner/systemmonitor/b/a$g;->apps_cache_contextual_menu:I

    invoke-virtual {v0, v1, p2}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 297
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/b;->e:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 298
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/b;->i:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 300
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/cgollner/systemmonitor/d/b;->k:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->getCheckedItemCount()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/ActionMode;->setTitle(Ljava/lang/CharSequence;)V

    .line 301
    const/4 v0, 0x1

    return v0
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/cgollner/systemmonitor/a/a$a;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 235
    new-instance v0, Lcom/cgollner/systemmonitor/a/a;

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/d/b;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/cgollner/systemmonitor/a/a;-><init>(Landroid/app/Activity;)V

    return-object v0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 2

    .prologue
    .line 210
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/d/b;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    sget v1, Lcom/cgollner/systemmonitor/b/a$g;->apps_cache_menu:I

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 211
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/Fragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 212
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 129
    sget v0, Lcom/cgollner/systemmonitor/b/a$f;->cache_fragment:I

    invoke-virtual {p1, v0, p2, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 130
    sget v0, Lcom/cgollner/systemmonitor/b/a$e;->cacheNumApps:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/b;->c:Landroid/widget/TextView;

    .line 131
    sget v0, Lcom/cgollner/systemmonitor/b/a$e;->cacheTotalSize:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/b;->d:Landroid/widget/TextView;

    .line 132
    sget v0, Lcom/cgollner/systemmonitor/b/a$e;->deleteAll:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/b;->e:Landroid/view/View;

    .line 133
    sget v0, Lcom/cgollner/systemmonitor/b/a$e;->deleteAllSeparator:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/b;->i:Landroid/view/View;

    .line 134
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/b;->e:Landroid/view/View;

    iget-object v2, p0, Lcom/cgollner/systemmonitor/d/b;->m:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 135
    sget v0, Lcom/cgollner/systemmonitor/b/a$e;->empty:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/b;->h:Landroid/widget/TextView;

    .line 136
    const v0, 0x102000d

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/b;->f:Landroid/view/View;

    .line 137
    const v0, 0x1020002

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/b;->g:Landroid/view/View;

    .line 138
    sget v0, Lcom/cgollner/systemmonitor/b/a$e;->askRootPermissions:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/b;->j:Landroid/view/View;

    .line 140
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/b;->j:Landroid/view/View;

    sget v2, Lcom/cgollner/systemmonitor/b/a$e;->askRootStart:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v2, Lcom/cgollner/systemmonitor/d/b$5;

    invoke-direct {v2, p0}, Lcom/cgollner/systemmonitor/d/b$5;-><init>(Lcom/cgollner/systemmonitor/d/b;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 147
    const v0, 0x102000a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/b;->k:Landroid/widget/ListView;

    .line 148
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/b;->k:Landroid/widget/ListView;

    const/4 v2, 0x3

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setChoiceMode(I)V

    .line 149
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/b;->k:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setMultiChoiceModeListener(Landroid/widget/AbsListView$MultiChoiceModeListener;)V

    .line 169
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/b;->k:Landroid/widget/ListView;

    new-instance v2, Lcom/cgollner/systemmonitor/d/b$b;

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/d/b;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    iget-object v4, p0, Lcom/cgollner/systemmonitor/d/b;->k:Landroid/widget/ListView;

    iget-object v5, p0, Lcom/cgollner/systemmonitor/d/b;->n:Lcom/cgollner/systemmonitor/a/a$b;

    invoke-direct {v2, v3, v4, v5}, Lcom/cgollner/systemmonitor/d/b$b;-><init>(Landroid/content/Context;Landroid/widget/ListView;Lcom/cgollner/systemmonitor/a/a$b;)V

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 170
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/d/b;->setHasOptionsMenu(Z)V

    .line 171
    iput-boolean v6, p0, Lcom/cgollner/systemmonitor/d/b;->l:Z

    .line 172
    return-object v1
.end method

.method public onDestroyActionMode(Landroid/view/ActionMode;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 376
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/b;->e:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 377
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/b;->i:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 378
    return-void
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 177
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroyView()V

    .line 178
    const/4 v0, 0x0

    sput-object v0, Lcom/cgollner/systemmonitor/d/b;->a:Landroid/app/ProgressDialog;

    .line 179
    return-void
.end method

.method public onItemCheckedStateChanged(Landroid/view/ActionMode;IJZ)V
    .locals 2

    .prologue
    .line 281
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/cgollner/systemmonitor/d/b;->k:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->getCheckedItemCount()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/ActionMode;->setTitle(Ljava/lang/CharSequence;)V

    .line 283
    return-void
.end method

.method public synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/16 v2, 0x8

    const/4 v5, 0x0

    .line 48
    check-cast p2, Ljava/util/List;

    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/b;->f:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    if-eqz p2, :cond_0

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    invoke-interface {p2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    sget-object v1, Lcom/cgollner/systemmonitor/a/a;->a:Lcom/cgollner/systemmonitor/a/a$a;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/b;->g:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/b;->h:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    iput-object p2, p0, Lcom/cgollner/systemmonitor/d/b;->b:Ljava/util/List;

    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/b;->k:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/d/b$b;

    invoke-static {v0, p2}, Lcom/cgollner/systemmonitor/d/b$b;->a(Lcom/cgollner/systemmonitor/d/b$b;Ljava/util/List;)Ljava/util/List;

    iget-object v1, p0, Lcom/cgollner/systemmonitor/d/b;->c:Landroid/widget/TextView;

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    sget-object v0, Lcom/cgollner/systemmonitor/App;->a:Landroid/content/Context;

    sget v3, Lcom/cgollner/systemmonitor/b/a$h;->x_app_apps_containing_cache:I

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v0, 0x2

    new-array v4, v0, [Ljava/lang/Object;

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v5

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-eq v0, v6, :cond_2

    sget-object v0, Lcom/cgollner/systemmonitor/App;->a:Landroid/content/Context;

    sget v5, Lcom/cgollner/systemmonitor/b/a$h;->apps:I

    invoke-virtual {v0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_1
    aput-object v0, v4, v6

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/b;->d:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p2}, Lcom/cgollner/systemmonitor/d/b;->a(Ljava/util/List;)J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/cgollner/systemmonitor/a/i;->a(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/cgollner/systemmonitor/App;->a:Landroid/content/Context;

    sget v3, Lcom/cgollner/systemmonitor/b/a$h;->x_mb_can_be_cleaned:I

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/b;->k:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Landroid/widget/BaseAdapter;

    invoke-virtual {v0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    return-void

    :cond_0
    if-eqz p2, :cond_1

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_1

    invoke-interface {p2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    sget-object v1, Lcom/cgollner/systemmonitor/a/a;->a:Lcom/cgollner/systemmonitor/a/a$a;

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/b;->h:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/b;->g:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/b;->h:Landroid/widget/TextView;

    const-string v1, "No SuperUser permissions."

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :cond_1
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/b;->h:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/b;->g:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0

    :cond_2
    sget-object v0, Lcom/cgollner/systemmonitor/App;->a:Landroid/content/Context;

    sget v5, Lcom/cgollner/systemmonitor/b/a$h;->app:I

    invoke-virtual {v0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/cgollner/systemmonitor/a/a$a;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 264
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 216
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sget v1, Lcom/cgollner/systemmonitor/b/a$e;->replay:I

    if-ne v0, v1, :cond_0

    .line 217
    invoke-direct {p0}, Lcom/cgollner/systemmonitor/d/b;->b()V

    .line 219
    :cond_0
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method public onPrepareActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 313
    const/4 v0, 0x0

    return v0
.end method

.method public onRefresh()V
    .locals 0

    .prologue
    .line 268
    invoke-direct {p0}, Lcom/cgollner/systemmonitor/d/b;->b()V

    .line 269
    return-void
.end method
