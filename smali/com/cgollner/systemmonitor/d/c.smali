.class public Lcom/cgollner/systemmonitor/d/c;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/cgollner/systemmonitor/d/c$a;
    }
.end annotation


# instance fields
.field public a:Lcom/cgollner/systemmonitor/d/c$a;

.field private b:Landroid/view/View;

.field private c:Landroid/view/View;

.field private d:Landroid/widget/TextView;

.field private e:Landroid/widget/TextView;

.field private f:Landroid/widget/TextView;

.field private g:Landroid/widget/TextView;

.field private h:Landroid/widget/TextView;

.field private i:Landroid/widget/TextView;

.field private j:Landroid/widget/TextView;

.field private k:Landroid/widget/TextView;

.field private l:Lcom/cgollner/systemmonitor/BatteryMonitorView;

.field private m:Landroid/content/Context;

.field private n:F

.field private o:Landroid/view/View$OnClickListener;

.field private p:Landroid/view/View$OnClickListener;

.field private q:Landroid/view/View$OnClickListener;

.field private r:Landroid/view/View$OnClickListener;

.field private s:Landroid/content/BroadcastReceiver;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 96
    new-instance v0, Lcom/cgollner/systemmonitor/d/c$1;

    invoke-direct {v0, p0}, Lcom/cgollner/systemmonitor/d/c$1;-><init>(Lcom/cgollner/systemmonitor/d/c;)V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/c;->o:Landroid/view/View$OnClickListener;

    .line 101
    new-instance v0, Lcom/cgollner/systemmonitor/d/c$2;

    invoke-direct {v0, p0}, Lcom/cgollner/systemmonitor/d/c$2;-><init>(Lcom/cgollner/systemmonitor/d/c;)V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/c;->p:Landroid/view/View$OnClickListener;

    .line 106
    new-instance v0, Lcom/cgollner/systemmonitor/d/c$3;

    invoke-direct {v0, p0}, Lcom/cgollner/systemmonitor/d/c$3;-><init>(Lcom/cgollner/systemmonitor/d/c;)V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/c;->q:Landroid/view/View$OnClickListener;

    .line 111
    new-instance v0, Lcom/cgollner/systemmonitor/d/c$4;

    invoke-direct {v0, p0}, Lcom/cgollner/systemmonitor/d/c$4;-><init>(Lcom/cgollner/systemmonitor/d/c;)V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/c;->r:Landroid/view/View$OnClickListener;

    .line 191
    new-instance v0, Lcom/cgollner/systemmonitor/d/c$5;

    invoke-direct {v0, p0}, Lcom/cgollner/systemmonitor/d/c$5;-><init>(Lcom/cgollner/systemmonitor/d/c;)V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/c;->s:Landroid/content/BroadcastReceiver;

    .line 48
    return-void
.end method

.method static synthetic a(Lcom/cgollner/systemmonitor/d/c;)Lcom/cgollner/systemmonitor/BatteryMonitorView;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/c;->l:Lcom/cgollner/systemmonitor/BatteryMonitorView;

    return-object v0
.end method

.method private b()V
    .locals 7

    .prologue
    .line 143
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/c;->m:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    sget-object v1, Lcom/cgollner/systemmonitor/App;->a:Landroid/content/Context;

    sget v2, Lcom/cgollner/systemmonitor/b/a$h;->battery_history_key:I

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 144
    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/c;->m:Landroid/content/Context;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/battery/BatteryService;->c(Landroid/content/Context;)Lcom/cgollner/systemmonitor/battery/a;

    move-result-object v0

    .line 145
    :goto_0
    if-nez v0, :cond_2

    .line 183
    :cond_0
    :goto_1
    return-void

    .line 144
    :cond_1
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/c;->m:Landroid/content/Context;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/battery/BatteryService;->a(Landroid/content/Context;)Lcom/cgollner/systemmonitor/battery/a;

    move-result-object v0

    goto :goto_0

    .line 148
    :cond_2
    iget v2, v0, Lcom/cgollner/systemmonitor/battery/a;->a:I

    int-to-float v2, v2

    iput v2, p0, Lcom/cgollner/systemmonitor/d/c;->n:F

    .line 149
    iget-boolean v2, v0, Lcom/cgollner/systemmonitor/battery/a;->d:Z

    .line 151
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/c;->h:Landroid/widget/TextView;

    iget v3, p0, Lcom/cgollner/systemmonitor/d/c;->n:F

    invoke-static {v3}, Lcom/cgollner/systemmonitor/a/i;->c(F)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 152
    iget-object v3, p0, Lcom/cgollner/systemmonitor/d/c;->e:Landroid/widget/TextView;

    sget-object v4, Lcom/cgollner/systemmonitor/App;->a:Landroid/content/Context;

    if-eqz v2, :cond_3

    sget v0, Lcom/cgollner/systemmonitor/b/a$h;->battery_charged_at:I

    :goto_2
    invoke-virtual {v4, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 153
    iget-object v3, p0, Lcom/cgollner/systemmonitor/d/c;->g:Landroid/widget/TextView;

    sget-object v4, Lcom/cgollner/systemmonitor/App;->a:Landroid/content/Context;

    if-eqz v2, :cond_4

    sget v0, Lcom/cgollner/systemmonitor/b/a$h;->battery_charged_in:I

    :goto_3
    invoke-virtual {v4, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 155
    if-nez v1, :cond_5

    .line 156
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/c;->i:Landroid/widget/TextView;

    const-string v1, "-"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 157
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/c;->j:Landroid/widget/TextView;

    const-string v1, "-"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 152
    :cond_3
    sget v0, Lcom/cgollner/systemmonitor/b/a$h;->battery_empty_at:I

    goto :goto_2

    .line 153
    :cond_4
    sget v0, Lcom/cgollner/systemmonitor/b/a$h;->battery_empty_in:I

    goto :goto_3

    .line 161
    :cond_5
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/c;->m:Landroid/content/Context;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/battery/BatteryService;->i(Landroid/content/Context;)Ljava/util/List;

    move-result-object v1

    .line 162
    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    if-eqz v0, :cond_0

    .line 166
    if-nez v2, :cond_6

    iget v0, p0, Lcom/cgollner/systemmonitor/d/c;->n:F

    const/4 v3, 0x0

    cmpl-float v0, v0, v3

    if-eqz v0, :cond_7

    :cond_6
    if-eqz v2, :cond_9

    iget v0, p0, Lcom/cgollner/systemmonitor/d/c;->n:F

    const/high16 v2, 0x42c80000    # 100.0f

    cmpl-float v0, v0, v2

    if-nez v0, :cond_9

    .line 167
    :cond_7
    const-wide/16 v2, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 172
    :goto_4
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    add-long/2addr v2, v4

    .line 173
    iget-object v4, p0, Lcom/cgollner/systemmonitor/d/c;->i:Landroid/widget/TextView;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/cgollner/systemmonitor/d/c;->m:Landroid/content/Context;

    invoke-static {v2, v3, v6}, Lcom/cgollner/systemmonitor/a/i;->a(JLandroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v2, v3}, Lcom/cgollner/systemmonitor/a/i;->f(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 174
    iget-object v2, p0, Lcom/cgollner/systemmonitor/d/c;->j:Landroid/widget/TextView;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v4, v5}, Lcom/cgollner/systemmonitor/a/i;->d(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 176
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/c;->m:Landroid/content/Context;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/battery/BatteryService;->e(Landroid/content/Context;)Ljava/util/List;

    move-result-object v0

    .line 177
    if-eqz v0, :cond_0

    .line 178
    if-eqz v1, :cond_8

    .line 179
    iget-object v2, p0, Lcom/cgollner/systemmonitor/d/c;->l:Lcom/cgollner/systemmonitor/BatteryMonitorView;

    invoke-virtual {v2, v1}, Lcom/cgollner/systemmonitor/BatteryMonitorView;->setPredictionArray(Ljava/util/List;)V

    .line 180
    :cond_8
    iget-object v1, p0, Lcom/cgollner/systemmonitor/d/c;->l:Lcom/cgollner/systemmonitor/BatteryMonitorView;

    invoke-virtual {v1, v0}, Lcom/cgollner/systemmonitor/BatteryMonitorView;->setValues(Ljava/util/List;)V

    .line 181
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/c;->l:Lcom/cgollner/systemmonitor/BatteryMonitorView;

    invoke-virtual {v0}, Lcom/cgollner/systemmonitor/BatteryMonitorView;->postInvalidate()V

    goto/16 :goto_1

    .line 170
    :cond_9
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/battery/c;

    iget-wide v2, v0, Lcom/cgollner/systemmonitor/battery/c;->a:J

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_4
.end method

.method static synthetic b(Lcom/cgollner/systemmonitor/d/c;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/cgollner/systemmonitor/d/c;->b()V

    return-void
.end method

.method static synthetic c(Lcom/cgollner/systemmonitor/d/c;)Lcom/cgollner/systemmonitor/d/c$a;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/c;->a:Lcom/cgollner/systemmonitor/d/c$a;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 2

    .prologue
    .line 118
    iget v0, p0, Lcom/cgollner/systemmonitor/d/c;->n:F

    const/high16 v1, 0x41c80000    # 25.0f

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    .line 119
    sget v0, Lcom/cgollner/systemmonitor/b/a$c;->batteryQ1Line:I

    .line 128
    :goto_0
    return v0

    .line 121
    :cond_0
    iget v0, p0, Lcom/cgollner/systemmonitor/d/c;->n:F

    const/high16 v1, 0x42480000    # 50.0f

    cmpg-float v0, v0, v1

    if-gez v0, :cond_1

    .line 122
    sget v0, Lcom/cgollner/systemmonitor/b/a$c;->batteryQ2Line:I

    goto :goto_0

    .line 124
    :cond_1
    iget v0, p0, Lcom/cgollner/systemmonitor/d/c;->n:F

    const/high16 v1, 0x42960000    # 75.0f

    cmpg-float v0, v0, v1

    if-gez v0, :cond_2

    .line 125
    sget v0, Lcom/cgollner/systemmonitor/b/a$c;->batteryQ3Line:I

    goto :goto_0

    .line 128
    :cond_2
    sget v0, Lcom/cgollner/systemmonitor/b/a$c;->batteryQ4Line:I

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 61
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/d/c;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/c;->m:Landroid/content/Context;

    .line 62
    sget v0, Lcom/cgollner/systemmonitor/b/a$f;->battery_fragment:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/c;->b:Landroid/view/View;

    .line 63
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/c;->b:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/b/a$e;->minus:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/cgollner/systemmonitor/d/c;->o:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 65
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/c;->b:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/b/a$e;->plus:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/cgollner/systemmonitor/d/c;->p:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 66
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/c;->b:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/b/a$e;->back:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/cgollner/systemmonitor/d/c;->q:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 67
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/c;->b:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/b/a$e;->forward:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/cgollner/systemmonitor/d/c;->r:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 69
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/c;->b:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/b/a$e;->batteryStats:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/c;->c:Landroid/view/View;

    .line 70
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/c;->b:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/b/a$e;->batteryHistoryView:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/BatteryMonitorView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/c;->l:Lcom/cgollner/systemmonitor/BatteryMonitorView;

    .line 71
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/c;->l:Lcom/cgollner/systemmonitor/BatteryMonitorView;

    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Lcom/cgollner/systemmonitor/BatteryMonitorView;->setTextSize(I)V

    .line 72
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/c;->c:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/b/a$e;->utilizationTitle:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/c;->d:Landroid/widget/TextView;

    .line 73
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/c;->c:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/b/a$e;->speedTitle:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/c;->e:Landroid/widget/TextView;

    .line 74
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/c;->c:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/b/a$e;->minSpeedTitle:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/c;->g:Landroid/widget/TextView;

    .line 75
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/c;->c:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/b/a$e;->maxSpeedTitle:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/c;->f:Landroid/widget/TextView;

    .line 77
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/c;->c:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/b/a$e;->usageAvg:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/c;->h:Landroid/widget/TextView;

    .line 78
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/c;->c:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/b/a$e;->speedValue:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/c;->i:Landroid/widget/TextView;

    .line 79
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/c;->c:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/b/a$e;->minSpeedValue:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/c;->j:Landroid/widget/TextView;

    .line 80
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/c;->c:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/b/a$e;->maxSpeedValue:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/c;->k:Landroid/widget/TextView;

    .line 82
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/c;->d:Landroid/widget/TextView;

    sget v1, Lcom/cgollner/systemmonitor/b/a$h;->battery_percentage:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 83
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/c;->e:Landroid/widget/TextView;

    const-string v1, "Complete at"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 84
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/c;->g:Landroid/widget/TextView;

    sget v1, Lcom/cgollner/systemmonitor/b/a$h;->battery_time_to_empty:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 85
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/c;->f:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 87
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/c;->h:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 88
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/c;->i:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 89
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/c;->j:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 90
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/c;->k:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 92
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/c;->b:Landroid/view/View;

    return-object v0
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 187
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onPause()V

    .line 188
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/c;->m:Landroid/content/Context;

    iget-object v1, p0, Lcom/cgollner/systemmonitor/d/c;->s:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 189
    return-void
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 133
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 134
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "com.cgollner.systemmonitor.battery.ACTION_BATTERY_INFO_UPDATE"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 135
    iget-object v1, p0, Lcom/cgollner/systemmonitor/d/c;->m:Landroid/content/Context;

    iget-object v2, p0, Lcom/cgollner/systemmonitor/d/c;->s:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 136
    invoke-direct {p0}, Lcom/cgollner/systemmonitor/d/c;->b()V

    .line 137
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/c;->a:Lcom/cgollner/systemmonitor/d/c$a;

    if-eqz v0, :cond_0

    .line 138
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/c;->a:Lcom/cgollner/systemmonitor/d/c$a;

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/d/c;->a()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/cgollner/systemmonitor/d/c$a;->a(I)V

    .line 140
    :cond_0
    return-void
.end method
