.class public Lcom/cgollner/systemmonitor/d/j;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"

# interfaces
.implements Lcom/cgollner/systemmonitor/c/e$a;


# static fields
.field public static a:I

.field private static i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private b:Lcom/cgollner/systemmonitor/MonitorView;

.field private c:Lcom/cgollner/systemmonitor/c/c;

.field private d:Landroid/widget/TextView;

.field private e:Landroid/widget/TextView;

.field private f:Landroid/widget/TextView;

.field private g:Landroid/widget/TextView;

.field private h:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/cgollner/systemmonitor/d/j;)Lcom/cgollner/systemmonitor/c/c;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/j;->c:Lcom/cgollner/systemmonitor/c/c;

    return-object v0
.end method

.method static synthetic b(Lcom/cgollner/systemmonitor/d/j;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/j;->d:Landroid/widget/TextView;

    return-object v0
.end method

.method private b()V
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/j;->c:Lcom/cgollner/systemmonitor/c/c;

    if-eqz v0, :cond_0

    .line 80
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/j;->c:Lcom/cgollner/systemmonitor/c/c;

    invoke-virtual {v0}, Lcom/cgollner/systemmonitor/c/c;->c()V

    .line 81
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/j;->c:Lcom/cgollner/systemmonitor/c/c;

    .line 83
    :cond_0
    return-void
.end method

.method static synthetic c(Lcom/cgollner/systemmonitor/d/j;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/j;->g:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic d(Lcom/cgollner/systemmonitor/d/j;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/j;->f:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic e(Lcom/cgollner/systemmonitor/d/j;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/j;->e:Landroid/widget/TextView;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 94
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/j;->b:Lcom/cgollner/systemmonitor/MonitorView;

    iget-object v1, p0, Lcom/cgollner/systemmonitor/d/j;->c:Lcom/cgollner/systemmonitor/c/c;

    iget v1, v1, Lcom/cgollner/systemmonitor/c/c;->d:F

    sget-boolean v2, Lcom/cgollner/systemmonitor/b;->a:Z

    invoke-virtual {v0, v1, v2}, Lcom/cgollner/systemmonitor/MonitorView;->a(FZ)V

    .line 95
    sget-boolean v0, Lcom/cgollner/systemmonitor/b;->a:Z

    if-nez v0, :cond_0

    .line 109
    :goto_0
    return-void

    .line 97
    :cond_0
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/j;->h:Landroid/os/Handler;

    new-instance v1, Lcom/cgollner/systemmonitor/d/j$1;

    invoke-direct {v1, p0}, Lcom/cgollner/systemmonitor/d/j$1;-><init>(Lcom/cgollner/systemmonitor/d/j;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .prologue
    .line 41
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/j;->h:Landroid/os/Handler;

    .line 42
    const/16 v0, 0x3e8

    sput v0, Lcom/cgollner/systemmonitor/d/j;->a:I

    .line 44
    sget v0, Lcom/cgollner/systemmonitor/b/a$f;->gpu_fragment_layout:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 45
    sget v0, Lcom/cgollner/systemmonitor/b/a$e;->monitorview:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/MonitorView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/j;->b:Lcom/cgollner/systemmonitor/MonitorView;

    .line 46
    sget-object v0, Lcom/cgollner/systemmonitor/d/j;->i:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 47
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/j;->b:Lcom/cgollner/systemmonitor/MonitorView;

    iget-object v0, v0, Lcom/cgollner/systemmonitor/MonitorView;->a:Ljava/util/List;

    sget-object v2, Lcom/cgollner/systemmonitor/d/j;->i:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 49
    :cond_0
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/j;->b:Lcom/cgollner/systemmonitor/MonitorView;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/cgollner/systemmonitor/MonitorView;->setDrawBorders$1d54120b(Z)V

    .line 51
    sget v0, Lcom/cgollner/systemmonitor/b/a$e;->statValue0:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/j;->d:Landroid/widget/TextView;

    .line 52
    sget v0, Lcom/cgollner/systemmonitor/b/a$e;->statValue1:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/j;->g:Landroid/widget/TextView;

    .line 53
    sget v0, Lcom/cgollner/systemmonitor/b/a$e;->statValue2:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/j;->f:Landroid/widget/TextView;

    .line 54
    sget v0, Lcom/cgollner/systemmonitor/b/a$e;->statValue3:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/j;->e:Landroid/widget/TextView;

    .line 55
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/d/j;->setHasOptionsMenu(Z)V

    .line 57
    return-object v1
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/j;->b:Lcom/cgollner/systemmonitor/MonitorView;

    iget-object v0, v0, Lcom/cgollner/systemmonitor/MonitorView;->a:Ljava/util/List;

    sput-object v0, Lcom/cgollner/systemmonitor/d/j;->i:Ljava/util/List;

    .line 75
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroyView()V

    .line 76
    return-void
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 68
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onPause()V

    .line 69
    invoke-direct {p0}, Lcom/cgollner/systemmonitor/d/j;->b()V

    .line 70
    return-void
.end method

.method public onResume()V
    .locals 4

    .prologue
    .line 62
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 63
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/j;->c:Lcom/cgollner/systemmonitor/c/c;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/cgollner/systemmonitor/d/j;->b()V

    :cond_0
    new-instance v0, Lcom/cgollner/systemmonitor/c/c;

    sget v1, Lcom/cgollner/systemmonitor/d/j;->a:I

    int-to-long v2, v1

    invoke-direct {v0, v2, v3, p0}, Lcom/cgollner/systemmonitor/c/c;-><init>(JLcom/cgollner/systemmonitor/c/e$a;)V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/j;->c:Lcom/cgollner/systemmonitor/c/c;

    .line 64
    return-void
.end method
