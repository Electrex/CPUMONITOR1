.class public Lcom/cgollner/systemmonitor/d/k;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"

# interfaces
.implements Lcom/cgollner/systemmonitor/c/e$a;


# static fields
.field public static a:I

.field private static i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private b:Lcom/cgollner/systemmonitor/MonitorView;

.field private c:Lcom/cgollner/systemmonitor/c/d;

.field private d:Landroid/widget/TextView;

.field private e:Landroid/widget/TextView;

.field private f:Landroid/widget/TextView;

.field private g:Landroid/content/Context;

.field private h:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/cgollner/systemmonitor/d/k;)Lcom/cgollner/systemmonitor/c/d;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/k;->c:Lcom/cgollner/systemmonitor/c/d;

    return-object v0
.end method

.method static synthetic b(Lcom/cgollner/systemmonitor/d/k;)Lcom/cgollner/systemmonitor/MonitorView;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/k;->b:Lcom/cgollner/systemmonitor/MonitorView;

    return-object v0
.end method

.method private b()V
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/k;->c:Lcom/cgollner/systemmonitor/c/d;

    if-eqz v0, :cond_0

    .line 80
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/k;->c:Lcom/cgollner/systemmonitor/c/d;

    invoke-virtual {v0}, Lcom/cgollner/systemmonitor/c/d;->c()V

    .line 81
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/k;->c:Lcom/cgollner/systemmonitor/c/d;

    .line 83
    :cond_0
    return-void
.end method

.method static synthetic c(Lcom/cgollner/systemmonitor/d/k;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/k;->d:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic d(Lcom/cgollner/systemmonitor/d/k;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/k;->e:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic e(Lcom/cgollner/systemmonitor/d/k;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/k;->f:Landroid/widget/TextView;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 96
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/k;->h:Landroid/os/Handler;

    new-instance v1, Lcom/cgollner/systemmonitor/d/k$1;

    invoke-direct {v1, p0}, Lcom/cgollner/systemmonitor/d/k$1;-><init>(Lcom/cgollner/systemmonitor/d/k;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 111
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 42
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/d/k;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/k;->g:Landroid/content/Context;

    .line 43
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/k;->h:Landroid/os/Handler;

    .line 44
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/k;->g:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    sget-object v1, Lcom/cgollner/systemmonitor/App;->a:Landroid/content/Context;

    sget v2, Lcom/cgollner/systemmonitor/b/a$h;->settings_app_io_updatefreq_key:I

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    mul-int/lit16 v0, v0, 0x3e8

    sput v0, Lcom/cgollner/systemmonitor/d/k;->a:I

    .line 45
    sget v0, Lcom/cgollner/systemmonitor/b/a$f;->io_fragment_layout:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 46
    sget v0, Lcom/cgollner/systemmonitor/b/a$e;->monitorview:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/MonitorView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/k;->b:Lcom/cgollner/systemmonitor/MonitorView;

    .line 47
    sget-object v0, Lcom/cgollner/systemmonitor/d/k;->i:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 48
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/k;->b:Lcom/cgollner/systemmonitor/MonitorView;

    iget-object v0, v0, Lcom/cgollner/systemmonitor/MonitorView;->a:Ljava/util/List;

    sget-object v2, Lcom/cgollner/systemmonitor/d/k;->i:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 50
    :cond_0
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/k;->b:Lcom/cgollner/systemmonitor/MonitorView;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/cgollner/systemmonitor/MonitorView;->setDrawBorders$1d54120b(Z)V

    .line 52
    sget v0, Lcom/cgollner/systemmonitor/b/a$e;->usageAvg:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/k;->d:Landroid/widget/TextView;

    .line 53
    sget v0, Lcom/cgollner/systemmonitor/b/a$e;->readSpeedValue:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/k;->e:Landroid/widget/TextView;

    .line 54
    sget v0, Lcom/cgollner/systemmonitor/b/a$e;->writeSpeedValue:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/k;->f:Landroid/widget/TextView;

    .line 56
    invoke-virtual {p0, v3}, Lcom/cgollner/systemmonitor/d/k;->setHasOptionsMenu(Z)V

    .line 57
    return-object v1
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/k;->b:Lcom/cgollner/systemmonitor/MonitorView;

    iget-object v0, v0, Lcom/cgollner/systemmonitor/MonitorView;->a:Ljava/util/List;

    sput-object v0, Lcom/cgollner/systemmonitor/d/k;->i:Ljava/util/List;

    .line 63
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroyView()V

    .line 64
    return-void
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 74
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onPause()V

    .line 75
    invoke-direct {p0}, Lcom/cgollner/systemmonitor/d/k;->b()V

    .line 76
    return-void
.end method

.method public onResume()V
    .locals 4

    .prologue
    .line 68
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 69
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/k;->c:Lcom/cgollner/systemmonitor/c/d;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/cgollner/systemmonitor/d/k;->b()V

    :cond_0
    new-instance v0, Lcom/cgollner/systemmonitor/c/d;

    sget v1, Lcom/cgollner/systemmonitor/d/k;->a:I

    int-to-long v2, v1

    invoke-direct {v0, v2, v3, p0}, Lcom/cgollner/systemmonitor/c/d;-><init>(JLcom/cgollner/systemmonitor/c/e$a;)V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/k;->c:Lcom/cgollner/systemmonitor/c/d;

    .line 70
    return-void
.end method
