.class final Lcom/cgollner/systemmonitor/d/k$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/cgollner/systemmonitor/d/k;->a()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/cgollner/systemmonitor/d/k;


# direct methods
.method constructor <init>(Lcom/cgollner/systemmonitor/d/k;)V
    .locals 0

    .prologue
    .line 96
    iput-object p1, p0, Lcom/cgollner/systemmonitor/d/k$1;->a:Lcom/cgollner/systemmonitor/d/k;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    .line 98
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/k$1;->a:Lcom/cgollner/systemmonitor/d/k;

    invoke-virtual {v0}, Lcom/cgollner/systemmonitor/d/k;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/k$1;->a:Lcom/cgollner/systemmonitor/d/k;

    invoke-virtual {v0}, Lcom/cgollner/systemmonitor/d/k;->isDetached()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 109
    :cond_0
    :goto_0
    return-void

    .line 101
    :cond_1
    :try_start_0
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/k$1;->a:Lcom/cgollner/systemmonitor/d/k;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/d/k;->b(Lcom/cgollner/systemmonitor/d/k;)Lcom/cgollner/systemmonitor/MonitorView;

    move-result-object v0

    iget-object v1, p0, Lcom/cgollner/systemmonitor/d/k$1;->a:Lcom/cgollner/systemmonitor/d/k;

    invoke-static {v1}, Lcom/cgollner/systemmonitor/d/k;->a(Lcom/cgollner/systemmonitor/d/k;)Lcom/cgollner/systemmonitor/c/d;

    move-result-object v1

    iget v1, v1, Lcom/cgollner/systemmonitor/c/d;->c:F

    sget-boolean v2, Lcom/cgollner/systemmonitor/b;->a:Z

    invoke-virtual {v0, v1, v2}, Lcom/cgollner/systemmonitor/MonitorView;->a(FZ)V

    .line 102
    sget-boolean v0, Lcom/cgollner/systemmonitor/b;->a:Z

    if-eqz v0, :cond_0

    .line 104
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/k$1;->a:Lcom/cgollner/systemmonitor/d/k;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/d/k;->c(Lcom/cgollner/systemmonitor/d/k;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/cgollner/systemmonitor/d/k$1;->a:Lcom/cgollner/systemmonitor/d/k;

    invoke-static {v1}, Lcom/cgollner/systemmonitor/d/k;->a(Lcom/cgollner/systemmonitor/d/k;)Lcom/cgollner/systemmonitor/c/d;

    move-result-object v1

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    const-string v3, "%.0f%%"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget v1, v1, Lcom/cgollner/systemmonitor/c/d;->c:F

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    aput-object v1, v4, v5

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 105
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/k$1;->a:Lcom/cgollner/systemmonitor/d/k;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/d/k;->d(Lcom/cgollner/systemmonitor/d/k;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/cgollner/systemmonitor/d/k$1;->a:Lcom/cgollner/systemmonitor/d/k;

    invoke-static {v1}, Lcom/cgollner/systemmonitor/d/k;->a(Lcom/cgollner/systemmonitor/d/k;)Lcom/cgollner/systemmonitor/c/d;

    move-result-object v1

    iget-wide v2, v1, Lcom/cgollner/systemmonitor/c/d;->d:J

    long-to-double v2, v2

    iget-wide v4, v1, Lcom/cgollner/systemmonitor/c/d;->f:J

    invoke-static {v2, v3, v4, v5}, Lcom/cgollner/systemmonitor/a/i;->a(DJ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 106
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/k$1;->a:Lcom/cgollner/systemmonitor/d/k;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/d/k;->e(Lcom/cgollner/systemmonitor/d/k;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/cgollner/systemmonitor/d/k$1;->a:Lcom/cgollner/systemmonitor/d/k;

    invoke-static {v1}, Lcom/cgollner/systemmonitor/d/k;->a(Lcom/cgollner/systemmonitor/d/k;)Lcom/cgollner/systemmonitor/c/d;

    move-result-object v1

    iget-wide v2, v1, Lcom/cgollner/systemmonitor/c/d;->e:J

    long-to-double v2, v2

    iget-wide v4, v1, Lcom/cgollner/systemmonitor/c/d;->f:J

    invoke-static {v2, v3, v4, v5}, Lcom/cgollner/systemmonitor/a/i;->a(DJ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 109
    :catch_0
    move-exception v0

    goto :goto_0
.end method
