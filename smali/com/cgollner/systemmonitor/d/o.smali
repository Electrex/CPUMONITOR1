.class public Lcom/cgollner/systemmonitor/d/o;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"

# interfaces
.implements Lcom/cgollner/systemmonitor/c/e$a;


# static fields
.field public static a:I

.field private static j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private b:Lcom/cgollner/systemmonitor/MonitorView;

.field private c:Lcom/cgollner/systemmonitor/c/g;

.field private d:Landroid/widget/TextView;

.field private e:Landroid/widget/TextView;

.field private f:Landroid/widget/TextView;

.field private g:Landroid/widget/TextView;

.field private h:Landroid/os/Handler;

.field private i:Landroid/content/Context;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/cgollner/systemmonitor/d/o;)Lcom/cgollner/systemmonitor/c/g;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/o;->c:Lcom/cgollner/systemmonitor/c/g;

    return-object v0
.end method

.method static synthetic b(Lcom/cgollner/systemmonitor/d/o;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/o;->d:Landroid/widget/TextView;

    return-object v0
.end method

.method private b()V
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/o;->c:Lcom/cgollner/systemmonitor/c/g;

    if-eqz v0, :cond_0

    .line 103
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/o;->c:Lcom/cgollner/systemmonitor/c/g;

    invoke-virtual {v0}, Lcom/cgollner/systemmonitor/c/g;->c()V

    .line 104
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/o;->c:Lcom/cgollner/systemmonitor/c/g;

    .line 106
    :cond_0
    return-void
.end method

.method static synthetic c(Lcom/cgollner/systemmonitor/d/o;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/o;->e:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic d(Lcom/cgollner/systemmonitor/d/o;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/o;->f:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic e(Lcom/cgollner/systemmonitor/d/o;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/o;->g:Landroid/widget/TextView;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 72
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/o;->b:Lcom/cgollner/systemmonitor/MonitorView;

    iget-object v1, p0, Lcom/cgollner/systemmonitor/d/o;->c:Lcom/cgollner/systemmonitor/c/g;

    iget v1, v1, Lcom/cgollner/systemmonitor/c/g;->a:F

    sget-boolean v2, Lcom/cgollner/systemmonitor/b;->a:Z

    invoke-virtual {v0, v1, v2}, Lcom/cgollner/systemmonitor/MonitorView;->a(FZ)V

    .line 73
    sget-boolean v0, Lcom/cgollner/systemmonitor/b;->a:Z

    if-nez v0, :cond_0

    .line 87
    :goto_0
    return-void

    .line 75
    :cond_0
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/o;->h:Landroid/os/Handler;

    new-instance v1, Lcom/cgollner/systemmonitor/d/o$1;

    invoke-direct {v1, p0}, Lcom/cgollner/systemmonitor/d/o$1;-><init>(Lcom/cgollner/systemmonitor/d/o;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 44
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/o;->h:Landroid/os/Handler;

    .line 45
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/d/o;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/o;->i:Landroid/content/Context;

    .line 46
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/d/o;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    sget-object v1, Lcom/cgollner/systemmonitor/App;->a:Landroid/content/Context;

    sget v2, Lcom/cgollner/systemmonitor/b/a$h;->settings_app_ram_updatefreq_key:I

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    mul-int/lit16 v0, v0, 0x3e8

    sput v0, Lcom/cgollner/systemmonitor/d/o;->a:I

    .line 48
    sget v0, Lcom/cgollner/systemmonitor/b/a$f;->ram_fragment_layout:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 49
    sget v0, Lcom/cgollner/systemmonitor/b/a$e;->monitorview:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/MonitorView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/o;->b:Lcom/cgollner/systemmonitor/MonitorView;

    .line 50
    sget-object v0, Lcom/cgollner/systemmonitor/d/o;->j:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 51
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/o;->b:Lcom/cgollner/systemmonitor/MonitorView;

    iget-object v0, v0, Lcom/cgollner/systemmonitor/MonitorView;->a:Ljava/util/List;

    sget-object v2, Lcom/cgollner/systemmonitor/d/o;->j:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 53
    :cond_0
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/o;->b:Lcom/cgollner/systemmonitor/MonitorView;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/cgollner/systemmonitor/MonitorView;->setDrawBorders$1d54120b(Z)V

    .line 55
    sget v0, Lcom/cgollner/systemmonitor/b/a$e;->usageAvg:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/o;->d:Landroid/widget/TextView;

    .line 56
    sget v0, Lcom/cgollner/systemmonitor/b/a$e;->freeRamValue:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/o;->e:Landroid/widget/TextView;

    .line 57
    sget v0, Lcom/cgollner/systemmonitor/b/a$e;->totalRamValue:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/o;->f:Landroid/widget/TextView;

    .line 58
    sget v0, Lcom/cgollner/systemmonitor/b/a$e;->usedRamValue:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/o;->g:Landroid/widget/TextView;

    .line 59
    invoke-virtual {p0, v3}, Lcom/cgollner/systemmonitor/d/o;->setHasOptionsMenu(Z)V

    .line 61
    return-object v1
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/o;->b:Lcom/cgollner/systemmonitor/MonitorView;

    iget-object v0, v0, Lcom/cgollner/systemmonitor/MonitorView;->a:Ljava/util/List;

    sput-object v0, Lcom/cgollner/systemmonitor/d/o;->j:Ljava/util/List;

    .line 67
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroyView()V

    .line 68
    return-void
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 97
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onPause()V

    .line 98
    invoke-direct {p0}, Lcom/cgollner/systemmonitor/d/o;->b()V

    .line 99
    return-void
.end method

.method public onResume()V
    .locals 4

    .prologue
    .line 91
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 92
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/o;->c:Lcom/cgollner/systemmonitor/c/g;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/cgollner/systemmonitor/d/o;->b()V

    :cond_0
    new-instance v0, Lcom/cgollner/systemmonitor/c/g;

    sget v1, Lcom/cgollner/systemmonitor/d/o;->a:I

    int-to-long v2, v1

    iget-object v1, p0, Lcom/cgollner/systemmonitor/d/o;->i:Landroid/content/Context;

    invoke-direct {v0, v2, v3, p0, v1}, Lcom/cgollner/systemmonitor/c/g;-><init>(JLcom/cgollner/systemmonitor/c/e$a;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/o;->c:Lcom/cgollner/systemmonitor/c/g;

    .line 93
    return-void
.end method
