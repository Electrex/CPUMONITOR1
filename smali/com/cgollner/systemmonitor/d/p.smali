.class public Lcom/cgollner/systemmonitor/d/p;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Landroid/view/View$OnClickListener;
.implements Lcom/cgollner/systemmonitor/d/f$b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v4/app/Fragment;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/cgollner/systemmonitor/a/h$b;",
        ">;>;",
        "Landroid/view/View$OnClickListener;",
        "Lcom/cgollner/systemmonitor/d/f$b;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Lcom/cgollner/systemmonitor/FrequenciesPieChart;

.field private c:I

.field private d:Landroid/view/View;

.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/cgollner/systemmonitor/a/h$b;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljava/io/File;

.field private g:J

.field private h:F

.field private i:J

.field private j:Landroid/view/ViewGroup;

.field private k:Landroid/content/Context;

.field private l:Landroid/os/Handler;

.field private m:Z

.field private n:Landroid/view/View$OnClickListener;

.field private o:Ljava/io/File;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 237
    new-instance v0, Lcom/cgollner/systemmonitor/d/p$2;

    invoke-direct {v0, p0}, Lcom/cgollner/systemmonitor/d/p$2;-><init>(Lcom/cgollner/systemmonitor/d/p;)V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/p;->n:Landroid/view/View$OnClickListener;

    return-void
.end method

.method static synthetic a(Lcom/cgollner/systemmonitor/d/p;I)I
    .locals 0

    .prologue
    .line 45
    iput p1, p0, Lcom/cgollner/systemmonitor/d/p;->c:I

    return p1
.end method

.method static synthetic a(Lcom/cgollner/systemmonitor/d/p;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/p;->k:Landroid/content/Context;

    return-object v0
.end method

.method private a(Landroid/view/ViewGroup;Lcom/cgollner/systemmonitor/a/h$b;I)V
    .locals 11

    .prologue
    const/4 v10, 0x0

    .line 160
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/p;->k:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/cgollner/systemmonitor/b/a$f;->storage_item:I

    invoke-virtual {v0, v1, p1, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v5

    .line 162
    sget v0, Lcom/cgollner/systemmonitor/b/a$e;->tvFreq:I

    invoke-virtual {v5, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 163
    sget v1, Lcom/cgollner/systemmonitor/b/a$e;->tvFreqTime:I

    invoke-virtual {v5, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 164
    sget v2, Lcom/cgollner/systemmonitor/b/a$e;->tvFreqPercentage:I

    invoke-virtual {v5, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 165
    sget v3, Lcom/cgollner/systemmonitor/b/a$e;->delete:I

    invoke-virtual {v5, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    .line 167
    iget-object v4, p2, Lcom/cgollner/systemmonitor/a/h$b;->a:Ljava/io/File;

    iget-object v6, p0, Lcom/cgollner/systemmonitor/d/p;->f:Ljava/io/File;

    invoke-virtual {v6}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/io/File;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 168
    const-string v4, "../"

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 169
    sget v4, Lcom/cgollner/systemmonitor/b/a$e;->delete:I

    invoke-virtual {v5, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    const/16 v6, 0x8

    invoke-virtual {v4, v6}, Landroid/view/View;->setVisibility(I)V

    .line 173
    :goto_0
    iget-wide v6, p2, Lcom/cgollner/systemmonitor/a/h$b;->b:J

    const-wide/16 v8, -0x1

    cmp-long v4, v6, v8

    if-eqz v4, :cond_0

    .line 174
    iget-wide v6, p2, Lcom/cgollner/systemmonitor/a/h$b;->b:J

    invoke-static {v6, v7}, Lcom/cgollner/systemmonitor/a/i;->b(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 175
    iget v4, p2, Lcom/cgollner/systemmonitor/a/h$b;->c:F

    invoke-static {v4}, Lcom/cgollner/systemmonitor/a/i;->c(F)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 177
    :cond_0
    sget-object v4, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->a:[[I

    .line 179
    iget v6, p0, Lcom/cgollner/systemmonitor/d/p;->c:I

    if-ne p3, v6, :cond_1

    .line 180
    array-length v6, v4

    rem-int v6, p3, v6

    aget-object v6, v4, v6

    aget v6, v6, v10

    invoke-virtual {v5, v6}, Landroid/view/View;->setBackgroundColor(I)V

    .line 181
    sget v6, Lcom/cgollner/systemmonitor/b/a$d;->ic_ac_content_discard_holo_dark:I

    invoke-virtual {v3, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 184
    :cond_1
    iget v6, p0, Lcom/cgollner/systemmonitor/d/p;->c:I

    if-ne p3, v6, :cond_4

    const/4 v4, -0x1

    .line 185
    :goto_1
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 186
    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 187
    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 189
    invoke-virtual {v5, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 190
    invoke-virtual {v5, p3}, Landroid/view/View;->setId(I)V

    .line 191
    invoke-virtual {v5, p2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 192
    new-instance v0, Lcom/cgollner/systemmonitor/d/p$1;

    invoke-direct {v0, p0, p2}, Lcom/cgollner/systemmonitor/d/p$1;-><init>(Lcom/cgollner/systemmonitor/d/p;Lcom/cgollner/systemmonitor/a/h$b;)V

    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 232
    invoke-virtual {p1, v5}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 235
    return-void

    .line 171
    :cond_2
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p2, Lcom/cgollner/systemmonitor/a/h$b;->a:Ljava/io/File;

    invoke-virtual {v6}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v4, p2, Lcom/cgollner/systemmonitor/a/h$b;->a:Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->isDirectory()Z

    move-result v4

    if-eqz v4, :cond_3

    const-string v4, "/"

    :goto_2
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_3
    const-string v4, ""

    goto :goto_2

    .line 184
    :cond_4
    array-length v6, v4

    rem-int v6, p3, v6

    aget-object v4, v4, v6

    const/4 v6, 0x1

    aget v4, v4, v6

    goto :goto_1
.end method

.method static synthetic a(Lcom/cgollner/systemmonitor/d/p;Ljava/io/File;)V
    .locals 1

    .prologue
    .line 45
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/cgollner/systemmonitor/d/p;->a(Ljava/io/File;Z)V

    return-void
.end method

.method private a(Ljava/io/File;Z)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 106
    iput-object p1, p0, Lcom/cgollner/systemmonitor/d/p;->f:Ljava/io/File;

    .line 107
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/p;->d:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/b/a$e;->progressBar:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 108
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/p;->d:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/b/a$e;->stats:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 110
    if-eqz p2, :cond_0

    .line 111
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/d/p;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    invoke-virtual {v0, v2, v3, p0}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 116
    :goto_0
    return-void

    .line 114
    :cond_0
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/d/p;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    invoke-virtual {v0, v2, v3, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    goto :goto_0
.end method

.method static synthetic b(Lcom/cgollner/systemmonitor/d/p;)Ljava/io/File;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/p;->f:Ljava/io/File;

    return-object v0
.end method

.method private b()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v1, 0x0

    .line 119
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/p;->k:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/p;->d:Landroid/view/View;

    sget v2, Lcom/cgollner/systemmonitor/b/a$e;->progressBar:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/p;->d:Landroid/view/View;

    sget v2, Lcom/cgollner/systemmonitor/b/a$e;->stats:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/p;->d:Landroid/view/View;

    sget v2, Lcom/cgollner/systemmonitor/b/a$e;->freeStorage:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/cgollner/systemmonitor/App;->a:Landroid/content/Context;

    sget v4, Lcom/cgollner/systemmonitor/b/a$h;->usedRam:I

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "%.1f%%"

    new-array v4, v8, [Ljava/lang/Object;

    const/high16 v5, 0x42c80000    # 100.0f

    iget v6, p0, Lcom/cgollner/systemmonitor/d/p;->h:F

    sub-float/2addr v5, v6

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v4, p0, Lcom/cgollner/systemmonitor/d/p;->i:J

    iget-wide v6, p0, Lcom/cgollner/systemmonitor/d/p;->g:J

    sub-long/2addr v4, v6

    invoke-static {v4, v5}, Lcom/cgollner/systemmonitor/a/i;->c(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v4, p0, Lcom/cgollner/systemmonitor/d/p;->i:J

    invoke-static {v4, v5}, Lcom/cgollner/systemmonitor/a/i;->c(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/p;->d:Landroid/view/View;

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/d/p;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const-string v3, "cpu0"

    const-string v4, "id"

    iget-object v5, p0, Lcom/cgollner/systemmonitor/d/p;->k:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/p;->j:Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/p;->j:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/p;->f:Ljava/io/File;

    iget-object v2, p0, Lcom/cgollner/systemmonitor/d/p;->o:Ljava/io/File;

    invoke-virtual {v0, v2}, Ljava/io/File;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/p;->j:Landroid/view/ViewGroup;

    new-instance v2, Lcom/cgollner/systemmonitor/a/h$b;

    iget-object v3, p0, Lcom/cgollner/systemmonitor/d/p;->f:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v3

    const-wide/16 v4, -0x1

    invoke-direct {v2, v3, v4, v5}, Lcom/cgollner/systemmonitor/a/h$b;-><init>(Ljava/io/File;J)V

    sget-object v3, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->a:[[I

    array-length v3, v3

    add-int/lit8 v3, v3, -0x1

    invoke-direct {p0, v0, v2, v3}, Lcom/cgollner/systemmonitor/d/p;->a(Landroid/view/ViewGroup;Lcom/cgollner/systemmonitor/a/h$b;I)V

    :cond_0
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/p;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/a/h$b;

    iget-object v3, p0, Lcom/cgollner/systemmonitor/d/p;->j:Landroid/view/ViewGroup;

    iget-object v4, v0, Lcom/cgollner/systemmonitor/a/h$b;->a:Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    invoke-direct {p0, v3, v0, v1}, Lcom/cgollner/systemmonitor/d/p;->a(Landroid/view/ViewGroup;Lcom/cgollner/systemmonitor/a/h$b;I)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    iput v1, p0, Lcom/cgollner/systemmonitor/d/p;->a:I

    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/p;->d:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/b/a$e;->pie:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/FrequenciesPieChart;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/p;->b:Lcom/cgollner/systemmonitor/FrequenciesPieChart;

    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/p;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/a/h$b;

    new-instance v4, Lcom/cgollner/systemmonitor/FrequenciesPieChart$a;

    invoke-direct {v4}, Lcom/cgollner/systemmonitor/FrequenciesPieChart$a;-><init>()V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, v0, Lcom/cgollner/systemmonitor/a/h$b;->a:Ljava/io/File;

    invoke-virtual {v5}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v1, v0, Lcom/cgollner/systemmonitor/a/h$b;->a:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->isDirectory()Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "/"

    :goto_2
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v4, Lcom/cgollner/systemmonitor/FrequenciesPieChart$a;->b:Ljava/lang/String;

    iget-wide v6, v0, Lcom/cgollner/systemmonitor/a/h$b;->b:J

    invoke-static {v6, v7}, Lcom/cgollner/systemmonitor/a/i;->c(J)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v4, Lcom/cgollner/systemmonitor/FrequenciesPieChart$a;->a:Ljava/lang/String;

    iget v1, v0, Lcom/cgollner/systemmonitor/a/h$b;->c:F

    iput v1, v4, Lcom/cgollner/systemmonitor/FrequenciesPieChart$a;->d:F

    iget-wide v0, v0, Lcom/cgollner/systemmonitor/a/h$b;->b:J

    iput-wide v0, v4, Lcom/cgollner/systemmonitor/FrequenciesPieChart$a;->c:J

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    const-string v1, ""

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/p;->b:Lcom/cgollner/systemmonitor/FrequenciesPieChart;

    iget v1, p0, Lcom/cgollner/systemmonitor/d/p;->c:I

    invoke-virtual {v0, v2, v1, v8}, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->a(Ljava/util/List;IZ)V

    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/p;->b:Lcom/cgollner/systemmonitor/FrequenciesPieChart;

    invoke-virtual {v0, v8}, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->setClickable(Z)V

    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/p;->b:Lcom/cgollner/systemmonitor/FrequenciesPieChart;

    iget-object v1, p0, Lcom/cgollner/systemmonitor/d/p;->n:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 120
    return-void
.end method

.method static synthetic c(Lcom/cgollner/systemmonitor/d/p;)I
    .locals 1

    .prologue
    .line 45
    iget v0, p0, Lcom/cgollner/systemmonitor/d/p;->c:I

    return v0
.end method

.method static synthetic d(Lcom/cgollner/systemmonitor/d/p;)I
    .locals 1

    .prologue
    .line 45
    iget v0, p0, Lcom/cgollner/systemmonitor/d/p;->a:I

    return v0
.end method

.method static synthetic e(Lcom/cgollner/systemmonitor/d/p;)V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/cgollner/systemmonitor/d/p;->b()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 80
    iget-boolean v0, p0, Lcom/cgollner/systemmonitor/d/p;->m:Z

    if-nez v0, :cond_0

    .line 81
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/cgollner/systemmonitor/d/p;->m:Z

    .line 82
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/p;->o:Ljava/io/File;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/cgollner/systemmonitor/d/p;->a(Ljava/io/File;Z)V

    .line 84
    :cond_0
    return-void
.end method

.method public final a(Ljava/io/File;)V
    .locals 3

    .prologue
    .line 264
    const/4 v0, 0x0

    iput v0, p0, Lcom/cgollner/systemmonitor/d/p;->c:I

    .line 265
    iput-object p1, p0, Lcom/cgollner/systemmonitor/d/p;->o:Ljava/io/File;

    .line 266
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/p;->k:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "ROOT_FOLDER"

    iget-object v2, p0, Lcom/cgollner/systemmonitor/d/p;->o:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 267
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/cgollner/systemmonitor/d/p;->a(Ljava/io/File;Z)V

    .line 268
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 248
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    iput v0, p0, Lcom/cgollner/systemmonitor/d/p;->c:I

    .line 249
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/a/h$b;

    .line 250
    iget-object v1, v0, Lcom/cgollner/systemmonitor/a/h$b;->a:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "../"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 251
    iput v3, p0, Lcom/cgollner/systemmonitor/d/p;->c:I

    .line 252
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/p;->f:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v0

    invoke-direct {p0, v0, v4}, Lcom/cgollner/systemmonitor/d/p;->a(Ljava/io/File;Z)V

    .line 260
    :goto_0
    return-void

    .line 254
    :cond_0
    iget-object v1, v0, Lcom/cgollner/systemmonitor/a/h$b;->a:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->isDirectory()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 255
    iput v3, p0, Lcom/cgollner/systemmonitor/d/p;->c:I

    .line 256
    iget-object v0, v0, Lcom/cgollner/systemmonitor/a/h$b;->a:Ljava/io/File;

    invoke-direct {p0, v0, v4}, Lcom/cgollner/systemmonitor/d/p;->a(Ljava/io/File;Z)V

    goto :goto_0

    .line 258
    :cond_1
    invoke-direct {p0}, Lcom/cgollner/systemmonitor/d/p;->b()V

    goto :goto_0
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/cgollner/systemmonitor/a/h$b;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 272
    new-instance v0, Lcom/cgollner/systemmonitor/a/h;

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/d/p;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/cgollner/systemmonitor/d/p;->f:Ljava/io/File;

    invoke-direct {v0, v1, v2}, Lcom/cgollner/systemmonitor/a/h;-><init>(Landroid/content/Context;Ljava/io/File;)V

    return-object v0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 1

    .prologue
    .line 88
    sget v0, Lcom/cgollner/systemmonitor/b/a$g;->menu_storage:I

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 89
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/Fragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 90
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 63
    invoke-virtual {p0, v5}, Lcom/cgollner/systemmonitor/d/p;->setHasOptionsMenu(Z)V

    .line 64
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/d/p;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/p;->k:Landroid/content/Context;

    .line 65
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/p;->l:Landroid/os/Handler;

    .line 66
    sget v0, Lcom/cgollner/systemmonitor/b/a$f;->storage_stats_layout:I

    invoke-virtual {p1, v0, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/p;->d:Landroid/view/View;

    .line 67
    iput v4, p0, Lcom/cgollner/systemmonitor/d/p;->c:I

    .line 69
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/cgollner/systemmonitor/d/p;->k:Landroid/content/Context;

    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v2, "ROOT_FOLDER"

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/p;->o:Ljava/io/File;

    .line 70
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/p;->o:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 71
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/p;->o:Ljava/io/File;

    .line 73
    :cond_0
    iput-boolean v4, p0, Lcom/cgollner/systemmonitor/d/p;->m:Z

    .line 75
    invoke-virtual {p0, v5}, Lcom/cgollner/systemmonitor/d/p;->setHasOptionsMenu(Z)V

    .line 76
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/p;->d:Landroid/view/View;

    return-object v0
.end method

.method public synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 45
    check-cast p2, Ljava/util/List;

    new-instance v0, Landroid/os/StatFs;

    iget-object v1, p0, Lcom/cgollner/systemmonitor/d/p;->f:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/cgollner/systemmonitor/d/p;->f:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/StatFs;->restat(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/os/StatFs;->getAvailableBlocks()I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {v0}, Landroid/os/StatFs;->getBlockSize()I

    move-result v1

    int-to-long v4, v1

    mul-long/2addr v2, v4

    iput-wide v2, p0, Lcom/cgollner/systemmonitor/d/p;->g:J

    invoke-virtual {v0}, Landroid/os/StatFs;->getAvailableBlocks()I

    move-result v1

    int-to-double v2, v1

    invoke-virtual {v0}, Landroid/os/StatFs;->getBlockCount()I

    move-result v1

    int-to-double v4, v1

    div-double/2addr v2, v4

    const-wide/high16 v4, 0x4059000000000000L    # 100.0

    mul-double/2addr v2, v4

    double-to-float v1, v2

    iput v1, p0, Lcom/cgollner/systemmonitor/d/p;->h:F

    invoke-virtual {v0}, Landroid/os/StatFs;->getBlockCount()I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {v0}, Landroid/os/StatFs;->getBlockSize()I

    move-result v0

    int-to-long v0, v0

    mul-long/2addr v0, v2

    iput-wide v0, p0, Lcom/cgollner/systemmonitor/d/p;->i:J

    iput-object p2, p0, Lcom/cgollner/systemmonitor/d/p;->e:Ljava/util/List;

    invoke-direct {p0}, Lcom/cgollner/systemmonitor/d/p;->b()V

    return-void
.end method

.method public onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/cgollner/systemmonitor/a/h$b;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 290
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    .line 94
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sget v1, Lcom/cgollner/systemmonitor/b/a$e;->menu_storage_folder:I

    if-ne v0, v1, :cond_1

    .line 95
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/d/p;->getChildFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    .line 96
    new-instance v1, Lcom/cgollner/systemmonitor/d/f;

    invoke-direct {v1}, Lcom/cgollner/systemmonitor/d/f;-><init>()V

    .line 97
    const-string v2, "tag"

    invoke-virtual {v1, v0, v2}, Lcom/cgollner/systemmonitor/d/f;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 102
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 99
    :cond_1
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sget v1, Lcom/cgollner/systemmonitor/b/a$e;->replay:I

    if-ne v0, v1, :cond_0

    .line 100
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/p;->f:Ljava/io/File;

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/cgollner/systemmonitor/d/p;->a(Ljava/io/File;Z)V

    goto :goto_0
.end method
