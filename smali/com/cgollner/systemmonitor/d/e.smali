.class public Lcom/cgollner/systemmonitor/d/e;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"


# instance fields
.field private a:Landroid/view/View;

.field private b:Landroid/view/View;

.field private c:Landroid/widget/TextView;

.field private d:Landroid/widget/TextView;

.field private e:Landroid/widget/TextView;

.field private f:Landroid/widget/TextView;

.field private g:Landroid/widget/TextView;

.field private h:Landroid/widget/TextView;

.field private i:Landroid/widget/TextView;

.field private j:Landroid/widget/TextView;

.field private k:Lcom/cgollner/systemmonitor/BatteryMonitorView;

.field private l:Landroid/content/Context;

.field private m:Landroid/view/View$OnClickListener;

.field private n:Landroid/view/View$OnClickListener;

.field private o:Landroid/view/View$OnClickListener;

.field private p:Landroid/view/View$OnClickListener;

.field private q:Landroid/content/BroadcastReceiver;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 95
    new-instance v0, Lcom/cgollner/systemmonitor/d/e$1;

    invoke-direct {v0, p0}, Lcom/cgollner/systemmonitor/d/e$1;-><init>(Lcom/cgollner/systemmonitor/d/e;)V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/e;->m:Landroid/view/View$OnClickListener;

    .line 100
    new-instance v0, Lcom/cgollner/systemmonitor/d/e$2;

    invoke-direct {v0, p0}, Lcom/cgollner/systemmonitor/d/e$2;-><init>(Lcom/cgollner/systemmonitor/d/e;)V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/e;->n:Landroid/view/View$OnClickListener;

    .line 105
    new-instance v0, Lcom/cgollner/systemmonitor/d/e$3;

    invoke-direct {v0, p0}, Lcom/cgollner/systemmonitor/d/e$3;-><init>(Lcom/cgollner/systemmonitor/d/e;)V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/e;->o:Landroid/view/View$OnClickListener;

    .line 110
    new-instance v0, Lcom/cgollner/systemmonitor/d/e$4;

    invoke-direct {v0, p0}, Lcom/cgollner/systemmonitor/d/e$4;-><init>(Lcom/cgollner/systemmonitor/d/e;)V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/e;->p:Landroid/view/View$OnClickListener;

    .line 183
    new-instance v0, Lcom/cgollner/systemmonitor/d/e$5;

    invoke-direct {v0, p0}, Lcom/cgollner/systemmonitor/d/e$5;-><init>(Lcom/cgollner/systemmonitor/d/e;)V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/e;->q:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method static synthetic a(Lcom/cgollner/systemmonitor/d/e;)Lcom/cgollner/systemmonitor/BatteryMonitorView;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/e;->k:Lcom/cgollner/systemmonitor/BatteryMonitorView;

    return-object v0
.end method

.method private a()V
    .locals 6

    .prologue
    .line 125
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/e;->l:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    sget-object v1, Lcom/cgollner/systemmonitor/App;->a:Landroid/content/Context;

    sget v2, Lcom/cgollner/systemmonitor/b/a$h;->battery_history_key:I

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 126
    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/e;->l:Landroid/content/Context;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/battery/BatteryService;->c(Landroid/content/Context;)Lcom/cgollner/systemmonitor/battery/a;

    move-result-object v0

    .line 128
    :goto_0
    if-nez v0, :cond_2

    .line 149
    :cond_0
    :goto_1
    return-void

    .line 126
    :cond_1
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/e;->l:Landroid/content/Context;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/battery/BatteryService;->a(Landroid/content/Context;)Lcom/cgollner/systemmonitor/battery/a;

    move-result-object v0

    goto :goto_0

    .line 131
    :cond_2
    iget-object v2, p0, Lcom/cgollner/systemmonitor/d/e;->g:Landroid/widget/TextView;

    iget v0, v0, Lcom/cgollner/systemmonitor/battery/a;->b:I

    int-to-float v0, v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    iget-object v3, p0, Lcom/cgollner/systemmonitor/d/e;->l:Landroid/content/Context;

    invoke-static {v0, v3}, Lcom/cgollner/systemmonitor/a/i;->a(ILandroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 133
    if-nez v1, :cond_3

    .line 134
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/e;->h:Landroid/widget/TextView;

    const-string v1, "-"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 135
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/e;->i:Landroid/widget/TextView;

    const-string v1, "-"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 136
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/e;->j:Landroid/widget/TextView;

    const-string v1, "-"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 139
    :cond_3
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/e;->l:Landroid/content/Context;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/battery/BatteryService;->f(Landroid/content/Context;)Ljava/util/List;

    move-result-object v2

    .line 140
    if-eqz v2, :cond_0

    .line 143
    iget-object v3, p0, Lcom/cgollner/systemmonitor/d/e;->h:Landroid/widget/TextView;

    const/4 v0, 0x0

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v0

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/battery/c;

    iget v0, v0, Lcom/cgollner/systemmonitor/battery/c;->b:F

    add-float/2addr v0, v1

    move v1, v0

    goto :goto_2

    :cond_4
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    int-to-float v0, v0

    div-float v0, v1, v0

    float-to-int v0, v0

    int-to-float v0, v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    iget-object v1, p0, Lcom/cgollner/systemmonitor/d/e;->l:Landroid/content/Context;

    invoke-static {v0, v1}, Lcom/cgollner/systemmonitor/a/i;->a(ILandroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 144
    iget-object v3, p0, Lcom/cgollner/systemmonitor/d/e;->i:Landroid/widget/TextView;

    const/high16 v0, -0x31000000

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v0

    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/battery/c;

    iget v5, v0, Lcom/cgollner/systemmonitor/battery/c;->b:F

    cmpl-float v5, v5, v1

    if-lez v5, :cond_8

    iget v0, v0, Lcom/cgollner/systemmonitor/battery/c;->b:F

    :goto_4
    move v1, v0

    goto :goto_3

    :cond_5
    float-to-int v0, v1

    int-to-float v0, v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    iget-object v1, p0, Lcom/cgollner/systemmonitor/d/e;->l:Landroid/content/Context;

    invoke-static {v0, v1}, Lcom/cgollner/systemmonitor/a/i;->a(ILandroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 145
    iget-object v3, p0, Lcom/cgollner/systemmonitor/d/e;->j:Landroid/widget/TextView;

    const/high16 v0, 0x4f000000

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v0

    :goto_5
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/battery/c;

    iget v5, v0, Lcom/cgollner/systemmonitor/battery/c;->b:F

    cmpg-float v5, v5, v1

    if-gez v5, :cond_7

    iget v0, v0, Lcom/cgollner/systemmonitor/battery/c;->b:F

    :goto_6
    move v1, v0

    goto :goto_5

    :cond_6
    float-to-int v0, v1

    int-to-float v0, v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    iget-object v1, p0, Lcom/cgollner/systemmonitor/d/e;->l:Landroid/content/Context;

    invoke-static {v0, v1}, Lcom/cgollner/systemmonitor/a/i;->a(ILandroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 147
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/e;->k:Lcom/cgollner/systemmonitor/BatteryMonitorView;

    invoke-virtual {v0, v2}, Lcom/cgollner/systemmonitor/BatteryMonitorView;->setValues(Ljava/util/List;)V

    .line 148
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/e;->k:Lcom/cgollner/systemmonitor/BatteryMonitorView;

    invoke-virtual {v0}, Lcom/cgollner/systemmonitor/BatteryMonitorView;->postInvalidate()V

    goto/16 :goto_1

    :cond_7
    move v0, v1

    goto :goto_6

    :cond_8
    move v0, v1

    goto :goto_4
.end method

.method static synthetic b(Lcom/cgollner/systemmonitor/d/e;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/cgollner/systemmonitor/d/e;->a()V

    return-void
.end method


# virtual methods
.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .prologue
    .line 47
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/d/e;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/e;->l:Landroid/content/Context;

    .line 48
    sget v0, Lcom/cgollner/systemmonitor/b/a$f;->battery_fragment:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/e;->a:Landroid/view/View;

    .line 49
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/e;->a:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/b/a$e;->minus:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/cgollner/systemmonitor/d/e;->m:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 51
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/e;->a:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/b/a$e;->plus:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/cgollner/systemmonitor/d/e;->n:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 52
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/e;->a:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/b/a$e;->back:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/cgollner/systemmonitor/d/e;->o:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 53
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/e;->a:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/b/a$e;->forward:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/cgollner/systemmonitor/d/e;->p:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 55
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/e;->a:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/b/a$e;->batteryStats:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/e;->b:Landroid/view/View;

    .line 56
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/e;->a:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/b/a$e;->batteryHistoryView:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/BatteryMonitorView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/e;->k:Lcom/cgollner/systemmonitor/BatteryMonitorView;

    .line 57
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/e;->k:Lcom/cgollner/systemmonitor/BatteryMonitorView;

    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Lcom/cgollner/systemmonitor/BatteryMonitorView;->setTextSize(I)V

    .line 58
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/e;->k:Lcom/cgollner/systemmonitor/BatteryMonitorView;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->c:Z

    .line 59
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/e;->k:Lcom/cgollner/systemmonitor/BatteryMonitorView;

    const/high16 v1, 0x447a0000    # 1000.0f

    iput v1, v0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->d:F

    .line 60
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/e;->k:Lcom/cgollner/systemmonitor/BatteryMonitorView;

    invoke-virtual {v0}, Lcom/cgollner/systemmonitor/BatteryMonitorView;->invalidate()V

    .line 62
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/e;->b:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/b/a$e;->utilizationTitle:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/e;->c:Landroid/widget/TextView;

    .line 63
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/e;->b:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/b/a$e;->speedTitle:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/e;->d:Landroid/widget/TextView;

    .line 64
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/e;->b:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/b/a$e;->minSpeedTitle:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/e;->f:Landroid/widget/TextView;

    .line 65
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/e;->b:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/b/a$e;->maxSpeedTitle:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/e;->e:Landroid/widget/TextView;

    .line 67
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/e;->b:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/b/a$e;->usageAvg:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/e;->g:Landroid/widget/TextView;

    .line 68
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/e;->b:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/b/a$e;->speedValue:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/e;->h:Landroid/widget/TextView;

    .line 69
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/e;->b:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/b/a$e;->minSpeedValue:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/e;->i:Landroid/widget/TextView;

    .line 70
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/e;->b:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/b/a$e;->maxSpeedValue:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/e;->j:Landroid/widget/TextView;

    .line 72
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/e;->c:Landroid/widget/TextView;

    sget v1, Lcom/cgollner/systemmonitor/b/a$h;->battery_temperature:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 73
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/e;->d:Landroid/widget/TextView;

    sget v1, Lcom/cgollner/systemmonitor/b/a$h;->statistics_average:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 74
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/e;->f:Landroid/widget/TextView;

    sget v1, Lcom/cgollner/systemmonitor/b/a$h;->max:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 75
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/e;->e:Landroid/widget/TextView;

    sget v1, Lcom/cgollner/systemmonitor/b/a$h;->min:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 77
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/e;->g:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 78
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/e;->h:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 79
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/e;->i:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 80
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/e;->j:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 82
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/e;->a:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/b/a$e;->plus:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 83
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 84
    iget-object v2, p0, Lcom/cgollner/systemmonitor/d/e;->k:Lcom/cgollner/systemmonitor/BatteryMonitorView;

    iget-object v2, v2, Lcom/cgollner/systemmonitor/BatteryMonitorView;->e:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    iput v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 85
    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 87
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/e;->a:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/b/a$e;->minus:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 88
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 89
    iget-object v2, p0, Lcom/cgollner/systemmonitor/d/e;->k:Lcom/cgollner/systemmonitor/BatteryMonitorView;

    iget-object v2, v2, Lcom/cgollner/systemmonitor/BatteryMonitorView;->e:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    iput v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 90
    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 92
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/e;->a:Landroid/view/View;

    return-object v0
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 179
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onPause()V

    .line 180
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/e;->l:Landroid/content/Context;

    iget-object v1, p0, Lcom/cgollner/systemmonitor/d/e;->q:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 181
    return-void
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 118
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 119
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "com.cgollner.systemmonitor.battery.ACTION_BATTERY_INFO_UPDATE"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 120
    iget-object v1, p0, Lcom/cgollner/systemmonitor/d/e;->l:Landroid/content/Context;

    iget-object v2, p0, Lcom/cgollner/systemmonitor/d/e;->q:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 121
    invoke-direct {p0}, Lcom/cgollner/systemmonitor/d/e;->a()V

    .line 122
    return-void
.end method
