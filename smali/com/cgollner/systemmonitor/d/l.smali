.class public Lcom/cgollner/systemmonitor/d/l;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"

# interfaces
.implements Lcom/cgollner/systemmonitor/c/e$a;


# static fields
.field public static a:I

.field private static h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private static i:D


# instance fields
.field private b:Lcom/cgollner/systemmonitor/MonitorView;

.field private c:Lcom/cgollner/systemmonitor/c/f;

.field private d:Landroid/widget/TextView;

.field private e:Landroid/widget/TextView;

.field private f:Landroid/widget/TextView;

.field private g:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 29
    const-wide/high16 v0, 0x40f9000000000000L    # 102400.0

    sput-wide v0, Lcom/cgollner/systemmonitor/d/l;->i:D

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/cgollner/systemmonitor/d/l;)Lcom/cgollner/systemmonitor/c/f;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/l;->c:Lcom/cgollner/systemmonitor/c/f;

    return-object v0
.end method

.method static synthetic b(Lcom/cgollner/systemmonitor/d/l;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/l;->d:Landroid/widget/TextView;

    return-object v0
.end method

.method private b()V
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/l;->c:Lcom/cgollner/systemmonitor/c/f;

    if-eqz v0, :cond_0

    .line 108
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/l;->c:Lcom/cgollner/systemmonitor/c/f;

    invoke-virtual {v0}, Lcom/cgollner/systemmonitor/c/f;->c()V

    .line 109
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/l;->c:Lcom/cgollner/systemmonitor/c/f;

    .line 111
    :cond_0
    return-void
.end method

.method static synthetic c(Lcom/cgollner/systemmonitor/d/l;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/l;->e:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic d(Lcom/cgollner/systemmonitor/d/l;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/l;->f:Landroid/widget/TextView;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 6

    .prologue
    .line 73
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/l;->b:Lcom/cgollner/systemmonitor/MonitorView;

    iget-object v1, p0, Lcom/cgollner/systemmonitor/d/l;->b:Lcom/cgollner/systemmonitor/MonitorView;

    iget-wide v2, v1, Lcom/cgollner/systemmonitor/MonitorView;->b:D

    iget-object v1, p0, Lcom/cgollner/systemmonitor/d/l;->c:Lcom/cgollner/systemmonitor/c/f;

    iget-wide v4, v1, Lcom/cgollner/systemmonitor/c/f;->k:J

    long-to-double v4, v4

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->max(DD)D

    move-result-wide v2

    iput-wide v2, v0, Lcom/cgollner/systemmonitor/MonitorView;->b:D

    .line 74
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/l;->b:Lcom/cgollner/systemmonitor/MonitorView;

    iget-object v1, p0, Lcom/cgollner/systemmonitor/d/l;->c:Lcom/cgollner/systemmonitor/c/f;

    iget-wide v2, v1, Lcom/cgollner/systemmonitor/c/f;->k:J

    long-to-float v1, v2

    sget-boolean v2, Lcom/cgollner/systemmonitor/b;->a:Z

    invoke-virtual {v0, v1, v2}, Lcom/cgollner/systemmonitor/MonitorView;->a(FZ)V

    .line 75
    sget-boolean v0, Lcom/cgollner/systemmonitor/b;->a:Z

    if-nez v0, :cond_0

    .line 92
    :goto_0
    return-void

    .line 78
    :cond_0
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/l;->g:Landroid/os/Handler;

    new-instance v1, Lcom/cgollner/systemmonitor/d/l$1;

    invoke-direct {v1, p0}, Lcom/cgollner/systemmonitor/d/l$1;-><init>(Lcom/cgollner/systemmonitor/d/l;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 41
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/l;->g:Landroid/os/Handler;

    .line 42
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/d/l;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    sget-object v1, Lcom/cgollner/systemmonitor/App;->a:Landroid/content/Context;

    sget v2, Lcom/cgollner/systemmonitor/b/a$h;->settings_app_net_updatefreq_key:I

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, v6}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    mul-int/lit16 v0, v0, 0x3e8

    sput v0, Lcom/cgollner/systemmonitor/d/l;->a:I

    .line 43
    sget v0, Lcom/cgollner/systemmonitor/b/a$f;->network_fragment_layout:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 44
    sget v0, Lcom/cgollner/systemmonitor/b/a$e;->monitorview:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/MonitorView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/l;->b:Lcom/cgollner/systemmonitor/MonitorView;

    .line 45
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/l;->b:Lcom/cgollner/systemmonitor/MonitorView;

    sget v2, Lcom/cgollner/systemmonitor/MonitorView$a;->c:I

    invoke-virtual {v0, v2}, Lcom/cgollner/systemmonitor/MonitorView;->setLabelType$36c8c2f0(I)V

    .line 46
    sget-object v0, Lcom/cgollner/systemmonitor/d/l;->h:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 47
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/l;->b:Lcom/cgollner/systemmonitor/MonitorView;

    iget-object v0, v0, Lcom/cgollner/systemmonitor/MonitorView;->a:Ljava/util/List;

    sget-object v2, Lcom/cgollner/systemmonitor/d/l;->h:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 49
    :cond_0
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/l;->b:Lcom/cgollner/systemmonitor/MonitorView;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/cgollner/systemmonitor/MonitorView;->setDrawBorders$1d54120b(Z)V

    .line 51
    sget-wide v2, Lcom/cgollner/systemmonitor/d/l;->i:D

    const-wide/high16 v4, -0x4010000000000000L    # -1.0

    cmpl-double v0, v2, v4

    if-eqz v0, :cond_1

    .line 52
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/l;->b:Lcom/cgollner/systemmonitor/MonitorView;

    sget-wide v2, Lcom/cgollner/systemmonitor/d/l;->i:D

    iput-wide v2, v0, Lcom/cgollner/systemmonitor/MonitorView;->b:D

    .line 54
    :cond_1
    sget v0, Lcom/cgollner/systemmonitor/b/a$e;->throughputValue:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/l;->d:Landroid/widget/TextView;

    .line 55
    sget v0, Lcom/cgollner/systemmonitor/b/a$e;->readSpeedValue:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/l;->e:Landroid/widget/TextView;

    .line 56
    sget v0, Lcom/cgollner/systemmonitor/b/a$e;->writeSpeedValue:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/l;->f:Landroid/widget/TextView;

    .line 57
    invoke-virtual {p0, v6}, Lcom/cgollner/systemmonitor/d/l;->setHasOptionsMenu(Z)V

    .line 59
    return-object v1
.end method

.method public onDestroyView()V
    .locals 2

    .prologue
    .line 64
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/l;->b:Lcom/cgollner/systemmonitor/MonitorView;

    iget-object v0, v0, Lcom/cgollner/systemmonitor/MonitorView;->a:Ljava/util/List;

    sput-object v0, Lcom/cgollner/systemmonitor/d/l;->h:Ljava/util/List;

    .line 65
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/l;->b:Lcom/cgollner/systemmonitor/MonitorView;

    iget-wide v0, v0, Lcom/cgollner/systemmonitor/MonitorView;->b:D

    sput-wide v0, Lcom/cgollner/systemmonitor/d/l;->i:D

    .line 67
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroyView()V

    .line 68
    return-void
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 102
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onPause()V

    .line 103
    invoke-direct {p0}, Lcom/cgollner/systemmonitor/d/l;->b()V

    .line 104
    return-void
.end method

.method public onResume()V
    .locals 4

    .prologue
    .line 95
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 96
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/l;->c:Lcom/cgollner/systemmonitor/c/f;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/cgollner/systemmonitor/d/l;->b()V

    :cond_0
    new-instance v0, Lcom/cgollner/systemmonitor/c/f;

    sget v1, Lcom/cgollner/systemmonitor/d/l;->a:I

    int-to-long v2, v1

    invoke-direct {v0, v2, v3, p0}, Lcom/cgollner/systemmonitor/c/f;-><init>(JLcom/cgollner/systemmonitor/c/e$a;)V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/l;->c:Lcom/cgollner/systemmonitor/c/f;

    .line 97
    return-void
.end method
