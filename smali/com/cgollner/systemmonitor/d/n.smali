.class public final Lcom/cgollner/systemmonitor/d/n;
.super Ljava/lang/Thread;
.source "SourceFile"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "UseSparseArrays"
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/cgollner/systemmonitor/d/n$a;
    }
.end annotation


# static fields
.field public static a:I


# instance fields
.field public b:Lcom/cgollner/systemmonitor/d/q;

.field public c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/cgollner/systemmonitor/d/n$a;",
            ">;"
        }
    .end annotation
.end field

.field public d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/cgollner/systemmonitor/d/n$a;",
            ">;"
        }
    .end annotation
.end field

.field public e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/cgollner/systemmonitor/d/a;",
            ">;"
        }
    .end annotation
.end field

.field public f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/cgollner/systemmonitor/d/a;",
            ">;"
        }
    .end annotation
.end field

.field public g:Z

.field public h:Z

.field public i:Ljava/lang/String;

.field public j:Ljava/lang/String;

.field public k:Z

.field public l:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/cgollner/systemmonitor/d/a;",
            ">;"
        }
    .end annotation
.end field

.field private m:Landroid/app/ActivityManager;

.field private n:Landroid/content/pm/PackageManager;

.field private o:Landroid/os/Handler;

.field private p:Landroid/view/View;

.field private q:F

.field private r:[Ljava/lang/String;

.field private s:[I


# direct methods
.method public constructor <init>(Lcom/cgollner/systemmonitor/d/q;Landroid/content/Context;Landroid/os/Handler;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    .line 67
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 68
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/n;->r:[Ljava/lang/String;

    .line 70
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/n;->c:Ljava/util/Map;

    .line 71
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/n;->d:Ljava/util/Map;

    .line 72
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/n;->e:Ljava/util/Map;

    .line 73
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xc8

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/n;->f:Ljava/util/List;

    .line 74
    iput-object p1, p0, Lcom/cgollner/systemmonitor/d/n;->b:Lcom/cgollner/systemmonitor/d/q;

    .line 76
    invoke-static {p2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 77
    sget-object v1, Lcom/cgollner/systemmonitor/App;->a:Landroid/content/Context;

    sget v2, Lcom/cgollner/systemmonitor/b/a$h;->settings_app_topapps_updatefreq_key:I

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    mul-int/lit16 v0, v0, 0x3e8

    .line 78
    sput v0, Lcom/cgollner/systemmonitor/d/n;->a:I

    .line 80
    const-string v0, "activity"

    invoke-virtual {p2, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/n;->m:Landroid/app/ActivityManager;

    .line 81
    invoke-virtual {p2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/n;->n:Landroid/content/pm/PackageManager;

    .line 82
    iput-object p3, p0, Lcom/cgollner/systemmonitor/d/n;->o:Landroid/os/Handler;

    .line 83
    iput-object p4, p0, Lcom/cgollner/systemmonitor/d/n;->p:Landroid/view/View;

    .line 84
    invoke-virtual {p4}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-ne v0, v3, :cond_0

    .line 85
    const/4 v0, 0x0

    invoke-virtual {p4, v0}, Landroid/view/View;->setVisibility(I)V

    .line 88
    :goto_0
    return-void

    .line 87
    :cond_0
    invoke-virtual {p4, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/cgollner/systemmonitor/d/n;)Landroid/view/View;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/n;->p:Landroid/view/View;

    return-object v0
.end method

.method private a(Ljava/util/Map;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/cgollner/systemmonitor/d/n$a;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 220
    iget-boolean v0, p0, Lcom/cgollner/systemmonitor/d/n;->k:Z

    if-eqz v0, :cond_1

    .line 221
    invoke-static {}, Lcom/cgollner/systemmonitor/a/b;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 222
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/d/n$a;

    .line 223
    if-nez v0, :cond_0

    .line 224
    new-instance v0, Lcom/cgollner/systemmonitor/d/n$a;

    const-wide/16 v4, 0x0

    invoke-static {v2}, Lcom/cgollner/systemmonitor/a/b;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, p0, v4, v5, v3}, Lcom/cgollner/systemmonitor/d/n$a;-><init>(Lcom/cgollner/systemmonitor/d/n;JLjava/lang/String;)V

    .line 225
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {p1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 227
    :cond_0
    invoke-static {v2}, Lcom/cgollner/systemmonitor/a/b;->a(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/cgollner/systemmonitor/d/n$a;->b:Ljava/lang/String;

    goto :goto_0

    .line 231
    :cond_1
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/n;->m:Landroid/app/ActivityManager;

    invoke-virtual {v0}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager$RunningAppProcessInfo;

    .line 232
    iget v1, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->pid:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/cgollner/systemmonitor/d/n$a;

    .line 233
    if-nez v1, :cond_2

    .line 234
    iget v1, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->uid:I

    invoke-static {v1}, Landroid/net/TrafficStats;->getUidRxBytes(I)J

    move-result-wide v4

    iget v1, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->uid:I

    invoke-static {v1}, Landroid/net/TrafficStats;->getUidTxBytes(I)J

    move-result-wide v6

    add-long/2addr v4, v6

    .line 235
    iget v1, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->pid:I

    invoke-static {v1}, Lcom/cgollner/systemmonitor/a/b;->a(I)Ljava/lang/String;

    move-result-object v1

    .line 236
    iget v0, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->pid:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    new-instance v3, Lcom/cgollner/systemmonitor/d/n$a;

    invoke-direct {v3, p0, v4, v5, v1}, Lcom/cgollner/systemmonitor/d/n$a;-><init>(Lcom/cgollner/systemmonitor/d/n;JLjava/lang/String;)V

    invoke-interface {p1, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 238
    :cond_2
    iget v3, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->uid:I

    invoke-static {v3}, Landroid/net/TrafficStats;->getUidRxBytes(I)J

    move-result-wide v4

    iget v3, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->uid:I

    invoke-static {v3}, Landroid/net/TrafficStats;->getUidTxBytes(I)J

    move-result-wide v6

    add-long/2addr v4, v6

    iput-wide v4, v1, Lcom/cgollner/systemmonitor/d/n$a;->a:J

    .line 239
    iget v0, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->pid:I

    invoke-static {v0}, Lcom/cgollner/systemmonitor/a/b;->a(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/cgollner/systemmonitor/d/n$a;->b:Ljava/lang/String;

    goto :goto_1

    .line 242
    :cond_3
    return-void
.end method


# virtual methods
.method public final run()V
    .locals 20

    .prologue
    .line 91
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/cgollner/systemmonitor/d/n;->g:Z

    .line 92
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/cgollner/systemmonitor/d/n;->h:Z

    .line 93
    :goto_0
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/cgollner/systemmonitor/d/n;->g:Z

    if-eqz v2, :cond_a

    .line 94
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cgollner/systemmonitor/d/n;->c:Ljava/util/Map;

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/cgollner/systemmonitor/d/n;->a(Ljava/util/Map;)V

    .line 95
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cgollner/systemmonitor/d/n;->r:[Ljava/lang/String;

    const/4 v3, 0x0

    invoke-static {}, Lcom/cgollner/systemmonitor/a/b;->a()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    .line 96
    invoke-static {}, Lcom/cgollner/systemmonitor/a/b;->a()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/cgollner/systemmonitor/d/n;->i:Ljava/lang/String;

    .line 98
    :try_start_0
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/cgollner/systemmonitor/d/n;->h:Z

    if-eqz v2, :cond_4

    const-wide/16 v2, 0x64

    :goto_1
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V

    .line 99
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/cgollner/systemmonitor/d/n;->h:Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_2

    .line 102
    :goto_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cgollner/systemmonitor/d/n;->r:[Ljava/lang/String;

    const/4 v3, 0x1

    invoke-static {}, Lcom/cgollner/systemmonitor/a/b;->a()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    .line 103
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cgollner/systemmonitor/d/n;->r:[Ljava/lang/String;

    const/4 v3, 0x0

    aget-object v2, v2, v3

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/cgollner/systemmonitor/d/n;->r:[Ljava/lang/String;

    const/4 v4, 0x1

    aget-object v3, v3, v4

    invoke-static {v2, v3}, Lcom/cgollner/systemmonitor/a/b;->a(Ljava/lang/String;Ljava/lang/String;)F

    move-result v2

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    int-to-float v2, v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/cgollner/systemmonitor/d/n;->q:F

    .line 105
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cgollner/systemmonitor/d/n;->d:Ljava/util/Map;

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/cgollner/systemmonitor/d/n;->a(Ljava/util/Map;)V

    .line 107
    invoke-static {}, Lcom/cgollner/systemmonitor/a/b;->a()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/cgollner/systemmonitor/d/n;->j:Ljava/lang/String;

    .line 108
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cgollner/systemmonitor/d/n;->i:Ljava/lang/String;

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/cgollner/systemmonitor/a/b;->b([Ljava/lang/String;)J

    move-result-wide v2

    .line 109
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/cgollner/systemmonitor/d/n;->j:Ljava/lang/String;

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/cgollner/systemmonitor/a/b;->b([Ljava/lang/String;)J

    move-result-wide v4

    .line 110
    sub-long v6, v4, v2

    .line 111
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cgollner/systemmonitor/d/n;->f:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 112
    invoke-static {}, Lcom/cgollner/systemmonitor/a/b;->d()Ljava/util/Set;

    move-result-object v8

    .line 113
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cgollner/systemmonitor/d/n;->m:Landroid/app/ActivityManager;

    invoke-virtual {v2}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/ActivityManager$RunningAppProcessInfo;

    .line 114
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/cgollner/systemmonitor/d/n;->c:Ljava/util/Map;

    iget v4, v2, Landroid/app/ActivityManager$RunningAppProcessInfo;->pid:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/cgollner/systemmonitor/d/n$a;

    .line 115
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/cgollner/systemmonitor/d/n;->d:Ljava/util/Map;

    iget v5, v2, Landroid/app/ActivityManager$RunningAppProcessInfo;->pid:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/cgollner/systemmonitor/d/n$a;

    .line 117
    if-eqz v3, :cond_0

    if-eqz v4, :cond_0

    iget v5, v2, Landroid/app/ActivityManager$RunningAppProcessInfo;->importance:I

    const/16 v10, 0x190

    if-eq v5, v10, :cond_0

    .line 118
    iget-object v5, v3, Lcom/cgollner/systemmonitor/d/n$a;->b:Ljava/lang/String;

    .line 121
    iget-object v10, v4, Lcom/cgollner/systemmonitor/d/n$a;->b:Ljava/lang/String;

    .line 123
    iget-wide v12, v4, Lcom/cgollner/systemmonitor/d/n$a;->a:J

    iget-wide v14, v3, Lcom/cgollner/systemmonitor/d/n$a;->a:J

    sub-long/2addr v12, v14

    .line 125
    move-object/from16 v0, p0

    iget v3, v0, Lcom/cgollner/systemmonitor/d/n;->q:F

    invoke-static {v5, v10, v6, v7, v3}, Lcom/cgollner/systemmonitor/a/b;->a(Ljava/lang/String;Ljava/lang/String;JF)F

    move-result v10

    .line 126
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/cgollner/systemmonitor/d/n;->s:[I

    if-nez v3, :cond_1

    .line 127
    const/4 v3, 0x1

    new-array v3, v3, [I

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/cgollner/systemmonitor/d/n;->s:[I

    .line 128
    :cond_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/cgollner/systemmonitor/d/n;->s:[I

    const/4 v4, 0x0

    iget v5, v2, Landroid/app/ActivityManager$RunningAppProcessInfo;->pid:I

    aput v5, v3, v4

    .line 130
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/cgollner/systemmonitor/d/n;->m:Landroid/app/ActivityManager;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/cgollner/systemmonitor/d/n;->s:[I

    invoke-virtual {v3, v4}, Landroid/app/ActivityManager;->getProcessMemoryInfo([I)[Landroid/os/Debug$MemoryInfo;

    move-result-object v3

    .line 131
    const-wide/16 v4, 0x0

    const/4 v11, 0x0

    aget-object v11, v3, v11

    iget v11, v11, Landroid/os/Debug$MemoryInfo;->dalvikPss:I

    int-to-long v14, v11

    add-long/2addr v4, v14

    .line 134
    const/4 v11, 0x0

    aget-object v11, v3, v11

    iget v11, v11, Landroid/os/Debug$MemoryInfo;->nativePss:I

    int-to-long v14, v11

    add-long/2addr v4, v14

    .line 135
    const/4 v11, 0x0

    aget-object v3, v3, v11

    iget v3, v3, Landroid/os/Debug$MemoryInfo;->otherPss:I

    int-to-long v14, v3

    add-long/2addr v4, v14

    .line 136
    const-wide/16 v14, 0x400

    mul-long/2addr v14, v4

    .line 138
    iget-object v11, v2, Landroid/app/ActivityManager$RunningAppProcessInfo;->pkgList:[Ljava/lang/String;

    array-length v0, v11

    move/from16 v16, v0

    const/4 v3, 0x0

    move v5, v3

    :goto_3
    move/from16 v0, v16

    if-ge v5, v0, :cond_0

    aget-object v17, v11, v5

    .line 140
    :try_start_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/cgollner/systemmonitor/d/n;->n:Landroid/content/pm/PackageManager;

    const/16 v4, 0x81

    move-object/from16 v0, v17

    invoke-virtual {v3, v0, v4}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v3

    .line 142
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/cgollner/systemmonitor/d/n;->n:Landroid/content/pm/PackageManager;

    invoke-virtual {v3, v4}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 143
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/cgollner/systemmonitor/d/n;->e:Ljava/util/Map;

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "-"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    iget v0, v2, Landroid/app/ActivityManager$RunningAppProcessInfo;->pid:I

    move/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/cgollner/systemmonitor/d/a;

    .line 144
    if-nez v4, :cond_2

    new-instance v4, Lcom/cgollner/systemmonitor/d/a;

    invoke-direct {v4}, Lcom/cgollner/systemmonitor/d/a;-><init>()V

    .line 146
    :cond_2
    monitor-enter v4
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    .line 147
    :try_start_2
    iput-object v3, v4, Lcom/cgollner/systemmonitor/d/a;->a:Ljava/lang/String;

    .line 148
    move-object/from16 v0, v17

    iput-object v0, v4, Lcom/cgollner/systemmonitor/d/a;->b:Ljava/lang/String;

    .line 149
    iget-object v0, v2, Landroid/app/ActivityManager$RunningAppProcessInfo;->processName:Ljava/lang/String;

    move-object/from16 v17, v0

    if-eqz v17, :cond_3

    iget-object v0, v2, Landroid/app/ActivityManager$RunningAppProcessInfo;->processName:Ljava/lang/String;

    move-object/from16 v17, v0

    const-string v18, ":"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v17

    if-eqz v17, :cond_3

    .line 151
    iget-object v0, v2, Landroid/app/ActivityManager$RunningAppProcessInfo;->processName:Ljava/lang/String;

    move-object/from16 v17, v0

    const-string v18, "(.*:)(.*)"

    const-string v19, "$2"

    invoke-virtual/range {v17 .. v19}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    iput-object v0, v4, Lcom/cgollner/systemmonitor/d/a;->g:Ljava/lang/String;

    .line 155
    :cond_3
    iput v10, v4, Lcom/cgollner/systemmonitor/d/a;->c:F

    .line 156
    iput-wide v14, v4, Lcom/cgollner/systemmonitor/d/a;->e:J

    .line 157
    iput-wide v12, v4, Lcom/cgollner/systemmonitor/d/a;->f:J

    .line 158
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 159
    :try_start_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cgollner/systemmonitor/d/n;->e:Ljava/util/Map;

    move-object/from16 v17, v0

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v18, "-"

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v0, v2, Landroid/app/ActivityManager$RunningAppProcessInfo;->pid:I

    move/from16 v18, v0

    move/from16 v0, v18

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v17

    invoke-interface {v0, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 160
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/cgollner/systemmonitor/d/n;->f:Ljava/util/List;

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 161
    iget v3, v2, Landroid/app/ActivityManager$RunningAppProcessInfo;->pid:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v8, v3}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z
    :try_end_3
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_3 .. :try_end_3} :catch_0

    .line 138
    :goto_4
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    goto/16 :goto_3

    .line 98
    :cond_4
    :try_start_4
    sget v2, Lcom/cgollner/systemmonitor/d/n;->a:I
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_2

    int-to-long v2, v2

    goto/16 :goto_1

    .line 158
    :catchall_0
    move-exception v3

    :try_start_5
    monitor-exit v4
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :try_start_6
    throw v3
    :try_end_6
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_6 .. :try_end_6} :catch_0

    :catch_0
    move-exception v3

    goto :goto_4

    .line 167
    :cond_5
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/cgollner/systemmonitor/d/n;->k:Z

    if-eqz v2, :cond_9

    .line 168
    invoke-interface {v8}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_6
    :goto_5
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_9

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 169
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cgollner/systemmonitor/d/n;->c:Ljava/util/Map;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/cgollner/systemmonitor/d/n$a;

    .line 170
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/cgollner/systemmonitor/d/n;->d:Ljava/util/Map;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v3, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/cgollner/systemmonitor/d/n$a;

    .line 172
    if-eqz v2, :cond_6

    if-eqz v3, :cond_6

    .line 173
    iget-object v5, v2, Lcom/cgollner/systemmonitor/d/n$a;->b:Ljava/lang/String;

    .line 177
    iget-object v9, v3, Lcom/cgollner/systemmonitor/d/n$a;->b:Ljava/lang/String;

    .line 179
    iget-wide v10, v3, Lcom/cgollner/systemmonitor/d/n$a;->a:J

    iget-wide v2, v2, Lcom/cgollner/systemmonitor/d/n$a;->a:J

    sub-long/2addr v10, v2

    .line 181
    move-object/from16 v0, p0

    iget v2, v0, Lcom/cgollner/systemmonitor/d/n;->q:F

    invoke-static {v5, v9, v6, v7, v2}, Lcom/cgollner/systemmonitor/a/b;->a(Ljava/lang/String;Ljava/lang/String;JF)F

    move-result v5

    .line 183
    invoke-static {v4}, Lcom/cgollner/systemmonitor/a/b;->b(I)Ljava/lang/CharSequence;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 184
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/cgollner/systemmonitor/d/n;->e:Ljava/util/Map;

    invoke-interface {v3, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/cgollner/systemmonitor/d/a;

    .line 185
    if-nez v3, :cond_7

    new-instance v3, Lcom/cgollner/systemmonitor/d/a;

    invoke-direct {v3}, Lcom/cgollner/systemmonitor/d/a;-><init>()V

    .line 187
    :cond_7
    monitor-enter v3

    .line 188
    :try_start_7
    iput-object v2, v3, Lcom/cgollner/systemmonitor/d/a;->a:Ljava/lang/String;

    .line 189
    const-string v2, "systemprocess"

    iput-object v2, v3, Lcom/cgollner/systemmonitor/d/a;->b:Ljava/lang/String;

    .line 190
    const/4 v2, 0x0

    iput-object v2, v3, Lcom/cgollner/systemmonitor/d/a;->d:Landroid/graphics/drawable/Drawable;

    .line 191
    iput v5, v3, Lcom/cgollner/systemmonitor/d/a;->c:F

    .line 192
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v5, "/proc/"

    invoke-direct {v2, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "/stat"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/cgollner/systemmonitor/a/c;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v4, " "

    invoke-virtual {v2, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    array-length v4, v2

    const/4 v5, 0x1

    if-gt v4, v5, :cond_8

    const-wide/16 v4, 0x0

    :goto_6
    iput-wide v4, v3, Lcom/cgollner/systemmonitor/d/a;->e:J

    .line 193
    iput-wide v10, v3, Lcom/cgollner/systemmonitor/d/a;->f:J

    .line 194
    monitor-exit v3
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 195
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cgollner/systemmonitor/d/n;->e:Ljava/util/Map;

    iget-object v4, v3, Lcom/cgollner/systemmonitor/d/a;->a:Ljava/lang/String;

    invoke-interface {v2, v4, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 196
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cgollner/systemmonitor/d/n;->f:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_5

    .line 192
    :cond_8
    const/16 v4, 0x17

    :try_start_8
    aget-object v2, v2, v4

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    const-wide/16 v12, 0x1000

    mul-long/2addr v4, v12

    goto :goto_6

    .line 194
    :catchall_1
    move-exception v2

    monitor-exit v3
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    throw v2

    .line 199
    :cond_9
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/cgollner/systemmonitor/d/n;->f:Ljava/util/List;

    monitor-enter v3

    .line 201
    :try_start_9
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cgollner/systemmonitor/d/n;->f:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/cgollner/systemmonitor/d/n;->l:Ljava/util/Comparator;

    invoke-static {v2, v4}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V
    :try_end_9
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_9 .. :try_end_9} :catch_1
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    .line 205
    :goto_7
    :try_start_a
    monitor-exit v3
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_2

    .line 207
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cgollner/systemmonitor/d/n;->o:Landroid/os/Handler;

    new-instance v3, Lcom/cgollner/systemmonitor/d/n$1;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lcom/cgollner/systemmonitor/d/n$1;-><init>(Lcom/cgollner/systemmonitor/d/n;)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_0

    .line 205
    :catchall_2
    move-exception v2

    :try_start_b
    monitor-exit v3
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_2

    throw v2

    .line 218
    :cond_a
    return-void

    :catch_1
    move-exception v2

    goto :goto_7

    :catch_2
    move-exception v2

    goto/16 :goto_2
.end method
