.class public Lcom/cgollner/systemmonitor/d/i;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"

# interfaces
.implements Lcom/cgollner/systemmonitor/c/e$a;


# static fields
.field public static a:I

.field private static i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private b:Lcom/cgollner/systemmonitor/MonitorView;

.field private c:Lcom/cgollner/systemmonitor/c/b;

.field private d:Landroid/widget/TextView;

.field private e:Landroid/widget/TextView;

.field private f:Landroid/widget/TextView;

.field private g:Landroid/widget/TextView;

.field private h:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/cgollner/systemmonitor/d/i;)Lcom/cgollner/systemmonitor/c/b;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/i;->c:Lcom/cgollner/systemmonitor/c/b;

    return-object v0
.end method

.method static synthetic b(Lcom/cgollner/systemmonitor/d/i;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/i;->d:Landroid/widget/TextView;

    return-object v0
.end method

.method private b()V
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/i;->c:Lcom/cgollner/systemmonitor/c/b;

    if-eqz v0, :cond_0

    .line 82
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/i;->c:Lcom/cgollner/systemmonitor/c/b;

    invoke-virtual {v0}, Lcom/cgollner/systemmonitor/c/b;->c()V

    .line 83
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/i;->c:Lcom/cgollner/systemmonitor/c/b;

    .line 85
    :cond_0
    return-void
.end method

.method static synthetic c(Lcom/cgollner/systemmonitor/d/i;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/i;->e:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic d(Lcom/cgollner/systemmonitor/d/i;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/i;->f:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic e(Lcom/cgollner/systemmonitor/d/i;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/i;->g:Landroid/widget/TextView;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 96
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/i;->b:Lcom/cgollner/systemmonitor/MonitorView;

    iget-object v1, p0, Lcom/cgollner/systemmonitor/d/i;->c:Lcom/cgollner/systemmonitor/c/b;

    iget v1, v1, Lcom/cgollner/systemmonitor/c/b;->a:I

    int-to-float v1, v1

    sget-boolean v2, Lcom/cgollner/systemmonitor/b;->a:Z

    invoke-virtual {v0, v1, v2}, Lcom/cgollner/systemmonitor/MonitorView;->a(FZ)V

    .line 97
    sget-boolean v0, Lcom/cgollner/systemmonitor/b;->a:Z

    if-nez v0, :cond_0

    .line 111
    :goto_0
    return-void

    .line 99
    :cond_0
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/i;->h:Landroid/os/Handler;

    new-instance v1, Lcom/cgollner/systemmonitor/d/i$1;

    invoke-direct {v1, p0}, Lcom/cgollner/systemmonitor/d/i$1;-><init>(Lcom/cgollner/systemmonitor/d/i;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 41
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/i;->h:Landroid/os/Handler;

    .line 42
    const/16 v0, 0x3e8

    sput v0, Lcom/cgollner/systemmonitor/d/i;->a:I

    .line 44
    sget v0, Lcom/cgollner/systemmonitor/b/a$f;->cpu_temp_fragment_layout:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 45
    sget v0, Lcom/cgollner/systemmonitor/b/a$e;->monitorview:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/MonitorView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/i;->b:Lcom/cgollner/systemmonitor/MonitorView;

    .line 46
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/i;->b:Lcom/cgollner/systemmonitor/MonitorView;

    const-wide v2, 0x408f400000000000L    # 1000.0

    iput-wide v2, v0, Lcom/cgollner/systemmonitor/MonitorView;->b:D

    .line 47
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/i;->b:Lcom/cgollner/systemmonitor/MonitorView;

    iput-boolean v4, v0, Lcom/cgollner/systemmonitor/MonitorView;->f:Z

    .line 48
    sget-object v0, Lcom/cgollner/systemmonitor/d/i;->i:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 49
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/i;->b:Lcom/cgollner/systemmonitor/MonitorView;

    iget-object v0, v0, Lcom/cgollner/systemmonitor/MonitorView;->a:Ljava/util/List;

    sget-object v2, Lcom/cgollner/systemmonitor/d/i;->i:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 51
    :cond_0
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/i;->b:Lcom/cgollner/systemmonitor/MonitorView;

    invoke-virtual {v0, v4}, Lcom/cgollner/systemmonitor/MonitorView;->setDrawBorders$1d54120b(Z)V

    .line 53
    sget v0, Lcom/cgollner/systemmonitor/b/a$e;->cpuTempCurrentValue:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/i;->d:Landroid/widget/TextView;

    .line 54
    sget v0, Lcom/cgollner/systemmonitor/b/a$e;->cpuTempMinValue:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/i;->e:Landroid/widget/TextView;

    .line 55
    sget v0, Lcom/cgollner/systemmonitor/b/a$e;->cpuTempMaxValue:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/i;->f:Landroid/widget/TextView;

    .line 56
    sget v0, Lcom/cgollner/systemmonitor/b/a$e;->cpuTempAvgValue:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/i;->g:Landroid/widget/TextView;

    .line 57
    invoke-virtual {p0, v4}, Lcom/cgollner/systemmonitor/d/i;->setHasOptionsMenu(Z)V

    .line 59
    return-object v1
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/i;->b:Lcom/cgollner/systemmonitor/MonitorView;

    iget-object v0, v0, Lcom/cgollner/systemmonitor/MonitorView;->a:Ljava/util/List;

    sput-object v0, Lcom/cgollner/systemmonitor/d/i;->i:Ljava/util/List;

    .line 77
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroyView()V

    .line 78
    return-void
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 70
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onPause()V

    .line 71
    invoke-direct {p0}, Lcom/cgollner/systemmonitor/d/i;->b()V

    .line 72
    return-void
.end method

.method public onResume()V
    .locals 4

    .prologue
    .line 64
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 65
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/i;->c:Lcom/cgollner/systemmonitor/c/b;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/cgollner/systemmonitor/d/i;->b()V

    :cond_0
    new-instance v0, Lcom/cgollner/systemmonitor/c/b;

    sget v1, Lcom/cgollner/systemmonitor/d/i;->a:I

    int-to-long v2, v1

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/d/i;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v2, v3, p0, v1}, Lcom/cgollner/systemmonitor/c/b;-><init>(JLcom/cgollner/systemmonitor/c/e$a;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/i;->c:Lcom/cgollner/systemmonitor/c/b;

    .line 66
    return-void
.end method
