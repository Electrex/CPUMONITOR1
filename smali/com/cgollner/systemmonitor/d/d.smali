.class public Lcom/cgollner/systemmonitor/d/d;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"


# instance fields
.field private a:Landroid/view/View;

.field private b:Landroid/widget/TextView;

.field private c:Landroid/widget/TextView;

.field private d:Landroid/widget/TextView;

.field private e:Landroid/widget/TextView;

.field private f:Landroid/widget/TextView;

.field private g:Landroid/widget/TextView;

.field private h:Landroid/widget/TextView;

.field private i:Landroid/widget/TextView;

.field private j:Landroid/widget/TextView;

.field private k:Landroid/widget/TextView;

.field private l:Landroid/content/Context;

.field private m:Landroid/content/BroadcastReceiver;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 104
    new-instance v0, Lcom/cgollner/systemmonitor/d/d$2;

    invoke-direct {v0, p0}, Lcom/cgollner/systemmonitor/d/d$2;-><init>(Lcom/cgollner/systemmonitor/d/d;)V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/d;->m:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method static synthetic a(Lcom/cgollner/systemmonitor/d/d;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/d;->l:Landroid/content/Context;

    return-object v0
.end method

.method private a()V
    .locals 12

    .prologue
    .line 111
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/d;->l:Landroid/content/Context;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/battery/BatteryService;->a(Landroid/content/Context;)Lcom/cgollner/systemmonitor/battery/a;

    move-result-object v0

    .line 113
    iget-boolean v1, v0, Lcom/cgollner/systemmonitor/battery/a;->d:Z

    .line 114
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/d;->l:Landroid/content/Context;

    const/4 v2, 0x1

    invoke-static {v0, v2}, Lcom/cgollner/systemmonitor/battery/b;->a(Landroid/content/Context;Z)J

    move-result-wide v2

    .line 115
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/d;->l:Landroid/content/Context;

    const/4 v4, 0x0

    invoke-static {v0, v4}, Lcom/cgollner/systemmonitor/battery/b;->a(Landroid/content/Context;Z)J

    move-result-wide v4

    .line 116
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/d;->l:Landroid/content/Context;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/battery/b;->b(Landroid/content/Context;)J

    move-result-wide v6

    .line 118
    iget-object v8, p0, Lcom/cgollner/systemmonitor/d/d;->j:Landroid/widget/TextView;

    sget-object v9, Lcom/cgollner/systemmonitor/App;->a:Landroid/content/Context;

    if-eqz v1, :cond_0

    sget v0, Lcom/cgollner/systemmonitor/b/a$h;->charge_speed_1_:I

    :goto_0
    invoke-virtual {v9, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 119
    iget-object v8, p0, Lcom/cgollner/systemmonitor/d/d;->h:Landroid/widget/TextView;

    const-wide/16 v10, -0x1

    cmp-long v0, v6, v10

    if-nez v0, :cond_1

    const-string v0, "N/A"

    :goto_1
    invoke-virtual {v8, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 121
    iget-object v8, p0, Lcom/cgollner/systemmonitor/d/d;->k:Landroid/widget/TextView;

    sget-object v9, Lcom/cgollner/systemmonitor/App;->a:Landroid/content/Context;

    if-eqz v1, :cond_2

    sget v0, Lcom/cgollner/systemmonitor/b/a$h;->charge_rate_per_hour:I

    :goto_2
    invoke-virtual {v9, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 122
    iget-object v1, p0, Lcom/cgollner/systemmonitor/d/d;->i:Landroid/widget/TextView;

    const-wide/16 v8, -0x1

    cmp-long v0, v6, v8

    if-nez v0, :cond_3

    const-string v0, "N/A"

    :goto_3
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 124
    iget-object v1, p0, Lcom/cgollner/systemmonitor/d/d;->b:Landroid/widget/TextView;

    const-wide/16 v6, -0x1

    cmp-long v0, v2, v6

    if-nez v0, :cond_4

    const-string v0, "N/A"

    :goto_4
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 125
    iget-object v1, p0, Lcom/cgollner/systemmonitor/d/d;->c:Landroid/widget/TextView;

    const-wide/16 v6, -0x1

    cmp-long v0, v2, v6

    if-nez v0, :cond_5

    const-string v0, "N/A"

    :goto_5
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 126
    iget-object v1, p0, Lcom/cgollner/systemmonitor/d/d;->d:Landroid/widget/TextView;

    const-wide/16 v6, -0x1

    cmp-long v0, v2, v6

    if-nez v0, :cond_6

    const-string v0, "N/A"

    :goto_6
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 128
    iget-object v1, p0, Lcom/cgollner/systemmonitor/d/d;->e:Landroid/widget/TextView;

    const-wide/16 v2, -0x1

    cmp-long v0, v4, v2

    if-nez v0, :cond_7

    const-string v0, "N/A"

    :goto_7
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 129
    iget-object v1, p0, Lcom/cgollner/systemmonitor/d/d;->f:Landroid/widget/TextView;

    const-wide/16 v2, -0x1

    cmp-long v0, v4, v2

    if-nez v0, :cond_8

    const-string v0, "N/A"

    :goto_8
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 130
    iget-object v1, p0, Lcom/cgollner/systemmonitor/d/d;->g:Landroid/widget/TextView;

    const-wide/16 v2, -0x1

    cmp-long v0, v4, v2

    if-nez v0, :cond_9

    const-string v0, "N/A"

    :goto_9
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 131
    return-void

    .line 118
    :cond_0
    sget v0, Lcom/cgollner/systemmonitor/b/a$h;->discharge_speed_1_:I

    goto/16 :goto_0

    .line 119
    :cond_1
    invoke-static {v6, v7}, Lcom/cgollner/systemmonitor/a/i;->d(J)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 121
    :cond_2
    sget v0, Lcom/cgollner/systemmonitor/b/a$h;->discharge_rate_per_hour:I

    goto :goto_2

    .line 122
    :cond_3
    const-string v0, "%.1f%%"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    const-wide v10, 0x414b774000000000L    # 3600000.0

    long-to-double v6, v6

    div-double v6, v10, v6

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    aput-object v6, v8, v9

    invoke-static {v0, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    .line 124
    :cond_4
    invoke-static {v2, v3}, Lcom/cgollner/systemmonitor/a/i;->d(J)Ljava/lang/String;

    move-result-object v0

    goto :goto_4

    .line 125
    :cond_5
    const-string v0, "%.1f%%"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    const-wide v8, 0x414b774000000000L    # 3600000.0

    long-to-double v10, v2

    div-double/2addr v8, v10

    invoke-static {v8, v9}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v0, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_5

    .line 126
    :cond_6
    const-wide/16 v6, 0x64

    mul-long/2addr v2, v6

    invoke-static {v2, v3}, Lcom/cgollner/systemmonitor/a/i;->d(J)Ljava/lang/String;

    move-result-object v0

    goto :goto_6

    .line 128
    :cond_7
    invoke-static {v4, v5}, Lcom/cgollner/systemmonitor/a/i;->d(J)Ljava/lang/String;

    move-result-object v0

    goto :goto_7

    .line 129
    :cond_8
    const-string v0, "%.1f%%"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-wide v6, 0x414b774000000000L    # 3600000.0

    long-to-double v8, v4

    div-double/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    aput-object v6, v2, v3

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_8

    .line 130
    :cond_9
    const-wide/16 v2, 0x64

    mul-long/2addr v2, v4

    invoke-static {v2, v3}, Lcom/cgollner/systemmonitor/a/i;->d(J)Ljava/lang/String;

    move-result-object v0

    goto :goto_9
.end method

.method static synthetic b(Lcom/cgollner/systemmonitor/d/d;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/cgollner/systemmonitor/d/d;->a()V

    return-void
.end method


# virtual methods
.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 1

    .prologue
    .line 88
    sget v0, Lcom/cgollner/systemmonitor/b/a$g;->battery_stats_menu:I

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 89
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/Fragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 90
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 39
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/d/d;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/d;->l:Landroid/content/Context;

    .line 40
    sget v0, Lcom/cgollner/systemmonitor/b/a$f;->battery_stats_layout:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/d;->a:Landroid/view/View;

    .line 42
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/d;->a:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/b/a$e;->textViewCycleSpeed:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/d;->h:Landroid/widget/TextView;

    .line 43
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/d;->a:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/b/a$e;->textViewCycleRateHour:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/d;->i:Landroid/widget/TextView;

    .line 44
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/d;->a:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/b/a$e;->textViewCycleSpeedTitle:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/d;->j:Landroid/widget/TextView;

    .line 45
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/d;->a:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/b/a$e;->textViewCycleRateTitle:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/d;->k:Landroid/widget/TextView;

    .line 47
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/d;->a:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/b/a$e;->textViewStatsChargeSpeed:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/d;->b:Landroid/widget/TextView;

    .line 48
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/d;->a:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/b/a$e;->textViewStatsChargeRateHour:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/d;->c:Landroid/widget/TextView;

    .line 49
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/d;->a:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/b/a$e;->textViewStatsTimeToCharge:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/d;->d:Landroid/widget/TextView;

    .line 51
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/d;->a:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/b/a$e;->textViewStatsDisChargeSpeed:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/d;->e:Landroid/widget/TextView;

    .line 52
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/d;->a:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/b/a$e;->textViewStatsDisChargeRateHour:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/d;->f:Landroid/widget/TextView;

    .line 53
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/d;->a:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/b/a$e;->textViewStatsTimeToDisCharge:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/d/d;->g:Landroid/widget/TextView;

    .line 55
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/d/d;->setHasOptionsMenu(Z)V

    .line 57
    invoke-direct {p0}, Lcom/cgollner/systemmonitor/d/d;->a()V

    .line 59
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/d;->a:Landroid/view/View;

    return-object v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 5

    .prologue
    .line 64
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sget v1, Lcom/cgollner/systemmonitor/b/a$e;->resetStats:I

    if-ne v0, v1, :cond_0

    .line 65
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/d/d;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget v1, Lcom/cgollner/systemmonitor/b/a$h;->reset_battery_statistics:I

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    sget-object v3, Lcom/cgollner/systemmonitor/App;->a:Landroid/content/Context;

    sget v4, Lcom/cgollner/systemmonitor/b/a$h;->reset_charging_statistics:I

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    sget-object v3, Lcom/cgollner/systemmonitor/App;->a:Landroid/content/Context;

    sget v4, Lcom/cgollner/systemmonitor/b/a$h;->reset_discharging_statistics:I

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    sget-object v3, Lcom/cgollner/systemmonitor/App;->a:Landroid/content/Context;

    sget v4, Lcom/cgollner/systemmonitor/b/a$h;->reset_all_statistics:I

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    new-instance v2, Lcom/cgollner/systemmonitor/d/d$1;

    invoke-direct {v2, p0}, Lcom/cgollner/systemmonitor/d/d$1;-><init>(Lcom/cgollner/systemmonitor/d/d;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setItems([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 84
    :cond_0
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 100
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onPause()V

    .line 101
    iget-object v0, p0, Lcom/cgollner/systemmonitor/d/d;->l:Landroid/content/Context;

    iget-object v1, p0, Lcom/cgollner/systemmonitor/d/d;->m:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 102
    return-void
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 93
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 94
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "com.cgollner.systemmonitor.battery.ACTION_BATTERY_INFO_UPDATE"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 95
    iget-object v1, p0, Lcom/cgollner/systemmonitor/d/d;->l:Landroid/content/Context;

    iget-object v2, p0, Lcom/cgollner/systemmonitor/d/d;->m:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 97
    return-void
.end method
