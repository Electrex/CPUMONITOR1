.class public Lcom/cgollner/systemmonitor/f/a;
.super Landroid/support/v7/app/ActionBarActivity;
.source "SourceFile"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# instance fields
.field private A:Landroid/widget/SeekBar$OnSeekBarChangeListener;

.field private B:Landroid/widget/SeekBar$OnSeekBarChangeListener;

.field private C:Landroid/view/View$OnClickListener;

.field private D:Lnet/margaritov/preference/colorpicker/ColorPickerDialog$OnColorChangedListener;

.field private E:Landroid/view/View$OnClickListener;

.field private F:Lnet/margaritov/preference/colorpicker/ColorPickerDialog$OnColorChangedListener;

.field private G:Landroid/view/View$OnClickListener;

.field private H:Lnet/margaritov/preference/colorpicker/ColorPickerDialog$OnColorChangedListener;

.field private a:Lcom/cgollner/systemmonitor/BatteryCircleView;

.field private b:Landroid/widget/SeekBar;

.field private c:Landroid/widget/SeekBar;

.field private d:Landroid/widget/SeekBar;

.field private e:Landroid/widget/SeekBar;

.field private f:Landroid/widget/TextView;

.field private g:Landroid/widget/TextView;

.field private h:Landroid/widget/TextView;

.field private i:Landroid/view/View;

.field private j:Landroid/view/View;

.field private k:Landroid/view/View;

.field private l:Landroid/view/View;

.field private m:Landroid/view/View;

.field private n:Landroid/view/View;

.field private o:Landroid/view/View;

.field private p:Landroid/view/View;

.field private q:Landroid/view/View;

.field private r:Landroid/view/View;

.field private s:Landroid/view/View;

.field private t:[Landroid/view/View;

.field private u:I

.field private v:Landroid/view/View;

.field private w:I

.field private x:Landroid/view/View$OnClickListener;

.field private y:Landroid/view/View$OnClickListener;

.field private z:Landroid/widget/SeekBar$OnSeekBarChangeListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Landroid/support/v7/app/ActionBarActivity;-><init>()V

    .line 231
    new-instance v0, Lcom/cgollner/systemmonitor/f/a$5;

    invoke-direct {v0, p0}, Lcom/cgollner/systemmonitor/f/a$5;-><init>(Lcom/cgollner/systemmonitor/f/a;)V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/f/a;->x:Landroid/view/View$OnClickListener;

    .line 239
    new-instance v0, Lcom/cgollner/systemmonitor/f/a$6;

    invoke-direct {v0, p0}, Lcom/cgollner/systemmonitor/f/a$6;-><init>(Lcom/cgollner/systemmonitor/f/a;)V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/f/a;->y:Landroid/view/View$OnClickListener;

    .line 249
    new-instance v0, Lcom/cgollner/systemmonitor/f/a$7;

    invoke-direct {v0, p0}, Lcom/cgollner/systemmonitor/f/a$7;-><init>(Lcom/cgollner/systemmonitor/f/a;)V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/f/a;->z:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    .line 260
    new-instance v0, Lcom/cgollner/systemmonitor/f/a$8;

    invoke-direct {v0, p0}, Lcom/cgollner/systemmonitor/f/a$8;-><init>(Lcom/cgollner/systemmonitor/f/a;)V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/f/a;->A:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    .line 270
    new-instance v0, Lcom/cgollner/systemmonitor/f/a$9;

    invoke-direct {v0, p0}, Lcom/cgollner/systemmonitor/f/a$9;-><init>(Lcom/cgollner/systemmonitor/f/a;)V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/f/a;->B:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    .line 280
    new-instance v0, Lcom/cgollner/systemmonitor/f/a$10;

    invoke-direct {v0, p0}, Lcom/cgollner/systemmonitor/f/a$10;-><init>(Lcom/cgollner/systemmonitor/f/a;)V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/f/a;->C:Landroid/view/View$OnClickListener;

    .line 291
    new-instance v0, Lcom/cgollner/systemmonitor/f/a$11;

    invoke-direct {v0, p0}, Lcom/cgollner/systemmonitor/f/a$11;-><init>(Lcom/cgollner/systemmonitor/f/a;)V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/f/a;->D:Lnet/margaritov/preference/colorpicker/ColorPickerDialog$OnColorChangedListener;

    .line 301
    new-instance v0, Lcom/cgollner/systemmonitor/f/a$12;

    invoke-direct {v0, p0}, Lcom/cgollner/systemmonitor/f/a$12;-><init>(Lcom/cgollner/systemmonitor/f/a;)V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/f/a;->E:Landroid/view/View$OnClickListener;

    .line 310
    new-instance v0, Lcom/cgollner/systemmonitor/f/a$2;

    invoke-direct {v0, p0}, Lcom/cgollner/systemmonitor/f/a$2;-><init>(Lcom/cgollner/systemmonitor/f/a;)V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/f/a;->F:Lnet/margaritov/preference/colorpicker/ColorPickerDialog$OnColorChangedListener;

    .line 320
    new-instance v0, Lcom/cgollner/systemmonitor/f/a$3;

    invoke-direct {v0, p0}, Lcom/cgollner/systemmonitor/f/a$3;-><init>(Lcom/cgollner/systemmonitor/f/a;)V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/f/a;->G:Landroid/view/View$OnClickListener;

    .line 330
    new-instance v0, Lcom/cgollner/systemmonitor/f/a$4;

    invoke-direct {v0, p0}, Lcom/cgollner/systemmonitor/f/a$4;-><init>(Lcom/cgollner/systemmonitor/f/a;)V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/f/a;->H:Lnet/margaritov/preference/colorpicker/ColorPickerDialog$OnColorChangedListener;

    return-void
.end method

.method static synthetic a(Lcom/cgollner/systemmonitor/f/a;)I
    .locals 1

    .prologue
    .line 33
    iget v0, p0, Lcom/cgollner/systemmonitor/f/a;->u:I

    return v0
.end method

.method static synthetic a(Lcom/cgollner/systemmonitor/f/a;I)I
    .locals 0

    .prologue
    .line 33
    iput p1, p0, Lcom/cgollner/systemmonitor/f/a;->u:I

    return p1
.end method

.method private a()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 221
    move v0, v1

    :goto_0
    iget-object v2, p0, Lcom/cgollner/systemmonitor/f/a;->t:[Landroid/view/View;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 222
    iget v2, p0, Lcom/cgollner/systemmonitor/f/a;->u:I

    if-ne v0, v2, :cond_0

    .line 223
    iget-object v2, p0, Lcom/cgollner/systemmonitor/f/a;->t:[Landroid/view/View;

    aget-object v2, v2, v0

    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 221
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 226
    :cond_0
    iget-object v2, p0, Lcom/cgollner/systemmonitor/f/a;->t:[Landroid/view/View;

    aget-object v2, v2, v0

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 230
    :cond_1
    return-void
.end method

.method static synthetic b(Lcom/cgollner/systemmonitor/f/a;)[Landroid/view/View;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/cgollner/systemmonitor/f/a;->t:[Landroid/view/View;

    return-object v0
.end method

.method static synthetic c(Lcom/cgollner/systemmonitor/f/a;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/cgollner/systemmonitor/f/a;->a()V

    return-void
.end method

.method static synthetic d(Lcom/cgollner/systemmonitor/f/a;)I
    .locals 2

    .prologue
    .line 33
    iget v0, p0, Lcom/cgollner/systemmonitor/f/a;->u:I

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Lcom/cgollner/systemmonitor/f/a;->u:I

    return v0
.end method

.method static synthetic e(Lcom/cgollner/systemmonitor/f/a;)Lcom/cgollner/systemmonitor/BatteryCircleView;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/cgollner/systemmonitor/f/a;->a:Lcom/cgollner/systemmonitor/BatteryCircleView;

    return-object v0
.end method

.method static synthetic f(Lcom/cgollner/systemmonitor/f/a;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/cgollner/systemmonitor/f/a;->f:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic g(Lcom/cgollner/systemmonitor/f/a;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/cgollner/systemmonitor/f/a;->h:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic h(Lcom/cgollner/systemmonitor/f/a;)Lnet/margaritov/preference/colorpicker/ColorPickerDialog$OnColorChangedListener;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/cgollner/systemmonitor/f/a;->D:Lnet/margaritov/preference/colorpicker/ColorPickerDialog$OnColorChangedListener;

    return-object v0
.end method

.method static synthetic i(Lcom/cgollner/systemmonitor/f/a;)Landroid/view/View;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/cgollner/systemmonitor/f/a;->r:Landroid/view/View;

    return-object v0
.end method

.method static synthetic j(Lcom/cgollner/systemmonitor/f/a;)Lnet/margaritov/preference/colorpicker/ColorPickerDialog$OnColorChangedListener;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/cgollner/systemmonitor/f/a;->F:Lnet/margaritov/preference/colorpicker/ColorPickerDialog$OnColorChangedListener;

    return-object v0
.end method

.method static synthetic k(Lcom/cgollner/systemmonitor/f/a;)Landroid/view/View;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/cgollner/systemmonitor/f/a;->s:Landroid/view/View;

    return-object v0
.end method

.method static synthetic l(Lcom/cgollner/systemmonitor/f/a;)Lnet/margaritov/preference/colorpicker/ColorPickerDialog$OnColorChangedListener;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/cgollner/systemmonitor/f/a;->H:Lnet/margaritov/preference/colorpicker/ColorPickerDialog$OnColorChangedListener;

    return-object v0
.end method

.method static synthetic m(Lcom/cgollner/systemmonitor/f/a;)Landroid/view/View;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/cgollner/systemmonitor/f/a;->j:Landroid/view/View;

    return-object v0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/16 v5, 0x64

    const/4 v4, 0x0

    .line 49
    invoke-super {p0, p1}, Landroid/support/v7/app/ActionBarActivity;->onCreate(Landroid/os/Bundle;)V

    .line 51
    sget v0, Lcom/cgollner/systemmonitor/b/a$f;->battery_circle_layout:I

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/f/a;->setContentView(I)V

    .line 53
    sget v0, Lcom/cgollner/systemmonitor/b/a$e;->batteryView:I

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/f/a;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/BatteryCircleView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/f/a;->a:Lcom/cgollner/systemmonitor/BatteryCircleView;

    .line 56
    sget v0, Lcom/cgollner/systemmonitor/b/a$e;->percentageSeekBar:I

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/f/a;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/f/a;->d:Landroid/widget/SeekBar;

    .line 57
    iget-object v0, p0, Lcom/cgollner/systemmonitor/f/a;->d:Landroid/widget/SeekBar;

    invoke-virtual {v0, p0}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 60
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/f/a;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/app/WallpaperManager;->getInstance(Landroid/content/Context;)Landroid/app/WallpaperManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/WallpaperManager;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 61
    iget-object v1, p0, Lcom/cgollner/systemmonitor/f/a;->a:Lcom/cgollner/systemmonitor/BatteryCircleView;

    invoke-virtual {v1, v0}, Lcom/cgollner/systemmonitor/BatteryCircleView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 63
    sget v0, Lcom/cgollner/systemmonitor/b/a$e;->percentageText:I

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/f/a;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/f/a;->g:Landroid/widget/TextView;

    .line 65
    sget v0, Lcom/cgollner/systemmonitor/b/a$e;->percentageLayout:I

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/f/a;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/f/a;->m:Landroid/view/View;

    .line 67
    sget v0, Lcom/cgollner/systemmonitor/b/a$e;->nextButton:I

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/f/a;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/f/a;->p:Landroid/view/View;

    .line 68
    iget-object v0, p0, Lcom/cgollner/systemmonitor/f/a;->p:Landroid/view/View;

    iget-object v1, p0, Lcom/cgollner/systemmonitor/f/a;->x:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 70
    sget v0, Lcom/cgollner/systemmonitor/b/a$e;->prevButton:I

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/f/a;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/f/a;->q:Landroid/view/View;

    .line 71
    iget-object v0, p0, Lcom/cgollner/systemmonitor/f/a;->q:Landroid/view/View;

    iget-object v1, p0, Lcom/cgollner/systemmonitor/f/a;->y:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 80
    sget v0, Lcom/cgollner/systemmonitor/b/a$e;->textColorLayout:I

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/f/a;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/f/a;->n:Landroid/view/View;

    .line 81
    iget-object v0, p0, Lcom/cgollner/systemmonitor/f/a;->n:Landroid/view/View;

    iget-object v1, p0, Lcom/cgollner/systemmonitor/f/a;->C:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 82
    sget v0, Lcom/cgollner/systemmonitor/b/a$e;->colorSquare:I

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/f/a;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/f/a;->r:Landroid/view/View;

    .line 83
    iget-object v0, p0, Lcom/cgollner/systemmonitor/f/a;->r:Landroid/view/View;

    iget-object v1, p0, Lcom/cgollner/systemmonitor/f/a;->a:Lcom/cgollner/systemmonitor/BatteryCircleView;

    invoke-virtual {v1}, Lcom/cgollner/systemmonitor/BatteryCircleView;->getTextColor()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 84
    iget-object v0, p0, Lcom/cgollner/systemmonitor/f/a;->r:Landroid/view/View;

    iget-object v1, p0, Lcom/cgollner/systemmonitor/f/a;->C:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 87
    sget v0, Lcom/cgollner/systemmonitor/b/a$e;->circleWidthLayout:I

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/f/a;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/f/a;->o:Landroid/view/View;

    .line 88
    sget v0, Lcom/cgollner/systemmonitor/b/a$e;->circleWidthSeekBar:I

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/f/a;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/f/a;->e:Landroid/widget/SeekBar;

    .line 89
    iget-object v0, p0, Lcom/cgollner/systemmonitor/f/a;->e:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/cgollner/systemmonitor/f/a;->a:Lcom/cgollner/systemmonitor/BatteryCircleView;

    invoke-virtual {v1}, Lcom/cgollner/systemmonitor/BatteryCircleView;->getCircleWidthMultiplier()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 90
    iget-object v0, p0, Lcom/cgollner/systemmonitor/f/a;->e:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/cgollner/systemmonitor/f/a;->B:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 91
    sget v0, Lcom/cgollner/systemmonitor/b/a$e;->circleWidthText:I

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/f/a;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/f/a;->h:Landroid/widget/TextView;

    .line 94
    sget v0, Lcom/cgollner/systemmonitor/b/a$e;->innerCircleColorLayout:I

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/f/a;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/f/a;->l:Landroid/view/View;

    .line 95
    sget v0, Lcom/cgollner/systemmonitor/b/a$e;->colorInnerCircle:I

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/f/a;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/f/a;->s:Landroid/view/View;

    .line 96
    iget-object v0, p0, Lcom/cgollner/systemmonitor/f/a;->s:Landroid/view/View;

    iget-object v1, p0, Lcom/cgollner/systemmonitor/f/a;->a:Lcom/cgollner/systemmonitor/BatteryCircleView;

    invoke-virtual {v1}, Lcom/cgollner/systemmonitor/BatteryCircleView;->getInnerCircleColor()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 97
    iget-object v0, p0, Lcom/cgollner/systemmonitor/f/a;->l:Landroid/view/View;

    iget-object v1, p0, Lcom/cgollner/systemmonitor/f/a;->E:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 98
    iget-object v0, p0, Lcom/cgollner/systemmonitor/f/a;->s:Landroid/view/View;

    iget-object v1, p0, Lcom/cgollner/systemmonitor/f/a;->E:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 99
    sget v0, Lcom/cgollner/systemmonitor/b/a$e;->innerCircleBgColorLayout:I

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/f/a;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/f/a;->i:Landroid/view/View;

    .line 100
    sget v0, Lcom/cgollner/systemmonitor/b/a$e;->colorInnerCircleBg:I

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/f/a;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/f/a;->j:Landroid/view/View;

    .line 101
    iget-object v0, p0, Lcom/cgollner/systemmonitor/f/a;->j:Landroid/view/View;

    iget-object v1, p0, Lcom/cgollner/systemmonitor/f/a;->G:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 102
    iget-object v0, p0, Lcom/cgollner/systemmonitor/f/a;->i:Landroid/view/View;

    iget-object v1, p0, Lcom/cgollner/systemmonitor/f/a;->G:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 105
    sget v0, Lcom/cgollner/systemmonitor/b/a$e;->outerCircleWidthLayout:I

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/f/a;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/f/a;->k:Landroid/view/View;

    .line 106
    sget v0, Lcom/cgollner/systemmonitor/b/a$e;->outerCircleWidthText:I

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/f/a;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/f/a;->f:Landroid/widget/TextView;

    .line 107
    sget v0, Lcom/cgollner/systemmonitor/b/a$e;->outerCircleWidthSeekBar:I

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/f/a;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/f/a;->c:Landroid/widget/SeekBar;

    .line 108
    iget-object v0, p0, Lcom/cgollner/systemmonitor/f/a;->c:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/cgollner/systemmonitor/f/a;->z:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 111
    sget v0, Lcom/cgollner/systemmonitor/b/a$e;->percentageCircleWidthLayout:I

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/f/a;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/f/a;->v:Landroid/view/View;

    .line 112
    sget v0, Lcom/cgollner/systemmonitor/b/a$e;->percentageCircleWidthSeekBar:I

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/f/a;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/f/a;->b:Landroid/widget/SeekBar;

    .line 113
    iget-object v0, p0, Lcom/cgollner/systemmonitor/f/a;->b:Landroid/widget/SeekBar;

    const/16 v1, 0xaf

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 114
    iget-object v0, p0, Lcom/cgollner/systemmonitor/f/a;->b:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/cgollner/systemmonitor/f/a;->A:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 116
    const/4 v0, 0x7

    new-array v0, v0, [Landroid/view/View;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/f/a;->t:[Landroid/view/View;

    .line 117
    iget-object v0, p0, Lcom/cgollner/systemmonitor/f/a;->t:[Landroid/view/View;

    iget-object v1, p0, Lcom/cgollner/systemmonitor/f/a;->m:Landroid/view/View;

    aput-object v1, v0, v4

    .line 118
    iget-object v0, p0, Lcom/cgollner/systemmonitor/f/a;->t:[Landroid/view/View;

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/cgollner/systemmonitor/f/a;->n:Landroid/view/View;

    aput-object v2, v0, v1

    .line 119
    iget-object v0, p0, Lcom/cgollner/systemmonitor/f/a;->t:[Landroid/view/View;

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/cgollner/systemmonitor/f/a;->o:Landroid/view/View;

    aput-object v2, v0, v1

    .line 120
    iget-object v0, p0, Lcom/cgollner/systemmonitor/f/a;->t:[Landroid/view/View;

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/cgollner/systemmonitor/f/a;->l:Landroid/view/View;

    aput-object v2, v0, v1

    .line 121
    iget-object v0, p0, Lcom/cgollner/systemmonitor/f/a;->t:[Landroid/view/View;

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/cgollner/systemmonitor/f/a;->i:Landroid/view/View;

    aput-object v2, v0, v1

    .line 122
    iget-object v0, p0, Lcom/cgollner/systemmonitor/f/a;->t:[Landroid/view/View;

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/cgollner/systemmonitor/f/a;->k:Landroid/view/View;

    aput-object v2, v0, v1

    .line 123
    iget-object v0, p0, Lcom/cgollner/systemmonitor/f/a;->t:[Landroid/view/View;

    const/4 v1, 0x6

    iget-object v2, p0, Lcom/cgollner/systemmonitor/f/a;->v:Landroid/view/View;

    aput-object v2, v0, v1

    .line 125
    iput v4, p0, Lcom/cgollner/systemmonitor/f/a;->u:I

    .line 127
    invoke-direct {p0}, Lcom/cgollner/systemmonitor/f/a;->a()V

    .line 129
    invoke-virtual {p0, v4}, Lcom/cgollner/systemmonitor/f/a;->setResult(I)V

    .line 132
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/f/a;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 133
    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 134
    if-eqz v0, :cond_0

    .line 135
    const-string v1, "appWidgetId"

    invoke-virtual {v0, v1, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/cgollner/systemmonitor/f/a;->w:I

    .line 141
    :cond_0
    iget v0, p0, Lcom/cgollner/systemmonitor/f/a;->w:I

    if-nez v0, :cond_1

    .line 142
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/f/a;->finish()V

    .line 144
    :cond_1
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iget-object v1, p0, Lcom/cgollner/systemmonitor/f/a;->a:Lcom/cgollner/systemmonitor/BatteryCircleView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget v3, p0, Lcom/cgollner/systemmonitor/f/a;->w:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "-textColor"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, -0x1

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/cgollner/systemmonitor/BatteryCircleView;->setTextColor(I)V

    iget-object v1, p0, Lcom/cgollner/systemmonitor/f/a;->a:Lcom/cgollner/systemmonitor/BatteryCircleView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget v3, p0, Lcom/cgollner/systemmonitor/f/a;->w:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "-innerCircleWidth"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2, v5}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/cgollner/systemmonitor/BatteryCircleView;->setInnerCircleWidth(I)V

    iget-object v1, p0, Lcom/cgollner/systemmonitor/f/a;->a:Lcom/cgollner/systemmonitor/BatteryCircleView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget v3, p0, Lcom/cgollner/systemmonitor/f/a;->w:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "-innerCircleColor"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/high16 v3, -0x1000000

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/cgollner/systemmonitor/BatteryCircleView;->setInnerCircleColor(I)V

    iget-object v1, p0, Lcom/cgollner/systemmonitor/f/a;->a:Lcom/cgollner/systemmonitor/BatteryCircleView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget v3, p0, Lcom/cgollner/systemmonitor/f/a;->w:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "-outerCircleWidth"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0xa

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/cgollner/systemmonitor/BatteryCircleView;->setOuterCircleWidth(I)V

    iget-object v1, p0, Lcom/cgollner/systemmonitor/f/a;->a:Lcom/cgollner/systemmonitor/BatteryCircleView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget v3, p0, Lcom/cgollner/systemmonitor/f/a;->w:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "-percentageCircleWidth"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2, v5}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/cgollner/systemmonitor/BatteryCircleView;->setPercentageCircleWidth(I)V

    iget-object v1, p0, Lcom/cgollner/systemmonitor/f/a;->a:Lcom/cgollner/systemmonitor/BatteryCircleView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget v3, p0, Lcom/cgollner/systemmonitor/f/a;->w:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "-innerCircleBG"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/cgollner/systemmonitor/BatteryCircleView;->setInnerCircleBgColor(I)V

    iget-object v0, p0, Lcom/cgollner/systemmonitor/f/a;->r:Landroid/view/View;

    iget-object v1, p0, Lcom/cgollner/systemmonitor/f/a;->a:Lcom/cgollner/systemmonitor/BatteryCircleView;

    invoke-virtual {v1}, Lcom/cgollner/systemmonitor/BatteryCircleView;->getTextColor()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    iget-object v0, p0, Lcom/cgollner/systemmonitor/f/a;->h:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Inner circle width: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/cgollner/systemmonitor/f/a;->a:Lcom/cgollner/systemmonitor/BatteryCircleView;

    invoke-virtual {v2}, Lcom/cgollner/systemmonitor/BatteryCircleView;->getCircleWidthMultiplier()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/cgollner/systemmonitor/f/a;->s:Landroid/view/View;

    iget-object v1, p0, Lcom/cgollner/systemmonitor/f/a;->a:Lcom/cgollner/systemmonitor/BatteryCircleView;

    invoke-virtual {v1}, Lcom/cgollner/systemmonitor/BatteryCircleView;->getInnerCircleColor()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    iget-object v0, p0, Lcom/cgollner/systemmonitor/f/a;->f:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Outer circle width: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/cgollner/systemmonitor/f/a;->a:Lcom/cgollner/systemmonitor/BatteryCircleView;

    invoke-virtual {v2}, Lcom/cgollner/systemmonitor/BatteryCircleView;->getOuterCircleWidth()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/cgollner/systemmonitor/f/a;->j:Landroid/view/View;

    iget-object v1, p0, Lcom/cgollner/systemmonitor/f/a;->a:Lcom/cgollner/systemmonitor/BatteryCircleView;

    invoke-virtual {v1}, Lcom/cgollner/systemmonitor/BatteryCircleView;->getInnerCircleBgColor()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 145
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    .prologue
    .line 149
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/f/a;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 150
    sget v1, Lcom/cgollner/systemmonitor/b/a$g;->activity_configure_battery_widget:I

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 151
    invoke-super {p0, p1}, Landroid/support/v7/app/ActionBarActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    .line 182
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/f/a;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 183
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, p0, Lcom/cgollner/systemmonitor/f/a;->w:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "-textColor"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/cgollner/systemmonitor/f/a;->a:Lcom/cgollner/systemmonitor/BatteryCircleView;

    invoke-virtual {v2}, Lcom/cgollner/systemmonitor/BatteryCircleView;->getTextColor()I

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 184
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, p0, Lcom/cgollner/systemmonitor/f/a;->w:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "-innerCircleWidth"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/cgollner/systemmonitor/f/a;->a:Lcom/cgollner/systemmonitor/BatteryCircleView;

    invoke-virtual {v2}, Lcom/cgollner/systemmonitor/BatteryCircleView;->getCircleWidthMultiplier()I

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 185
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, p0, Lcom/cgollner/systemmonitor/f/a;->w:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "-innerCircleColor"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/cgollner/systemmonitor/f/a;->a:Lcom/cgollner/systemmonitor/BatteryCircleView;

    invoke-virtual {v2}, Lcom/cgollner/systemmonitor/BatteryCircleView;->getInnerCircleColor()I

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 186
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, p0, Lcom/cgollner/systemmonitor/f/a;->w:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "-outerCircleWidth"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/cgollner/systemmonitor/f/a;->a:Lcom/cgollner/systemmonitor/BatteryCircleView;

    invoke-virtual {v2}, Lcom/cgollner/systemmonitor/BatteryCircleView;->getOuterCircleWidth()I

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 187
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, p0, Lcom/cgollner/systemmonitor/f/a;->w:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "-percentageCircleWidth"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/cgollner/systemmonitor/f/a;->a:Lcom/cgollner/systemmonitor/BatteryCircleView;

    invoke-virtual {v2}, Lcom/cgollner/systemmonitor/BatteryCircleView;->getPercentageCircleWidth()I

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 188
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, p0, Lcom/cgollner/systemmonitor/f/a;->w:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "-innerCircleBG"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/cgollner/systemmonitor/f/a;->a:Lcom/cgollner/systemmonitor/BatteryCircleView;

    invoke-virtual {v2}, Lcom/cgollner/systemmonitor/BatteryCircleView;->getInnerCircleBgColor()I

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 189
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 192
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 193
    const-string v1, "android.appwidget.action.APPWIDGET_UPDATE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 194
    const-string v1, "appWidgetId"

    iget v2, p0, Lcom/cgollner/systemmonitor/f/a;->w:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 197
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/cgollner/systemmonitor/f/a;->setResult(ILandroid/content/Intent;)V

    .line 198
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/f/a;->finish()V

    .line 200
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/cgollner/systemmonitor/f/a$1;

    invoke-direct {v1, p0}, Lcom/cgollner/systemmonitor/f/a$1;-><init>(Lcom/cgollner/systemmonitor/f/a;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 201
    const/4 v0, 0x1

    return v0
.end method

.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 3

    .prologue
    .line 340
    iget-object v0, p0, Lcom/cgollner/systemmonitor/f/a;->a:Lcom/cgollner/systemmonitor/BatteryCircleView;

    int-to-float v1, p2

    invoke-virtual {v0, v1}, Lcom/cgollner/systemmonitor/BatteryCircleView;->setValue(F)V

    .line 341
    iget-object v0, p0, Lcom/cgollner/systemmonitor/f/a;->g:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Percentage: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 342
    return-void
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0

    .prologue
    .line 347
    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0

    .prologue
    .line 352
    return-void
.end method
