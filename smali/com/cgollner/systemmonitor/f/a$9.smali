.class final Lcom/cgollner/systemmonitor/f/a$9;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cgollner/systemmonitor/f/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/cgollner/systemmonitor/f/a;


# direct methods
.method constructor <init>(Lcom/cgollner/systemmonitor/f/a;)V
    .locals 0

    .prologue
    .line 270
    iput-object p1, p0, Lcom/cgollner/systemmonitor/f/a$9;->a:Lcom/cgollner/systemmonitor/f/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 3

    .prologue
    .line 275
    iget-object v0, p0, Lcom/cgollner/systemmonitor/f/a$9;->a:Lcom/cgollner/systemmonitor/f/a;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/f/a;->e(Lcom/cgollner/systemmonitor/f/a;)Lcom/cgollner/systemmonitor/BatteryCircleView;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/cgollner/systemmonitor/BatteryCircleView;->setInnerCircleWidth(I)V

    .line 276
    iget-object v0, p0, Lcom/cgollner/systemmonitor/f/a$9;->a:Lcom/cgollner/systemmonitor/f/a;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/f/a;->e(Lcom/cgollner/systemmonitor/f/a;)Lcom/cgollner/systemmonitor/BatteryCircleView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/cgollner/systemmonitor/BatteryCircleView;->invalidate()V

    .line 277
    iget-object v0, p0, Lcom/cgollner/systemmonitor/f/a$9;->a:Lcom/cgollner/systemmonitor/f/a;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/f/a;->g(Lcom/cgollner/systemmonitor/f/a;)Landroid/widget/TextView;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Inner circle width: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 278
    return-void
.end method

.method public final onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0

    .prologue
    .line 272
    return-void
.end method

.method public final onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0

    .prologue
    .line 271
    return-void
.end method
