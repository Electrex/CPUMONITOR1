.class final Lcom/cgollner/systemmonitor/b$b;
.super Landroid/support/v4/app/FragmentPagerAdapter;
.source "SourceFile"

# interfaces
.implements Landroid/support/v4/view/ViewPager$OnPageChangeListener;
.implements Lcom/cgollner/systemmonitor/d/c$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cgollner/systemmonitor/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "b"
.end annotation


# instance fields
.field final synthetic a:Lcom/cgollner/systemmonitor/b;


# direct methods
.method public constructor <init>(Lcom/cgollner/systemmonitor/b;Landroid/support/v4/app/FragmentManager;)V
    .locals 0

    .prologue
    .line 355
    iput-object p1, p0, Lcom/cgollner/systemmonitor/b$b;->a:Lcom/cgollner/systemmonitor/b;

    .line 356
    invoke-direct {p0, p2}, Landroid/support/v4/app/FragmentPagerAdapter;-><init>(Landroid/support/v4/app/FragmentManager;)V

    .line 357
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 3

    .prologue
    .line 384
    iget-object v0, p0, Lcom/cgollner/systemmonitor/b$b;->a:Lcom/cgollner/systemmonitor/b;

    iget-object v0, v0, Lcom/cgollner/systemmonitor/b;->b:Ljava/util/List;

    iget-object v1, p0, Lcom/cgollner/systemmonitor/b$b;->a:Lcom/cgollner/systemmonitor/b;

    invoke-static {v1}, Lcom/cgollner/systemmonitor/b;->a(Lcom/cgollner/systemmonitor/b;)Landroid/support/v4/view/ViewPager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/b$a;

    iget-object v0, v0, Lcom/cgollner/systemmonitor/b$a;->d:Ljava/lang/Class;

    const-class v1, Lcom/cgollner/systemmonitor/d/c;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 385
    iget-object v0, p0, Lcom/cgollner/systemmonitor/b$b;->a:Lcom/cgollner/systemmonitor/b;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/b;->b(Lcom/cgollner/systemmonitor/b;)Landroid/support/v4/view/PagerTitleStrip;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v4/view/PagerTitleStrip;->setBackgroundResource(I)V

    .line 386
    iget-object v0, p0, Lcom/cgollner/systemmonitor/b$b;->a:Lcom/cgollner/systemmonitor/b;

    invoke-virtual {v0}, Lcom/cgollner/systemmonitor/b;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    iget-object v2, p0, Lcom/cgollner/systemmonitor/b$b;->a:Lcom/cgollner/systemmonitor/b;

    invoke-virtual {v2}, Lcom/cgollner/systemmonitor/b;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 387
    iget-object v0, p0, Lcom/cgollner/systemmonitor/b$b;->a:Lcom/cgollner/systemmonitor/b;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/b;->c(Lcom/cgollner/systemmonitor/b;)Lcom/cgollner/systemmonitor/NavigationDrawerFragment;

    move-result-object v1

    iget-object v0, p0, Lcom/cgollner/systemmonitor/b$b;->a:Lcom/cgollner/systemmonitor/b;

    iget-object v0, v0, Lcom/cgollner/systemmonitor/b;->b:Ljava/util/List;

    iget-object v2, p0, Lcom/cgollner/systemmonitor/b$b;->a:Lcom/cgollner/systemmonitor/b;

    invoke-static {v2}, Lcom/cgollner/systemmonitor/b;->a(Lcom/cgollner/systemmonitor/b;)Landroid/support/v4/view/ViewPager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v2

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/b$a;

    iget v0, v0, Lcom/cgollner/systemmonitor/b$a;->c:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v0, p1}, Lcom/cgollner/systemmonitor/NavigationDrawerFragment;->a(Ljava/lang/Integer;I)V

    .line 389
    :cond_0
    return-void
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 375
    iget-object v0, p0, Lcom/cgollner/systemmonitor/b$b;->a:Lcom/cgollner/systemmonitor/b;

    iget-object v0, v0, Lcom/cgollner/systemmonitor/b;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final getItem(I)Landroid/support/v4/app/Fragment;
    .locals 4

    .prologue
    .line 362
    :try_start_0
    iget-object v1, p0, Lcom/cgollner/systemmonitor/b$b;->a:Lcom/cgollner/systemmonitor/b;

    iget-object v1, v1, Lcom/cgollner/systemmonitor/b;->b:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/cgollner/systemmonitor/b$a;

    iget-object v1, v1, Lcom/cgollner/systemmonitor/b$a;->d:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v4/app/Fragment;

    .line 363
    iget-object v2, p0, Lcom/cgollner/systemmonitor/b$b;->a:Lcom/cgollner/systemmonitor/b;

    iget-object v2, v2, Lcom/cgollner/systemmonitor/b;->b:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/cgollner/systemmonitor/b$a;

    iget-object v2, v2, Lcom/cgollner/systemmonitor/b$a;->d:Ljava/lang/Class;

    const-class v3, Lcom/cgollner/systemmonitor/d/c;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 364
    move-object v0, v1

    check-cast v0, Lcom/cgollner/systemmonitor/d/c;

    move-object v2, v0

    iput-object p0, v2, Lcom/cgollner/systemmonitor/d/c;->a:Lcom/cgollner/systemmonitor/d/c$a;
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    .line 370
    :cond_0
    :goto_0
    return-object v1

    .line 367
    :catch_0
    move-exception v1

    :goto_1
    invoke-virtual {v1}, Ljava/lang/ReflectiveOperationException;->printStackTrace()V

    .line 370
    const/4 v1, 0x0

    goto :goto_0

    .line 367
    :catch_1
    move-exception v1

    goto :goto_1
.end method

.method public final getPageTitle(I)Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 379
    sget-object v1, Lcom/cgollner/systemmonitor/App;->a:Landroid/content/Context;

    iget-object v0, p0, Lcom/cgollner/systemmonitor/b$b;->a:Lcom/cgollner/systemmonitor/b;

    iget-object v0, v0, Lcom/cgollner/systemmonitor/b;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/b$a;

    iget v0, v0, Lcom/cgollner/systemmonitor/b$a;->b:I

    invoke-virtual {v1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final onPageScrollStateChanged(I)V
    .locals 1

    .prologue
    .line 427
    const/4 v0, 0x1

    sput-boolean v0, Lcom/cgollner/systemmonitor/b;->a:Z

    .line 428
    return-void
.end method

.method public final onPageScrolled(IFI)V
    .locals 1

    .prologue
    .line 393
    const/4 v0, 0x1

    sput-boolean v0, Lcom/cgollner/systemmonitor/b;->a:Z

    .line 394
    return-void
.end method

.method public final onPageSelected(I)V
    .locals 5

    .prologue
    .line 398
    iget-object v0, p0, Lcom/cgollner/systemmonitor/b$b;->a:Lcom/cgollner/systemmonitor/b;

    iget-object v0, v0, Lcom/cgollner/systemmonitor/b;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/b$a;

    .line 399
    iget v2, v0, Lcom/cgollner/systemmonitor/b$a;->a:I

    .line 400
    iget-object v1, v0, Lcom/cgollner/systemmonitor/b$a;->d:Ljava/lang/Class;

    const-class v3, Lcom/cgollner/systemmonitor/d/c;

    invoke-virtual {v1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 401
    iget-object v1, p0, Lcom/cgollner/systemmonitor/b$b;->a:Lcom/cgollner/systemmonitor/b;

    invoke-virtual {v1}, Lcom/cgollner/systemmonitor/b;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "android:switcher:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v4, Lcom/cgollner/systemmonitor/b/a$e;->viewPager:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/cgollner/systemmonitor/d/c;

    .line 404
    if-eqz v1, :cond_3

    .line 405
    invoke-virtual {v1}, Lcom/cgollner/systemmonitor/d/c;->a()I

    move-result v1

    :goto_0
    move v2, v1

    .line 421
    :cond_0
    :goto_1
    iget-object v1, p0, Lcom/cgollner/systemmonitor/b$b;->a:Lcom/cgollner/systemmonitor/b;

    iget v0, v0, Lcom/cgollner/systemmonitor/b$a;->c:I

    invoke-static {v1, v0, v2}, Lcom/cgollner/systemmonitor/b;->a(Lcom/cgollner/systemmonitor/b;II)V

    .line 422
    return-void

    .line 408
    :cond_1
    iget-object v1, v0, Lcom/cgollner/systemmonitor/b$a;->d:Ljava/lang/Class;

    const-class v3, Lcom/cgollner/systemmonitor/d/b;

    invoke-virtual {v1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 409
    iget-object v1, p0, Lcom/cgollner/systemmonitor/b$b;->a:Lcom/cgollner/systemmonitor/b;

    invoke-virtual {v1}, Lcom/cgollner/systemmonitor/b;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "android:switcher:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v4, Lcom/cgollner/systemmonitor/b/a$e;->viewPager:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/cgollner/systemmonitor/d/b;

    .line 412
    invoke-virtual {v1}, Lcom/cgollner/systemmonitor/d/b;->a()V

    goto :goto_1

    .line 414
    :cond_2
    iget-object v1, v0, Lcom/cgollner/systemmonitor/b$a;->d:Ljava/lang/Class;

    const-class v3, Lcom/cgollner/systemmonitor/d/p;

    invoke-virtual {v1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 415
    iget-object v1, p0, Lcom/cgollner/systemmonitor/b$b;->a:Lcom/cgollner/systemmonitor/b;

    invoke-virtual {v1}, Lcom/cgollner/systemmonitor/b;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "android:switcher:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v4, Lcom/cgollner/systemmonitor/b/a$e;->viewPager:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/cgollner/systemmonitor/d/p;

    .line 418
    invoke-virtual {v1}, Lcom/cgollner/systemmonitor/d/p;->a()V

    goto :goto_1

    :cond_3
    move v1, v2

    goto :goto_0
.end method
