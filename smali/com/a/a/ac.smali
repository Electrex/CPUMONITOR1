.class abstract Lcom/a/a/ac;
.super Lcom/a/a/a/y;
.source "SourceFile"


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/a/a/a/bt;I)V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/a/a/a/y;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/a/a/a/bt;I)V

    .line 44
    return-void
.end method

.method private static a(Lcom/a/a/a/bw;Lcom/a/a/ah;)Lcom/a/a/a/bw;
    .locals 7

    .prologue
    .line 81
    const-string v0, "app[identifier]"

    iget-object v1, p1, Lcom/a/a/ah;->b:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/a/a/a/bw;->c(Ljava/lang/String;Ljava/lang/String;)Lcom/a/a/a/bw;

    move-result-object v0

    const-string v1, "app[name]"

    iget-object v2, p1, Lcom/a/a/ah;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/a/a/a/bw;->c(Ljava/lang/String;Ljava/lang/String;)Lcom/a/a/a/bw;

    move-result-object v0

    const-string v1, "app[display_version]"

    iget-object v2, p1, Lcom/a/a/ah;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/a/a/a/bw;->c(Ljava/lang/String;Ljava/lang/String;)Lcom/a/a/a/bw;

    move-result-object v0

    const-string v1, "app[build_version]"

    iget-object v2, p1, Lcom/a/a/ah;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/a/a/a/bw;->c(Ljava/lang/String;Ljava/lang/String;)Lcom/a/a/a/bw;

    move-result-object v0

    const-string v1, "app[source]"

    iget v2, p1, Lcom/a/a/ah;->g:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/a/a/a/bw;->a(Ljava/lang/String;Ljava/lang/Number;)Lcom/a/a/a/bw;

    move-result-object v0

    const-string v1, "app[minimum_sdk_version]"

    iget-object v2, p1, Lcom/a/a/ah;->h:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/a/a/a/bw;->c(Ljava/lang/String;Ljava/lang/String;)Lcom/a/a/a/bw;

    move-result-object v0

    const-string v1, "app[built_sdk_version]"

    iget-object v2, p1, Lcom/a/a/ah;->i:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/a/a/a/bw;->c(Ljava/lang/String;Ljava/lang/String;)Lcom/a/a/a/bw;

    move-result-object v1

    .line 89
    iget-object v0, p1, Lcom/a/a/ah;->e:Ljava/lang/String;

    invoke-static {v0}, Lcom/a/a/a/ba;->e(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 90
    const-string v0, "app[instance_identifier]"

    iget-object v2, p1, Lcom/a/a/ah;->e:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lcom/a/a/a/bw;->c(Ljava/lang/String;Ljava/lang/String;)Lcom/a/a/a/bw;

    .line 93
    :cond_0
    iget-object v0, p1, Lcom/a/a/ah;->j:Lcom/a/a/aa;

    if-eqz v0, :cond_1

    .line 94
    const/4 v0, 0x0

    .line 97
    :try_start_0
    invoke-static {}, Lcom/a/a/a/co;->a()Lcom/a/a/a/cl;

    move-result-object v2

    iget-object v2, v2, Lcom/a/a/a/ch;->g:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget-object v3, p1, Lcom/a/a/ah;->j:Lcom/a/a/aa;

    iget v3, v3, Lcom/a/a/aa;->b:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 100
    :try_start_1
    const-string v2, "app[icon][hash]"

    iget-object v3, p1, Lcom/a/a/ah;->j:Lcom/a/a/aa;

    iget-object v3, v3, Lcom/a/a/aa;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/a/a/a/bw;->c(Ljava/lang/String;Ljava/lang/String;)Lcom/a/a/a/bw;

    move-result-object v2

    const-string v3, "app[icon][data]"

    const-string v4, "icon.png"

    const-string v5, "application/octet-stream"

    invoke-virtual {v2, v3, v4, v5, v0}, Lcom/a/a/a/bw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/io/InputStream;)Lcom/a/a/a/bw;

    move-result-object v2

    const-string v3, "app[icon][width]"

    iget-object v4, p1, Lcom/a/a/ah;->j:Lcom/a/a/aa;

    iget v4, v4, Lcom/a/a/aa;->c:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/a/a/a/bw;->a(Ljava/lang/String;Ljava/lang/Number;)Lcom/a/a/a/bw;

    move-result-object v2

    const-string v3, "app[icon][height]"

    iget-object v4, p1, Lcom/a/a/ah;->j:Lcom/a/a/aa;

    iget v4, v4, Lcom/a/a/aa;->d:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/a/a/a/bw;->a(Ljava/lang/String;Ljava/lang/Number;)Lcom/a/a/a/bw;
    :try_end_1
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 109
    invoke-static {v0}, Lcom/a/a/a/ba;->a(Ljava/io/Closeable;)V

    .line 113
    :cond_1
    :goto_0
    return-object v1

    .line 105
    :catch_0
    move-exception v2

    :try_start_2
    invoke-static {}, Lcom/a/a/a/co;->a()Lcom/a/a/a/cl;

    move-result-object v2

    invoke-virtual {v2}, Lcom/a/a/a/cl;->a()Lcom/a/a/a/ci;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Failed to find app icon with resource ID: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p1, Lcom/a/a/ah;->j:Lcom/a/a/aa;

    iget v4, v4, Lcom/a/a/aa;->b:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-interface {v2}, Lcom/a/a/a/ci;->a()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 109
    invoke-static {v0}, Lcom/a/a/a/ba;->a(Ljava/io/Closeable;)V

    goto :goto_0

    :catchall_0
    move-exception v1

    move-object v6, v1

    move-object v1, v0

    move-object v0, v6

    :goto_1
    invoke-static {v1}, Lcom/a/a/a/ba;->a(Ljava/io/Closeable;)V

    throw v0

    :catchall_1
    move-exception v1

    move-object v6, v1

    move-object v1, v0

    move-object v0, v6

    goto :goto_1
.end method


# virtual methods
.method public final a(Lcom/a/a/ah;)Z
    .locals 5

    .prologue
    .line 48
    invoke-virtual {p0}, Lcom/a/a/ac;->a()Lcom/a/a/a/bw;

    move-result-object v0

    .line 49
    const-string v1, "X-CRASHLYTICS-API-KEY"

    iget-object v2, p1, Lcom/a/a/ah;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/a/a/a/bw;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/a/a/a/bw;

    move-result-object v0

    const-string v1, "X-CRASHLYTICS-API-CLIENT-TYPE"

    const-string v2, "android"

    invoke-virtual {v0, v1, v2}, Lcom/a/a/a/bw;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/a/a/a/bw;

    move-result-object v0

    const-string v1, "X-CRASHLYTICS-API-CLIENT-VERSION"

    invoke-static {}, Lcom/a/a/a/co;->a()Lcom/a/a/a/cl;

    const-string v2, "1.1.13.29"

    invoke-virtual {v0, v1, v2}, Lcom/a/a/a/bw;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/a/a/a/bw;

    move-result-object v0

    .line 50
    invoke-static {v0, p1}, Lcom/a/a/ac;->a(Lcom/a/a/a/bw;Lcom/a/a/ah;)Lcom/a/a/a/bw;

    move-result-object v1

    .line 52
    invoke-static {}, Lcom/a/a/a/co;->a()Lcom/a/a/a/cl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/a/a/cl;->a()Lcom/a/a/a/ci;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Sending app info to "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/a/a/a/y;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {v0}, Lcom/a/a/a/ci;->b()V

    .line 53
    iget-object v0, p1, Lcom/a/a/ah;->j:Lcom/a/a/aa;

    if-eqz v0, :cond_0

    .line 54
    invoke-static {}, Lcom/a/a/a/co;->a()Lcom/a/a/a/cl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/a/a/cl;->a()Lcom/a/a/a/ci;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "App icon hash is "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p1, Lcom/a/a/ah;->j:Lcom/a/a/aa;

    iget-object v3, v3, Lcom/a/a/aa;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {v0}, Lcom/a/a/a/ci;->b()V

    .line 56
    invoke-static {}, Lcom/a/a/a/co;->a()Lcom/a/a/a/cl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/a/a/cl;->a()Lcom/a/a/a/ci;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "App icon size is "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p1, Lcom/a/a/ah;->j:Lcom/a/a/aa;

    iget v3, v3, Lcom/a/a/aa;->c:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "x"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p1, Lcom/a/a/ah;->j:Lcom/a/a/aa;

    iget v3, v3, Lcom/a/a/aa;->d:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-interface {v0}, Lcom/a/a/a/ci;->b()V

    .line 60
    :cond_0
    invoke-virtual {v1}, Lcom/a/a/a/bw;->b()I

    move-result v2

    .line 61
    const-string v0, "POST"

    invoke-virtual {v1}, Lcom/a/a/a/bw;->a()Ljava/net/HttpURLConnection;

    move-result-object v3

    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->getRequestMethod()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "Create"

    .line 63
    :goto_0
    invoke-static {}, Lcom/a/a/a/co;->a()Lcom/a/a/a/cl;

    move-result-object v3

    invoke-virtual {v3}, Lcom/a/a/a/cl;->a()Lcom/a/a/a/ci;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " app request ID: "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "X-REQUEST-ID"

    invoke-virtual {v1, v4}, Lcom/a/a/a/bw;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {v3}, Lcom/a/a/a/ci;->b()V

    .line 65
    invoke-static {}, Lcom/a/a/a/co;->a()Lcom/a/a/a/cl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/a/a/cl;->a()Lcom/a/a/a/ci;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/a/a/ci;->b()V

    .line 67
    invoke-static {v2}, Lcom/a/a/a/cj;->a(I)I

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    return v0

    .line 61
    :cond_1
    const-string v0, "Update"

    goto :goto_0

    .line 67
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method
