.class final Lcom/a/a/az;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Thread$UncaughtExceptionHandler;


# static fields
.field static final a:Ljava/io/FilenameFilter;

.field private static c:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field

.field private static d:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field

.field private static final e:Ljava/util/regex/Pattern;

.field private static final f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final g:Lcom/a/a/aj;


# instance fields
.field final b:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final h:Ljava/util/concurrent/atomic/AtomicInteger;

.field private final i:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final j:I

.field private final k:Ljava/lang/Thread$UncaughtExceptionHandler;

.field private final l:Ljava/io/File;

.field private final m:Ljava/io/File;

.field private final n:Ljava/lang/String;

.field private final o:Landroid/content/BroadcastReceiver;

.field private final p:Landroid/content/BroadcastReceiver;

.field private final q:Lcom/a/a/aj;

.field private final r:Lcom/a/a/aj;

.field private final s:Ljava/util/concurrent/ExecutorService;

.field private t:Landroid/app/ActivityManager$RunningAppProcessInfo;

.field private u:Lcom/a/a/a/bo;

.field private v:Z

.field private w:[Ljava/lang/Thread;

.field private x:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<[",
            "Ljava/lang/StackTraceElement;",
            ">;"
        }
    .end annotation
.end field

.field private y:[Ljava/lang/StackTraceElement;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 124
    new-instance v0, Lcom/a/a/ba;

    invoke-direct {v0}, Lcom/a/a/ba;-><init>()V

    sput-object v0, Lcom/a/a/az;->a:Ljava/io/FilenameFilter;

    .line 132
    new-instance v0, Lcom/a/a/j;

    invoke-direct {v0}, Lcom/a/a/j;-><init>()V

    sput-object v0, Lcom/a/a/az;->c:Ljava/util/Comparator;

    .line 139
    new-instance v0, Lcom/a/a/l;

    invoke-direct {v0}, Lcom/a/a/l;-><init>()V

    sput-object v0, Lcom/a/a/az;->d:Ljava/util/Comparator;

    .line 149
    new-instance v0, Lcom/a/a/m;

    invoke-direct {v0}, Lcom/a/a/m;-><init>()V

    .line 156
    const-string v0, "([\\d|A-Z|a-z]{12}\\-[\\d|A-Z|a-z]{4}\\-[\\d|A-Z|a-z]{4}\\-[\\d|A-Z|a-z]{12}).+"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/a/a/az;->e:Ljava/util/regex/Pattern;

    .line 159
    const-string v0, "X-CRASHLYTICS-SEND-FLAGS"

    const-string v1, "1"

    invoke-static {v0, v1}, Ljava/util/Collections;->singletonMap(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lcom/a/a/az;->f:Ljava/util/Map;

    .line 181
    const-string v0, "0"

    invoke-static {v0}, Lcom/a/a/aj;->a(Ljava/lang/String;)Lcom/a/a/aj;

    move-result-object v0

    sput-object v0, Lcom/a/a/az;->g:Lcom/a/a/aj;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Thread$UncaughtExceptionHandler;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 236
    const-string v0, "Crashlytics Exception Handler"

    invoke-static {v0}, Lcom/a/a/a/bf;->b(Ljava/lang/String;)Ljava/util/concurrent/ThreadFactory;

    move-result-object v1

    invoke-static {v1}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor(Ljava/util/concurrent/ThreadFactory;)Ljava/util/concurrent/ExecutorService;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/a/a/a/bf;->a(Ljava/lang/String;Ljava/util/concurrent/ExecutorService;)V

    invoke-direct {p0, p1, v1, p2}, Lcom/a/a/az;-><init>(Ljava/lang/Thread$UncaughtExceptionHandler;Ljava/util/concurrent/ExecutorService;Ljava/lang/String;)V

    .line 238
    return-void
.end method

.method private constructor <init>(Ljava/lang/Thread$UncaughtExceptionHandler;Ljava/util/concurrent/ExecutorService;Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 244
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 192
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0, v4}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lcom/a/a/az;->h:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 193
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v4}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/a/a/az;->i:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 245
    iput-object p1, p0, Lcom/a/a/az;->k:Ljava/lang/Thread$UncaughtExceptionHandler;

    .line 246
    iput-object p2, p0, Lcom/a/a/az;->s:Ljava/util/concurrent/ExecutorService;

    .line 247
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v4}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/a/a/az;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 248
    invoke-static {}, Lcom/a/a/a/co;->a()Lcom/a/a/a/cl;

    move-result-object v0

    iget-object v0, v0, Lcom/a/a/a/cl;->c:Ljava/io/File;

    iput-object v0, p0, Lcom/a/a/az;->l:Ljava/io/File;

    .line 249
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/a/a/az;->l:Ljava/io/File;

    const-string v2, "initialization_marker"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/a/a/az;->m:Ljava/io/File;

    .line 250
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "Crashlytics Android SDK/%s"

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {}, Lcom/a/a/d;->a()Lcom/a/a/d;

    invoke-static {}, Lcom/a/a/d;->c()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/a/a/az;->n:Ljava/lang/String;

    .line 257
    const/16 v0, 0x8

    iput v0, p0, Lcom/a/a/az;->j:I

    .line 259
    invoke-static {}, Lcom/a/a/a/co;->a()Lcom/a/a/a/cl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/a/a/cl;->a()Lcom/a/a/a/ci;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/a/a/ci;->b()V

    new-instance v0, Ljava/io/File;

    invoke-static {}, Lcom/a/a/a/co;->a()Lcom/a/a/a/cl;

    move-result-object v1

    iget-object v1, v1, Lcom/a/a/a/cl;->c:Ljava/io/File;

    const-string v2, "crash_marker"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 261
    :cond_0
    invoke-static {}, Lcom/a/a/d;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/a/a/aj;->a(Ljava/lang/String;)Lcom/a/a/aj;

    move-result-object v0

    iput-object v0, p0, Lcom/a/a/az;->q:Lcom/a/a/aj;

    .line 262
    if-nez p3, :cond_1

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lcom/a/a/az;->r:Lcom/a/a/aj;

    .line 264
    new-instance v0, Lcom/a/a/n;

    invoke-direct {v0, p0}, Lcom/a/a/n;-><init>(Lcom/a/a/az;)V

    iput-object v0, p0, Lcom/a/a/az;->p:Landroid/content/BroadcastReceiver;

    .line 271
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.ACTION_POWER_CONNECTED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 273
    new-instance v1, Lcom/a/a/o;

    invoke-direct {v1, p0}, Lcom/a/a/o;-><init>(Lcom/a/a/az;)V

    iput-object v1, p0, Lcom/a/a/az;->o:Landroid/content/BroadcastReceiver;

    .line 280
    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "android.intent.action.ACTION_POWER_DISCONNECTED"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 282
    invoke-static {}, Lcom/a/a/d;->a()Lcom/a/a/d;

    move-result-object v2

    iget-object v2, v2, Lcom/a/a/a/ch;->g:Landroid/content/Context;

    iget-object v3, p0, Lcom/a/a/az;->p:Landroid/content/BroadcastReceiver;

    invoke-virtual {v2, v3, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 283
    invoke-static {}, Lcom/a/a/d;->a()Lcom/a/a/d;

    move-result-object v0

    iget-object v0, v0, Lcom/a/a/a/ch;->g:Landroid/content/Context;

    iget-object v2, p0, Lcom/a/a/az;->o:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v2, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 284
    iget-object v0, p0, Lcom/a/a/az;->i:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v5}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 285
    return-void

    .line 262
    :cond_1
    const-string v0, "-"

    const-string v1, ""

    invoke-virtual {p3, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/a/a/aj;->a(Ljava/lang/String;)Lcom/a/a/aj;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(IIJJ)I
    .locals 2

    .prologue
    .line 1775
    invoke-static {}, Lcom/a/a/an;->a()I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1778
    invoke-static {p0}, Lcom/a/a/an;->c(I)I

    move-result v1

    add-int/2addr v0, v1

    .line 1779
    const/4 v1, 0x3

    invoke-static {v1}, Lcom/a/a/an;->b(I)I

    move-result v1

    add-int/2addr v0, v1

    .line 1780
    const/4 v1, 0x4

    invoke-static {v1, p1}, Lcom/a/a/an;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1781
    const/4 v1, 0x5

    invoke-static {v1, p2, p3}, Lcom/a/a/an;->b(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 1782
    const/4 v1, 0x6

    invoke-static {v1, p4, p5}, Lcom/a/a/an;->b(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 1784
    return v0
.end method

.method private static a(ILcom/a/a/aj;Lcom/a/a/aj;IJJLjava/util/Map;ILcom/a/a/aj;Lcom/a/a/aj;)I
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/a/a/aj;",
            "Lcom/a/a/aj;",
            "IJJ",
            "Ljava/util/Map",
            "<",
            "Lcom/a/a/a/bn;",
            "Ljava/lang/String;",
            ">;I",
            "Lcom/a/a/aj;",
            "Lcom/a/a/aj;",
            ")I"
        }
    .end annotation

    .prologue
    .line 1292
    const/4 v1, 0x1

    invoke-static {v1, p1}, Lcom/a/a/an;->b(ILcom/a/a/aj;)I

    move-result v1

    add-int/lit8 v1, v1, 0x0

    .line 1295
    const/4 v2, 0x3

    invoke-static {v2, p0}, Lcom/a/a/an;->d(II)I

    move-result v2

    add-int/2addr v2, v1

    .line 1296
    if-nez p2, :cond_0

    const/4 v1, 0x0

    :goto_0
    add-int/2addr v1, v2

    .line 1297
    const/4 v2, 0x5

    invoke-static {v2, p3}, Lcom/a/a/an;->c(II)I

    move-result v2

    add-int/2addr v1, v2

    .line 1298
    const/4 v2, 0x6

    invoke-static {v2, p4, p5}, Lcom/a/a/an;->b(IJ)I

    move-result v2

    add-int/2addr v1, v2

    .line 1299
    const/4 v2, 0x7

    invoke-static {v2, p6, p7}, Lcom/a/a/an;->b(IJ)I

    move-result v2

    add-int/2addr v1, v2

    .line 1300
    const/16 v2, 0xa

    invoke-static {v2}, Lcom/a/a/an;->b(I)I

    move-result v2

    add-int/2addr v1, v2

    .line 1301
    if-eqz p8, :cond_1

    .line 1302
    invoke-interface {p8}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v3, v1

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Ljava/util/Map$Entry;

    .line 1303
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/a/a/a/bn;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/a/a/az;->a(Lcom/a/a/a/bn;Ljava/lang/String;)I

    move-result v1

    .line 1304
    const/16 v2, 0xb

    invoke-static {v2}, Lcom/a/a/an;->e(I)I

    move-result v2

    invoke-static {v1}, Lcom/a/a/an;->g(I)I

    move-result v5

    add-int/2addr v2, v5

    add-int/2addr v1, v2

    add-int/2addr v1, v3

    move v3, v1

    .line 1306
    goto :goto_1

    .line 1296
    :cond_0
    const/4 v1, 0x4

    invoke-static {v1, p2}, Lcom/a/a/an;->b(ILcom/a/a/aj;)I

    move-result v1

    goto :goto_0

    :cond_1
    move v3, v1

    .line 1308
    :cond_2
    const/16 v1, 0xc

    invoke-static {v1, p9}, Lcom/a/a/an;->c(II)I

    move-result v1

    add-int v2, v3, v1

    .line 1309
    if-nez p10, :cond_3

    const/4 v1, 0x0

    :goto_2
    add-int/2addr v2, v1

    .line 1310
    if-nez p11, :cond_4

    const/4 v1, 0x0

    :goto_3
    add-int/2addr v1, v2

    .line 1312
    return v1

    .line 1309
    :cond_3
    const/16 v1, 0xd

    move-object/from16 v0, p10

    invoke-static {v1, v0}, Lcom/a/a/an;->b(ILcom/a/a/aj;)I

    move-result v1

    goto :goto_2

    .line 1310
    :cond_4
    const/16 v1, 0xe

    move-object/from16 v0, p11

    invoke-static {v1, v0}, Lcom/a/a/an;->b(ILcom/a/a/aj;)I

    move-result v1

    goto :goto_3
.end method

.method private static a(Lcom/a/a/a/bn;Ljava/lang/String;)I
    .locals 3

    .prologue
    .line 1283
    const/4 v0, 0x1

    iget v1, p0, Lcom/a/a/a/bn;->f:I

    invoke-static {v0, v1}, Lcom/a/a/an;->d(II)I

    move-result v0

    .line 1284
    const/4 v1, 0x2

    invoke-static {p1}, Lcom/a/a/aj;->a(Ljava/lang/String;)Lcom/a/a/aj;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/a/a/an;->b(ILcom/a/a/aj;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1286
    return v0
.end method

.method private static a(Ljava/lang/StackTraceElement;Z)I
    .locals 6

    .prologue
    const/4 v1, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1608
    invoke-virtual {p0}, Ljava/lang/StackTraceElement;->isNativeMethod()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1612
    invoke-virtual {p0}, Ljava/lang/StackTraceElement;->getLineNumber()I

    move-result v0

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    int-to-long v4, v0

    invoke-static {v3, v4, v5}, Lcom/a/a/an;->b(IJ)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1617
    :goto_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/StackTraceElement;->getClassName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/a/a/aj;->a(Ljava/lang/String;)Lcom/a/a/aj;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/a/a/an;->b(ILcom/a/a/aj;)I

    move-result v3

    add-int/2addr v0, v3

    .line 1621
    invoke-virtual {p0}, Ljava/lang/StackTraceElement;->getFileName()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 1622
    const/4 v3, 0x3

    invoke-virtual {p0}, Ljava/lang/StackTraceElement;->getFileName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/a/a/aj;->a(Ljava/lang/String;)Lcom/a/a/aj;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/a/a/an;->b(ILcom/a/a/aj;)I

    move-result v3

    add-int/2addr v0, v3

    .line 1625
    :cond_0
    invoke-virtual {p0}, Ljava/lang/StackTraceElement;->isNativeMethod()Z

    move-result v3

    if-nez v3, :cond_3

    .line 1626
    invoke-virtual {p0}, Ljava/lang/StackTraceElement;->getLineNumber()I

    move-result v3

    if-lez v3, :cond_3

    .line 1628
    const/4 v3, 0x4

    invoke-virtual {p0}, Ljava/lang/StackTraceElement;->getLineNumber()I

    move-result v4

    int-to-long v4, v4

    invoke-static {v3, v4, v5}, Lcom/a/a/an;->b(IJ)I

    move-result v3

    add-int/2addr v0, v3

    move v3, v0

    .line 1631
    :goto_1
    const/4 v4, 0x5

    if-eqz p1, :cond_2

    move v0, v1

    :goto_2
    invoke-static {v4, v0}, Lcom/a/a/an;->c(II)I

    move-result v0

    add-int/2addr v0, v3

    .line 1633
    return v0

    .line 1614
    :cond_1
    const-wide/16 v4, 0x0

    invoke-static {v3, v4, v5}, Lcom/a/a/an;->b(IJ)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    goto :goto_0

    :cond_2
    move v0, v2

    .line 1631
    goto :goto_2

    :cond_3
    move v3, v0

    goto :goto_1
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;)I
    .locals 3

    .prologue
    .line 1768
    const/4 v0, 0x1

    invoke-static {p0}, Lcom/a/a/aj;->a(Ljava/lang/String;)Lcom/a/a/aj;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/a/a/an;->b(ILcom/a/a/aj;)I

    move-result v0

    .line 1769
    const/4 v1, 0x2

    if-nez p1, :cond_0

    const-string p1, ""

    :cond_0
    invoke-static {p1}, Lcom/a/a/aj;->a(Ljava/lang/String;)Lcom/a/a/aj;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/a/a/an;->b(ILcom/a/a/aj;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1770
    return v0
.end method

.method private a(Ljava/lang/Thread;Ljava/lang/Throwable;)I
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v2, 0x0

    const/4 v7, 0x1

    .line 1740
    iget-object v0, p0, Lcom/a/a/az;->y:[Ljava/lang/StackTraceElement;

    const/4 v1, 0x4

    invoke-static {p1, v0, v1, v7}, Lcom/a/a/az;->a(Ljava/lang/Thread;[Ljava/lang/StackTraceElement;IZ)I

    move-result v0

    .line 1743
    invoke-static {v7}, Lcom/a/a/an;->e(I)I

    move-result v1

    invoke-static {v0}, Lcom/a/a/an;->g(I)I

    move-result v3

    add-int/2addr v1, v3

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    .line 1745
    iget-object v1, p0, Lcom/a/a/az;->w:[Ljava/lang/Thread;

    array-length v4, v1

    move v1, v2

    move v3, v0

    .line 1746
    :goto_0
    if-ge v1, v4, :cond_0

    .line 1747
    iget-object v0, p0, Lcom/a/a/az;->w:[Ljava/lang/Thread;

    aget-object v5, v0, v1

    .line 1748
    iget-object v0, p0, Lcom/a/a/az;->x:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/StackTraceElement;

    invoke-static {v5, v0, v2, v2}, Lcom/a/a/az;->a(Ljava/lang/Thread;[Ljava/lang/StackTraceElement;IZ)I

    move-result v0

    .line 1749
    invoke-static {v7}, Lcom/a/a/an;->e(I)I

    move-result v5

    invoke-static {v0}, Lcom/a/a/an;->g(I)I

    move-result v6

    add-int/2addr v5, v6

    add-int/2addr v0, v5

    add-int/2addr v3, v0

    .line 1746
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1753
    :cond_0
    invoke-direct {p0, p2, v7}, Lcom/a/a/az;->a(Ljava/lang/Throwable;I)I

    move-result v0

    .line 1754
    const/4 v1, 0x2

    invoke-static {v1}, Lcom/a/a/an;->e(I)I

    move-result v1

    invoke-static {v0}, Lcom/a/a/an;->g(I)I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    add-int/2addr v0, v3

    .line 1757
    invoke-static {}, Lcom/a/a/az;->j()I

    move-result v1

    .line 1758
    invoke-static {v8}, Lcom/a/a/an;->e(I)I

    move-result v2

    invoke-static {v1}, Lcom/a/a/an;->g(I)I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 1760
    invoke-direct {p0}, Lcom/a/a/az;->i()I

    move-result v1

    .line 1761
    invoke-static {v8}, Lcom/a/a/an;->e(I)I

    move-result v2

    invoke-static {v1}, Lcom/a/a/an;->g(I)I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 1764
    return v0
.end method

.method private a(Ljava/lang/Thread;Ljava/lang/Throwable;Ljava/util/Map;)I
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Thread;",
            "Ljava/lang/Throwable;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 1714
    invoke-direct {p0, p1, p2}, Lcom/a/a/az;->a(Ljava/lang/Thread;Ljava/lang/Throwable;)I

    move-result v0

    .line 1717
    const/4 v1, 0x1

    invoke-static {v1}, Lcom/a/a/an;->e(I)I

    move-result v1

    invoke-static {v0}, Lcom/a/a/an;->g(I)I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    .line 1720
    if-eqz p3, :cond_0

    .line 1721
    invoke-interface {p3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v2, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Ljava/util/Map$Entry;

    .line 1722
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/a/a/az;->a(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 1723
    const/4 v1, 0x2

    invoke-static {v1}, Lcom/a/a/an;->e(I)I

    move-result v1

    invoke-static {v0}, Lcom/a/a/an;->g(I)I

    move-result v4

    add-int/2addr v1, v4

    add-int/2addr v0, v1

    add-int/2addr v0, v2

    move v2, v0

    .line 1725
    goto :goto_0

    :cond_0
    move v2, v0

    .line 1729
    :cond_1
    iget-object v0, p0, Lcom/a/a/az;->t:Landroid/app/ActivityManager$RunningAppProcessInfo;

    if-eqz v0, :cond_2

    .line 1730
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/a/a/az;->t:Landroid/app/ActivityManager$RunningAppProcessInfo;

    iget v1, v1, Landroid/app/ActivityManager$RunningAppProcessInfo;->importance:I

    invoke-static {v0}, Lcom/a/a/an;->b(I)I

    move-result v0

    add-int/2addr v2, v0

    .line 1733
    :cond_2
    const/4 v0, 0x4

    invoke-static {}, Lcom/a/a/d;->a()Lcom/a/a/d;

    move-result-object v1

    iget-object v1, v1, Lcom/a/a/a/ch;->g:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    invoke-static {v0, v1}, Lcom/a/a/an;->c(II)I

    move-result v0

    add-int/2addr v0, v2

    .line 1736
    return v0
.end method

.method private static a(Ljava/lang/Thread;[Ljava/lang/StackTraceElement;IZ)I
    .locals 6

    .prologue
    .line 1582
    const/4 v0, 0x1

    invoke-virtual {p0}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/a/a/aj;->a(Ljava/lang/String;)Lcom/a/a/aj;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/a/a/an;->b(ILcom/a/a/aj;)I

    move-result v0

    .line 1583
    const/4 v1, 0x2

    invoke-static {v1, p2}, Lcom/a/a/an;->c(II)I

    move-result v1

    add-int/2addr v1, v0

    .line 1585
    array-length v2, p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, p1, v0

    .line 1586
    invoke-static {v3, p3}, Lcom/a/a/az;->a(Ljava/lang/StackTraceElement;Z)I

    move-result v3

    .line 1587
    const/4 v4, 0x3

    invoke-static {v4}, Lcom/a/a/an;->e(I)I

    move-result v4

    invoke-static {v3}, Lcom/a/a/an;->g(I)I

    move-result v5

    add-int/2addr v4, v5

    add-int/2addr v3, v4

    add-int/2addr v1, v3

    .line 1585
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1591
    :cond_0
    return v1
.end method

.method private a(Ljava/lang/Throwable;I)I
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v1, 0x0

    .line 1793
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/a/a/aj;->a(Ljava/lang/String;)Lcom/a/a/aj;

    move-result-object v0

    invoke-static {v8, v0}, Lcom/a/a/an;->b(ILcom/a/a/aj;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1798
    invoke-virtual {p1}, Ljava/lang/Throwable;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    .line 1799
    if-eqz v2, :cond_0

    .line 1800
    const/4 v3, 0x3

    invoke-static {v2}, Lcom/a/a/aj;->a(Ljava/lang/String;)Lcom/a/a/aj;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/a/a/an;->b(ILcom/a/a/aj;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1803
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Throwable;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v4

    array-length v5, v4

    move v2, v1

    :goto_0
    if-ge v2, v5, :cond_1

    aget-object v3, v4, v2

    .line 1804
    invoke-static {v3, v8}, Lcom/a/a/az;->a(Ljava/lang/StackTraceElement;Z)I

    move-result v3

    .line 1805
    const/4 v6, 0x4

    invoke-static {v6}, Lcom/a/a/an;->e(I)I

    move-result v6

    invoke-static {v3}, Lcom/a/a/an;->g(I)I

    move-result v7

    add-int/2addr v6, v7

    add-int/2addr v3, v6

    add-int/2addr v3, v0

    .line 1803
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v0, v3

    goto :goto_0

    .line 1811
    :cond_1
    invoke-virtual {p1}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v2

    .line 1812
    if-eqz v2, :cond_2

    .line 1813
    const/16 v3, 0x8

    if-ge p2, v3, :cond_3

    .line 1814
    add-int/lit8 v1, p2, 0x1

    invoke-direct {p0, v2, v1}, Lcom/a/a/az;->a(Ljava/lang/Throwable;I)I

    move-result v1

    .line 1815
    const/4 v2, 0x6

    invoke-static {v2}, Lcom/a/a/an;->e(I)I

    move-result v2

    invoke-static {v1}, Lcom/a/a/an;->g(I)I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 1830
    :cond_2
    :goto_1
    return v0

    .line 1821
    :cond_3
    :goto_2
    if-eqz v2, :cond_4

    .line 1822
    invoke-virtual {v2}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v2

    .line 1823
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1826
    :cond_4
    const/4 v2, 0x7

    invoke-static {v2, v1}, Lcom/a/a/an;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_1
.end method

.method private static a(Lcom/a/a/a/bo;)Lcom/a/a/aj;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 402
    if-nez p0, :cond_0

    .line 403
    const/4 v0, 0x0

    .line 429
    :goto_0
    return-object v0

    .line 409
    :cond_0
    const/4 v0, 0x1

    new-array v0, v0, [I

    aput v3, v0, v3

    .line 411
    invoke-virtual {p0}, Lcom/a/a/a/bo;->a()I

    move-result v1

    new-array v1, v1, [B

    .line 414
    :try_start_0
    new-instance v2, Lcom/a/a/bb;

    invoke-direct {v2, v1, v0}, Lcom/a/a/bb;-><init>([B[I)V

    invoke-virtual {p0, v2}, Lcom/a/a/a/bo;->a(Lcom/a/a/a/bs;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 429
    :goto_1
    aget v0, v0, v3

    invoke-static {v1, v0}, Lcom/a/a/aj;->a([BI)Lcom/a/a/aj;

    move-result-object v0

    goto :goto_0

    .line 426
    :catch_0
    move-exception v2

    invoke-static {}, Lcom/a/a/a/co;->a()Lcom/a/a/a/cl;

    move-result-object v2

    invoke-virtual {v2}, Lcom/a/a/a/cl;->a()Lcom/a/a/a/ci;

    move-result-object v2

    invoke-interface {v2}, Lcom/a/a/a/ci;->a()V

    goto :goto_1
.end method

.method static a(Ljava/io/File;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 786
    invoke-virtual {p0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    const/16 v2, 0x23

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/a/a/az;)Ljava/util/concurrent/atomic/AtomicBoolean;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/a/a/az;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-object v0
.end method

.method private static a(Lcom/a/a/an;ILjava/lang/StackTraceElement;Z)V
    .locals 6

    .prologue
    const/4 v0, 0x4

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 1638
    invoke-virtual {p0, p1, v5}, Lcom/a/a/an;->e(II)V

    .line 1639
    invoke-static {p2, p3}, Lcom/a/a/az;->a(Ljava/lang/StackTraceElement;Z)I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/a/a/an;->f(I)V

    .line 1641
    invoke-virtual {p2}, Ljava/lang/StackTraceElement;->isNativeMethod()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1644
    invoke-virtual {p2}, Ljava/lang/StackTraceElement;->getLineNumber()I

    move-result v2

    invoke-static {v2, v1}, Ljava/lang/Math;->max(II)I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {p0, v4, v2, v3}, Lcom/a/a/an;->a(IJ)V

    .line 1649
    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2}, Ljava/lang/StackTraceElement;->getClassName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p2}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/a/a/aj;->a(Ljava/lang/String;)Lcom/a/a/aj;

    move-result-object v2

    invoke-virtual {p0, v5, v2}, Lcom/a/a/an;->a(ILcom/a/a/aj;)V

    .line 1651
    invoke-virtual {p2}, Ljava/lang/StackTraceElement;->getFileName()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 1652
    const/4 v2, 0x3

    invoke-virtual {p2}, Ljava/lang/StackTraceElement;->getFileName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/a/a/aj;->a(Ljava/lang/String;)Lcom/a/a/aj;

    move-result-object v3

    invoke-virtual {p0, v2, v3}, Lcom/a/a/an;->a(ILcom/a/a/aj;)V

    .line 1655
    :cond_0
    invoke-virtual {p2}, Ljava/lang/StackTraceElement;->isNativeMethod()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1658
    invoke-virtual {p2}, Ljava/lang/StackTraceElement;->getLineNumber()I

    move-result v2

    if-lez v2, :cond_1

    .line 1659
    invoke-virtual {p2}, Ljava/lang/StackTraceElement;->getLineNumber()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {p0, v0, v2, v3}, Lcom/a/a/an;->a(IJ)V

    .line 1666
    :cond_1
    const/4 v2, 0x5

    if-eqz p3, :cond_3

    :goto_1
    invoke-virtual {p0, v2, v0}, Lcom/a/a/an;->a(II)V

    .line 1667
    return-void

    .line 1646
    :cond_2
    const-wide/16 v2, 0x0

    invoke-virtual {p0, v4, v2, v3}, Lcom/a/a/an;->a(IJ)V

    goto :goto_0

    :cond_3
    move v0, v1

    .line 1666
    goto :goto_1
.end method

.method private static a(Lcom/a/a/an;Ljava/io/File;)V
    .locals 4

    .prologue
    .line 1319
    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1321
    invoke-virtual {p1}, Ljava/io/File;->length()J

    move-result-wide v0

    .line 1322
    long-to-int v0, v0

    new-array v3, v0, [B

    .line 1324
    const/4 v2, 0x0

    .line 1327
    :try_start_0
    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1329
    const/4 v0, 0x0

    .line 1330
    :goto_0
    :try_start_1
    array-length v2, v3

    if-ge v0, v2, :cond_0

    array-length v2, v3

    sub-int/2addr v2, v0

    invoke-virtual {v1, v3, v0, v2}, Ljava/io/FileInputStream;->read([BII)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v2

    if-ltz v2, :cond_0

    .line 1333
    add-int/2addr v0, v2

    goto :goto_0

    .line 1336
    :cond_0
    invoke-static {v1}, Lcom/a/a/a/ba;->a(Ljava/io/Closeable;)V

    .line 1339
    invoke-virtual {p0, v3}, Lcom/a/a/an;->a([B)V

    .line 1343
    :goto_1
    return-void

    .line 1336
    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_2
    invoke-static {v1}, Lcom/a/a/a/ba;->a(Ljava/io/Closeable;)V

    throw v0

    .line 1341
    :cond_1
    invoke-static {}, Lcom/a/a/a/co;->a()Lcom/a/a/a/cl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/a/a/cl;->a()Lcom/a/a/a/ci;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Tried to include a file that doesn\'t exist: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {v0}, Lcom/a/a/a/ci;->a()V

    goto :goto_1

    .line 1336
    :catchall_1
    move-exception v0

    goto :goto_2
.end method

.method private a(Lcom/a/a/an;Ljava/lang/String;)V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v1, 0x0

    .line 895
    new-array v2, v8, [Ljava/lang/String;

    const-string v0, "SessionUser"

    aput-object v0, v2, v1

    const/4 v0, 0x1

    const-string v3, "SessionApp"

    aput-object v3, v2, v0

    const/4 v0, 0x2

    const-string v3, "SessionOS"

    aput-object v3, v2, v0

    const/4 v0, 0x3

    const-string v3, "SessionDevice"

    aput-object v3, v2, v0

    move v0, v1

    .line 897
    :goto_0
    if-ge v0, v8, :cond_1

    aget-object v3, v2, v0

    .line 898
    new-instance v4, Lcom/a/a/t;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/a/a/t;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v4}, Lcom/a/a/az;->a(Ljava/io/FilenameFilter;)[Ljava/io/File;

    move-result-object v4

    .line 900
    array-length v5, v4

    if-nez v5, :cond_0

    .line 901
    invoke-static {}, Lcom/a/a/a/co;->a()Lcom/a/a/a/cl;

    move-result-object v4

    invoke-virtual {v4}, Lcom/a/a/a/cl;->a()Lcom/a/a/a/ci;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Can\'t find "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " data for session ID "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {v4}, Lcom/a/a/a/ci;->a()V

    .line 897
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 903
    :cond_0
    invoke-static {}, Lcom/a/a/a/co;->a()Lcom/a/a/a/cl;

    move-result-object v5

    invoke-virtual {v5}, Lcom/a/a/a/cl;->a()Lcom/a/a/a/ci;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Collecting "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, " data for session ID "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {v5}, Lcom/a/a/a/ci;->b()V

    .line 904
    aget-object v3, v4, v1

    invoke-static {p1, v3}, Lcom/a/a/az;->a(Lcom/a/a/an;Ljava/io/File;)V

    goto :goto_1

    .line 907
    :cond_1
    return-void
.end method

.method private a(Lcom/a/a/an;Ljava/lang/Thread;Ljava/lang/Throwable;)V
    .locals 12

    .prologue
    const/4 v11, 0x4

    const/4 v10, 0x3

    const/4 v6, 0x0

    const/4 v1, 0x2

    const/4 v3, 0x1

    .line 1491
    invoke-virtual {p1, v3, v1}, Lcom/a/a/an;->e(II)V

    .line 1492
    invoke-direct {p0, p2, p3}, Lcom/a/a/az;->a(Ljava/lang/Thread;Ljava/lang/Throwable;)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/a/a/an;->f(I)V

    .line 1494
    iget-object v0, p0, Lcom/a/a/az;->y:[Ljava/lang/StackTraceElement;

    invoke-static {p1, p2, v0, v11, v3}, Lcom/a/a/az;->a(Lcom/a/a/an;Ljava/lang/Thread;[Ljava/lang/StackTraceElement;IZ)V

    .line 1498
    iget-object v0, p0, Lcom/a/a/az;->w:[Ljava/lang/Thread;

    array-length v4, v0

    move v2, v6

    .line 1499
    :goto_0
    if-ge v2, v4, :cond_0

    .line 1500
    iget-object v0, p0, Lcom/a/a/az;->w:[Ljava/lang/Thread;

    aget-object v5, v0, v2

    .line 1501
    iget-object v0, p0, Lcom/a/a/az;->x:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/StackTraceElement;

    invoke-static {p1, v5, v0, v6, v6}, Lcom/a/a/az;->a(Lcom/a/a/an;Ljava/lang/Thread;[Ljava/lang/StackTraceElement;IZ)V

    .line 1499
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_0
    move v0, v1

    move v2, v3

    move-object v4, p1

    move-object v5, p0

    .line 1504
    :goto_1
    invoke-virtual {v4, v0, v1}, Lcom/a/a/an;->e(II)V

    invoke-direct {v5, p3, v3}, Lcom/a/a/az;->a(Ljava/lang/Throwable;I)I

    move-result v0

    invoke-virtual {v4, v0}, Lcom/a/a/an;->f(I)V

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/a/a/aj;->a(Ljava/lang/String;)Lcom/a/a/aj;

    move-result-object v0

    invoke-virtual {v4, v3, v0}, Lcom/a/a/an;->a(ILcom/a/a/aj;)V

    invoke-virtual {p3}, Ljava/lang/Throwable;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-static {v0}, Lcom/a/a/aj;->a(Ljava/lang/String;)Lcom/a/a/aj;

    move-result-object v0

    invoke-virtual {v4, v10, v0}, Lcom/a/a/an;->a(ILcom/a/a/aj;)V

    :cond_1
    invoke-virtual {p3}, Ljava/lang/Throwable;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v7

    array-length v8, v7

    move v0, v6

    :goto_2
    if-ge v0, v8, :cond_2

    aget-object v9, v7, v0

    invoke-static {v4, v11, v9, v3}, Lcom/a/a/az;->a(Lcom/a/a/an;ILjava/lang/StackTraceElement;Z)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    invoke-virtual {p3}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object p3

    if-eqz p3, :cond_5

    const/16 v0, 0x8

    if-ge v2, v0, :cond_3

    add-int/lit8 v2, v2, 0x1

    const/4 v0, 0x6

    goto :goto_1

    :cond_3
    move v0, v6

    :goto_3
    if-eqz p3, :cond_4

    invoke-virtual {p3}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object p3

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_4
    const/4 v2, 0x7

    invoke-virtual {v4, v2, v0}, Lcom/a/a/an;->a(II)V

    .line 1506
    :cond_5
    invoke-virtual {p1, v10, v1}, Lcom/a/a/an;->e(II)V

    .line 1507
    invoke-static {}, Lcom/a/a/az;->j()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/a/a/an;->f(I)V

    .line 1508
    sget-object v0, Lcom/a/a/az;->g:Lcom/a/a/aj;

    invoke-virtual {p1, v3, v0}, Lcom/a/a/an;->a(ILcom/a/a/aj;)V

    .line 1509
    sget-object v0, Lcom/a/a/az;->g:Lcom/a/a/aj;

    invoke-virtual {p1, v1, v0}, Lcom/a/a/an;->a(ILcom/a/a/aj;)V

    .line 1510
    const-wide/16 v4, 0x0

    invoke-virtual {p1, v10, v4, v5}, Lcom/a/a/an;->a(IJ)V

    .line 1512
    invoke-virtual {p1, v11, v1}, Lcom/a/a/an;->e(II)V

    .line 1513
    invoke-direct {p0}, Lcom/a/a/az;->i()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/a/a/an;->f(I)V

    .line 1514
    const-wide/16 v4, 0x0

    invoke-virtual {p1, v3, v4, v5}, Lcom/a/a/an;->a(IJ)V

    .line 1515
    const-wide/16 v2, 0x0

    invoke-virtual {p1, v1, v2, v3}, Lcom/a/a/an;->a(IJ)V

    .line 1516
    iget-object v0, p0, Lcom/a/a/az;->q:Lcom/a/a/aj;

    invoke-virtual {p1, v10, v0}, Lcom/a/a/an;->a(ILcom/a/a/aj;)V

    .line 1517
    iget-object v0, p0, Lcom/a/a/az;->r:Lcom/a/a/aj;

    if-eqz v0, :cond_6

    .line 1518
    iget-object v0, p0, Lcom/a/a/az;->r:Lcom/a/a/aj;

    invoke-virtual {p1, v11, v0}, Lcom/a/a/an;->a(ILcom/a/a/aj;)V

    .line 1520
    :cond_6
    return-void
.end method

.method private static a(Lcom/a/a/an;Ljava/lang/Thread;[Ljava/lang/StackTraceElement;IZ)V
    .locals 4

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 1596
    invoke-virtual {p0, v1, v2}, Lcom/a/a/an;->e(II)V

    .line 1597
    invoke-static {p1, p2, p3, p4}, Lcom/a/a/az;->a(Ljava/lang/Thread;[Ljava/lang/StackTraceElement;IZ)I

    move-result v0

    .line 1598
    invoke-virtual {p0, v0}, Lcom/a/a/an;->f(I)V

    .line 1599
    invoke-virtual {p1}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/a/a/aj;->a(Ljava/lang/String;)Lcom/a/a/aj;

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Lcom/a/a/an;->a(ILcom/a/a/aj;)V

    .line 1600
    invoke-virtual {p0, v2, p3}, Lcom/a/a/an;->a(II)V

    .line 1602
    array-length v1, p2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    aget-object v2, p2, v0

    .line 1603
    const/4 v3, 0x3

    invoke-static {p0, v3, v2, p4}, Lcom/a/a/az;->a(Lcom/a/a/an;ILjava/lang/StackTraceElement;Z)V

    .line 1602
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1605
    :cond_0
    return-void
.end method

.method private static a(Lcom/a/a/an;Ljava/util/Map;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/a/a/an;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v4, 0x2

    .line 1525
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1526
    invoke-virtual {p0, v4, v4}, Lcom/a/a/an;->e(II)V

    .line 1527
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/a/a/az;->a(Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/a/a/an;->f(I)V

    .line 1529
    const/4 v2, 0x1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Lcom/a/a/aj;->a(Ljava/lang/String;)Lcom/a/a/aj;

    move-result-object v1

    invoke-virtual {p0, v2, v1}, Lcom/a/a/an;->a(ILcom/a/a/aj;)V

    .line 1530
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1531
    if-nez v0, :cond_0

    const-string v0, ""

    :cond_0
    invoke-static {v0}, Lcom/a/a/aj;->a(Ljava/lang/String;)Lcom/a/a/aj;

    move-result-object v0

    invoke-virtual {p0, v4, v0}, Lcom/a/a/an;->a(ILcom/a/a/aj;)V

    goto :goto_0

    .line 1533
    :cond_1
    return-void
.end method

.method private static a(Lcom/a/a/an;[Ljava/io/File;Ljava/lang/String;)V
    .locals 9

    .prologue
    const/4 v0, 0x0

    .line 882
    sget-object v1, Lcom/a/a/a/ba;->a:Ljava/util/Comparator;

    invoke-static {p1, v1}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    .line 884
    array-length v1, p1

    :goto_0
    if-ge v0, v1, :cond_0

    aget-object v2, p1, v0

    .line 886
    :try_start_0
    invoke-static {}, Lcom/a/a/a/co;->a()Lcom/a/a/a/cl;

    move-result-object v3

    invoke-virtual {v3}, Lcom/a/a/a/cl;->a()Lcom/a/a/a/ci;

    move-result-object v3

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v5, "Found Non Fatal for session ID %s in %s "

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object p2, v6, v7

    const/4 v7, 0x1

    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v4, v5, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    invoke-interface {v3}, Lcom/a/a/a/ci;->b()V

    .line 887
    invoke-static {p0, v2}, Lcom/a/a/az;->a(Lcom/a/a/an;Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 884
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 889
    :catch_0
    move-exception v2

    invoke-static {}, Lcom/a/a/a/co;->a()Lcom/a/a/a/cl;

    move-result-object v2

    invoke-virtual {v2}, Lcom/a/a/a/cl;->a()Lcom/a/a/a/ci;

    move-result-object v2

    invoke-interface {v2}, Lcom/a/a/a/ci;->a()V

    goto :goto_1

    .line 892
    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/a/a/az;Ljava/util/Date;Ljava/lang/Thread;Ljava/lang/Throwable;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 66
    :try_start_0
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/a/a/az;->l:Ljava/io/File;

    const-string v3, "crash_marker"

    invoke-direct {v0, v1, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->createNewFile()Z

    invoke-direct {p0}, Lcom/a/a/az;->f()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-static {v0}, Lcom/a/a/d;->a(Ljava/lang/String;)V

    new-instance v6, Lcom/a/a/al;

    iget-object v1, p0, Lcom/a/a/az;->l:Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "SessionCrash"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v6, v1, v0}, Lcom/a/a/al;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-static {v6}, Lcom/a/a/an;->a(Ljava/io/OutputStream;)Lcom/a/a/an;

    move-result-object v2

    const-string v5, "crash"

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/a/a/az;->a(Ljava/util/Date;Lcom/a/a/an;Ljava/lang/Thread;Ljava/lang/Throwable;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-object v0, v6

    :goto_0
    invoke-static {v2}, Lcom/a/a/a/ba;->a(Ljava/io/Flushable;)V

    invoke-static {v0}, Lcom/a/a/a/ba;->a(Ljava/io/Closeable;)V

    :goto_1
    invoke-direct {p0}, Lcom/a/a/az;->e()V

    invoke-direct {p0}, Lcom/a/a/az;->d()V

    iget-object v0, p0, Lcom/a/a/az;->l:Ljava/io/File;

    sget-object v1, Lcom/a/a/az;->a:Ljava/io/FilenameFilter;

    const/4 v2, 0x4

    sget-object v3, Lcom/a/a/az;->d:Ljava/util/Comparator;

    invoke-static {v0, v1, v2, v3}, Lcom/a/a/ah;->a(Ljava/io/File;Ljava/io/FilenameFilter;ILjava/util/Comparator;)V

    invoke-static {}, Lcom/a/a/d;->a()Lcom/a/a/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/a/d;->j()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/a/a/az;->g()V

    :cond_0
    return-void

    :cond_1
    :try_start_2
    invoke-static {}, Lcom/a/a/a/co;->a()Lcom/a/a/a/cl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/a/a/cl;->a()Lcom/a/a/a/ci;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/a/a/ci;->a()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-object v0, v2

    goto :goto_0

    :catch_0
    move-exception v0

    move-object v6, v2

    :goto_2
    :try_start_3
    invoke-static {}, Lcom/a/a/a/co;->a()Lcom/a/a/a/cl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/a/a/a/cl;->a()Lcom/a/a/a/ci;

    move-result-object v1

    invoke-interface {v1}, Lcom/a/a/a/ci;->a()V

    invoke-static {v0, v6}, Lcom/a/a/az;->a(Ljava/lang/Throwable;Ljava/io/OutputStream;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    invoke-static {v2}, Lcom/a/a/a/ba;->a(Ljava/io/Flushable;)V

    invoke-static {v6}, Lcom/a/a/a/ba;->a(Ljava/io/Closeable;)V

    goto :goto_1

    :catchall_0
    move-exception v0

    move-object v6, v2

    :goto_3
    invoke-static {v2}, Lcom/a/a/a/ba;->a(Ljava/io/Flushable;)V

    invoke-static {v6}, Lcom/a/a/a/ba;->a(Ljava/io/Closeable;)V

    throw v0

    :catchall_1
    move-exception v0

    goto :goto_3

    :catch_1
    move-exception v0

    goto :goto_2
.end method

.method private a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 913
    new-instance v0, Lcom/a/a/u;

    invoke-direct {v0, p1}, Lcom/a/a/u;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/a/a/az;->a(Ljava/io/FilenameFilter;)[Ljava/io/File;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 914
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    .line 913
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 916
    :cond_0
    return-void
.end method

.method private static a(Ljava/lang/Throwable;Ljava/io/OutputStream;)V
    .locals 3

    .prologue
    .line 1089
    if-eqz p1, :cond_0

    .line 1090
    const/4 v1, 0x0

    :try_start_0
    new-instance v0, Ljava/io/PrintWriter;

    invoke-direct {v0, p1}, Ljava/io/PrintWriter;-><init>(Ljava/io/OutputStream;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-static {p0, v0}, Lcom/a/a/az;->a(Ljava/lang/Throwable;Ljava/io/Writer;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    invoke-static {v0}, Lcom/a/a/a/ba;->a(Ljava/io/Closeable;)V

    .line 1092
    :cond_0
    :goto_0
    return-void

    .line 1090
    :catch_0
    move-exception v0

    move-object v0, v1

    :goto_1
    :try_start_2
    invoke-static {}, Lcom/a/a/a/co;->a()Lcom/a/a/a/cl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/a/a/a/cl;->a()Lcom/a/a/a/ci;

    move-result-object v1

    invoke-interface {v1}, Lcom/a/a/a/ci;->a()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    invoke-static {v0}, Lcom/a/a/a/ba;->a(Ljava/io/Closeable;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    :goto_2
    invoke-static {v1}, Lcom/a/a/a/ba;->a(Ljava/io/Closeable;)V

    throw v0

    :catchall_1
    move-exception v1

    move-object v2, v1

    move-object v1, v0

    move-object v0, v2

    goto :goto_2

    :catch_1
    move-exception v1

    goto :goto_1
.end method

.method private static a(Ljava/lang/Throwable;Ljava/io/Writer;)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 1122
    const/4 v0, 0x1

    move v3, v0

    .line 1124
    :goto_0
    if-eqz p0, :cond_4

    .line 1125
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Throwable;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 1126
    :goto_1
    if-eqz v0, :cond_1

    move-object v2, v0

    .line 1128
    :goto_2
    if-eqz v3, :cond_2

    const-string v0, ""

    .line 1129
    :goto_3
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ": "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 1133
    invoke-virtual {p0}, Ljava/lang/Throwable;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v2

    array-length v3, v2

    move v0, v1

    :goto_4
    if-ge v0, v3, :cond_3

    aget-object v4, v2, v0

    .line 1134
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "\tat "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/lang/StackTraceElement;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 1133
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 1125
    :cond_0
    const-string v2, "(\r\n|\n|\u000c)"

    const-string v4, " "

    invoke-virtual {v0, v2, v4}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 1126
    :cond_1
    const-string v0, ""

    move-object v2, v0

    goto :goto_2

    .line 1128
    :cond_2
    const-string v0, "Caused by: "

    goto :goto_3

    .line 1137
    :cond_3
    invoke-virtual {p0}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p0

    move v3, v1

    .line 1138
    goto :goto_0

    .line 1140
    :catch_0
    move-exception v0

    invoke-static {}, Lcom/a/a/a/co;->a()Lcom/a/a/a/cl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/a/a/cl;->a()Lcom/a/a/a/ci;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/a/a/ci;->a()V

    .line 1142
    :cond_4
    return-void
.end method

.method private a(Ljava/util/Date;Lcom/a/a/an;Ljava/lang/Thread;Ljava/lang/Throwable;Ljava/lang/String;)V
    .locals 20

    .prologue
    .line 1406
    invoke-virtual/range {p1 .. p1}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    div-long v14, v4, v6

    .line 1407
    invoke-static {}, Lcom/a/a/d;->a()Lcom/a/a/d;

    move-result-object v4

    iget-object v4, v4, Lcom/a/a/a/ch;->g:Landroid/content/Context;

    invoke-static {v4}, Lcom/a/a/a/ba;->b(Landroid/content/Context;)F

    move-result v13

    .line 1408
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/a/a/az;->v:Z

    invoke-static {v4}, Lcom/a/a/a/ba;->a(Z)I

    move-result v4

    .line 1409
    invoke-static {}, Lcom/a/a/d;->a()Lcom/a/a/d;

    move-result-object v5

    iget-object v5, v5, Lcom/a/a/a/ch;->g:Landroid/content/Context;

    invoke-static {v5}, Lcom/a/a/a/ba;->c(Landroid/content/Context;)Z

    move-result v16

    .line 1410
    invoke-static {}, Lcom/a/a/d;->a()Lcom/a/a/d;

    move-result-object v5

    iget-object v5, v5, Lcom/a/a/a/ch;->g:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v5

    iget v5, v5, Landroid/content/res/Configuration;->orientation:I

    .line 1411
    invoke-static {}, Lcom/a/a/a/ba;->c()J

    move-result-wide v6

    invoke-static {}, Lcom/a/a/d;->a()Lcom/a/a/d;

    move-result-object v8

    iget-object v8, v8, Lcom/a/a/a/ch;->g:Landroid/content/Context;

    invoke-static {v8}, Lcom/a/a/a/ba;->a(Landroid/content/Context;)J

    move-result-wide v8

    sub-long/2addr v6, v8

    .line 1412
    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    move-result-object v8

    invoke-virtual {v8}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/a/a/a/ba;->b(Ljava/lang/String;)J

    move-result-wide v8

    .line 1414
    invoke-static {}, Lcom/a/a/d;->d()Ljava/lang/String;

    move-result-object v10

    invoke-static {}, Lcom/a/a/d;->a()Lcom/a/a/d;

    move-result-object v11

    iget-object v11, v11, Lcom/a/a/a/ch;->g:Landroid/content/Context;

    invoke-static {v10, v11}, Lcom/a/a/a/ba;->a(Ljava/lang/String;Landroid/content/Context;)Landroid/app/ActivityManager$RunningAppProcessInfo;

    move-result-object v10

    move-object/from16 v0, p0

    iput-object v10, v0, Lcom/a/a/az;->t:Landroid/app/ActivityManager$RunningAppProcessInfo;

    .line 1415
    new-instance v10, Ljava/util/LinkedList;

    invoke-direct {v10}, Ljava/util/LinkedList;-><init>()V

    move-object/from16 v0, p0

    iput-object v10, v0, Lcom/a/a/az;->x:Ljava/util/List;

    .line 1416
    invoke-virtual/range {p4 .. p4}, Ljava/lang/Throwable;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v10

    move-object/from16 v0, p0

    iput-object v10, v0, Lcom/a/a/az;->y:[Ljava/lang/StackTraceElement;

    .line 1418
    invoke-static {}, Ljava/lang/Thread;->getAllStackTraces()Ljava/util/Map;

    move-result-object v11

    .line 1420
    invoke-interface {v11}, Ljava/util/Map;->size()I

    move-result v10

    new-array v10, v10, [Ljava/lang/Thread;

    move-object/from16 v0, p0

    iput-object v10, v0, Lcom/a/a/az;->w:[Ljava/lang/Thread;

    .line 1421
    const/4 v10, 0x0

    .line 1422
    invoke-interface {v11}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v11

    invoke-interface {v11}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v17

    move v12, v10

    :goto_0
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/util/Map$Entry;

    .line 1423
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/a/a/az;->w:[Ljava/lang/Thread;

    move-object/from16 v18, v0

    invoke-interface {v10}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Thread;

    aput-object v11, v18, v12

    .line 1424
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/a/a/az;->x:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v10

    invoke-interface {v11, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1425
    add-int/lit8 v10, v12, 0x1

    move v12, v10

    .line 1426
    goto :goto_0

    .line 1427
    :cond_0
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/a/a/az;->u:Lcom/a/a/a/bo;

    invoke-static {v10}, Lcom/a/a/az;->a(Lcom/a/a/a/bo;)Lcom/a/a/aj;

    move-result-object v12

    .line 1435
    if-nez v12, :cond_1

    .line 1436
    invoke-static {}, Lcom/a/a/a/co;->a()Lcom/a/a/a/cl;

    move-result-object v10

    invoke-virtual {v10}, Lcom/a/a/a/cl;->a()Lcom/a/a/a/ci;

    move-result-object v10

    invoke-interface {v10}, Lcom/a/a/a/ci;->b()V

    .line 1440
    :cond_1
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/a/a/az;->u:Lcom/a/a/a/bo;

    invoke-static {v10}, Lcom/a/a/a/ba;->a(Ljava/io/Closeable;)V

    .line 1441
    const/4 v10, 0x0

    move-object/from16 v0, p0

    iput-object v10, v0, Lcom/a/a/az;->u:Lcom/a/a/a/bo;

    .line 1444
    invoke-static {}, Lcom/a/a/d;->a()Lcom/a/a/d;

    move-result-object v10

    iget-object v10, v10, Lcom/a/a/a/ch;->g:Landroid/content/Context;

    const-string v11, "com.crashlytics.CollectCustomKeys"

    const/16 v17, 0x1

    move/from16 v0, v17

    invoke-static {v10, v11, v0}, Lcom/a/a/a/ba;->a(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v10

    if-nez v10, :cond_6

    .line 1445
    new-instance v10, Ljava/util/TreeMap;

    invoke-direct {v10}, Ljava/util/TreeMap;-><init>()V

    .line 1456
    :goto_1
    const/16 v11, 0xa

    const/16 v17, 0x2

    move-object/from16 v0, p2

    move/from16 v1, v17

    invoke-virtual {v0, v11, v1}, Lcom/a/a/an;->e(II)V

    .line 1457
    const/4 v11, 0x1

    invoke-static {v11, v14, v15}, Lcom/a/a/an;->b(IJ)I

    move-result v11

    add-int/lit8 v11, v11, 0x0

    const/16 v17, 0x2

    invoke-static/range {p5 .. p5}, Lcom/a/a/aj;->a(Ljava/lang/String;)Lcom/a/a/aj;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Lcom/a/a/an;->b(ILcom/a/a/aj;)I

    move-result v17

    add-int v11, v11, v17

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    move-object/from16 v2, p4

    invoke-direct {v0, v1, v2, v10}, Lcom/a/a/az;->a(Ljava/lang/Thread;Ljava/lang/Throwable;Ljava/util/Map;)I

    move-result v17

    const/16 v18, 0x3

    invoke-static/range {v18 .. v18}, Lcom/a/a/an;->e(I)I

    move-result v18

    invoke-static/range {v17 .. v17}, Lcom/a/a/an;->g(I)I

    move-result v19

    add-int v18, v18, v19

    add-int v17, v17, v18

    add-int v11, v11, v17

    invoke-static/range {v4 .. v9}, Lcom/a/a/az;->a(IIJJ)I

    move-result v17

    const/16 v18, 0x5

    invoke-static/range {v18 .. v18}, Lcom/a/a/an;->e(I)I

    move-result v18

    invoke-static/range {v17 .. v17}, Lcom/a/a/an;->g(I)I

    move-result v19

    add-int v18, v18, v19

    add-int v17, v17, v18

    add-int v11, v11, v17

    if-eqz v12, :cond_2

    const/16 v17, 0x1

    move/from16 v0, v17

    invoke-static {v0, v12}, Lcom/a/a/an;->b(ILcom/a/a/aj;)I

    move-result v17

    const/16 v18, 0x6

    invoke-static/range {v18 .. v18}, Lcom/a/a/an;->e(I)I

    move-result v18

    invoke-static/range {v17 .. v17}, Lcom/a/a/an;->g(I)I

    move-result v19

    add-int v18, v18, v19

    add-int v17, v17, v18

    add-int v11, v11, v17

    :cond_2
    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Lcom/a/a/an;->f(I)V

    .line 1460
    const/4 v11, 0x1

    move-object/from16 v0, p2

    invoke-virtual {v0, v11, v14, v15}, Lcom/a/a/an;->a(IJ)V

    .line 1461
    const/4 v11, 0x2

    invoke-static/range {p5 .. p5}, Lcom/a/a/aj;->a(Ljava/lang/String;)Lcom/a/a/aj;

    move-result-object v14

    move-object/from16 v0, p2

    invoke-virtual {v0, v11, v14}, Lcom/a/a/an;->a(ILcom/a/a/aj;)V

    .line 1463
    const/4 v11, 0x3

    const/4 v14, 0x2

    move-object/from16 v0, p2

    invoke-virtual {v0, v11, v14}, Lcom/a/a/an;->e(II)V

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    move-object/from16 v2, p4

    invoke-direct {v0, v1, v2, v10}, Lcom/a/a/az;->a(Ljava/lang/Thread;Ljava/lang/Throwable;Ljava/util/Map;)I

    move-result v11

    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Lcom/a/a/an;->f(I)V

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    move-object/from16 v3, p4

    invoke-direct {v0, v1, v2, v3}, Lcom/a/a/az;->a(Lcom/a/a/an;Ljava/lang/Thread;Ljava/lang/Throwable;)V

    if-eqz v10, :cond_3

    invoke-interface {v10}, Ljava/util/Map;->isEmpty()Z

    move-result v11

    if-nez v11, :cond_3

    move-object/from16 v0, p2

    invoke-static {v0, v10}, Lcom/a/a/az;->a(Lcom/a/a/an;Ljava/util/Map;)V

    :cond_3
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/a/a/az;->t:Landroid/app/ActivityManager$RunningAppProcessInfo;

    if-eqz v10, :cond_4

    const/4 v11, 0x3

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/a/a/az;->t:Landroid/app/ActivityManager$RunningAppProcessInfo;

    iget v10, v10, Landroid/app/ActivityManager$RunningAppProcessInfo;->importance:I

    const/16 v14, 0x64

    if-eq v10, v14, :cond_7

    const/4 v10, 0x1

    :goto_2
    move-object/from16 v0, p2

    invoke-virtual {v0, v11, v10}, Lcom/a/a/an;->a(IZ)V

    :cond_4
    const/4 v10, 0x4

    invoke-static {}, Lcom/a/a/d;->a()Lcom/a/a/d;

    move-result-object v11

    iget-object v11, v11, Lcom/a/a/a/ch;->g:Landroid/content/Context;

    invoke-virtual {v11}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    invoke-virtual {v11}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v11

    iget v11, v11, Landroid/content/res/Configuration;->orientation:I

    move-object/from16 v0, p2

    invoke-virtual {v0, v10, v11}, Lcom/a/a/an;->a(II)V

    .line 1464
    const/4 v10, 0x5

    const/4 v11, 0x2

    move-object/from16 v0, p2

    invoke-virtual {v0, v10, v11}, Lcom/a/a/an;->e(II)V

    invoke-static/range {v4 .. v9}, Lcom/a/a/az;->a(IIJJ)I

    move-result v10

    move-object/from16 v0, p2

    invoke-virtual {v0, v10}, Lcom/a/a/an;->f(I)V

    const/4 v10, 0x1

    const/4 v11, 0x5

    move-object/from16 v0, p2

    invoke-virtual {v0, v10, v11}, Lcom/a/a/an;->e(II)V

    invoke-static {v13}, Ljava/lang/Float;->floatToRawIntBits(F)I

    move-result v10

    and-int/lit16 v11, v10, 0xff

    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Lcom/a/a/an;->d(I)V

    shr-int/lit8 v11, v10, 0x8

    and-int/lit16 v11, v11, 0xff

    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Lcom/a/a/an;->d(I)V

    shr-int/lit8 v11, v10, 0x10

    and-int/lit16 v11, v11, 0xff

    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Lcom/a/a/an;->d(I)V

    ushr-int/lit8 v10, v10, 0x18

    move-object/from16 v0, p2

    invoke-virtual {v0, v10}, Lcom/a/a/an;->d(I)V

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Lcom/a/a/an;->a(I)V

    const/4 v4, 0x3

    move-object/from16 v0, p2

    move/from16 v1, v16

    invoke-virtual {v0, v4, v1}, Lcom/a/a/an;->a(IZ)V

    const/4 v4, 0x4

    move-object/from16 v0, p2

    invoke-virtual {v0, v4, v5}, Lcom/a/a/an;->a(II)V

    const/4 v4, 0x5

    move-object/from16 v0, p2

    invoke-virtual {v0, v4, v6, v7}, Lcom/a/a/an;->a(IJ)V

    const/4 v4, 0x6

    move-object/from16 v0, p2

    invoke-virtual {v0, v4, v8, v9}, Lcom/a/a/an;->a(IJ)V

    .line 1466
    if-eqz v12, :cond_5

    const/4 v4, 0x6

    const/4 v5, 0x2

    move-object/from16 v0, p2

    invoke-virtual {v0, v4, v5}, Lcom/a/a/an;->e(II)V

    const/4 v4, 0x1

    invoke-static {v4, v12}, Lcom/a/a/an;->b(ILcom/a/a/aj;)I

    move-result v4

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Lcom/a/a/an;->f(I)V

    const/4 v4, 0x1

    move-object/from16 v0, p2

    invoke-virtual {v0, v4, v12}, Lcom/a/a/an;->a(ILcom/a/a/aj;)V

    .line 1467
    :cond_5
    return-void

    .line 1447
    :cond_6
    invoke-static {}, Lcom/a/a/d;->a()Lcom/a/a/d;

    move-result-object v10

    iget-object v10, v10, Lcom/a/a/d;->a:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-static {v10}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v11

    .line 1448
    if-eqz v11, :cond_8

    invoke-interface {v11}, Ljava/util/Map;->size()I

    move-result v10

    const/16 v17, 0x1

    move/from16 v0, v17

    if-le v10, v0, :cond_8

    .line 1452
    new-instance v10, Ljava/util/TreeMap;

    invoke-direct {v10, v11}, Ljava/util/TreeMap;-><init>(Ljava/util/Map;)V

    goto/16 :goto_1

    .line 1463
    :cond_7
    const/4 v10, 0x0

    goto/16 :goto_2

    :cond_8
    move-object v10, v11

    goto/16 :goto_1
.end method

.method static synthetic a(Lcom/a/a/az;Z)Z
    .locals 0

    .prologue
    .line 66
    iput-boolean p1, p0, Lcom/a/a/az;->v:Z

    return p1
.end method

.method static synthetic a(Lcom/a/a/az;Ljava/io/FilenameFilter;)[Ljava/io/File;
    .locals 1

    .prologue
    .line 66
    invoke-virtual {p0, p1}, Lcom/a/a/az;->a(Ljava/io/FilenameFilter;)[Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method private static b(Ljava/lang/String;)Lcom/a/a/aj;
    .locals 1

    .prologue
    .line 1210
    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0}, Lcom/a/a/aj;->a(Ljava/lang/String;)Lcom/a/a/aj;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic b()Ljava/util/regex/Pattern;
    .locals 1

    .prologue
    .line 66
    sget-object v0, Lcom/a/a/az;->e:Ljava/util/regex/Pattern;

    return-object v0
.end method

.method static synthetic b(Lcom/a/a/az;)V
    .locals 0

    .prologue
    .line 66
    invoke-direct {p0}, Lcom/a/a/az;->e()V

    return-void
.end method

.method static synthetic c()Ljava/util/Map;
    .locals 1

    .prologue
    .line 66
    sget-object v0, Lcom/a/a/az;->f:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic c(Lcom/a/a/az;)V
    .locals 0

    .prologue
    .line 66
    invoke-direct {p0}, Lcom/a/a/az;->d()V

    return-void
.end method

.method private static c(Ljava/lang/String;)V
    .locals 19

    .prologue
    .line 1218
    const/4 v4, 0x0

    .line 1219
    const/4 v3, 0x0

    .line 1221
    :try_start_0
    new-instance v15, Lcom/a/a/al;

    invoke-static {}, Lcom/a/a/a/co;->a()Lcom/a/a/a/cl;

    move-result-object v2

    iget-object v2, v2, Lcom/a/a/a/cl;->c:Ljava/io/File;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "SessionDevice"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v15, v2, v5}, Lcom/a/a/al;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1222
    :try_start_1
    invoke-static {v15}, Lcom/a/a/an;->a(Ljava/io/OutputStream;)Lcom/a/a/an;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result-object v14

    .line 1224
    :try_start_2
    new-instance v3, Landroid/os/StatFs;

    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v2}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 1226
    invoke-static {}, Lcom/a/a/a/ba;->b()I

    move-result v2

    .line 1228
    sget-object v4, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-static {v4}, Lcom/a/a/az;->b(Ljava/lang/String;)Lcom/a/a/aj;

    move-result-object v4

    .line 1229
    sget-object v5, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    invoke-static {v5}, Lcom/a/a/az;->b(Ljava/lang/String;)Lcom/a/a/aj;

    move-result-object v12

    .line 1230
    sget-object v5, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    invoke-static {v5}, Lcom/a/a/az;->b(Ljava/lang/String;)Lcom/a/a/aj;

    move-result-object v13

    .line 1232
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Runtime;->availableProcessors()I

    move-result v5

    .line 1233
    invoke-static {}, Lcom/a/a/a/ba;->c()J

    move-result-wide v6

    .line 1234
    invoke-virtual {v3}, Landroid/os/StatFs;->getBlockCount()I

    move-result v8

    int-to-long v8, v8

    invoke-virtual {v3}, Landroid/os/StatFs;->getBlockSize()I

    move-result v3

    int-to-long v10, v3

    mul-long/2addr v8, v10

    .line 1235
    invoke-static {}, Lcom/a/a/a/ba;->f()Z

    move-result v16

    .line 1237
    invoke-static {}, Lcom/a/a/d;->a()Lcom/a/a/d;

    move-result-object v3

    iget-object v10, v3, Lcom/a/a/d;->c:Lcom/a/a/a/bm;

    .line 1238
    const-string v3, ""

    iget-boolean v11, v10, Lcom/a/a/a/bm;->a:Z

    if-eqz v11, :cond_0

    invoke-virtual {v10}, Lcom/a/a/a/bm;->e()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_0

    invoke-static {}, Lcom/a/a/a/ba;->a()Landroid/content/SharedPreferences;

    move-result-object v11

    const-string v3, "crashlytics.installation.id"

    const/16 v17, 0x0

    move-object/from16 v0, v17

    invoke-interface {v11, v3, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_0

    invoke-virtual {v10, v11}, Lcom/a/a/a/bm;->a(Landroid/content/SharedPreferences;)Ljava/lang/String;

    move-result-object v3

    :cond_0
    invoke-static {v3}, Lcom/a/a/aj;->a(Ljava/lang/String;)Lcom/a/a/aj;

    move-result-object v3

    .line 1240
    invoke-virtual {v10}, Lcom/a/a/a/bm;->d()Ljava/util/Map;

    move-result-object v10

    .line 1242
    invoke-static {}, Lcom/a/a/a/ba;->h()I

    move-result v11

    .line 1244
    const/16 v17, 0x9

    const/16 v18, 0x2

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v14, v0, v1}, Lcom/a/a/an;->e(II)V

    .line 1246
    invoke-static/range {v2 .. v13}, Lcom/a/a/az;->a(ILcom/a/a/aj;Lcom/a/a/aj;IJJLjava/util/Map;ILcom/a/a/aj;Lcom/a/a/aj;)I

    move-result v17

    move/from16 v0, v17

    invoke-virtual {v14, v0}, Lcom/a/a/an;->f(I)V

    .line 1249
    const/16 v17, 0x1

    move/from16 v0, v17

    invoke-virtual {v14, v0, v3}, Lcom/a/a/an;->a(ILcom/a/a/aj;)V

    .line 1251
    const/4 v3, 0x3

    invoke-virtual {v14, v3, v2}, Lcom/a/a/an;->b(II)V

    .line 1252
    const/4 v2, 0x4

    invoke-virtual {v14, v2, v4}, Lcom/a/a/an;->a(ILcom/a/a/aj;)V

    .line 1253
    const/4 v2, 0x5

    invoke-virtual {v14, v2, v5}, Lcom/a/a/an;->a(II)V

    .line 1254
    const/4 v2, 0x6

    invoke-virtual {v14, v2, v6, v7}, Lcom/a/a/an;->a(IJ)V

    .line 1255
    const/4 v2, 0x7

    invoke-virtual {v14, v2, v8, v9}, Lcom/a/a/an;->a(IJ)V

    .line 1256
    const/16 v2, 0xa

    move/from16 v0, v16

    invoke-virtual {v14, v2, v0}, Lcom/a/a/an;->a(IZ)V

    .line 1258
    invoke-interface {v10}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Ljava/util/Map$Entry;

    move-object v4, v0

    .line 1259
    const/16 v2, 0xb

    const/4 v3, 0x2

    invoke-virtual {v14, v2, v3}, Lcom/a/a/an;->e(II)V

    .line 1260
    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/a/a/a/bn;

    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/a/a/az;->a(Lcom/a/a/a/bn;Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v14, v2}, Lcom/a/a/an;->f(I)V

    .line 1262
    const/4 v3, 0x1

    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/a/a/a/bn;

    iget v2, v2, Lcom/a/a/a/bn;->f:I

    invoke-virtual {v14, v3, v2}, Lcom/a/a/an;->b(II)V

    .line 1263
    const/4 v3, 0x2

    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {v2}, Lcom/a/a/aj;->a(Ljava/lang/String;)Lcom/a/a/aj;

    move-result-object v2

    invoke-virtual {v14, v3, v2}, Lcom/a/a/an;->a(ILcom/a/a/aj;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_3

    goto :goto_0

    .line 1273
    :catch_0
    move-exception v2

    move-object v3, v14

    move-object v4, v15

    .line 1274
    :goto_1
    :try_start_3
    invoke-static {v2, v4}, Lcom/a/a/az;->a(Ljava/lang/Throwable;Ljava/io/OutputStream;)V

    .line 1275
    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1277
    :catchall_0
    move-exception v2

    move-object v14, v3

    move-object v15, v4

    :goto_2
    invoke-static {v14}, Lcom/a/a/a/ba;->a(Ljava/io/Flushable;)V

    .line 1278
    invoke-static {v15}, Lcom/a/a/a/ba;->a(Ljava/io/Closeable;)V

    throw v2

    .line 1265
    :cond_1
    const/16 v2, 0xc

    :try_start_4
    invoke-virtual {v14, v2, v11}, Lcom/a/a/an;->a(II)V

    .line 1267
    if-eqz v12, :cond_2

    .line 1268
    const/16 v2, 0xd

    invoke-virtual {v14, v2, v12}, Lcom/a/a/an;->a(ILcom/a/a/aj;)V

    .line 1270
    :cond_2
    if-eqz v13, :cond_3

    .line 1271
    const/16 v2, 0xe

    invoke-virtual {v14, v2, v13}, Lcom/a/a/an;->a(ILcom/a/a/aj;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    .line 1277
    :cond_3
    invoke-static {v14}, Lcom/a/a/a/ba;->a(Ljava/io/Flushable;)V

    .line 1278
    invoke-static {v15}, Lcom/a/a/a/ba;->a(Ljava/io/Closeable;)V

    .line 1279
    return-void

    .line 1277
    :catchall_1
    move-exception v2

    move-object v14, v3

    move-object v15, v4

    goto :goto_2

    :catchall_2
    move-exception v2

    move-object v14, v3

    goto :goto_2

    :catchall_3
    move-exception v2

    goto :goto_2

    .line 1273
    :catch_1
    move-exception v2

    goto :goto_1

    :catch_2
    move-exception v2

    move-object v4, v15

    goto :goto_1
.end method

.method static synthetic d(Lcom/a/a/az;)Ljava/io/File;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/a/a/az;->m:Ljava/io/File;

    return-object v0
.end method

.method private d()V
    .locals 13

    .prologue
    const/4 v2, 0x0

    .line 714
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    .line 715
    new-instance v1, Lcom/a/a/ak;

    invoke-static {}, Lcom/a/a/d;->a()Lcom/a/a/d;

    move-result-object v3

    iget-object v3, v3, Lcom/a/a/d;->c:Lcom/a/a/a/bm;

    invoke-direct {v1, v3}, Lcom/a/a/ak;-><init>(Lcom/a/a/a/bm;)V

    invoke-virtual {v1}, Lcom/a/a/ak;->toString()Ljava/lang/String;

    move-result-object v4

    .line 717
    invoke-static {}, Lcom/a/a/a/co;->a()Lcom/a/a/a/cl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/a/a/a/cl;->a()Lcom/a/a/a/ci;

    move-result-object v1

    invoke-interface {v1}, Lcom/a/a/a/ci;->b()V

    .line 719
    :try_start_0
    new-instance v3, Lcom/a/a/al;

    invoke-static {}, Lcom/a/a/a/co;->a()Lcom/a/a/a/cl;

    move-result-object v1

    iget-object v1, v1, Lcom/a/a/a/cl;->c:Ljava/io/File;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "BeginSession"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v1, v5}, Lcom/a/a/al;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_7

    :try_start_1
    invoke-static {v3}, Lcom/a/a/an;->a(Ljava/io/OutputStream;)Lcom/a/a/an;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_8

    move-result-object v1

    const/4 v5, 0x1

    :try_start_2
    iget-object v6, p0, Lcom/a/a/az;->n:Ljava/lang/String;

    invoke-static {v6}, Lcom/a/a/aj;->a(Ljava/lang/String;)Lcom/a/a/aj;

    move-result-object v6

    invoke-virtual {v1, v5, v6}, Lcom/a/a/an;->a(ILcom/a/a/aj;)V

    const/4 v5, 0x2

    invoke-static {v4}, Lcom/a/a/aj;->a(Ljava/lang/String;)Lcom/a/a/aj;

    move-result-object v6

    invoke-virtual {v1, v5, v6}, Lcom/a/a/an;->a(ILcom/a/a/aj;)V

    const/4 v5, 0x3

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v6

    const-wide/16 v8, 0x3e8

    div-long/2addr v6, v8

    invoke-virtual {v1, v5, v6, v7}, Lcom/a/a/an;->a(IJ)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_7
    .catchall {:try_start_2 .. :try_end_2} :catchall_9

    invoke-static {v1}, Lcom/a/a/a/ba;->a(Ljava/io/Flushable;)V

    invoke-static {v3}, Lcom/a/a/a/ba;->a(Ljava/io/Closeable;)V

    .line 720
    :try_start_3
    new-instance v3, Lcom/a/a/al;

    invoke-static {}, Lcom/a/a/a/co;->a()Lcom/a/a/a/cl;

    move-result-object v0

    iget-object v0, v0, Lcom/a/a/a/cl;->c:Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, "SessionApp"

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v3, v0, v1}, Lcom/a/a/al;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_4

    :try_start_4
    invoke-static {v3}, Lcom/a/a/an;->a(Ljava/io/OutputStream;)Lcom/a/a/an;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_5

    move-result-object v1

    :try_start_5
    invoke-static {}, Lcom/a/a/d;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/a/a/aj;->a(Ljava/lang/String;)Lcom/a/a/aj;

    move-result-object v0

    invoke-static {}, Lcom/a/a/d;->g()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/a/a/aj;->a(Ljava/lang/String;)Lcom/a/a/aj;

    move-result-object v5

    invoke-static {}, Lcom/a/a/d;->f()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/a/a/aj;->a(Ljava/lang/String;)Lcom/a/a/aj;

    move-result-object v6

    invoke-static {}, Lcom/a/a/d;->h()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/a/a/aj;->a(Ljava/lang/String;)Lcom/a/a/aj;

    invoke-static {}, Lcom/a/a/d;->a()Lcom/a/a/d;

    move-result-object v7

    iget-object v7, v7, Lcom/a/a/a/ch;->g:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getPackageCodePath()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/a/a/aj;->a(Ljava/lang/String;)Lcom/a/a/aj;

    invoke-static {}, Lcom/a/a/d;->a()Lcom/a/a/d;

    move-result-object v7

    iget-object v7, v7, Lcom/a/a/d;->c:Lcom/a/a/a/bm;

    invoke-virtual {v7}, Lcom/a/a/a/bm;->a()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/a/a/aj;->a(Ljava/lang/String;)Lcom/a/a/aj;

    move-result-object v7

    invoke-static {}, Lcom/a/a/d;->e()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/a/a/a/bg;->a(Ljava/lang/String;)Lcom/a/a/a/bg;

    move-result-object v8

    iget v8, v8, Lcom/a/a/a/bg;->a:I

    const/4 v9, 0x7

    const/4 v10, 0x2

    invoke-virtual {v1, v9, v10}, Lcom/a/a/an;->e(II)V

    const/4 v9, 0x1

    invoke-static {v9, v0}, Lcom/a/a/an;->b(ILcom/a/a/aj;)I

    move-result v9

    add-int/lit8 v9, v9, 0x0

    const/4 v10, 0x2

    invoke-static {v10, v5}, Lcom/a/a/an;->b(ILcom/a/a/aj;)I

    move-result v10

    add-int/2addr v9, v10

    const/4 v10, 0x3

    invoke-static {v10, v6}, Lcom/a/a/an;->b(ILcom/a/a/aj;)I

    move-result v10

    add-int/2addr v9, v10

    invoke-static {}, Lcom/a/a/az;->h()I

    move-result v10

    const/4 v11, 0x5

    invoke-static {v11}, Lcom/a/a/an;->e(I)I

    move-result v11

    invoke-static {v10}, Lcom/a/a/an;->g(I)I

    move-result v12

    add-int/2addr v11, v12

    add-int/2addr v10, v11

    add-int/2addr v9, v10

    const/4 v10, 0x6

    invoke-static {v10, v7}, Lcom/a/a/an;->b(ILcom/a/a/aj;)I

    move-result v10

    add-int/2addr v9, v10

    const/16 v10, 0xa

    invoke-static {v10, v8}, Lcom/a/a/an;->d(II)I

    move-result v10

    add-int/2addr v9, v10

    invoke-virtual {v1, v9}, Lcom/a/a/an;->f(I)V

    const/4 v9, 0x1

    invoke-virtual {v1, v9, v0}, Lcom/a/a/an;->a(ILcom/a/a/aj;)V

    const/4 v0, 0x2

    invoke-virtual {v1, v0, v5}, Lcom/a/a/an;->a(ILcom/a/a/aj;)V

    const/4 v0, 0x3

    invoke-virtual {v1, v0, v6}, Lcom/a/a/an;->a(ILcom/a/a/aj;)V

    const/4 v0, 0x5

    const/4 v5, 0x2

    invoke-virtual {v1, v0, v5}, Lcom/a/a/an;->e(II)V

    invoke-static {}, Lcom/a/a/az;->h()I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/a/a/an;->f(I)V

    invoke-static {}, Lcom/a/a/d;->a()Lcom/a/a/d;

    move-result-object v0

    iget-object v0, v0, Lcom/a/a/a/ch;->g:Landroid/content/Context;

    const/4 v5, 0x0

    invoke-static {v0, v5}, Lcom/a/a/a/cj;->a(Landroid/content/Context;Z)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/a/a/an;->a(Ljava/lang/String;)V

    const/4 v0, 0x6

    invoke-virtual {v1, v0, v7}, Lcom/a/a/an;->a(ILcom/a/a/aj;)V

    const/16 v0, 0xa

    invoke-virtual {v1, v0, v8}, Lcom/a/a/an;->b(II)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_6

    invoke-static {v1}, Lcom/a/a/a/ba;->a(Ljava/io/Flushable;)V

    invoke-static {v3}, Lcom/a/a/a/ba;->a(Ljava/io/Closeable;)V

    .line 721
    :try_start_6
    new-instance v1, Lcom/a/a/al;

    invoke-static {}, Lcom/a/a/a/co;->a()Lcom/a/a/a/cl;

    move-result-object v0

    iget-object v0, v0, Lcom/a/a/a/cl;->c:Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "SessionOS"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v0, v3}, Lcom/a/a/al;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_2
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    :try_start_7
    invoke-static {v1}, Lcom/a/a/an;->a(Ljava/io/OutputStream;)Lcom/a/a/an;

    move-result-object v2

    sget-object v0, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    invoke-static {v0}, Lcom/a/a/aj;->a(Ljava/lang/String;)Lcom/a/a/aj;

    move-result-object v0

    sget-object v3, Landroid/os/Build$VERSION;->CODENAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/a/a/aj;->a(Ljava/lang/String;)Lcom/a/a/aj;

    move-result-object v3

    invoke-static {}, Lcom/a/a/a/ba;->g()Z

    move-result v5

    const/16 v6, 0x8

    const/4 v7, 0x2

    invoke-virtual {v2, v6, v7}, Lcom/a/a/an;->e(II)V

    const/4 v6, 0x1

    const/4 v7, 0x3

    invoke-static {v6, v7}, Lcom/a/a/an;->d(II)I

    move-result v6

    add-int/lit8 v6, v6, 0x0

    const/4 v7, 0x2

    invoke-static {v7, v0}, Lcom/a/a/an;->b(ILcom/a/a/aj;)I

    move-result v7

    add-int/2addr v6, v7

    const/4 v7, 0x3

    invoke-static {v7, v3}, Lcom/a/a/an;->b(ILcom/a/a/aj;)I

    move-result v7

    add-int/2addr v6, v7

    const/4 v7, 0x4

    invoke-static {v7}, Lcom/a/a/an;->b(I)I

    move-result v7

    add-int/2addr v6, v7

    invoke-virtual {v2, v6}, Lcom/a/a/an;->f(I)V

    const/4 v6, 0x1

    const/4 v7, 0x3

    invoke-virtual {v2, v6, v7}, Lcom/a/a/an;->b(II)V

    const/4 v6, 0x2

    invoke-virtual {v2, v6, v0}, Lcom/a/a/an;->a(ILcom/a/a/aj;)V

    const/4 v0, 0x3

    invoke-virtual {v2, v0, v3}, Lcom/a/a/an;->a(ILcom/a/a/aj;)V

    const/4 v0, 0x4

    invoke-virtual {v2, v0, v5}, Lcom/a/a/an;->a(IZ)V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_3
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    invoke-static {v2}, Lcom/a/a/a/ba;->a(Ljava/io/Flushable;)V

    invoke-static {v1}, Lcom/a/a/a/ba;->a(Ljava/io/Closeable;)V

    .line 722
    invoke-static {v4}, Lcom/a/a/az;->c(Ljava/lang/String;)V

    .line 723
    return-void

    .line 719
    :catch_0
    move-exception v0

    move-object v1, v2

    :goto_0
    :try_start_8
    invoke-static {v0, v2}, Lcom/a/a/az;->a(Ljava/lang/Throwable;Ljava/io/OutputStream;)V

    throw v0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    :catchall_0
    move-exception v0

    move-object v3, v2

    move-object v2, v1

    :goto_1
    invoke-static {v2}, Lcom/a/a/a/ba;->a(Ljava/io/Flushable;)V

    invoke-static {v3}, Lcom/a/a/a/ba;->a(Ljava/io/Closeable;)V

    throw v0

    .line 720
    :catch_1
    move-exception v0

    move-object v1, v2

    :goto_2
    :try_start_9
    invoke-static {v0, v1}, Lcom/a/a/az;->a(Ljava/lang/Throwable;Ljava/io/OutputStream;)V

    throw v0
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    :catchall_1
    move-exception v0

    move-object v3, v1

    move-object v1, v2

    :goto_3
    invoke-static {v1}, Lcom/a/a/a/ba;->a(Ljava/io/Flushable;)V

    invoke-static {v3}, Lcom/a/a/a/ba;->a(Ljava/io/Closeable;)V

    throw v0

    .line 721
    :catch_2
    move-exception v0

    move-object v1, v2

    :goto_4
    :try_start_a
    invoke-static {v0, v1}, Lcom/a/a/az;->a(Ljava/lang/Throwable;Ljava/io/OutputStream;)V

    throw v0
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_2

    :catchall_2
    move-exception v0

    :goto_5
    invoke-static {v2}, Lcom/a/a/a/ba;->a(Ljava/io/Flushable;)V

    invoke-static {v1}, Lcom/a/a/a/ba;->a(Ljava/io/Closeable;)V

    throw v0

    :catchall_3
    move-exception v0

    move-object v1, v2

    goto :goto_5

    :catch_3
    move-exception v0

    goto :goto_4

    .line 720
    :catchall_4
    move-exception v0

    move-object v1, v2

    move-object v3, v2

    goto :goto_3

    :catchall_5
    move-exception v0

    move-object v1, v2

    goto :goto_3

    :catchall_6
    move-exception v0

    goto :goto_3

    :catch_4
    move-exception v0

    move-object v1, v3

    goto :goto_2

    :catch_5
    move-exception v0

    move-object v2, v1

    move-object v1, v3

    goto :goto_2

    .line 719
    :catchall_7
    move-exception v0

    move-object v3, v2

    goto :goto_1

    :catchall_8
    move-exception v0

    goto :goto_1

    :catchall_9
    move-exception v0

    move-object v2, v1

    goto :goto_1

    :catch_6
    move-exception v0

    move-object v1, v2

    move-object v2, v3

    goto :goto_0

    :catch_7
    move-exception v0

    move-object v2, v3

    goto :goto_0
.end method

.method private e()V
    .locals 20

    .prologue
    .line 729
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    invoke-virtual/range {p0 .. p0}, Lcom/a/a/az;->a()[Ljava/io/File;

    move-result-object v4

    sget-object v2, Lcom/a/a/az;->c:Ljava/util/Comparator;

    invoke-static {v4, v2}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    const/16 v2, 0x8

    array-length v5, v4

    invoke-static {v2, v5}, Ljava/lang/Math;->min(II)I

    move-result v5

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v5, :cond_0

    aget-object v6, v4, v2

    invoke-static {v6}, Lcom/a/a/az;->a(Ljava/io/File;)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v3, v6}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    new-instance v2, Lcom/a/a/s;

    const/4 v4, 0x0

    invoke-direct {v2, v4}, Lcom/a/a/s;-><init>(B)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/a/a/az;->a(Ljava/io/FilenameFilter;)[Ljava/io/File;

    move-result-object v4

    array-length v5, v4

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v5, :cond_2

    aget-object v6, v4, v2

    invoke-virtual {v6}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v7

    sget-object v8, Lcom/a/a/az;->e:Ljava/util/regex/Pattern;

    invoke-virtual {v8, v7}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/regex/Matcher;->matches()Z

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v7

    invoke-interface {v3, v7}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_1

    invoke-static {}, Lcom/a/a/a/co;->a()Lcom/a/a/a/cl;

    move-result-object v7

    invoke-virtual {v7}, Lcom/a/a/a/cl;->a()Lcom/a/a/a/ci;

    move-result-object v7

    invoke-interface {v7}, Lcom/a/a/a/ci;->b()V

    invoke-virtual {v6}, Ljava/io/File;->delete()Z

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 731
    :cond_2
    invoke-direct/range {p0 .. p0}, Lcom/a/a/az;->f()Ljava/lang/String;

    move-result-object v2

    .line 733
    if-eqz v2, :cond_17

    .line 736
    const/4 v5, 0x0

    const/4 v3, 0x0

    :try_start_0
    new-instance v4, Lcom/a/a/al;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/a/a/az;->l:Ljava/io/File;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v7, "SessionUser"

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v4, v6, v2}, Lcom/a/a/al;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_4

    :try_start_1
    invoke-static {v4}, Lcom/a/a/an;->a(Ljava/io/OutputStream;)Lcom/a/a/an;

    move-result-object v3

    invoke-static {}, Lcom/a/a/d;->a()Lcom/a/a/d;

    move-result-object v2

    iget-object v5, v2, Lcom/a/a/d;->c:Lcom/a/a/a/bm;

    iget-boolean v5, v5, Lcom/a/a/a/bm;->b:Z

    if-eqz v5, :cond_7

    iget-object v2, v2, Lcom/a/a/d;->d:Ljava/lang/String;

    :goto_2
    invoke-static {}, Lcom/a/a/d;->a()Lcom/a/a/d;

    move-result-object v5

    iget-object v6, v5, Lcom/a/a/d;->c:Lcom/a/a/a/bm;

    iget-boolean v6, v6, Lcom/a/a/a/bm;->b:Z

    if-eqz v6, :cond_8

    iget-object v5, v5, Lcom/a/a/d;->f:Ljava/lang/String;

    move-object v6, v5

    :goto_3
    invoke-static {}, Lcom/a/a/d;->a()Lcom/a/a/d;

    move-result-object v5

    iget-object v7, v5, Lcom/a/a/d;->c:Lcom/a/a/a/bm;

    iget-boolean v7, v7, Lcom/a/a/a/bm;->b:Z

    if-eqz v7, :cond_9

    iget-object v5, v5, Lcom/a/a/d;->e:Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_5
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_4
    if-nez v2, :cond_a

    if-nez v6, :cond_a

    if-nez v5, :cond_a

    invoke-static {v3}, Lcom/a/a/a/ba;->a(Ljava/io/Flushable;)V

    invoke-static {v4}, Lcom/a/a/a/ba;->a(Ljava/io/Closeable;)V

    .line 738
    :goto_5
    invoke-static {}, Lcom/a/a/d;->a()Lcom/a/a/d;

    invoke-static {}, Lcom/a/a/a/as;->a()Lcom/a/a/a/ar;

    move-result-object v2

    new-instance v3, Lcom/a/a/as;

    invoke-direct {v3}, Lcom/a/a/as;-><init>()V

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/a/a/a/ar;->a(Lcom/a/a/a/at;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/a/a/a/aq;

    .line 740
    if-eqz v2, :cond_15

    .line 741
    iget v9, v2, Lcom/a/a/a/aq;->a:I

    .line 743
    invoke-static {}, Lcom/a/a/a/co;->a()Lcom/a/a/a/cl;

    move-result-object v2

    invoke-virtual {v2}, Lcom/a/a/a/cl;->a()Lcom/a/a/a/ci;

    move-result-object v2

    invoke-interface {v2}, Lcom/a/a/a/ci;->b()V

    .line 744
    invoke-virtual/range {p0 .. p0}, Lcom/a/a/az;->a()[Ljava/io/File;

    move-result-object v10

    .line 746
    if-eqz v10, :cond_16

    array-length v2, v10

    if-lez v2, :cond_16

    .line 747
    array-length v11, v10

    const/4 v2, 0x0

    move v7, v2

    :goto_6
    if-ge v7, v11, :cond_16

    aget-object v12, v10, v7

    .line 748
    invoke-static {v12}, Lcom/a/a/az;->a(Ljava/io/File;)Ljava/lang/String;

    move-result-object v13

    .line 749
    invoke-static {}, Lcom/a/a/a/co;->a()Lcom/a/a/a/cl;

    move-result-object v2

    invoke-virtual {v2}, Lcom/a/a/a/cl;->a()Lcom/a/a/a/ci;

    move-result-object v2

    invoke-interface {v2}, Lcom/a/a/a/ci;->b()V

    .line 750
    invoke-static {}, Lcom/a/a/a/co;->a()Lcom/a/a/a/cl;

    move-result-object v2

    invoke-virtual {v2}, Lcom/a/a/a/cl;->a()Lcom/a/a/a/ci;

    move-result-object v2

    invoke-interface {v2}, Lcom/a/a/a/ci;->b()V

    new-instance v2, Lcom/a/a/t;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "SessionCrash"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/a/a/t;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/a/a/az;->a(Ljava/io/FilenameFilter;)[Ljava/io/File;

    move-result-object v14

    if-eqz v14, :cond_12

    array-length v2, v14

    if-lez v2, :cond_12

    const/4 v2, 0x1

    move v3, v2

    :goto_7
    invoke-static {}, Lcom/a/a/a/co;->a()Lcom/a/a/a/cl;

    move-result-object v2

    invoke-virtual {v2}, Lcom/a/a/a/cl;->a()Lcom/a/a/a/ci;

    move-result-object v2

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v5, "Session %s has fatal exception: %s"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v13, v6, v8

    const/4 v8, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v15

    aput-object v15, v6, v8

    invoke-static {v4, v5, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    invoke-interface {v2}, Lcom/a/a/a/ci;->b()V

    new-instance v2, Lcom/a/a/t;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "SessionEvent"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v4}, Lcom/a/a/t;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/a/a/az;->a(Ljava/io/FilenameFilter;)[Ljava/io/File;

    move-result-object v8

    if-eqz v8, :cond_13

    array-length v2, v8

    if-lez v2, :cond_13

    const/4 v2, 0x1

    :goto_8
    invoke-static {}, Lcom/a/a/a/co;->a()Lcom/a/a/a/cl;

    move-result-object v4

    invoke-virtual {v4}, Lcom/a/a/a/cl;->a()Lcom/a/a/a/ci;

    move-result-object v4

    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v6, "Session %s has non-fatal exceptions: %s"

    const/4 v15, 0x2

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    aput-object v13, v15, v16

    const/16 v16, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v17

    aput-object v17, v15, v16

    invoke-static {v5, v6, v15}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    invoke-interface {v4}, Lcom/a/a/a/ci;->b()V

    if-nez v3, :cond_3

    if-eqz v2, :cond_14

    :cond_3
    const/4 v6, 0x0

    const/4 v4, 0x0

    :try_start_2
    new-instance v5, Lcom/a/a/al;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/a/a/az;->l:Ljava/io/File;

    invoke-direct {v5, v15, v13}, Lcom/a/a/al;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :try_start_3
    invoke-static {v5}, Lcom/a/a/an;->a(Ljava/io/OutputStream;)Lcom/a/a/an;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    move-result-object v4

    :try_start_4
    invoke-static {}, Lcom/a/a/a/co;->a()Lcom/a/a/a/cl;

    move-result-object v6

    invoke-virtual {v6}, Lcom/a/a/a/cl;->a()Lcom/a/a/a/ci;

    move-result-object v6

    invoke-interface {v6}, Lcom/a/a/a/ci;->b()V

    invoke-static {v4, v12}, Lcom/a/a/az;->a(Lcom/a/a/an;Ljava/io/File;)V

    const/4 v6, 0x4

    new-instance v12, Ljava/util/Date;

    invoke-direct {v12}, Ljava/util/Date;-><init>()V

    invoke-virtual {v12}, Ljava/util/Date;->getTime()J

    move-result-wide v16

    const-wide/16 v18, 0x3e8

    div-long v16, v16, v18

    move-wide/from16 v0, v16

    invoke-virtual {v4, v6, v0, v1}, Lcom/a/a/an;->a(IJ)V

    const/4 v6, 0x5

    invoke-virtual {v4, v6, v3}, Lcom/a/a/an;->a(IZ)V

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v13}, Lcom/a/a/az;->a(Lcom/a/a/an;Ljava/lang/String;)V

    if-eqz v2, :cond_4

    array-length v2, v8

    if-le v2, v9, :cond_18

    invoke-static {}, Lcom/a/a/a/co;->a()Lcom/a/a/a/cl;

    move-result-object v2

    invoke-virtual {v2}, Lcom/a/a/a/cl;->a()Lcom/a/a/a/ci;

    move-result-object v2

    sget-object v6, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v8, "Trimming down to %d logged exceptions."

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v15, 0x0

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    aput-object v16, v12, v15

    invoke-static {v6, v8, v12}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    invoke-interface {v2}, Lcom/a/a/a/ci;->b()V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/a/a/az;->l:Ljava/io/File;

    new-instance v6, Lcom/a/a/t;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v12, "SessionEvent"

    invoke-virtual {v8, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v6, v8}, Lcom/a/a/t;-><init>(Ljava/lang/String;)V

    sget-object v8, Lcom/a/a/az;->d:Ljava/util/Comparator;

    invoke-static {v2, v6, v9, v8}, Lcom/a/a/ah;->a(Ljava/io/File;Ljava/io/FilenameFilter;ILjava/util/Comparator;)V

    new-instance v2, Lcom/a/a/t;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, "SessionEvent"

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v2, v6}, Lcom/a/a/t;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/a/a/az;->a(Ljava/io/FilenameFilter;)[Ljava/io/File;

    move-result-object v2

    :goto_9
    invoke-static {v4, v2, v13}, Lcom/a/a/az;->a(Lcom/a/a/an;[Ljava/io/File;Ljava/lang/String;)V

    :cond_4
    if-eqz v3, :cond_5

    const/4 v2, 0x0

    aget-object v2, v14, v2

    invoke-static {v4, v2}, Lcom/a/a/az;->a(Lcom/a/a/an;Ljava/io/File;)V

    :cond_5
    const/16 v2, 0xb

    const/4 v3, 0x1

    invoke-virtual {v4, v2, v3}, Lcom/a/a/an;->a(II)V

    const/16 v2, 0xc

    const/4 v3, 0x3

    invoke-virtual {v4, v2, v3}, Lcom/a/a/an;->b(II)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    invoke-static {v4}, Lcom/a/a/a/ba;->a(Ljava/io/Flushable;)V

    invoke-static {v5}, Lcom/a/a/a/ba;->a(Ljava/io/Closeable;)V

    :cond_6
    :goto_a
    invoke-static {}, Lcom/a/a/a/co;->a()Lcom/a/a/a/cl;

    move-result-object v2

    invoke-virtual {v2}, Lcom/a/a/a/cl;->a()Lcom/a/a/a/ci;

    move-result-object v2

    invoke-interface {v2}, Lcom/a/a/a/ci;->b()V

    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lcom/a/a/az;->a(Ljava/lang/String;)V

    .line 747
    add-int/lit8 v2, v7, 0x1

    move v7, v2

    goto/16 :goto_6

    .line 736
    :cond_7
    const/4 v2, 0x0

    goto/16 :goto_2

    :cond_8
    const/4 v5, 0x0

    move-object v6, v5

    goto/16 :goto_3

    :cond_9
    const/4 v5, 0x0

    goto/16 :goto_4

    :cond_a
    if-nez v2, :cond_b

    :try_start_5
    const-string v2, ""

    :cond_b
    invoke-static {v2}, Lcom/a/a/aj;->a(Ljava/lang/String;)Lcom/a/a/aj;

    move-result-object v7

    if-nez v6, :cond_10

    const/4 v2, 0x0

    move-object v6, v2

    :goto_b
    if-nez v5, :cond_11

    const/4 v2, 0x0

    move-object v5, v2

    :goto_c
    const/4 v2, 0x1

    invoke-static {v2, v7}, Lcom/a/a/an;->b(ILcom/a/a/aj;)I

    move-result v2

    add-int/lit8 v2, v2, 0x0

    if-eqz v6, :cond_c

    const/4 v8, 0x2

    invoke-static {v8, v6}, Lcom/a/a/an;->b(ILcom/a/a/aj;)I

    move-result v8

    add-int/2addr v2, v8

    :cond_c
    if-eqz v5, :cond_d

    const/4 v8, 0x3

    invoke-static {v8, v5}, Lcom/a/a/an;->b(ILcom/a/a/aj;)I

    move-result v8

    add-int/2addr v2, v8

    :cond_d
    const/4 v8, 0x6

    const/4 v9, 0x2

    invoke-virtual {v3, v8, v9}, Lcom/a/a/an;->e(II)V

    invoke-virtual {v3, v2}, Lcom/a/a/an;->f(I)V

    const/4 v2, 0x1

    invoke-virtual {v3, v2, v7}, Lcom/a/a/an;->a(ILcom/a/a/aj;)V

    if-eqz v6, :cond_e

    const/4 v2, 0x2

    invoke-virtual {v3, v2, v6}, Lcom/a/a/an;->a(ILcom/a/a/aj;)V

    :cond_e
    if-eqz v5, :cond_f

    const/4 v2, 0x3

    invoke-virtual {v3, v2, v5}, Lcom/a/a/an;->a(ILcom/a/a/aj;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :cond_f
    invoke-static {v3}, Lcom/a/a/a/ba;->a(Ljava/io/Flushable;)V

    invoke-static {v4}, Lcom/a/a/a/ba;->a(Ljava/io/Closeable;)V

    goto/16 :goto_5

    :cond_10
    :try_start_6
    invoke-static {v6}, Lcom/a/a/aj;->a(Ljava/lang/String;)Lcom/a/a/aj;

    move-result-object v2

    move-object v6, v2

    goto :goto_b

    :cond_11
    invoke-static {v5}, Lcom/a/a/aj;->a(Ljava/lang/String;)Lcom/a/a/aj;
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_5
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    move-result-object v2

    move-object v5, v2

    goto :goto_c

    :catch_0
    move-exception v2

    move-object v4, v5

    :goto_d
    :try_start_7
    invoke-static {v2, v4}, Lcom/a/a/az;->a(Ljava/lang/Throwable;Ljava/io/OutputStream;)V

    throw v2
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    :catchall_0
    move-exception v2

    :goto_e
    invoke-static {v3}, Lcom/a/a/a/ba;->a(Ljava/io/Flushable;)V

    invoke-static {v4}, Lcom/a/a/a/ba;->a(Ljava/io/Closeable;)V

    throw v2

    .line 750
    :cond_12
    const/4 v2, 0x0

    move v3, v2

    goto/16 :goto_7

    :cond_13
    const/4 v2, 0x0

    goto/16 :goto_8

    :catch_1
    move-exception v2

    move-object v3, v4

    move-object v4, v6

    :goto_f
    :try_start_8
    invoke-static {}, Lcom/a/a/a/co;->a()Lcom/a/a/a/cl;

    move-result-object v5

    invoke-virtual {v5}, Lcom/a/a/a/cl;->a()Lcom/a/a/a/ci;

    move-result-object v5

    invoke-interface {v5}, Lcom/a/a/a/ci;->a()V

    invoke-static {v2, v4}, Lcom/a/a/az;->a(Ljava/lang/Throwable;Ljava/io/OutputStream;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    invoke-static {v3}, Lcom/a/a/a/ba;->a(Ljava/io/Flushable;)V

    if-eqz v4, :cond_6

    :try_start_9
    invoke-virtual {v4}, Lcom/a/a/al;->a()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_2

    goto/16 :goto_a

    :catch_2
    move-exception v2

    invoke-static {}, Lcom/a/a/a/co;->a()Lcom/a/a/a/cl;

    move-result-object v2

    invoke-virtual {v2}, Lcom/a/a/a/cl;->a()Lcom/a/a/a/ci;

    move-result-object v2

    invoke-interface {v2}, Lcom/a/a/a/ci;->a()V

    goto/16 :goto_a

    :catchall_1
    move-exception v2

    move-object v5, v6

    :goto_10
    invoke-static {v4}, Lcom/a/a/a/ba;->a(Ljava/io/Flushable;)V

    invoke-static {v5}, Lcom/a/a/a/ba;->a(Ljava/io/Closeable;)V

    throw v2

    :cond_14
    invoke-static {}, Lcom/a/a/a/co;->a()Lcom/a/a/a/cl;

    move-result-object v2

    invoke-virtual {v2}, Lcom/a/a/a/cl;->a()Lcom/a/a/a/ci;

    move-result-object v2

    invoke-interface {v2}, Lcom/a/a/a/ci;->b()V

    goto/16 :goto_a

    .line 754
    :cond_15
    invoke-static {}, Lcom/a/a/a/co;->a()Lcom/a/a/a/cl;

    move-result-object v2

    invoke-virtual {v2}, Lcom/a/a/a/cl;->a()Lcom/a/a/a/ci;

    move-result-object v2

    invoke-interface {v2}, Lcom/a/a/a/ci;->b()V

    .line 759
    :cond_16
    :goto_11
    return-void

    .line 757
    :cond_17
    invoke-static {}, Lcom/a/a/a/co;->a()Lcom/a/a/a/cl;

    move-result-object v2

    invoke-virtual {v2}, Lcom/a/a/a/cl;->a()Lcom/a/a/a/ci;

    move-result-object v2

    invoke-interface {v2}, Lcom/a/a/a/ci;->b()V

    goto :goto_11

    .line 750
    :catchall_2
    move-exception v2

    goto :goto_10

    :catchall_3
    move-exception v2

    move-object v5, v4

    move-object v4, v3

    goto :goto_10

    :catch_3
    move-exception v2

    move-object v3, v4

    move-object v4, v5

    goto :goto_f

    :catch_4
    move-exception v2

    move-object v3, v4

    move-object v4, v5

    goto :goto_f

    .line 736
    :catchall_4
    move-exception v2

    move-object v4, v5

    goto :goto_e

    :catch_5
    move-exception v2

    goto :goto_d

    :cond_18
    move-object v2, v8

    goto/16 :goto_9
.end method

.method private f()Ljava/lang/String;
    .locals 2

    .prologue
    .line 771
    new-instance v0, Lcom/a/a/t;

    const-string v1, "BeginSession"

    invoke-direct {v0, v1}, Lcom/a/a/t;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/a/a/az;->a(Ljava/io/FilenameFilter;)[Ljava/io/File;

    move-result-object v0

    .line 772
    sget-object v1, Lcom/a/a/az;->c:Ljava/util/Comparator;

    invoke-static {v0, v1}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    .line 773
    array-length v1, v0

    if-lez v1, :cond_0

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-static {v0}, Lcom/a/a/az;->a(Ljava/io/File;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private g()V
    .locals 6

    .prologue
    .line 1003
    sget-object v0, Lcom/a/a/az;->a:Ljava/io/FilenameFilter;

    invoke-virtual {p0, v0}, Lcom/a/a/az;->a(Ljava/io/FilenameFilter;)[Ljava/io/File;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 1005
    invoke-static {}, Lcom/a/a/a/co;->a()Lcom/a/a/a/cl;

    move-result-object v4

    invoke-virtual {v4}, Lcom/a/a/a/cl;->a()Lcom/a/a/a/ci;

    move-result-object v4

    invoke-interface {v4}, Lcom/a/a/a/ci;->b()V

    .line 1007
    new-instance v4, Ljava/lang/Thread;

    new-instance v5, Lcom/a/a/f;

    invoke-direct {v5, v3}, Lcom/a/a/f;-><init>(Ljava/io/File;)V

    const-string v3, "Crashlytics Report Uploader"

    invoke-direct {v4, v5, v3}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/lang/Thread;->start()V

    .line 1003
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1019
    :cond_0
    return-void
.end method

.method private static h()I
    .locals 3

    .prologue
    .line 1160
    invoke-static {}, Lcom/a/a/d;->a()Lcom/a/a/d;

    move-result-object v0

    iget-object v0, v0, Lcom/a/a/a/ch;->g:Landroid/content/Context;

    .line 1162
    invoke-static {}, Lcom/a/a/a/co;->a()Lcom/a/a/a/cl;

    move-result-object v1

    iget-boolean v1, v1, Lcom/a/a/a/cl;->b:Z

    .line 1163
    const/4 v2, 0x1

    invoke-static {v0, v1}, Lcom/a/a/a/cj;->a(Landroid/content/Context;Z)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/a/a/aj;->a(Ljava/lang/String;)Lcom/a/a/aj;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/a/a/an;->b(ILcom/a/a/aj;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1165
    return v0
.end method

.method private i()I
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 1536
    const/4 v0, 0x1

    invoke-static {v0, v2, v3}, Lcom/a/a/an;->b(IJ)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1539
    const/4 v1, 0x2

    invoke-static {v1, v2, v3}, Lcom/a/a/an;->b(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 1540
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/a/a/az;->q:Lcom/a/a/aj;

    invoke-static {v1, v2}, Lcom/a/a/an;->b(ILcom/a/a/aj;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1541
    iget-object v1, p0, Lcom/a/a/az;->r:Lcom/a/a/aj;

    if-eqz v1, :cond_0

    .line 1542
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/a/a/az;->r:Lcom/a/a/aj;

    invoke-static {v1, v2}, Lcom/a/a/an;->b(ILcom/a/a/aj;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1545
    :cond_0
    return v0
.end method

.method private static j()I
    .locals 4

    .prologue
    .line 1834
    const/4 v0, 0x1

    sget-object v1, Lcom/a/a/az;->g:Lcom/a/a/aj;

    invoke-static {v0, v1}, Lcom/a/a/an;->b(ILcom/a/a/aj;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1837
    const/4 v1, 0x2

    sget-object v2, Lcom/a/a/az;->g:Lcom/a/a/aj;

    invoke-static {v1, v2}, Lcom/a/a/an;->b(ILcom/a/a/aj;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1838
    const/4 v1, 0x3

    const-wide/16 v2, 0x0

    invoke-static {v1, v2, v3}, Lcom/a/a/an;->b(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 1840
    return v0
.end method


# virtual methods
.method final a(Ljava/util/concurrent/Callable;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/Callable",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1936
    :try_start_0
    iget-object v1, p0, Lcom/a/a/az;->s:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v1, p1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/concurrent/Future;->get()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/util/concurrent/RejectedExecutionException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 1942
    :goto_0
    return-object v0

    .line 1938
    :catch_0
    move-exception v1

    invoke-static {}, Lcom/a/a/a/co;->a()Lcom/a/a/a/cl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/a/a/a/cl;->a()Lcom/a/a/a/ci;

    move-result-object v1

    invoke-interface {v1}, Lcom/a/a/a/ci;->b()V

    goto :goto_0

    .line 1941
    :catch_1
    move-exception v1

    invoke-static {}, Lcom/a/a/a/co;->a()Lcom/a/a/a/cl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/a/a/a/cl;->a()Lcom/a/a/a/ci;

    move-result-object v1

    invoke-interface {v1}, Lcom/a/a/a/ci;->a()V

    goto :goto_0
.end method

.method final a(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Runnable;",
            ")",
            "Ljava/util/concurrent/Future",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 1958
    :try_start_0
    iget-object v0, p0, Lcom/a/a/az;->s:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/a/a/i;

    invoke-direct {v1, p1}, Lcom/a/a/i;-><init>(Ljava/lang/Runnable;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;
    :try_end_0
    .catch Ljava/util/concurrent/RejectedExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1970
    :goto_0
    return-object v0

    .line 1969
    :catch_0
    move-exception v0

    invoke-static {}, Lcom/a/a/a/co;->a()Lcom/a/a/a/cl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/a/a/cl;->a()Lcom/a/a/a/ci;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/a/a/ci;->b()V

    .line 1970
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final a()[Ljava/io/File;
    .locals 2

    .prologue
    .line 930
    new-instance v0, Lcom/a/a/t;

    const-string v1, "BeginSession"

    invoke-direct {v0, v1}, Lcom/a/a/t;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/a/a/az;->a(Ljava/io/FilenameFilter;)[Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method final a(Ljava/io/FilenameFilter;)[Ljava/io/File;
    .locals 1

    .prologue
    .line 941
    iget-object v0, p0, Lcom/a/a/az;->l:Ljava/io/File;

    invoke-virtual {v0, p1}, Ljava/io/File;->listFiles(Ljava/io/FilenameFilter;)[Ljava/io/File;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/io/File;

    :cond_0
    return-object v0
.end method

.method final b(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/Callable",
            "<TT;>;)",
            "Ljava/util/concurrent/Future",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 1986
    :try_start_0
    iget-object v0, p0, Lcom/a/a/az;->s:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/a/a/k;

    invoke-direct {v1, p1}, Lcom/a/a/k;-><init>(Ljava/util/concurrent/Callable;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;
    :try_end_0
    .catch Ljava/util/concurrent/RejectedExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1999
    :goto_0
    return-object v0

    .line 1998
    :catch_0
    move-exception v0

    invoke-static {}, Lcom/a/a/a/co;->a()Lcom/a/a/a/cl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/a/a/cl;->a()Lcom/a/a/a/ci;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/a/a/ci;->b()V

    .line 1999
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final declared-synchronized uncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 293
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/a/a/az;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 295
    :try_start_1
    invoke-static {}, Lcom/a/a/a/co;->a()Lcom/a/a/a/cl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/a/a/cl;->a()Lcom/a/a/a/ci;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Crashlytics is handling uncaught exception \""

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\" from thread "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {v0}, Lcom/a/a/a/ci;->b()V

    .line 297
    iget-object v0, p0, Lcom/a/a/az;->i:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 298
    invoke-static {}, Lcom/a/a/a/co;->a()Lcom/a/a/a/cl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/a/a/cl;->a()Lcom/a/a/a/ci;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/a/a/ci;->b()V

    .line 299
    invoke-static {}, Lcom/a/a/d;->a()Lcom/a/a/d;

    move-result-object v0

    iget-object v0, v0, Lcom/a/a/a/ch;->g:Landroid/content/Context;

    iget-object v1, p0, Lcom/a/a/az;->p:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 300
    invoke-static {}, Lcom/a/a/d;->a()Lcom/a/a/d;

    move-result-object v0

    iget-object v0, v0, Lcom/a/a/a/ch;->g:Landroid/content/Context;

    iget-object v1, p0, Lcom/a/a/az;->o:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 305
    :cond_0
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    .line 307
    new-instance v1, Lcom/a/a/p;

    invoke-direct {v1, p0, v0, p1, p2}, Lcom/a/a/p;-><init>(Lcom/a/a/az;Ljava/util/Date;Ljava/lang/Thread;Ljava/lang/Throwable;)V

    invoke-virtual {p0, v1}, Lcom/a/a/az;->a(Ljava/util/concurrent/Callable;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 317
    :try_start_2
    invoke-static {}, Lcom/a/a/a/co;->a()Lcom/a/a/a/cl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/a/a/cl;->a()Lcom/a/a/a/ci;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/a/a/ci;->b()V

    .line 318
    iget-object v0, p0, Lcom/a/a/az;->k:Ljava/lang/Thread$UncaughtExceptionHandler;

    invoke-interface {v0, p1, p2}, Ljava/lang/Thread$UncaughtExceptionHandler;->uncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V

    .line 319
    iget-object v0, p0, Lcom/a/a/az;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 320
    :goto_0
    monitor-exit p0

    return-void

    .line 315
    :catch_0
    move-exception v0

    :try_start_3
    invoke-static {}, Lcom/a/a/a/co;->a()Lcom/a/a/a/cl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/a/a/cl;->a()Lcom/a/a/a/ci;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/a/a/ci;->a()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 317
    :try_start_4
    invoke-static {}, Lcom/a/a/a/co;->a()Lcom/a/a/a/cl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/a/a/cl;->a()Lcom/a/a/a/ci;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/a/a/ci;->b()V

    .line 318
    iget-object v0, p0, Lcom/a/a/az;->k:Ljava/lang/Thread$UncaughtExceptionHandler;

    invoke-interface {v0, p1, p2}, Ljava/lang/Thread$UncaughtExceptionHandler;->uncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V

    .line 319
    iget-object v0, p0, Lcom/a/a/az;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 293
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 317
    :catchall_1
    move-exception v0

    :try_start_5
    invoke-static {}, Lcom/a/a/a/co;->a()Lcom/a/a/a/cl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/a/a/a/cl;->a()Lcom/a/a/a/ci;

    move-result-object v1

    invoke-interface {v1}, Lcom/a/a/a/ci;->b()V

    .line 318
    iget-object v1, p0, Lcom/a/a/az;->k:Ljava/lang/Thread$UncaughtExceptionHandler;

    invoke-interface {v1, p1, p2}, Ljava/lang/Thread$UncaughtExceptionHandler;->uncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V

    .line 319
    iget-object v1, p0, Lcom/a/a/az;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0
.end method
