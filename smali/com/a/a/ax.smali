.class final Lcom/a/a/ax;
.super Lcom/a/a/a/az;
.source "SourceFile"


# instance fields
.field private synthetic a:Landroid/content/Context;

.field private synthetic b:F

.field private synthetic c:Ljava/util/concurrent/CountDownLatch;

.field private synthetic d:Lcom/a/a/d;


# direct methods
.method constructor <init>(Lcom/a/a/d;Landroid/content/Context;FLjava/util/concurrent/CountDownLatch;)V
    .locals 0

    .prologue
    .line 835
    iput-object p1, p0, Lcom/a/a/ax;->d:Lcom/a/a/d;

    iput-object p2, p0, Lcom/a/a/ax;->a:Landroid/content/Context;

    iput p3, p0, Lcom/a/a/ax;->b:F

    iput-object p4, p0, Lcom/a/a/ax;->c:Ljava/util/concurrent/CountDownLatch;

    invoke-direct {p0}, Lcom/a/a/a/az;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 839
    :try_start_0
    iget-object v0, p0, Lcom/a/a/ax;->d:Lcom/a/a/d;

    iget-object v1, p0, Lcom/a/a/ax;->a:Landroid/content/Context;

    iget v2, p0, Lcom/a/a/ax;->b:F

    invoke-static {v0, v1, v2}, Lcom/a/a/d;->a(Lcom/a/a/d;Landroid/content/Context;F)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 842
    iget-object v0, p0, Lcom/a/a/ax;->d:Lcom/a/a/d;

    invoke-static {v0}, Lcom/a/a/d;->a(Lcom/a/a/d;)Lcom/a/a/az;

    move-result-object v0

    new-instance v1, Lcom/a/a/b;

    invoke-direct {v1, v0}, Lcom/a/a/b;-><init>(Lcom/a/a/az;)V

    invoke-virtual {v0, v1}, Lcom/a/a/az;->b(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 847
    :cond_0
    iget-object v0, p0, Lcom/a/a/ax;->c:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 848
    :goto_0
    return-void

    .line 845
    :catch_0
    move-exception v0

    :try_start_1
    invoke-static {}, Lcom/a/a/a/co;->a()Lcom/a/a/a/cl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/a/a/cl;->a()Lcom/a/a/a/ci;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/a/a/ci;->a()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 847
    iget-object v0, p0, Lcom/a/a/ax;->c:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/a/a/ax;->c:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v1}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    throw v0
.end method
