.class public final Lcom/a/a/a/av;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Lcom/a/a/a/ay;

.field private final b:Lcom/a/a/a/ax;

.field private final c:Lcom/a/a/a/bf;

.field private final d:Lcom/a/a/a/am;

.field private final e:Lcom/a/a/a/bz;


# direct methods
.method public constructor <init>(Lcom/a/a/a/ay;Lcom/a/a/a/bf;Lcom/a/a/a/ax;Lcom/a/a/a/am;Lcom/a/a/a/bz;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/a/a/a/av;->a:Lcom/a/a/a/ay;

    .line 29
    iput-object p2, p0, Lcom/a/a/a/av;->c:Lcom/a/a/a/bf;

    .line 30
    iput-object p3, p0, Lcom/a/a/a/av;->b:Lcom/a/a/a/ax;

    .line 31
    iput-object p4, p0, Lcom/a/a/a/av;->d:Lcom/a/a/a/am;

    .line 32
    iput-object p5, p0, Lcom/a/a/a/av;->e:Lcom/a/a/a/bz;

    .line 33
    return-void
.end method

.method private static a(Lorg/json/JSONObject;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 103
    invoke-static {}, Lcom/a/a/a/co;->a()Lcom/a/a/a/cl;

    move-result-object v0

    iget-object v0, v0, Lcom/a/a/a/ch;->g:Landroid/content/Context;

    invoke-static {v0}, Lcom/a/a/a/ba;->e(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 104
    new-instance v0, Lorg/json/JSONObject;

    invoke-virtual {p0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string v1, "features"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    const-string v2, "collect_analytics"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->remove(Ljava/lang/String;)Ljava/lang/Object;

    const-string v1, "analytics"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->remove(Ljava/lang/String;)Ljava/lang/Object;

    move-object p0, v0

    .line 107
    :cond_0
    invoke-static {}, Lcom/a/a/a/co;->a()Lcom/a/a/a/cl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/a/a/cl;->a()Lcom/a/a/a/ci;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {v0}, Lcom/a/a/a/ci;->b()V

    .line 108
    return-void
.end method

.method private static b(Lcom/a/a/a/au;)Lcom/a/a/a/aw;
    .locals 6

    .prologue
    .line 68
    const/4 v0, 0x0

    .line 71
    :try_start_0
    sget-object v1, Lcom/a/a/a/au;->b:Lcom/a/a/a/au;

    invoke-virtual {v1, p0}, Lcom/a/a/a/au;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 72
    invoke-static {}, Lcom/a/a/a/am;->a()Lorg/json/JSONObject;

    move-result-object v2

    .line 74
    if-eqz v2, :cond_4

    .line 75
    invoke-static {v2}, Lcom/a/a/a/ax;->a(Lorg/json/JSONObject;)Lcom/a/a/a/aw;

    move-result-object v1

    .line 78
    const-string v3, "Loaded cached settings: "

    invoke-static {v2, v3}, Lcom/a/a/a/av;->a(Lorg/json/JSONObject;Ljava/lang/String;)V

    .line 80
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 82
    sget-object v4, Lcom/a/a/a/au;->c:Lcom/a/a/a/au;

    invoke-virtual {v4, p0}, Lcom/a/a/a/au;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    iget-wide v4, v1, Lcom/a/a/a/aw;->f:J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    cmp-long v2, v4, v2

    if-gez v2, :cond_2

    const/4 v2, 0x1

    :goto_0
    if-nez v2, :cond_3

    .line 84
    :cond_0
    :try_start_1
    invoke-static {}, Lcom/a/a/a/co;->a()Lcom/a/a/a/cl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/a/a/cl;->a()Lcom/a/a/a/ci;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/a/a/ci;->b()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-object v0, v1

    .line 99
    :cond_1
    :goto_1
    return-object v0

    .line 82
    :cond_2
    const/4 v2, 0x0

    goto :goto_0

    .line 86
    :cond_3
    :try_start_2
    invoke-static {}, Lcom/a/a/a/co;->a()Lcom/a/a/a/cl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/a/a/a/cl;->a()Lcom/a/a/a/ci;

    move-result-object v1

    invoke-interface {v1}, Lcom/a/a/a/ci;->b()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_1

    .line 96
    :catch_0
    move-exception v1

    :goto_2
    invoke-static {}, Lcom/a/a/a/co;->a()Lcom/a/a/a/cl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/a/a/a/cl;->a()Lcom/a/a/a/ci;

    move-result-object v1

    invoke-interface {v1}, Lcom/a/a/a/ci;->a()V

    goto :goto_1

    .line 89
    :cond_4
    :try_start_3
    invoke-static {}, Lcom/a/a/a/co;->a()Lcom/a/a/a/cl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/a/a/a/cl;->a()Lcom/a/a/a/ci;

    move-result-object v1

    invoke-interface {v1}, Lcom/a/a/a/ci;->b()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_1

    .line 96
    :catch_1
    move-exception v0

    move-object v0, v1

    goto :goto_2
.end method


# virtual methods
.method public final a(Lcom/a/a/a/au;)Lcom/a/a/a/aw;
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 40
    .line 43
    :try_start_0
    invoke-static {}, Lcom/a/a/a/co;->a()Lcom/a/a/a/cl;

    move-result-object v0

    iget-boolean v0, v0, Lcom/a/a/a/cl;->b:Z

    if-nez v0, :cond_3

    .line 44
    invoke-static {p1}, Lcom/a/a/a/av;->b(Lcom/a/a/a/au;)Lcom/a/a/a/aw;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v0

    .line 47
    :goto_0
    if-nez v0, :cond_1

    .line 48
    :try_start_1
    iget-object v2, p0, Lcom/a/a/a/av;->e:Lcom/a/a/a/bz;

    iget-object v3, p0, Lcom/a/a/a/av;->a:Lcom/a/a/a/ay;

    invoke-interface {v2, v3}, Lcom/a/a/a/bz;->a(Lcom/a/a/a/ay;)Lorg/json/JSONObject;

    move-result-object v3

    .line 50
    if-eqz v3, :cond_1

    .line 51
    invoke-static {v3}, Lcom/a/a/a/ax;->a(Lorg/json/JSONObject;)Lcom/a/a/a/aw;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3

    move-result-object v2

    .line 52
    :try_start_2
    iget-wide v4, v2, Lcom/a/a/a/aw;->f:J

    invoke-static {}, Lcom/a/a/a/co;->a()Lcom/a/a/a/cl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/a/a/cl;->a()Lcom/a/a/a/ci;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/a/a/ci;->b()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    if-eqz v3, :cond_0

    :try_start_3
    const-string v0, "expires_at"

    invoke-virtual {v3, v0, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    new-instance v0, Ljava/io/FileWriter;

    new-instance v4, Ljava/io/File;

    invoke-static {}, Lcom/a/a/a/co;->a()Lcom/a/a/a/cl;

    move-result-object v5

    iget-object v5, v5, Lcom/a/a/a/cl;->c:Ljava/io/File;

    const-string v6, "com.crashlytics.settings.json"

    invoke-direct {v4, v5, v6}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-direct {v0, v4}, Ljava/io/FileWriter;-><init>(Ljava/io/File;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    invoke-virtual {v3}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/FileWriter;->flush()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :try_start_5
    invoke-static {v0}, Lcom/a/a/a/ba;->a(Ljava/io/Closeable;)V

    .line 53
    :cond_0
    :goto_1
    const-string v0, "Loaded settings: "

    invoke-static {v3, v0}, Lcom/a/a/a/av;->a(Lorg/json/JSONObject;Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1

    move-object v0, v2

    .line 58
    :cond_1
    if-nez v0, :cond_2

    .line 59
    :try_start_6
    sget-object v1, Lcom/a/a/a/au;->c:Lcom/a/a/a/au;

    invoke-static {v1}, Lcom/a/a/a/av;->b(Lcom/a/a/a/au;)Lcom/a/a/a/aw;
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_3

    move-result-object v0

    .line 64
    :cond_2
    :goto_2
    return-object v0

    .line 52
    :catch_0
    move-exception v0

    move-object v0, v1

    :goto_3
    :try_start_7
    invoke-static {}, Lcom/a/a/a/co;->a()Lcom/a/a/a/cl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/a/a/a/cl;->a()Lcom/a/a/a/ci;

    move-result-object v1

    invoke-interface {v1}, Lcom/a/a/a/ci;->a()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    :try_start_8
    invoke-static {v0}, Lcom/a/a/a/ba;->a(Ljava/io/Closeable;)V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_1

    goto :goto_1

    .line 62
    :catch_1
    move-exception v0

    move-object v0, v2

    :goto_4
    invoke-static {}, Lcom/a/a/a/co;->a()Lcom/a/a/a/cl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/a/a/a/cl;->a()Lcom/a/a/a/ci;

    move-result-object v1

    invoke-interface {v1}, Lcom/a/a/a/ci;->a()V

    goto :goto_2

    .line 52
    :catchall_0
    move-exception v0

    :goto_5
    :try_start_9
    invoke-static {v1}, Lcom/a/a/a/ba;->a(Ljava/io/Closeable;)V

    throw v0
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_1

    .line 62
    :catch_2
    move-exception v0

    move-object v0, v1

    goto :goto_4

    :catch_3
    move-exception v1

    goto :goto_4

    .line 52
    :catchall_1
    move-exception v1

    move-object v7, v1

    move-object v1, v0

    move-object v0, v7

    goto :goto_5

    :catch_4
    move-exception v1

    goto :goto_3

    :cond_3
    move-object v0, v1

    goto/16 :goto_0
.end method
