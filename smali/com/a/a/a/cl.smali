.class public final Lcom/a/a/a/cl;
.super Lcom/a/a/a/ch;
.source "SourceFile"


# instance fields
.field public a:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "Lcom/a/a/a/ci;",
            ">;"
        }
    .end annotation
.end field

.field public b:Z

.field public c:Ljava/io/File;

.field d:Landroid/app/Application;

.field public e:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/app/Activity;",
            ">;"
        }
    .end annotation
.end field

.field f:Ljava/lang/String;

.field h:I

.field private i:Lcom/a/a/a/by;

.field private j:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Lcom/a/a/a/ck;",
            ">;",
            "Lcom/a/a/a/ck;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    .line 74
    invoke-direct {p0}, Lcom/a/a/a/ch;-><init>()V

    .line 33
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    iput-object v0, p0, Lcom/a/a/a/cl;->a:Ljava/util/concurrent/atomic/AtomicReference;

    .line 49
    const/4 v0, 0x4

    iput v0, p0, Lcom/a/a/a/cl;->h:I

    .line 75
    new-instance v0, Lcom/a/a/a/b;

    sget-object v1, Lcom/a/a/a/ce;->a:Lcom/a/a/a/ce;

    invoke-direct {v0, v1}, Lcom/a/a/a/b;-><init>(Lcom/a/a/a/ce;)V

    iput-object v0, p0, Lcom/a/a/a/cl;->i:Lcom/a/a/a/by;

    .line 76
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/a/a/a/cl;->j:Ljava/util/concurrent/ConcurrentHashMap;

    .line 77
    return-void
.end method

.method public static varargs declared-synchronized a(Landroid/content/Context;[Lcom/a/a/a/ck;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 89
    const-class v3, Lcom/a/a/a/cl;

    monitor-enter v3

    :try_start_0
    invoke-static {}, Lcom/a/a/a/co;->a()Lcom/a/a/a/cl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/a/a/a/cl;->o()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-eqz v1, :cond_0

    .line 98
    :goto_0
    monitor-exit v3

    return-void

    .line 93
    :cond_0
    :try_start_1
    invoke-static {}, Lcom/a/a/a/co;->a()Lcom/a/a/a/cl;

    move-result-object v4

    instance-of v1, p0, Landroid/app/Application;

    if-eqz v1, :cond_2

    move-object v0, p0

    check-cast v0, Landroid/app/Application;

    move-object v1, v0

    :goto_1
    iput-object v1, v4, Lcom/a/a/a/cl;->d:Landroid/app/Application;

    instance-of v1, p0, Landroid/app/Activity;

    if-eqz v1, :cond_6

    move-object v0, p0

    check-cast v0, Landroid/app/Activity;

    move-object v1, v0

    :goto_2
    invoke-virtual {v4, v1}, Lcom/a/a/a/cl;->a(Landroid/app/Activity;)Lcom/a/a/a/cl;

    move-result-object v2

    const/4 v1, 0x0

    :goto_3
    const/4 v4, 0x2

    if-ge v1, v4, :cond_7

    aget-object v4, p1, v1

    iget-object v5, v2, Lcom/a/a/a/cl;->j:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v5, p1}, Ljava/util/concurrent/ConcurrentHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    iget-object v5, v2, Lcom/a/a/a/cl;->j:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v5, v6, v4}, Ljava/util/concurrent/ConcurrentHashMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :cond_2
    instance-of v1, p0, Landroid/app/Activity;

    if-eqz v1, :cond_3

    move-object v0, p0

    check-cast v0, Landroid/app/Activity;

    move-object v1, v0

    invoke-virtual {v1}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v1

    goto :goto_1

    :cond_3
    instance-of v1, p0, Landroid/app/Service;

    if-eqz v1, :cond_4

    move-object v0, p0

    check-cast v0, Landroid/app/Service;

    move-object v1, v0

    invoke-virtual {v1}, Landroid/app/Service;->getApplication()Landroid/app/Application;

    move-result-object v1

    goto :goto_1

    :cond_4
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    instance-of v1, v1, Landroid/app/Application;

    if-eqz v1, :cond_5

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    check-cast v1, Landroid/app/Application;

    goto :goto_1

    :cond_5
    move-object v1, v2

    goto :goto_1

    :cond_6
    move-object v1, v2

    goto :goto_2

    :cond_7
    invoke-virtual {v2, p0}, Lcom/a/a/a/cl;->b(Landroid/content/Context;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 89
    :catchall_0
    move-exception v1

    monitor-exit v3

    throw v1
.end method


# virtual methods
.method public final a()Lcom/a/a/a/ci;
    .locals 3

    .prologue
    .line 106
    iget-object v0, p0, Lcom/a/a/a/cl;->a:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/a/a/ci;

    .line 108
    if-nez v0, :cond_0

    .line 109
    new-instance v0, Lcom/a/a/a/cj;

    invoke-direct {v0}, Lcom/a/a/a/cj;-><init>()V

    .line 111
    iget-object v1, p0, Lcom/a/a/a/cl;->a:Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v0}, Ljava/util/concurrent/atomic/AtomicReference;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 112
    iget-object v0, p0, Lcom/a/a/a/cl;->a:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/a/a/ci;

    .line 115
    :cond_0
    return-object v0
.end method

.method public final a(Ljava/lang/Class;)Lcom/a/a/a/ck;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/a/a/a/ck;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 213
    iget-object v0, p0, Lcom/a/a/a/cl;->j:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/a/a/ck;

    return-object v0
.end method

.method final a(Landroid/app/Activity;)Lcom/a/a/a/cl;
    .locals 1

    .prologue
    .line 140
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/a/a/a/cl;->e:Ljava/lang/ref/WeakReference;

    .line 141
    return-object p0
.end method

.method protected final b()V
    .locals 8

    .prologue
    .line 157
    iget-object v1, p0, Lcom/a/a/a/ch;->g:Landroid/content/Context;

    .line 158
    new-instance v0, Ljava/io/File;

    invoke-virtual {v1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v2

    const-string v3, "com.crashlytics.sdk.android"

    invoke-direct {v0, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/a/a/a/cl;->c:Ljava/io/File;

    iget-object v0, p0, Lcom/a/a/a/cl;->c:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/a/a/a/cl;->c:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 160
    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xe

    if-lt v0, v2, :cond_1

    .line 161
    new-instance v0, Lcom/a/a/a/cm;

    const/4 v2, 0x0

    invoke-direct {v0, p0, v2}, Lcom/a/a/a/cm;-><init>(Lcom/a/a/a/cl;B)V

    iget-object v2, p0, Lcom/a/a/a/cl;->d:Landroid/app/Application;

    if-eqz v2, :cond_1

    new-instance v3, Lcom/a/a/a/cn;

    invoke-direct {v3, v0}, Lcom/a/a/a/cn;-><init>(Lcom/a/a/a/cm;)V

    invoke-virtual {v2, v3}, Landroid/app/Application;->registerActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V

    .line 165
    :cond_1
    iget-boolean v0, p0, Lcom/a/a/a/cl;->b:Z

    if-eqz v0, :cond_2

    const-string v0, "CrashlyticsInternal"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 166
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 167
    iget-object v0, p0, Lcom/a/a/a/cl;->j:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/a/a/ch;

    .line 168
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v4

    .line 169
    invoke-virtual {v0, v1}, Lcom/a/a/a/ch;->b(Landroid/content/Context;)V

    .line 172
    const-string v6, "sdkPerfStart."

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v6, 0x3d

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v6

    sub-long v4, v6, v4

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v4, 0xa

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 181
    :cond_2
    iget-object v0, p0, Lcom/a/a/a/cl;->j:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/a/a/ch;

    .line 182
    invoke-virtual {v0, v1}, Lcom/a/a/a/ch;->b(Landroid/content/Context;)V

    goto :goto_1

    .line 185
    :cond_3
    return-void
.end method
