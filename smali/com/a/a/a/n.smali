.class public Lcom/a/a/a/n;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/a/a/a/bi;


# instance fields
.field final a:Ljava/lang/String;

.field final b:Ljava/lang/String;

.field final c:Ljava/lang/String;

.field final d:Ljava/lang/String;

.field final e:Ljava/lang/String;

.field final f:Ljava/lang/String;

.field final g:Ljava/lang/String;

.field final h:Ljava/lang/String;

.field public final i:Ljava/util/concurrent/ScheduledExecutorService;

.field j:Lcom/a/a/a/t;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/a/a/a/j;Lcom/a/a/a/bt;)V
    .locals 11

    .prologue
    .line 37
    const-string v0, "Crashlytics SAM"

    invoke-static {v0}, Lcom/a/a/a/bf;->a(Ljava/lang/String;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v9

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v10, p9

    invoke-direct/range {v0 .. v10}, Lcom/a/a/a/n;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/a/a/a/j;Ljava/util/concurrent/ScheduledExecutorService;Lcom/a/a/a/bt;)V

    .line 41
    return-void
.end method

.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/a/a/a/j;Ljava/util/concurrent/ScheduledExecutorService;Lcom/a/a/a/bt;)V
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    iput-object p1, p0, Lcom/a/a/a/n;->a:Ljava/lang/String;

    .line 49
    iput-object p2, p0, Lcom/a/a/a/n;->b:Ljava/lang/String;

    .line 50
    iput-object p3, p0, Lcom/a/a/a/n;->c:Ljava/lang/String;

    .line 51
    iput-object p4, p0, Lcom/a/a/a/n;->d:Ljava/lang/String;

    .line 52
    iput-object p5, p0, Lcom/a/a/a/n;->e:Ljava/lang/String;

    .line 53
    iput-object p6, p0, Lcom/a/a/a/n;->f:Ljava/lang/String;

    .line 54
    iput-object p7, p0, Lcom/a/a/a/n;->g:Ljava/lang/String;

    .line 55
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/a/a/a/n;->h:Ljava/lang/String;

    .line 56
    iput-object p9, p0, Lcom/a/a/a/n;->i:Ljava/util/concurrent/ScheduledExecutorService;

    .line 58
    new-instance v0, Lcom/a/a/a/i;

    invoke-direct {v0, p9, p8, p10}, Lcom/a/a/a/i;-><init>(Ljava/util/concurrent/ScheduledExecutorService;Lcom/a/a/a/j;Lcom/a/a/a/bt;)V

    iput-object v0, p0, Lcom/a/a/a/n;->j:Lcom/a/a/a/t;

    .line 61
    if-eqz p0, :cond_0

    iget-object v0, p8, Lcom/a/a/a/j;->c:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 62
    :cond_0
    return-void
.end method


# virtual methods
.method a()V
    .locals 1

    .prologue
    .line 191
    new-instance v0, Lcom/a/a/a/s;

    invoke-direct {v0, p0}, Lcom/a/a/a/s;-><init>(Lcom/a/a/a/n;)V

    invoke-virtual {p0, v0}, Lcom/a/a/a/n;->a(Ljava/lang/Runnable;)V

    .line 203
    return-void
.end method

.method final a(Lcom/a/a/a/u;Z)V
    .locals 1

    .prologue
    .line 141
    new-instance v0, Lcom/a/a/a/p;

    invoke-direct {v0, p0, p1, p2}, Lcom/a/a/a/p;-><init>(Lcom/a/a/a/n;Lcom/a/a/a/u;Z)V

    invoke-virtual {p0, v0}, Lcom/a/a/a/n;->a(Ljava/lang/Runnable;)V

    .line 157
    return-void
.end method

.method final a(Lcom/a/a/a/v;Landroid/app/Activity;)V
    .locals 10

    .prologue
    .line 135
    iget-object v0, p0, Lcom/a/a/a/n;->a:Ljava/lang/String;

    iget-object v1, p0, Lcom/a/a/a/n;->h:Ljava/lang/String;

    iget-object v2, p0, Lcom/a/a/a/n;->b:Ljava/lang/String;

    iget-object v3, p0, Lcom/a/a/a/n;->c:Ljava/lang/String;

    iget-object v4, p0, Lcom/a/a/a/n;->d:Ljava/lang/String;

    iget-object v5, p0, Lcom/a/a/a/n;->e:Ljava/lang/String;

    iget-object v6, p0, Lcom/a/a/a/n;->f:Ljava/lang/String;

    iget-object v7, p0, Lcom/a/a/a/n;->g:Ljava/lang/String;

    const-string v8, "activity"

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Ljava/util/Collections;->singletonMap(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v9

    move-object v8, p1

    invoke-static/range {v0 .. v9}, Lcom/a/a/a/u;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/a/a/a/v;Ljava/util/Map;)Lcom/a/a/a/u;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/a/a/a/n;->a(Lcom/a/a/a/u;Z)V

    .line 138
    return-void
.end method

.method final a(Ljava/lang/Runnable;)V
    .locals 1

    .prologue
    .line 215
    :try_start_0
    iget-object v0, p0, Lcom/a/a/a/n;->i:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-interface {v0, p1}, Ljava/util/concurrent/ScheduledExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 219
    :goto_0
    return-void

    .line 217
    :catch_0
    move-exception v0

    invoke-static {}, Lcom/a/a/a/ba;->e()V

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 177
    new-instance v0, Lcom/a/a/a/r;

    invoke-direct {v0, p0}, Lcom/a/a/a/r;-><init>(Lcom/a/a/a/n;)V

    invoke-virtual {p0, v0}, Lcom/a/a/a/n;->a(Ljava/lang/Runnable;)V

    .line 188
    return-void
.end method
