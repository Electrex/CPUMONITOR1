.class public final Lcom/a/a/a/bh;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:Lcom/a/a/a/bo;

.field b:Ljava/io/File;

.field c:Ljava/io/File;

.field private final d:Ljava/io/File;

.field private final e:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/a/a/a/bh;->d:Ljava/io/File;

    .line 32
    iput-object p3, p0, Lcom/a/a/a/bh;->e:Ljava/lang/String;

    .line 34
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1, p2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/a/a/a/bh;->b:Ljava/io/File;

    .line 36
    new-instance v0, Lcom/a/a/a/bo;

    iget-object v1, p0, Lcom/a/a/a/bh;->b:Ljava/io/File;

    invoke-direct {v0, v1}, Lcom/a/a/a/bo;-><init>(Ljava/io/File;)V

    iput-object v0, p0, Lcom/a/a/a/bh;->a:Lcom/a/a/a/bo;

    .line 38
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/a/a/a/bh;->d:Ljava/io/File;

    iget-object v2, p0, Lcom/a/a/a/bh;->e:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/a/a/a/bh;->c:Ljava/io/File;

    iget-object v0, p0, Lcom/a/a/a/bh;->c:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/a/a/a/bh;->c:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 39
    :cond_0
    return-void
.end method

.method public static a(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/io/File;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 117
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    .line 118
    const-string v2, "deleting sent analytics file %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    invoke-static {}, Lcom/a/a/a/ba;->d()V

    .line 119
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    goto :goto_0

    .line 121
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation

    .prologue
    .line 125
    iget-object v0, p0, Lcom/a/a/a/bh;->c:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
