.class public Lcom/a/a/a/c;
.super Lcom/a/a/a/ck;
.source "SourceFile"


# instance fields
.field public a:Lcom/a/a/a/n;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Lcom/a/a/a/bm;

.field private f:Lcom/a/a/a/ai;

.field private h:J

.field private i:Lcom/a/a/a/bt;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/a/a/a/ck;-><init>()V

    return-void
.end method

.method public static a()Lcom/a/a/a/c;
    .locals 2

    .prologue
    .line 57
    invoke-static {}, Lcom/a/a/a/co;->a()Lcom/a/a/a/cl;

    move-result-object v0

    const-class v1, Lcom/a/a/a/c;

    invoke-virtual {v0, v1}, Lcom/a/a/a/cl;->a(Ljava/lang/Class;)Lcom/a/a/a/ck;

    move-result-object v0

    check-cast v0, Lcom/a/a/a/c;

    return-object v0
.end method

.method static synthetic a(Lcom/a/a/a/c;)V
    .locals 14

    .prologue
    const/4 v11, 0x1

    const/4 v12, 0x0

    .line 30
    iget-object v13, p0, Lcom/a/a/a/ch;->g:Landroid/content/Context;

    :try_start_0
    new-instance v0, Lcom/a/a/a/w;

    invoke-direct {v0}, Lcom/a/a/a/w;-><init>()V

    new-instance v1, Lcom/a/a/a/bf;

    invoke-direct {v1}, Lcom/a/a/a/bf;-><init>()V

    new-instance v2, Lcom/a/a/a/bh;

    invoke-static {}, Lcom/a/a/a/co;->a()Lcom/a/a/a/cl;

    move-result-object v3

    iget-object v3, v3, Lcom/a/a/a/cl;->c:Ljava/io/File;

    const-string v4, "session_analytics.tap"

    const-string v5, "session_analytics_to_send"

    invoke-direct {v2, v3, v4, v5}, Lcom/a/a/a/bh;-><init>(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)V

    new-instance v9, Lcom/a/a/a/j;

    const/4 v3, 0x0

    invoke-direct {v9, v0, v1, v2, v3}, Lcom/a/a/a/j;-><init>(Lcom/a/a/a/w;Lcom/a/a/a/bf;Lcom/a/a/a/bh;B)V

    iget-object v0, p0, Lcom/a/a/a/c;->e:Lcom/a/a/a/bm;

    invoke-virtual {v0}, Lcom/a/a/a/bm;->a()Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lcom/a/a/a/c;->e:Lcom/a/a/a/bm;

    invoke-virtual {v0}, Lcom/a/a/a/bm;->e()Ljava/lang/String;

    move-result-object v4

    invoke-static {}, Lcom/a/a/a/bm;->b()Ljava/lang/String;

    move-result-object v5

    invoke-static {}, Lcom/a/a/a/bm;->c()Ljava/lang/String;

    move-result-object v6

    invoke-static {}, Lcom/a/a/a/co;->a()Lcom/a/a/a/cl;

    move-result-object v0

    iget-object v1, v0, Lcom/a/a/a/cl;->d:Landroid/app/Application;

    if-eqz v1, :cond_2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xe

    if-lt v0, v2, :cond_2

    new-instance v0, Lcom/a/a/a/e;

    invoke-virtual {v13}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    iget-object v7, p0, Lcom/a/a/a/c;->c:Ljava/lang/String;

    iget-object v8, p0, Lcom/a/a/a/c;->d:Ljava/lang/String;

    iget-object v10, p0, Lcom/a/a/a/c;->i:Lcom/a/a/a/bt;

    invoke-direct/range {v0 .. v10}, Lcom/a/a/a/e;-><init>(Landroid/app/Application;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/a/a/a/j;Lcom/a/a/a/bt;)V

    iput-object v0, p0, Lcom/a/a/a/c;->a:Lcom/a/a/a/n;

    :goto_0
    iget-wide v0, p0, Lcom/a/a/a/c;->h:J

    iget-object v2, p0, Lcom/a/a/a/c;->f:Lcom/a/a/a/ai;

    invoke-virtual {v2}, Lcom/a/a/a/ai;->a()Landroid/content/SharedPreferences;

    move-result-object v2

    const-string v3, "analytics_launched"

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-nez v2, :cond_4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sub-long v0, v2, v0

    const-wide/32 v2, 0x36ee80

    cmp-long v0, v0, v2

    if-gez v0, :cond_3

    move v0, v11

    :goto_1
    if-eqz v0, :cond_4

    move v0, v11

    :goto_2
    if-eqz v0, :cond_0

    invoke-static {}, Lcom/a/a/a/co;->a()Lcom/a/a/a/cl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/a/a/cl;->a()Lcom/a/a/a/ci;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/a/a/ci;->b()V

    iget-object v0, p0, Lcom/a/a/a/c;->a:Lcom/a/a/a/n;

    if-eqz v0, :cond_0

    iget-object v10, p0, Lcom/a/a/a/c;->a:Lcom/a/a/a/n;

    iget-object v0, v10, Lcom/a/a/a/n;->a:Ljava/lang/String;

    iget-object v1, v10, Lcom/a/a/a/n;->h:Ljava/lang/String;

    iget-object v2, v10, Lcom/a/a/a/n;->b:Ljava/lang/String;

    iget-object v3, v10, Lcom/a/a/a/n;->c:Ljava/lang/String;

    iget-object v4, v10, Lcom/a/a/a/n;->d:Ljava/lang/String;

    iget-object v5, v10, Lcom/a/a/a/n;->e:Ljava/lang/String;

    iget-object v6, v10, Lcom/a/a/a/n;->f:Ljava/lang/String;

    iget-object v7, v10, Lcom/a/a/a/n;->g:Ljava/lang/String;

    sget-object v8, Lcom/a/a/a/v;->j:Lcom/a/a/a/v;

    new-instance v9, Ljava/util/HashMap;

    invoke-direct {v9}, Ljava/util/HashMap;-><init>()V

    invoke-static/range {v0 .. v9}, Lcom/a/a/a/u;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/a/a/a/v;Ljava/util/Map;)Lcom/a/a/a/u;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v10, v0, v1}, Lcom/a/a/a/n;->a(Lcom/a/a/a/u;Z)V

    iget-object v0, p0, Lcom/a/a/a/c;->f:Lcom/a/a/a/ai;

    invoke-virtual {v0}, Lcom/a/a/a/ai;->a()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "analytics_launched"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x9

    if-lt v1, v2, :cond_5

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_3
    :try_start_1
    invoke-static {}, Lcom/a/a/a/as;->a()Lcom/a/a/a/ar;

    move-result-object v0

    iget-object v2, p0, Lcom/a/a/a/c;->i:Lcom/a/a/a/bt;

    iget-object v3, p0, Lcom/a/a/a/c;->c:Ljava/lang/String;

    iget-object v4, p0, Lcom/a/a/a/c;->d:Ljava/lang/String;

    invoke-direct {p0}, Lcom/a/a/a/c;->c()Ljava/lang/String;

    move-result-object v5

    move-object v1, v13

    invoke-virtual/range {v0 .. v5}, Lcom/a/a/a/ar;->a(Landroid/content/Context;Lcom/a/a/a/bt;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/a/a/a/ar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/a/a/ar;->b()Z

    invoke-static {}, Lcom/a/a/a/as;->a()Lcom/a/a/a/ar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/a/a/ar;->a()Lcom/a/a/a/aw;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v1, v0, Lcom/a/a/a/aw;->d:Lcom/a/a/a/ao;

    iget-boolean v1, v1, Lcom/a/a/a/ao;->c:Z

    if-nez v1, :cond_6

    invoke-static {}, Lcom/a/a/a/ba;->d()V

    iget-object v0, p0, Lcom/a/a/a/c;->a:Lcom/a/a/a/n;

    invoke-virtual {v0}, Lcom/a/a/a/n;->a()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :cond_1
    :goto_4
    return-void

    :cond_2
    :try_start_2
    new-instance v1, Lcom/a/a/a/n;

    invoke-virtual {v13}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    iget-object v7, p0, Lcom/a/a/a/c;->c:Ljava/lang/String;

    iget-object v8, p0, Lcom/a/a/a/c;->d:Ljava/lang/String;

    iget-object v10, p0, Lcom/a/a/a/c;->i:Lcom/a/a/a/bt;

    invoke-direct/range {v1 .. v10}, Lcom/a/a/a/n;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/a/a/a/j;Lcom/a/a/a/bt;)V

    iput-object v1, p0, Lcom/a/a/a/c;->a:Lcom/a/a/a/n;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_0

    :catch_0
    move-exception v0

    invoke-static {}, Lcom/a/a/a/ba;->e()V

    goto :goto_3

    :cond_3
    move v0, v12

    goto/16 :goto_1

    :cond_4
    move v0, v12

    goto/16 :goto_2

    :cond_5
    :try_start_3
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_3

    :cond_6
    :try_start_4
    iget-object v1, p0, Lcom/a/a/a/c;->a:Lcom/a/a/a/n;

    iget-object v0, v0, Lcom/a/a/a/aw;->e:Lcom/a/a/a/aj;

    invoke-direct {p0}, Lcom/a/a/a/c;->c()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/a/a/a/q;

    invoke-direct {v3, v1, v0, v2}, Lcom/a/a/a/q;-><init>(Lcom/a/a/a/n;Lcom/a/a/a/aj;Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Lcom/a/a/a/n;->a(Ljava/lang/Runnable;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_4

    :catch_1
    move-exception v0

    invoke-static {}, Lcom/a/a/a/co;->a()Lcom/a/a/a/cl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/a/a/cl;->a()Lcom/a/a/a/ci;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/a/a/ci;->a()V

    goto :goto_4
.end method

.method private c()Ljava/lang/String;
    .locals 2

    .prologue
    .line 180
    iget-object v0, p0, Lcom/a/a/a/ch;->g:Landroid/content/Context;

    const-string v1, "com.crashlytics.ApiEndpoint"

    invoke-static {v0, v1}, Lcom/a/a/a/ba;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected final b()V
    .locals 4

    .prologue
    .line 63
    :try_start_0
    new-instance v0, Lcom/a/a/a/bt;

    invoke-static {}, Lcom/a/a/a/co;->a()Lcom/a/a/a/cl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/a/a/a/cl;->a()Lcom/a/a/a/ci;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/a/a/a/bt;-><init>(Lcom/a/a/a/ci;)V

    iput-object v0, p0, Lcom/a/a/a/c;->i:Lcom/a/a/a/bt;

    .line 64
    new-instance v0, Lcom/a/a/a/ai;

    invoke-static {}, Lcom/a/a/a/co;->a()Lcom/a/a/a/cl;

    move-result-object v1

    const-class v2, Lcom/a/a/a/c;

    invoke-virtual {v1, v2}, Lcom/a/a/a/cl;->a(Ljava/lang/Class;)Lcom/a/a/a/ck;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/a/a/a/ai;-><init>(Lcom/a/a/a/ck;)V

    iput-object v0, p0, Lcom/a/a/a/c;->f:Lcom/a/a/a/ai;

    .line 67
    iget-object v1, p0, Lcom/a/a/a/ch;->g:Landroid/content/Context;

    .line 68
    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 69
    new-instance v2, Lcom/a/a/a/bm;

    invoke-direct {v2, v1}, Lcom/a/a/a/bm;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/a/a/a/c;->e:Lcom/a/a/a/bm;

    .line 70
    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/a/a/a/c;->b:Ljava/lang/String;

    .line 72
    iget-object v2, p0, Lcom/a/a/a/c;->b:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v2

    .line 74
    iget v0, v2, Landroid/content/pm/PackageInfo;->versionCode:I

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/a/a/a/c;->c:Ljava/lang/String;

    .line 75
    iget-object v0, v2, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, "0.0"

    :goto_0
    iput-object v0, p0, Lcom/a/a/a/c;->d:Ljava/lang/String;

    .line 78
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x9

    if-lt v0, v3, :cond_1

    .line 79
    iget-wide v0, v2, Landroid/content/pm/PackageInfo;->firstInstallTime:J

    iput-wide v0, p0, Lcom/a/a/a/c;->h:J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 89
    :goto_1
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/a/a/a/d;

    invoke-direct {v1, p0}, Lcom/a/a/a/d;-><init>(Lcom/a/a/a/c;)V

    const-string v2, "Crashlytics Initializer"

    invoke-direct {v0, v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 100
    return-void

    .line 75
    :cond_0
    :try_start_1
    iget-object v0, v2, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    goto :goto_0

    .line 81
    :cond_1
    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    .line 83
    new-instance v1, Ljava/io/File;

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->lastModified()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/a/a/a/c;->h:J
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 87
    :catch_0
    move-exception v0

    invoke-static {}, Lcom/a/a/a/co;->a()Lcom/a/a/a/cl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/a/a/cl;->a()Lcom/a/a/a/ci;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/a/a/ci;->a()V

    goto :goto_1
.end method
