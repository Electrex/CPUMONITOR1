.class final Lcom/a/a/a/j;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:Lcom/a/a/a/aj;

.field final b:Lcom/a/a/a/bh;

.field c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/a/a/a/bi;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcom/a/a/a/w;

.field private final e:Lcom/a/a/a/bf;

.field private final f:I


# direct methods
.method private constructor <init>(Lcom/a/a/a/w;Lcom/a/a/a/bf;Lcom/a/a/a/bh;)V
    .locals 1

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/a/a/a/j;->c:Ljava/util/List;

    .line 54
    iput-object p1, p0, Lcom/a/a/a/j;->d:Lcom/a/a/a/w;

    .line 55
    iput-object p3, p0, Lcom/a/a/a/j;->b:Lcom/a/a/a/bh;

    .line 56
    iput-object p2, p0, Lcom/a/a/a/j;->e:Lcom/a/a/a/bf;

    .line 58
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 60
    const/16 v0, 0x64

    iput v0, p0, Lcom/a/a/a/j;->f:I

    .line 61
    return-void
.end method

.method constructor <init>(Lcom/a/a/a/w;Lcom/a/a/a/bf;Lcom/a/a/a/bh;B)V
    .locals 0

    .prologue
    .line 65
    invoke-direct {p0, p1, p2, p3}, Lcom/a/a/a/j;-><init>(Lcom/a/a/a/w;Lcom/a/a/a/bf;Lcom/a/a/a/bh;)V

    .line 66
    return-void
.end method

.method private static a(Ljava/lang/String;)J
    .locals 5

    .prologue
    const-wide/16 v0, 0x0

    .line 209
    const-string v2, "_"

    invoke-virtual {p0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 211
    array-length v3, v2

    const/4 v4, 0x3

    if-eq v3, v4, :cond_0

    .line 216
    :goto_0
    return-wide v0

    .line 214
    :cond_0
    const/4 v3, 0x2

    :try_start_0
    aget-object v2, v2, v3

    invoke-static {v2}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    goto :goto_0

    .line 216
    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method private e()V
    .locals 2

    .prologue
    .line 153
    iget-object v0, p0, Lcom/a/a/a/j;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/a/a/bi;

    .line 155
    :try_start_0
    invoke-interface {v0}, Lcom/a/a/a/bi;->b()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 157
    :catch_0
    move-exception v0

    invoke-static {}, Lcom/a/a/a/co;->a()Lcom/a/a/a/cl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/a/a/cl;->a()Lcom/a/a/a/ci;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/a/a/ci;->a()V

    goto :goto_0

    .line 160
    :cond_0
    return-void
.end method


# virtual methods
.method final a()Z
    .locals 10

    .prologue
    const/4 v3, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 88
    .line 89
    iget-object v2, p0, Lcom/a/a/a/j;->b:Lcom/a/a/a/bh;

    iget-object v2, v2, Lcom/a/a/a/bh;->a:Lcom/a/a/a/bo;

    invoke-virtual {v2}, Lcom/a/a/a/bo;->b()Z

    move-result v2

    if-nez v2, :cond_0

    .line 94
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "sa"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, "_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "_"

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v2, ".tap"

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 95
    iget-object v6, p0, Lcom/a/a/a/j;->b:Lcom/a/a/a/bh;

    iget-object v2, v6, Lcom/a/a/a/bh;->a:Lcom/a/a/a/bo;

    invoke-virtual {v2}, Lcom/a/a/a/bo;->close()V

    iget-object v7, v6, Lcom/a/a/a/bh;->b:Ljava/io/File;

    new-instance v8, Ljava/io/File;

    iget-object v2, v6, Lcom/a/a/a/bh;->c:Ljava/io/File;

    invoke-direct {v8, v2, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    :try_start_0
    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, v7}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    new-instance v4, Ljava/util/zip/GZIPOutputStream;

    new-instance v9, Ljava/io/FileOutputStream;

    invoke-direct {v9, v8}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v4, v9}, Ljava/util/zip/GZIPOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    const/16 v3, 0x400

    :try_start_2
    new-array v3, v3, [B

    invoke-static {v2, v4, v3}, Lcom/a/a/a/ba;->a(Ljava/io/InputStream;Ljava/io/OutputStream;[B)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    invoke-static {v2}, Lcom/a/a/a/ba;->a(Ljava/io/Closeable;)V

    invoke-static {v4}, Lcom/a/a/a/ba;->a(Ljava/io/Closeable;)V

    invoke-virtual {v7}, Ljava/io/File;->delete()Z

    new-instance v2, Lcom/a/a/a/bo;

    iget-object v3, v6, Lcom/a/a/a/bh;->b:Ljava/io/File;

    invoke-direct {v2, v3}, Lcom/a/a/a/bo;-><init>(Ljava/io/File;)V

    iput-object v2, v6, Lcom/a/a/a/bh;->a:Lcom/a/a/a/bo;

    .line 97
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "generated new to-send analytics file %s"

    new-array v4, v0, [Ljava/lang/Object;

    aput-object v5, v4, v1

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/a/a/a/ba;->c(Ljava/lang/String;)V

    .line 100
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 104
    :goto_0
    invoke-direct {p0}, Lcom/a/a/a/j;->e()V

    .line 105
    return v0

    .line 95
    :catchall_0
    move-exception v0

    move-object v1, v3

    :goto_1
    invoke-static {v1}, Lcom/a/a/a/ba;->a(Ljava/io/Closeable;)V

    invoke-static {v3}, Lcom/a/a/a/ba;->a(Ljava/io/Closeable;)V

    invoke-virtual {v7}, Ljava/io/File;->delete()Z

    throw v0

    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_1

    :catchall_2
    move-exception v0

    move-object v1, v2

    move-object v3, v4

    goto :goto_1

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method final b()I
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lcom/a/a/a/j;->a:Lcom/a/a/a/aj;

    if-nez v0, :cond_0

    const/16 v0, 0x1f40

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/a/a/a/j;->a:Lcom/a/a/a/aj;

    iget v0, v0, Lcom/a/a/a/aj;->c:I

    goto :goto_0
.end method

.method final c()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation

    .prologue
    .line 163
    iget-object v0, p0, Lcom/a/a/a/j;->b:Lcom/a/a/a/bh;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, v0, Lcom/a/a/a/bh;->c:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    if-gtz v4, :cond_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method final d()V
    .locals 7

    .prologue
    .line 176
    iget-object v0, p0, Lcom/a/a/a/j;->b:Lcom/a/a/a/bh;

    invoke-virtual {v0}, Lcom/a/a/a/bh;->a()Ljava/util/List;

    move-result-object v0

    .line 178
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    iget v2, p0, Lcom/a/a/a/j;->f:I

    if-gt v1, v2, :cond_0

    .line 206
    :goto_0
    return-void

    .line 180
    :cond_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    iget v2, p0, Lcom/a/a/a/j;->f:I

    sub-int/2addr v1, v2

    .line 182
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "Found %d files in session analytics roll over directory, this is greater than %d, deleting %d oldest files"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    iget v6, p0, Lcom/a/a/a/j;->f:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    invoke-static {}, Lcom/a/a/a/ba;->d()V

    .line 186
    new-instance v2, Ljava/util/TreeSet;

    new-instance v3, Lcom/a/a/a/k;

    invoke-direct {v3}, Lcom/a/a/a/k;-><init>()V

    invoke-direct {v2, v3}, Ljava/util/TreeSet;-><init>(Ljava/util/Comparator;)V

    .line 193
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    .line 194
    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/a/a/a/j;->a(Ljava/lang/String;)J

    move-result-wide v4

    .line 195
    new-instance v6, Lcom/a/a/a/l;

    invoke-direct {v6, v0, v4, v5}, Lcom/a/a/a/l;-><init>(Ljava/io/File;J)V

    invoke-virtual {v2, v6}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 198
    :cond_1
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 199
    invoke-virtual {v2}, Ljava/util/TreeSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/a/a/l;

    .line 200
    iget-object v0, v0, Lcom/a/a/a/l;->a:Ljava/io/File;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 202
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ne v0, v1, :cond_2

    .line 203
    :cond_3
    invoke-static {v3}, Lcom/a/a/a/bh;->a(Ljava/util/List;)V

    goto :goto_0
.end method
