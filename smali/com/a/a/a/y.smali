.class public abstract Lcom/a/a/a/y;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static b:Ljava/lang/String;

.field private static final c:Ljava/util/regex/Pattern;


# instance fields
.field public final a:Ljava/lang/String;

.field private final d:Lcom/a/a/a/bt;

.field private final e:I

.field private final f:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 30
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Crashlytics Android SDK/"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/a/a/a/co;->a()Lcom/a/a/a/cl;

    const-string v1, "1.1.13.29"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/a/a/a/y;->b:Ljava/lang/String;

    .line 40
    const-string v0, "http(s?)://[^\\/]+"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/a/a/a/y;->c:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/a/a/a/bt;I)V
    .locals 2

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    if-nez p2, :cond_0

    .line 70
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "url must not be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 72
    :cond_0
    if-nez p3, :cond_1

    .line 73
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "requestFactory must not be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 75
    :cond_1
    iput-object p1, p0, Lcom/a/a/a/y;->f:Ljava/lang/String;

    .line 76
    iget-object v0, p0, Lcom/a/a/a/y;->f:Ljava/lang/String;

    invoke-static {v0}, Lcom/a/a/a/ba;->e(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    sget-object v0, Lcom/a/a/a/y;->c:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p2}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    iget-object v1, p0, Lcom/a/a/a/y;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/regex/Matcher;->replaceFirst(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    :cond_2
    iput-object p2, p0, Lcom/a/a/a/y;->a:Ljava/lang/String;

    .line 77
    iput-object p3, p0, Lcom/a/a/a/y;->d:Lcom/a/a/a/bt;

    .line 78
    iput p4, p0, Lcom/a/a/a/y;->e:I

    .line 79
    return-void
.end method


# virtual methods
.method public final a()Lcom/a/a/a/bw;
    .locals 1

    .prologue
    .line 99
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/a/a/a/y;->a(Ljava/util/Map;)Lcom/a/a/a/bw;

    move-result-object v0

    return-object v0
.end method

.method protected final a(Ljava/util/Map;)Lcom/a/a/a/bw;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/a/a/a/bw;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 119
    iget-object v3, p0, Lcom/a/a/a/y;->d:Lcom/a/a/a/bt;

    iget v0, p0, Lcom/a/a/a/y;->e:I

    iget-object v4, p0, Lcom/a/a/a/y;->a:Ljava/lang/String;

    sget-object v2, Lcom/a/a/a/bu;->a:[I

    add-int/lit8 v0, v0, -0x1

    aget v0, v2, v0

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unsupported HTTP method!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    invoke-static {v4, p1}, Lcom/a/a/a/bw;->a(Ljava/lang/CharSequence;Ljava/util/Map;)Lcom/a/a/a/bw;

    move-result-object v0

    move-object v2, v0

    :goto_0
    if-nez v4, :cond_1

    move v0, v1

    :goto_1
    if-eqz v0, :cond_0

    iget-object v0, v3, Lcom/a/a/a/bt;->a:Lcom/a/a/a/af;

    if-eqz v0, :cond_0

    invoke-virtual {v3}, Lcom/a/a/a/bt;->a()Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v2}, Lcom/a/a/a/bw;->a()Ljava/net/HttpURLConnection;

    move-result-object v0

    check-cast v0, Ljavax/net/ssl/HttpsURLConnection;

    invoke-virtual {v0, v3}, Ljavax/net/ssl/HttpsURLConnection;->setSSLSocketFactory(Ljavax/net/ssl/SSLSocketFactory;)V

    .line 120
    :cond_0
    invoke-virtual {v2}, Lcom/a/a/a/bw;->a()Ljava/net/HttpURLConnection;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setUseCaches(Z)V

    invoke-virtual {v2}, Lcom/a/a/a/bw;->a()Ljava/net/HttpURLConnection;

    move-result-object v0

    const/16 v1, 0x2710

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    const-string v0, "User-Agent"

    sget-object v1, Lcom/a/a/a/y;->b:Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Lcom/a/a/a/bw;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/a/a/a/bw;

    move-result-object v0

    const-string v1, "X-CRASHLYTICS-DEVELOPER-TOKEN"

    const-string v2, "bca6990fc3c15a8105800c0673517a4b579634a1"

    invoke-virtual {v0, v1, v2}, Lcom/a/a/a/bw;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/a/a/a/bw;

    move-result-object v0

    return-object v0

    .line 119
    :pswitch_1
    invoke-static {v4, p1}, Lcom/a/a/a/bw;->b(Ljava/lang/CharSequence;Ljava/util/Map;)Lcom/a/a/a/bw;

    move-result-object v0

    move-object v2, v0

    goto :goto_0

    :pswitch_2
    invoke-static {v4}, Lcom/a/a/a/bw;->a(Ljava/lang/CharSequence;)Lcom/a/a/a/bw;

    move-result-object v0

    move-object v2, v0

    goto :goto_0

    :pswitch_3
    invoke-static {v4}, Lcom/a/a/a/bw;->b(Ljava/lang/CharSequence;)Lcom/a/a/a/bw;

    move-result-object v0

    move-object v2, v0

    goto :goto_0

    :cond_1
    invoke-virtual {v4}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v4, "https"

    invoke-virtual {v0, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
