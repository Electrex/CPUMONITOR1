.class final Lcom/a/a/y;
.super Lcom/a/a/a/y;
.source "SourceFile"

# interfaces
.implements Lcom/a/a/x;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/a/a/a/bt;)V
    .locals 1

    .prologue
    .line 34
    sget v0, Lcom/a/a/a/bv;->b:I

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/a/a/a/y;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/a/a/a/bt;I)V

    .line 35
    return-void
.end method


# virtual methods
.method public final a(Lcom/a/a/w;)Z
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 53
    invoke-virtual {p0}, Lcom/a/a/y;->a()Lcom/a/a/a/bw;

    move-result-object v0

    .line 54
    const-string v1, "X-CRASHLYTICS-API-KEY"

    iget-object v2, p1, Lcom/a/a/w;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/a/a/a/bw;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/a/a/a/bw;

    move-result-object v0

    const-string v1, "X-CRASHLYTICS-API-CLIENT-TYPE"

    const-string v2, "android"

    invoke-virtual {v0, v1, v2}, Lcom/a/a/a/bw;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/a/a/a/bw;

    move-result-object v0

    const-string v1, "X-CRASHLYTICS-API-CLIENT-VERSION"

    invoke-static {}, Lcom/a/a/d;->a()Lcom/a/a/d;

    invoke-static {}, Lcom/a/a/d;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/a/a/a/bw;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/a/a/a/bw;

    move-result-object v0

    iget-object v1, p1, Lcom/a/a/w;->b:Lcom/a/a/ab;

    iget-object v1, v1, Lcom/a/a/ab;->b:Ljava/util/Map;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move-object v2, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v1, v0}, Lcom/a/a/a/bw;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/a/a/a/bw;

    move-result-object v0

    move-object v2, v0

    goto :goto_0

    .line 55
    :cond_0
    iget-object v0, p1, Lcom/a/a/w;->b:Lcom/a/a/ab;

    const-string v1, "report[file]"

    iget-object v4, v0, Lcom/a/a/ab;->a:Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    const-string v5, "application/octet-stream"

    iget-object v6, v0, Lcom/a/a/ab;->a:Ljava/io/File;

    invoke-virtual {v2, v1, v4, v5, v6}, Lcom/a/a/a/bw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Lcom/a/a/a/bw;

    move-result-object v1

    const-string v2, "report[identifier]"

    iget-object v0, v0, Lcom/a/a/ab;->a:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    const/16 v4, 0x2e

    invoke-virtual {v0, v4}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v4

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/a/a/a/bw;->c(Ljava/lang/String;Ljava/lang/String;)Lcom/a/a/a/bw;

    move-result-object v0

    .line 57
    invoke-static {}, Lcom/a/a/a/co;->a()Lcom/a/a/a/cl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/a/a/a/cl;->a()Lcom/a/a/a/ci;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "Sending report to: "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/a/a/a/y;->a:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {v1}, Lcom/a/a/a/ci;->b()V

    .line 59
    invoke-virtual {v0}, Lcom/a/a/a/bw;->b()I

    move-result v1

    .line 61
    invoke-static {}, Lcom/a/a/a/co;->a()Lcom/a/a/a/cl;

    move-result-object v2

    invoke-virtual {v2}, Lcom/a/a/a/cl;->a()Lcom/a/a/a/ci;

    move-result-object v2

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Create report request ID: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, "X-REQUEST-ID"

    invoke-virtual {v0, v5}, Lcom/a/a/a/bw;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {v2}, Lcom/a/a/a/ci;->b()V

    .line 62
    invoke-static {}, Lcom/a/a/a/co;->a()Lcom/a/a/a/cl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/a/a/cl;->a()Lcom/a/a/a/ci;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/a/a/ci;->b()V

    .line 64
    invoke-static {v1}, Lcom/a/a/a/cj;->a(I)I

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_1
    move v0, v3

    goto :goto_1
.end method
