.class public final Lcom/a/a/d;
.super Lcom/a/a/a/ck;
.source "SourceFile"


# static fields
.field private static j:Landroid/content/ContextWrapper;

.field private static k:Ljava/lang/String;

.field private static l:Ljava/lang/String;

.field private static m:Ljava/lang/String;

.field private static n:Ljava/lang/String;

.field private static o:Ljava/lang/String;

.field private static p:Ljava/lang/String;

.field private static q:Ljava/lang/String;

.field private static r:Z

.field private static s:Lcom/a/a/r;

.field private static t:Lcom/a/a/a/bt;

.field private static u:F

.field private static v:Lcom/a/a/d;


# instance fields
.field final a:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field b:Lcom/a/a/az;

.field c:Lcom/a/a/a/bm;

.field d:Ljava/lang/String;

.field e:Ljava/lang/String;

.field f:Ljava/lang/String;

.field private final h:J

.field private i:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 99
    const/4 v0, 0x0

    sput-boolean v0, Lcom/a/a/d;->r:Z

    .line 100
    const/4 v0, 0x0

    sput-object v0, Lcom/a/a/d;->s:Lcom/a/a/r;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 112
    invoke-direct {p0}, Lcom/a/a/a/ck;-><init>()V

    .line 82
    iput-object v0, p0, Lcom/a/a/d;->c:Lcom/a/a/a/bm;

    .line 84
    iput-object v0, p0, Lcom/a/a/d;->d:Ljava/lang/String;

    .line 85
    iput-object v0, p0, Lcom/a/a/d;->e:Ljava/lang/String;

    .line 86
    iput-object v0, p0, Lcom/a/a/d;->f:Ljava/lang/String;

    .line 113
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/a/a/d;->a:Ljava/util/concurrent/ConcurrentHashMap;

    .line 114
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/a/a/d;->h:J

    .line 115
    return-void
.end method

.method static synthetic a(FI)I
    .locals 1

    .prologue
    .line 54
    int-to-float v0, p1

    mul-float/2addr v0, p0

    float-to-int v0, v0

    return v0
.end method

.method private a(Lcom/a/a/aa;)Lcom/a/a/ah;
    .locals 11

    .prologue
    .line 977
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/a/a/d;->i:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/a/a/a/ba;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 978
    sget-object v0, Lcom/a/a/d;->l:Ljava/lang/String;

    invoke-static {v0}, Lcom/a/a/a/bg;->a(Ljava/lang/String;)Lcom/a/a/a/bg;

    move-result-object v0

    iget v7, v0, Lcom/a/a/a/bg;->a:I

    .line 980
    new-instance v0, Lcom/a/a/ah;

    sget-object v1, Lcom/a/a/d;->p:Ljava/lang/String;

    sget-object v2, Lcom/a/a/d;->k:Ljava/lang/String;

    sget-object v3, Lcom/a/a/d;->o:Ljava/lang/String;

    sget-object v4, Lcom/a/a/d;->n:Ljava/lang/String;

    sget-object v6, Lcom/a/a/d;->m:Ljava/lang/String;

    sget-object v8, Lcom/a/a/d;->q:Ljava/lang/String;

    const-string v9, "0"

    move-object v10, p1

    invoke-direct/range {v0 .. v10}, Lcom/a/a/ah;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Lcom/a/a/aa;)V

    return-object v0
.end method

.method static synthetic a(Lcom/a/a/d;)Lcom/a/a/az;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/a/a/d;->b:Lcom/a/a/az;

    return-object v0
.end method

.method public static declared-synchronized a()Lcom/a/a/d;
    .locals 3

    .prologue
    .line 153
    const-class v1, Lcom/a/a/d;

    monitor-enter v1

    :try_start_0
    invoke-static {}, Lcom/a/a/a/co;->a()Lcom/a/a/a/cl;

    move-result-object v0

    const-class v2, Lcom/a/a/d;

    invoke-virtual {v0, v2}, Lcom/a/a/a/cl;->a(Ljava/lang/Class;)Lcom/a/a/a/ck;

    move-result-object v0

    check-cast v0, Lcom/a/a/d;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 155
    if-eqz v0, :cond_0

    .line 164
    :goto_0
    monitor-exit v1

    return-object v0

    .line 161
    :cond_0
    :try_start_1
    sget-object v0, Lcom/a/a/d;->v:Lcom/a/a/d;

    if-nez v0, :cond_1

    .line 162
    new-instance v0, Lcom/a/a/d;

    invoke-direct {v0}, Lcom/a/a/d;-><init>()V

    sput-object v0, Lcom/a/a/d;->v:Lcom/a/a/d;

    .line 164
    :cond_1
    sget-object v0, Lcom/a/a/d;->v:Lcom/a/a/d;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 153
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static a(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 124
    const/high16 v0, 0x3f800000    # 1.0f

    sput v0, Lcom/a/a/d;->u:F

    invoke-static {p0}, Lcom/a/a/a/ba;->d(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/a/a/a/co;->a()Lcom/a/a/a/cl;

    move-result-object v0

    new-instance v1, Lcom/a/a/a/a;

    invoke-direct {v1}, Lcom/a/a/a/a;-><init>()V

    iget-object v0, v0, Lcom/a/a/a/cl;->a:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    :cond_0
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/a/a/a/ck;

    const/4 v1, 0x0

    invoke-static {}, Lcom/a/a/d;->a()Lcom/a/a/d;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    new-instance v2, Lcom/a/a/a/c;

    invoke-direct {v2}, Lcom/a/a/a/c;-><init>()V

    aput-object v2, v0, v1

    invoke-static {p0, v0}, Lcom/a/a/a/cl;->a(Landroid/content/Context;[Lcom/a/a/a/ck;)V

    .line 125
    return-void
.end method

.method static a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 206
    invoke-static {}, Lcom/a/a/a/co;->a()Lcom/a/a/a/cl;

    move-result-object v0

    const-class v1, Lcom/a/a/a/c;

    invoke-virtual {v0, v1}, Lcom/a/a/a/cl;->a(Ljava/lang/Class;)Lcom/a/a/a/ck;

    move-result-object v0

    check-cast v0, Lcom/a/a/a/c;

    .line 207
    if-eqz v0, :cond_1

    .line 208
    new-instance v1, Lcom/a/a/a/be;

    invoke-direct {v1, p0}, Lcom/a/a/a/be;-><init>(Ljava/lang/String;)V

    iget-object v2, v0, Lcom/a/a/a/c;->a:Lcom/a/a/a/n;

    if-eqz v2, :cond_1

    iget-object v0, v0, Lcom/a/a/a/c;->a:Lcom/a/a/a/n;

    iget-object v1, v1, Lcom/a/a/a/bd;->a:Ljava/lang/String;

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v3

    if-ne v2, v3, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "onCrash called from main thread!!!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    new-instance v2, Lcom/a/a/a/o;

    invoke-direct {v2, v0, v1}, Lcom/a/a/a/o;-><init>(Lcom/a/a/a/n;Ljava/lang/String;)V

    :try_start_0
    iget-object v0, v0, Lcom/a/a/a/n;->i:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-interface {v0, v2}, Ljava/util/concurrent/ScheduledExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/Future;->get()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 211
    :cond_1
    :goto_0
    return-void

    .line 208
    :catch_0
    move-exception v0

    invoke-static {}, Lcom/a/a/a/ba;->e()V

    goto :goto_0
.end method

.method private declared-synchronized a(Ljava/lang/String;Landroid/content/Context;F)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 765
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/a/a/d;->j:Landroid/content/ContextWrapper;

    if-eqz v0, :cond_1

    .line 766
    invoke-static {}, Lcom/a/a/a/co;->a()Lcom/a/a/a/cl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/a/a/cl;->a()Lcom/a/a/a/ci;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/a/a/ci;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 867
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 770
    :cond_1
    :try_start_1
    sput-object p1, Lcom/a/a/d;->p:Ljava/lang/String;

    .line 771
    new-instance v0, Landroid/content/ContextWrapper;

    invoke-virtual {p2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/content/ContextWrapper;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/a/a/d;->j:Landroid/content/ContextWrapper;

    .line 772
    new-instance v0, Lcom/a/a/a/bt;

    invoke-static {}, Lcom/a/a/a/co;->a()Lcom/a/a/a/cl;

    move-result-object v2

    invoke-virtual {v2}, Lcom/a/a/a/cl;->a()Lcom/a/a/a/ci;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/a/a/a/bt;-><init>(Lcom/a/a/a/ci;)V

    sput-object v0, Lcom/a/a/d;->t:Lcom/a/a/a/bt;

    .line 774
    invoke-static {}, Lcom/a/a/a/co;->a()Lcom/a/a/a/cl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/a/a/cl;->a()Lcom/a/a/a/ci;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Initializing Crashlytics "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/a/a/d;->a()Lcom/a/a/d;

    invoke-static {}, Lcom/a/a/a/co;->a()Lcom/a/a/a/cl;

    const-string v3, "1.1.13.29"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {v0}, Lcom/a/a/a/ci;->c()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 777
    :try_start_2
    sget-object v0, Lcom/a/a/d;->j:Landroid/content/ContextWrapper;

    invoke-virtual {v0}, Landroid/content/ContextWrapper;->getPackageName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/a/a/d;->k:Ljava/lang/String;

    .line 778
    sget-object v0, Lcom/a/a/d;->j:Landroid/content/ContextWrapper;

    invoke-virtual {v0}, Landroid/content/ContextWrapper;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 780
    sget-object v2, Lcom/a/a/d;->k:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/content/pm/PackageManager;->getInstallerPackageName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/a/a/d;->l:Ljava/lang/String;

    .line 781
    invoke-static {}, Lcom/a/a/a/co;->a()Lcom/a/a/a/cl;

    move-result-object v2

    invoke-virtual {v2}, Lcom/a/a/a/cl;->a()Lcom/a/a/a/ci;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Installer package name is: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v4, Lcom/a/a/d;->l:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {v2}, Lcom/a/a/a/ci;->b()V

    .line 783
    sget-object v2, Lcom/a/a/d;->k:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    .line 784
    iget v2, v0, Landroid/content/pm/PackageInfo;->versionCode:I

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/a/a/d;->n:Ljava/lang/String;

    .line 785
    iget-object v2, v0, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    if-nez v2, :cond_2

    const-string v0, "0.0"

    :goto_1
    sput-object v0, Lcom/a/a/d;->o:Ljava/lang/String;

    .line 786
    invoke-virtual {p2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {p2}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/a/a/d;->m:Ljava/lang/String;

    .line 788
    invoke-virtual {p2}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    iget v0, v0, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/a/a/d;->q:Ljava/lang/String;

    .line 791
    invoke-static {p2}, Lcom/a/a/a/ba;->i(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/a/a/d;->i:Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 797
    :goto_2
    :try_start_3
    new-instance v0, Lcom/a/a/a/bm;

    sget-object v2, Lcom/a/a/d;->j:Landroid/content/ContextWrapper;

    invoke-direct {v0, v2}, Lcom/a/a/a/bm;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/a/a/d;->c:Lcom/a/a/a/bm;

    .line 800
    iget-object v0, p0, Lcom/a/a/d;->c:Lcom/a/a/a/bm;

    invoke-virtual {v0}, Lcom/a/a/a/bm;->f()Ljava/lang/String;

    .line 803
    new-instance v0, Lcom/a/a/ai;

    iget-object v2, p0, Lcom/a/a/d;->i:Ljava/lang/String;

    sget-object v3, Lcom/a/a/d;->j:Landroid/content/ContextWrapper;

    const-string v4, "com.crashlytics.RequireBuildId"

    const/4 v5, 0x1

    invoke-static {v3, v4, v5}, Lcom/a/a/a/ba;->a(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v3

    invoke-direct {v0, v2, v3}, Lcom/a/a/ai;-><init>(Ljava/lang/String;Z)V

    sget-object v2, Lcom/a/a/d;->k:Ljava/lang/String;

    iget-object v3, v0, Lcom/a/a/ai;->a:Ljava/lang/String;

    invoke-static {v3}, Lcom/a/a/a/ba;->e(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-boolean v3, v0, Lcom/a/a/ai;->b:Z

    if-eqz v3, :cond_3

    const-string v0, "https://crashlytics.com/register/%s/android/%s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v1, v3

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    new-instance v0, Lcom/a/a/e;

    invoke-direct {v0, p1, v2}, Lcom/a/a/e;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 765
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 785
    :cond_2
    :try_start_4
    iget-object v0, v0, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    .line 794
    :catch_0
    move-exception v0

    :try_start_5
    invoke-static {}, Lcom/a/a/a/co;->a()Lcom/a/a/a/cl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/a/a/cl;->a()Lcom/a/a/a/ci;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/a/a/ci;->a()V

    goto :goto_2

    .line 803
    :cond_3
    iget-boolean v0, v0, Lcom/a/a/ai;->b:Z

    if-nez v0, :cond_4

    invoke-static {}, Lcom/a/a/a/co;->a()Lcom/a/a/a/cl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/a/a/cl;->a()Lcom/a/a/a/ci;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/a/a/ci;->b()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 808
    :cond_4
    :try_start_6
    invoke-static {}, Lcom/a/a/a/co;->a()Lcom/a/a/a/cl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/a/a/cl;->a()Lcom/a/a/a/ci;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/a/a/ci;->b()V

    .line 809
    new-instance v0, Lcom/a/a/az;

    invoke-static {}, Ljava/lang/Thread;->getDefaultUncaughtExceptionHandler()Ljava/lang/Thread$UncaughtExceptionHandler;

    move-result-object v2

    iget-object v3, p0, Lcom/a/a/d;->i:Ljava/lang/String;

    invoke-direct {v0, v2, v3}, Lcom/a/a/az;-><init>(Ljava/lang/Thread$UncaughtExceptionHandler;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/a/a/d;->b:Lcom/a/a/az;

    .line 816
    iget-object v0, p0, Lcom/a/a/d;->b:Lcom/a/a/az;

    new-instance v2, Lcom/a/a/c;

    invoke-direct {v2, v0}, Lcom/a/a/c;-><init>(Lcom/a/a/az;)V

    invoke-virtual {v0, v2}, Lcom/a/a/az;->a(Ljava/util/concurrent/Callable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_2
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    move-result v0

    .line 819
    :try_start_7
    iget-object v1, p0, Lcom/a/a/d;->b:Lcom/a/a/az;

    new-instance v2, Lcom/a/a/a;

    invoke-direct {v2, v1}, Lcom/a/a/a;-><init>(Lcom/a/a/az;)V

    invoke-virtual {v1, v2}, Lcom/a/a/az;->b(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;

    .line 821
    iget-object v1, p0, Lcom/a/a/d;->b:Lcom/a/a/az;

    new-instance v2, Lcom/a/a/bc;

    invoke-direct {v2, v1}, Lcom/a/a/bc;-><init>(Lcom/a/a/az;)V

    invoke-virtual {v1, v2}, Lcom/a/a/az;->b(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;

    .line 822
    iget-object v1, p0, Lcom/a/a/d;->b:Lcom/a/a/az;

    new-instance v2, Lcom/a/a/g;

    invoke-direct {v2, v1}, Lcom/a/a/g;-><init>(Lcom/a/a/az;)V

    invoke-virtual {v1, v2}, Lcom/a/a/az;->a(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 823
    iget-object v1, p0, Lcom/a/a/d;->b:Lcom/a/a/az;

    invoke-static {v1}, Ljava/lang/Thread;->setDefaultUncaughtExceptionHandler(Ljava/lang/Thread$UncaughtExceptionHandler;)V

    .line 824
    invoke-static {}, Lcom/a/a/a/co;->a()Lcom/a/a/a/cl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/a/a/a/cl;->a()Lcom/a/a/a/ci;

    move-result-object v1

    invoke-interface {v1}, Lcom/a/a/a/ci;->b()V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_3
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 833
    :goto_3
    :try_start_8
    new-instance v1, Ljava/util/concurrent/CountDownLatch;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    .line 835
    new-instance v2, Ljava/lang/Thread;

    new-instance v3, Lcom/a/a/ax;

    invoke-direct {v3, p0, p2, p3, v1}, Lcom/a/a/ax;-><init>(Lcom/a/a/d;Landroid/content/Context;FLjava/util/concurrent/CountDownLatch;)V

    const-string v4, "Crashlytics Initializer"

    invoke-direct {v2, v3, v4}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/Thread;->start()V

    .line 855
    if-eqz v0, :cond_0

    .line 856
    invoke-static {}, Lcom/a/a/a/co;->a()Lcom/a/a/a/cl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/a/a/cl;->a()Lcom/a/a/a/ci;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/a/a/ci;->b()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 860
    const-wide/16 v2, 0xfa0

    :try_start_9
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v1, v2, v3, v0}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 861
    invoke-static {}, Lcom/a/a/a/co;->a()Lcom/a/a/a/cl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/a/a/cl;->a()Lcom/a/a/a/ci;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/a/a/ci;->d()V
    :try_end_9
    .catch Ljava/lang/InterruptedException; {:try_start_9 .. :try_end_9} :catch_1
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto/16 :goto_0

    .line 864
    :catch_1
    move-exception v0

    :try_start_a
    invoke-static {}, Lcom/a/a/a/co;->a()Lcom/a/a/a/cl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/a/a/cl;->a()Lcom/a/a/a/ci;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/a/a/ci;->a()V

    goto/16 :goto_0

    .line 826
    :catch_2
    move-exception v0

    move v0, v1

    :goto_4
    invoke-static {}, Lcom/a/a/a/co;->a()Lcom/a/a/a/cl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/a/a/a/cl;->a()Lcom/a/a/a/ci;

    move-result-object v1

    invoke-interface {v1}, Lcom/a/a/a/ci;->a()V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    goto :goto_3

    :catch_3
    move-exception v1

    goto :goto_4
.end method

.method private a(Landroid/content/Context;F)Z
    .locals 10

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 876
    .line 880
    const/4 v8, 0x0

    .line 881
    iget-object v0, p0, Lcom/a/a/a/ch;->g:Landroid/content/Context;

    invoke-static {v0}, Lcom/a/a/a/ba;->g(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v9

    .line 884
    :try_start_0
    invoke-static {}, Lcom/a/a/a/as;->a()Lcom/a/a/a/ar;

    move-result-object v0

    sget-object v2, Lcom/a/a/d;->t:Lcom/a/a/a/bt;

    sget-object v3, Lcom/a/a/d;->n:Ljava/lang/String;

    sget-object v4, Lcom/a/a/d;->o:Ljava/lang/String;

    invoke-static {}, Lcom/a/a/d;->i()Ljava/lang/String;

    move-result-object v5

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/a/a/a/ar;->a(Landroid/content/Context;Lcom/a/a/a/bt;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/a/a/a/ar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/a/a/ar;->b()Z

    .line 888
    invoke-static {}, Lcom/a/a/a/as;->a()Lcom/a/a/a/ar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/a/a/ar;->a()Lcom/a/a/a/aw;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    move-object v2, v0

    .line 893
    :goto_0
    if-eqz v2, :cond_7

    .line 895
    :try_start_1
    iget-object v0, v2, Lcom/a/a/a/aw;->a:Lcom/a/a/a/al;

    const-string v1, "new"

    iget-object v3, v0, Lcom/a/a/a/al;->a:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/a/a/a/ch;->g:Landroid/content/Context;

    invoke-static {v1, v9}, Lcom/a/a/aa;->a(Landroid/content/Context;Ljava/lang/String;)Lcom/a/a/aa;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/a/a/d;->a(Lcom/a/a/aa;)Lcom/a/a/ah;

    move-result-object v1

    new-instance v3, Lcom/a/a/v;

    invoke-static {}, Lcom/a/a/d;->i()Ljava/lang/String;

    move-result-object v4

    iget-object v0, v0, Lcom/a/a/a/al;->b:Ljava/lang/String;

    sget-object v5, Lcom/a/a/d;->t:Lcom/a/a/a/bt;

    invoke-direct {v3, v4, v0, v5}, Lcom/a/a/v;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/a/a/a/bt;)V

    invoke-virtual {v3, v1}, Lcom/a/a/v;->a(Lcom/a/a/ah;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {}, Lcom/a/a/a/as;->a()Lcom/a/a/a/ar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/a/a/ar;->c()Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result v0

    :goto_1
    move v1, v0

    .line 900
    :goto_2
    :try_start_2
    iget-object v0, v2, Lcom/a/a/a/aw;->d:Lcom/a/a/a/ao;

    iget-boolean v0, v0, Lcom/a/a/a/ao;->b:Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    .line 907
    :goto_3
    if-eqz v1, :cond_6

    if-eqz v0, :cond_6

    .line 909
    :try_start_3
    iget-object v0, p0, Lcom/a/a/d;->b:Lcom/a/a/az;

    new-instance v1, Lcom/a/a/q;

    invoke-direct {v1, v0}, Lcom/a/a/q;-><init>(Lcom/a/a/az;)V

    invoke-virtual {v0, v1}, Lcom/a/a/az;->a(Ljava/util/concurrent/Callable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    and-int/lit8 v7, v0, 0x1

    .line 911
    invoke-static {}, Lcom/a/a/d;->m()Lcom/a/a/x;

    move-result-object v0

    .line 912
    if-eqz v0, :cond_0

    .line 913
    new-instance v1, Lcom/a/a/ad;

    invoke-direct {v1, v0}, Lcom/a/a/ad;-><init>(Lcom/a/a/x;)V

    invoke-virtual {v1, p2}, Lcom/a/a/ad;->a(F)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    .line 920
    :cond_0
    :goto_4
    if-eqz v6, :cond_1

    .line 921
    invoke-static {}, Lcom/a/a/a/co;->a()Lcom/a/a/a/cl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/a/a/cl;->a()Lcom/a/a/a/ci;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/a/a/ci;->b()V

    .line 923
    :cond_1
    return v7

    .line 890
    :catch_0
    move-exception v0

    invoke-static {}, Lcom/a/a/a/co;->a()Lcom/a/a/a/cl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/a/a/cl;->a()Lcom/a/a/a/ci;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/a/a/ci;->a()V

    move-object v2, v8

    goto :goto_0

    .line 895
    :cond_2
    :try_start_4
    invoke-static {}, Lcom/a/a/a/co;->a()Lcom/a/a/a/cl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/a/a/cl;->a()Lcom/a/a/a/ci;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/a/a/ci;->a()V

    move v0, v6

    goto :goto_1

    :cond_3
    const-string v1, "configured"

    iget-object v3, v0, Lcom/a/a/a/al;->a:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-static {}, Lcom/a/a/a/as;->a()Lcom/a/a/a/ar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/a/a/ar;->c()Z

    move-result v0

    goto :goto_1

    :cond_4
    iget-boolean v1, v0, Lcom/a/a/a/al;->d:Z

    if-eqz v1, :cond_5

    invoke-static {}, Lcom/a/a/a/co;->a()Lcom/a/a/a/cl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/a/a/a/cl;->a()Lcom/a/a/a/ci;

    move-result-object v1

    invoke-interface {v1}, Lcom/a/a/a/ci;->b()V

    iget-object v1, p0, Lcom/a/a/a/ch;->g:Landroid/content/Context;

    invoke-static {v1, v9}, Lcom/a/a/aa;->a(Landroid/content/Context;Ljava/lang/String;)Lcom/a/a/aa;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/a/a/d;->a(Lcom/a/a/aa;)Lcom/a/a/ah;

    move-result-object v1

    new-instance v3, Lcom/a/a/ag;

    invoke-static {}, Lcom/a/a/d;->i()Ljava/lang/String;

    move-result-object v4

    iget-object v0, v0, Lcom/a/a/a/al;->b:Ljava/lang/String;

    sget-object v5, Lcom/a/a/d;->t:Lcom/a/a/a/bt;

    invoke-direct {v3, v4, v0, v5}, Lcom/a/a/ag;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/a/a/a/bt;)V

    invoke-virtual {v3, v1}, Lcom/a/a/ag;->a(Lcom/a/a/ah;)Z
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    :cond_5
    move v0, v7

    goto/16 :goto_1

    .line 897
    :catch_1
    move-exception v0

    invoke-static {}, Lcom/a/a/a/co;->a()Lcom/a/a/a/cl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/a/a/cl;->a()Lcom/a/a/a/ci;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/a/a/ci;->a()V

    move v1, v6

    goto/16 :goto_2

    .line 902
    :catch_2
    move-exception v0

    invoke-static {}, Lcom/a/a/a/co;->a()Lcom/a/a/a/cl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/a/a/cl;->a()Lcom/a/a/a/ci;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/a/a/ci;->a()V

    move v0, v6

    goto/16 :goto_3

    .line 917
    :catch_3
    move-exception v0

    invoke-static {}, Lcom/a/a/a/co;->a()Lcom/a/a/a/cl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/a/a/cl;->a()Lcom/a/a/a/ci;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/a/a/ci;->a()V

    goto/16 :goto_4

    :cond_6
    move v6, v7

    goto/16 :goto_4

    :cond_7
    move v0, v6

    move v1, v6

    goto/16 :goto_3
.end method

.method static synthetic a(Lcom/a/a/d;Landroid/app/Activity;Lcom/a/a/a/ap;)Z
    .locals 6

    .prologue
    .line 54
    new-instance v4, Lcom/a/a/z;

    invoke-direct {v4, p1, p2}, Lcom/a/a/z;-><init>(Landroid/content/Context;Lcom/a/a/a/ap;)V

    new-instance v3, Lcom/a/a/ay;

    const/4 v0, 0x0

    invoke-direct {v3, v0}, Lcom/a/a/ay;-><init>(B)V

    new-instance v0, Lcom/a/a/at;

    move-object v1, p0

    move-object v2, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/a/a/at;-><init>(Lcom/a/a/d;Landroid/app/Activity;Lcom/a/a/ay;Lcom/a/a/z;Lcom/a/a/a/ap;)V

    invoke-virtual {p1, v0}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    invoke-static {}, Lcom/a/a/a/co;->a()Lcom/a/a/a/cl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/a/a/cl;->a()Lcom/a/a/a/ci;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/a/a/ci;->b()V

    :try_start_0
    iget-object v0, v3, Lcom/a/a/ay;->b:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->await()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-boolean v0, v3, Lcom/a/a/ay;->a:Z

    return v0

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method static synthetic a(Lcom/a/a/d;Landroid/content/Context;F)Z
    .locals 1

    .prologue
    .line 54
    invoke-direct {p0, p1, p2}, Lcom/a/a/d;->a(Landroid/content/Context;F)Z

    move-result v0

    return v0
.end method

.method public static c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 542
    invoke-static {}, Lcom/a/a/a/co;->a()Lcom/a/a/a/cl;

    const-string v0, "1.1.13.29"

    return-object v0
.end method

.method static d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 546
    sget-object v0, Lcom/a/a/d;->k:Ljava/lang/String;

    return-object v0
.end method

.method static e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 556
    sget-object v0, Lcom/a/a/d;->l:Ljava/lang/String;

    return-object v0
.end method

.method static f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 560
    sget-object v0, Lcom/a/a/d;->o:Ljava/lang/String;

    return-object v0
.end method

.method static g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 564
    sget-object v0, Lcom/a/a/d;->n:Ljava/lang/String;

    return-object v0
.end method

.method static h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 568
    sget-object v0, Lcom/a/a/d;->m:Ljava/lang/String;

    return-object v0
.end method

.method static i()Ljava/lang/String;
    .locals 2

    .prologue
    .line 572
    sget-object v0, Lcom/a/a/d;->j:Landroid/content/ContextWrapper;

    const-string v1, "com.crashlytics.ApiEndpoint"

    invoke-static {v0, v1}, Lcom/a/a/a/ba;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static k()Z
    .locals 3

    .prologue
    .line 593
    invoke-static {}, Lcom/a/a/a/ba;->a()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "always_send_reports_opt_in"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method static l()V
    .locals 3

    .prologue
    .line 597
    invoke-static {}, Lcom/a/a/a/ba;->a()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "always_send_reports_opt_in"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 598
    return-void
.end method

.method static m()Lcom/a/a/x;
    .locals 3

    .prologue
    .line 637
    invoke-static {}, Lcom/a/a/a/as;->a()Lcom/a/a/a/ar;

    move-result-object v0

    new-instance v1, Lcom/a/a/ar;

    invoke-direct {v1}, Lcom/a/a/ar;-><init>()V

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/a/a/a/ar;->a(Lcom/a/a/a/at;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/a/x;

    return-object v0
.end method

.method static synthetic n()Lcom/a/a/a/bt;
    .locals 1

    .prologue
    .line 54
    sget-object v0, Lcom/a/a/d;->t:Lcom/a/a/a/bt;

    return-object v0
.end method


# virtual methods
.method protected final b()V
    .locals 3

    .prologue
    .line 523
    iget-object v0, p0, Lcom/a/a/a/ch;->g:Landroid/content/Context;

    .line 524
    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/a/a/a/cj;->a(Landroid/content/Context;Z)Ljava/lang/String;

    move-result-object v1

    .line 526
    if-nez v1, :cond_0

    .line 538
    :goto_0
    return-void

    .line 531
    :cond_0
    :try_start_0
    sget v2, Lcom/a/a/d;->u:F

    invoke-direct {p0, v1, v0, v2}, Lcom/a/a/d;->a(Ljava/lang/String;Landroid/content/Context;F)V
    :try_end_0
    .catch Lcom/a/a/e; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 532
    :catch_0
    move-exception v0

    throw v0

    .line 536
    :catch_1
    move-exception v0

    invoke-static {}, Lcom/a/a/a/co;->a()Lcom/a/a/a/cl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/a/a/cl;->a()Lcom/a/a/a/ci;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/a/a/ci;->a()V

    goto :goto_0
.end method

.method final j()Z
    .locals 3

    .prologue
    .line 580
    invoke-static {}, Lcom/a/a/a/as;->a()Lcom/a/a/a/ar;

    move-result-object v0

    new-instance v1, Lcom/a/a/ap;

    invoke-direct {v1, p0}, Lcom/a/a/ap;-><init>(Lcom/a/a/d;)V

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/a/a/a/ar;->a(Lcom/a/a/a/at;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method
