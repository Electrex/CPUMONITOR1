.class final Lcom/a/a/g;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private synthetic a:Lcom/a/a/az;


# direct methods
.method constructor <init>(Lcom/a/a/az;)V
    .locals 0

    .prologue
    .line 1861
    iput-object p1, p0, Lcom/a/a/g;->a:Lcom/a/a/az;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 12

    .prologue
    const/4 v1, 0x0

    .line 1864
    iget-object v3, p0, Lcom/a/a/g;->a:Lcom/a/a/az;

    iget-object v0, p0, Lcom/a/a/g;->a:Lcom/a/a/az;

    sget-object v2, Lcom/a/a/al;->a:Ljava/io/FilenameFilter;

    invoke-static {v0, v2}, Lcom/a/a/az;->a(Lcom/a/a/az;Ljava/io/FilenameFilter;)[Ljava/io/File;

    move-result-object v4

    new-instance v2, Ljava/io/File;

    invoke-static {}, Lcom/a/a/a/co;->a()Lcom/a/a/a/cl;

    move-result-object v0

    iget-object v0, v0, Lcom/a/a/a/cl;->c:Ljava/io/File;

    const-string v5, "invalidClsFiles"

    invoke-direct {v2, v0, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {v2}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v2}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v5

    array-length v6, v5

    move v0, v1

    :goto_0
    if-ge v0, v6, :cond_0

    aget-object v7, v5, v0

    invoke-virtual {v7}, Ljava/io/File;->delete()Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    :cond_1
    array-length v5, v4

    move v2, v1

    :goto_1
    if-ge v2, v5, :cond_3

    aget-object v0, v4, v2

    invoke-static {}, Lcom/a/a/a/co;->a()Lcom/a/a/a/cl;

    move-result-object v6

    invoke-virtual {v6}, Lcom/a/a/a/cl;->a()Lcom/a/a/a/ci;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Found invalid session part file: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-interface {v6}, Lcom/a/a/a/ci;->b()V

    invoke-static {v0}, Lcom/a/a/az;->a(Ljava/io/File;)Ljava/lang/String;

    move-result-object v0

    new-instance v6, Lcom/a/a/h;

    invoke-direct {v6, v0}, Lcom/a/a/h;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/a/a/a/co;->a()Lcom/a/a/a/cl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/a/a/cl;->a()Lcom/a/a/a/ci;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/a/a/ci;->b()V

    invoke-virtual {v3, v6}, Lcom/a/a/az;->a(Ljava/io/FilenameFilter;)[Ljava/io/File;

    move-result-object v6

    array-length v7, v6

    move v0, v1

    :goto_2
    if-ge v0, v7, :cond_2

    aget-object v8, v6, v0

    invoke-static {}, Lcom/a/a/a/co;->a()Lcom/a/a/a/cl;

    move-result-object v9

    invoke-virtual {v9}, Lcom/a/a/a/cl;->a()Lcom/a/a/a/ci;

    move-result-object v9

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "Deleting session file: "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-interface {v9}, Lcom/a/a/a/ci;->b()V

    invoke-virtual {v8}, Ljava/io/File;->delete()Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 1865
    :cond_3
    return-void
.end method
