.class final Lcom/a/a/at;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/a/a/ay;

.field final synthetic b:Lcom/a/a/d;

.field private synthetic c:Landroid/app/Activity;

.field private synthetic d:Lcom/a/a/z;

.field private synthetic e:Lcom/a/a/a/ap;


# direct methods
.method constructor <init>(Lcom/a/a/d;Landroid/app/Activity;Lcom/a/a/ay;Lcom/a/a/z;Lcom/a/a/a/ap;)V
    .locals 0

    .prologue
    .line 669
    iput-object p1, p0, Lcom/a/a/at;->b:Lcom/a/a/d;

    iput-object p2, p0, Lcom/a/a/at;->c:Landroid/app/Activity;

    iput-object p3, p0, Lcom/a/a/at;->a:Lcom/a/a/ay;

    iput-object p4, p0, Lcom/a/a/at;->d:Lcom/a/a/z;

    iput-object p5, p0, Lcom/a/a/at;->e:Lcom/a/a/a/ap;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 10

    .prologue
    const/4 v9, 0x0

    .line 672
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/a/a/at;->c:Landroid/app/Activity;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 674
    new-instance v1, Lcom/a/a/au;

    invoke-direct {v1, p0}, Lcom/a/a/au;-><init>(Lcom/a/a/at;)V

    .line 682
    iget-object v2, p0, Lcom/a/a/at;->c:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->density:F

    .line 686
    const/4 v3, 0x5

    invoke-static {v2, v3}, Lcom/a/a/d;->a(FI)I

    move-result v3

    .line 688
    new-instance v4, Landroid/widget/TextView;

    iget-object v5, p0, Lcom/a/a/at;->c:Landroid/app/Activity;

    invoke-direct {v4, v5}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 689
    const/16 v5, 0xf

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setAutoLinkMask(I)V

    .line 690
    iget-object v5, p0, Lcom/a/a/at;->d:Lcom/a/a/z;

    const-string v6, "com.crashlytics.CrashSubmissionPromptMessage"

    iget-object v7, v5, Lcom/a/a/z;->a:Lcom/a/a/a/ap;

    iget-object v7, v7, Lcom/a/a/a/ap;->b:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Lcom/a/a/z;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 691
    iget-object v5, p0, Lcom/a/a/at;->c:Landroid/app/Activity;

    const v6, 0x1030044

    invoke-virtual {v4, v5, v6}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 692
    invoke-virtual {v4, v3, v3, v3, v3}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 693
    invoke-virtual {v4, v9}, Landroid/widget/TextView;->setFocusable(Z)V

    .line 695
    new-instance v3, Landroid/widget/ScrollView;

    iget-object v5, p0, Lcom/a/a/at;->c:Landroid/app/Activity;

    invoke-direct {v3, v5}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;)V

    .line 696
    const/16 v5, 0xe

    invoke-static {v2, v5}, Lcom/a/a/d;->a(FI)I

    move-result v5

    const/4 v6, 0x2

    invoke-static {v2, v6}, Lcom/a/a/d;->a(FI)I

    move-result v6

    const/16 v7, 0xa

    invoke-static {v2, v7}, Lcom/a/a/d;->a(FI)I

    move-result v7

    const/16 v8, 0xc

    invoke-static {v2, v8}, Lcom/a/a/d;->a(FI)I

    move-result v2

    invoke-virtual {v3, v5, v6, v7, v2}, Landroid/widget/ScrollView;->setPadding(IIII)V

    .line 698
    invoke-virtual {v3, v4}, Landroid/widget/ScrollView;->addView(Landroid/view/View;)V

    .line 702
    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    iget-object v3, p0, Lcom/a/a/at;->d:Lcom/a/a/z;

    const-string v4, "com.crashlytics.CrashSubmissionPromptTitle"

    iget-object v5, v3, Lcom/a/a/z;->a:Lcom/a/a/a/ap;

    iget-object v5, v5, Lcom/a/a/a/ap;->a:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Lcom/a/a/z;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, v9}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    iget-object v3, p0, Lcom/a/a/at;->d:Lcom/a/a/z;

    const-string v4, "com.crashlytics.CrashSubmissionSendTitle"

    iget-object v5, v3, Lcom/a/a/z;->a:Lcom/a/a/a/ap;

    iget-object v5, v5, Lcom/a/a/a/ap;->c:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Lcom/a/a/z;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v1}, Landroid/app/AlertDialog$Builder;->setNeutralButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 705
    iget-object v1, p0, Lcom/a/a/at;->e:Lcom/a/a/a/ap;

    iget-boolean v1, v1, Lcom/a/a/a/ap;->d:Z

    if-eqz v1, :cond_0

    .line 706
    new-instance v1, Lcom/a/a/av;

    invoke-direct {v1, p0}, Lcom/a/a/av;-><init>(Lcom/a/a/at;)V

    .line 713
    iget-object v2, p0, Lcom/a/a/at;->d:Lcom/a/a/z;

    const-string v3, "com.crashlytics.CrashSubmissionCancelTitle"

    iget-object v4, v2, Lcom/a/a/z;->a:Lcom/a/a/a/ap;

    iget-object v4, v4, Lcom/a/a/a/ap;->e:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Lcom/a/a/z;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 716
    :cond_0
    iget-object v1, p0, Lcom/a/a/at;->e:Lcom/a/a/a/ap;

    iget-boolean v1, v1, Lcom/a/a/a/ap;->f:Z

    if-eqz v1, :cond_1

    .line 717
    new-instance v1, Lcom/a/a/aw;

    invoke-direct {v1, p0}, Lcom/a/a/aw;-><init>(Lcom/a/a/at;)V

    .line 725
    iget-object v2, p0, Lcom/a/a/at;->d:Lcom/a/a/z;

    const-string v3, "com.crashlytics.CrashSubmissionAlwaysSendTitle"

    iget-object v4, v2, Lcom/a/a/z;->a:Lcom/a/a/a/ap;

    iget-object v4, v4, Lcom/a/a/a/ap;->g:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Lcom/a/a/z;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 728
    :cond_1
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 729
    return-void
.end method
